<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$template_id = (isset($template['template_id']))?$template['template_id']:"";
?>
<?php
	$is_login = $this->user_access->is_login();
	if($is_login)
	{
?>
	<div class="ajaxcontainer background-white padding-vert-20">
		<div class="container">
			<div class="row hidden">
				<div class="progress col-lg-12 col-md-12 col-sm-12 col-xs-12" id="progress1">
					<div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
					</div>
					<span class="progress-type">Overall Progress</span>
					<span class="progress-completed">20%</span>
				</div>
			</div>
			<div class="row step">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nav-step-progress activestep" data-percent="30" data-step="step-1">
					<div class="content-step-progress">
						<div class="number-step-progress">1</div>
						<div class="title-step-progress">
							<span class="fa fa-file-text"></span>
							<p>Buat Halaman</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nav-step-progress" data-percent="60" data-step="step-2">
					<div class="content-step-progress">
						<div class="number-step-progress">2</div>
						<div class="title-step-progress">
							<span class="fa fa-columns"></span>
							<p>Pilih Template</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nav-step-progress" data-percent="100" data-step="step-3">
					<div class="content-step-progress">
						<div class="number-step-progress">3</div>
						<div class="title-step-progress">
							<span class="fa fa-th-large"></span>
							<p>Pilih Theme</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<form method="post" action="<?php echo base_url("admin/adminwizard/choose_template");?>" class="ajax" data-target-ajax=".ajaxcontainer">
				<div class="row background-white bottom-border">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-vert-20">
						<h2 class="background-grey">Buat Halaman</h2>
						<hr/>
						<div class="form-group">
							<label>Judul Halaman:</label>
							<div class="input-form">
							  <input type="text" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label>URL Halaman:</label>
							<div class="input-form">
							  <input type="text" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label>Meta Title:</label>
							<div class="input-form">
							  <input type="text" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label>Meta Keywords:</label>
							<div class="input-form">
							  <input type="text" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label>Meta Description:</label>
							<div class="input-form">
							  <textarea class="form-control" rows="7"></textarea>
							</div>
						</div>

					</div>
				</div>
				<div class="row background-white padding-vert-10">
					<div class="col-lg-12 text-right">
						<button type="submit" class="btn btn-lg btn-primary">Lanjutkan &raquo;</button>
					</div>
				</div>
			</form>
			<script type="text/javascript">
				function resetActive(event, percent, step) {
					$(".progress-bar").css("width", percent + "%").attr("aria-valuenow", percent);
					$(".progress-completed").text(percent + "%");

					$(".nav-step-progress").each(function () {
						if ($(this).hasClass("activestep")) {
							$(this).removeClass("activestep");
						}
					});
					
					$(event).addClass("activestep");
					hideSteps();
					showCurrentStepInfo(step);
				}

				function hideSteps() {
					$(".nav-step-progress").each(function () {
						if ($(this).hasClass("activeStepInfo")) {
							$(this).removeClass("activeStepInfo");
							$(this).addClass("hiddenStepInfo");
						}
					});
				}

				function showCurrentStepInfo(step) {        
					var id = "#" + step;
					$(id).addClass("activeStepInfo");
				}
				
				$(document).ready(function(){
					/*
					$(".nav-step-progress").on("click",function(){
						var percent = $(this).attr("data-percent");
						var step = $(this).attr("data-step");
						resetActive($(this), percent, step)
					});
					*/
				});
			</script>
		</div>
	</div>
<?php
	}		
?>
<?php
?>
