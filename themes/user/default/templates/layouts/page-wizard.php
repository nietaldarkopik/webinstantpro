<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$this->load->view("default/templates/header");
$template_id = (isset($template['template_id']))?$template['template_id']:"";
?>
<?php
	$is_login = $this->user_access->is_login();
	if($is_login)
	{
?>
<div id="body-bg">
	<div id="pre-header" class="background-gray-lighter">
		<div class="container">
			<div class="col-lg-12">&nbsp;</div>
		</div>
	</div>
	<div id="header">
		<div class="container">
			<div class="row">
				<div class="logo">
					<a class="site-logo" href="<?php echo base_url();?>"><img src="<?php echo base_url('themes/user/default/assets/theme/img/logo-webinstantpro.png');?>" alt="Logo Web Instant Pro"></a>
				</div>
			</div>
		</div>
	</div>
	<div id="content" class="bottom-border-shadow">
		<div id="icons">
			<div class="container background-grey bottom-border">
				<div class="row padding-vert-20">
					<!-- Icons -->
					<div class="col-lg-12 col-md-12 text-center padding-vert-60">
						<h2 class="title margin-bottom-10">Terimakasih Telah Menggunakan WebInstantPro</h2>
					</div>
					<div class="col-md-4 text-center">
						<i class="fa-cogs fa-4x color-primary animate fadeIn animated"></i>
						<h2 class="padding-top-10 animate fadeIn animated">Mudah Digunakan</h2>
						<p class="animate fadeIn animated">Tanpa memiliki keahlian program, anda dapat mengelola website dengan sangat mudah, seperti halnya membalikan telapak tangan Anda sendiri.</p>
					</div>
					<div class="col-md-4 text-center">
						<i class="fa-clock-o fa-4x color-primary animate fadeIn animated"></i>
						<h2 class="padding-top-10 animate fadeIn animated">Hemat Waktu</h2>
						<p class="animate fadeIn animated">Cukup Dengan waktu yang sangat singkat anda dapat membuat artikel / mengupload gambar ataupun video kedalam website anda. Sehingga anda dapat menghemat waktu yang anda miliki.</p>
					</div>
					<div class="col-md-4 text-center">
						<i class="fa-credit-card fa-4x color-primary animate fadeIn animated"></i>
						<h2 class="padding-top-10 animate fadeIn animated">Sistem Pembayaran Mudah</h2>
						<p class="animate fadeIn animated">Anda dapat memiliki website impian anda tanpa harus memiliki Kartu Kredit, cukup dengan transfer via bank semua beres.</p>
					</div>
					<!-- End Icons -->
				</div>
			</div>
			<div class="container background-white bottom-border-shadow">
				<div class="row padding-vert-20">
					<div class="col-lg-12">
						<button type="button" class="btn btn-primary col-lg-5 col-md-5 col-sm-12 col-xs-12" id="create-first-page-btn">Buat Halaman Pertama Anda</button>
						<button type="button" class="btn background-white btn-disabled col-lg-2 col-md-2 col-sm-12 col-xs-12"><strong>atau</strong></button>
						<button type="button" class="btn btn-success col-lg-5 col-md-5 col-sm-12 col-xs-12" id="install-content-btn">Install Sample Konten</button>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row padding-vert-20">
				<div class="col-lg-12">
					&nbsp;
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<i>Copyright &copy; webinstantpro.com <?php echo date("Y");?></i>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}		
?>
<?php
$this->load->view("default/templates/footer");
?>
