<?php

class Default_theme{
  var $CI;
  var $front_panel = true;
  var $current_theme_url = '';
  function __construct()
  {
    $this->CI =& get_instance();
    $this->current_theme_url = base_url() . 'themes/user/default/';
  }
  
  function setAssets()
  {
    $css = '
        <!-- Favicon -->
        <link href="'.$this->current_theme_url.'assets/image/favicon.ico" rel="shortcut icon">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/bootstrap.css" rel="stylesheet">
        <!-- Template CSS -->
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/animate.css" rel="stylesheet">
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/nexus.css" rel="stylesheet">
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="'.$this->current_theme_url.'assets/theme/css/custom.css" rel="stylesheet">
        <!-- Google Fonts-->';
        //<link href="http://fonts.googleapis.com/css?family=Raleway:100,300,400" type="text/css" rel="stylesheet">
        //<link href="http://fonts.googleapis.com/css?family=Roboto:400,300" type="text/css" rel="stylesheet">
    
    $js_head = '
		<!-- JS -->
		<script type="text/javascript" src="'.$this->current_theme_url.'assets/theme/js/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="'.$this->current_theme_url.'assets/theme/js/bootstrap.min.js" type="text/javascript"></script>
    ';
    $js_body = '';
    $this->CI->assets->add_css($css,"head");
    $this->CI->assets->add_js($js_head,"head");
    $this->CI->assets->add_js($js_body,"body");
  }
  
  function setAdminAssets()
  {
	$is_login = $this->CI->user_access->is_login();
	if($is_login)
	{
		if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php'))
		{
		  require_once(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php');
		  $theme_config = CURRENT_ADMIN_THEME;
		  $theme_loader = new $theme_config();
		  $theme_loader->setFrontAdminAssets();
		}
		$js = '<script type="text/javascript" src="'.base_url().'assets/core/angularjs/angular-1.4.1/angular.min.js"></script>';
		$this->CI->assets->add_js($js,"body");
	}
  }
}
