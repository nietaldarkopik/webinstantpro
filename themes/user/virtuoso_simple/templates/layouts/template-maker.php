<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$this->load->view("header");
$template_id = (isset($template['template_id']))?$template['template_id']:"";
?>
<?php echo $this->templates->show_containers($template_id);?>
<?php
$this->load->view("footer");
?>