<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else
  {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '3'));?>">
    <meta name="author" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '4'));?>">
    <meta name="keywords" content="<?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '5'));?>">
    <link rel="shortcut icon" href="<?php echo current_admin_theme_url();?>dist/ico/favicon.ico">

    <title><?php echo $this->master->get_value(TABLE_SYS_SETTINGS,"value",array("setting_id" => '2'));?></title>
    <?php echo $this->assets->print_css("head");?>
    <script type="text/javascript">
       var base_url = "<?php echo base_url();?>";
       var current_url = "<?php echo current_url();?>";
       var current_theme_url = "<?php echo current_admin_theme_url();?>";
       var theme_name = "<?php echo CURRENT_THEME;?>";
    </script>
    <?php echo $this->assets->print_js("head");?>
    <style type="text/css">
      <?php echo $this->assets->print_css_inline("head");?>
    </style>
    <script type="text/javascript">
      <?php echo $this->assets->print_js_inline("head");?>
    </script>
  </head>

  <body id="body_bg">
<?php
}
?>
