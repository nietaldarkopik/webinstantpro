<?php
$config = array();

$config['data']['theme_name'] = basename(__FILE__, '.php');
$config['data']['theme_title'] = 'Lifestyle Theme Simple';
$config['data']['author'] = 'Webinstant Pro';
$config['data']['version'] = '1.1';
$config['data']['description'] = 'Lifestyle Theme Simple';
