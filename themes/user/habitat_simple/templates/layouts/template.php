<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$this->load->view("header");
$page = (isset($page))?$page:"";
$theme = (isset($page))?$theme:"";
$template = (isset($page))?$page:"";
?>
<?php echo $this->page_templates->show_containers($page);?>
<?php
$this->load->view("footer");
?>
