<?php
$config = array();

$config['data']['theme_name'] = basename(__FILE__, '.php');
$config['data']['theme_title'] = 'Habitat Theme Simple';
$config['data']['author'] = 'Webinstant Pro';
$config['data']['version'] = '1.1';
$config['data']['description'] = 'Habitat Theme Simple';
