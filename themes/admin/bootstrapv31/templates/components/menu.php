<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	#echo $this->user_access->get_menus_allowed_structured();
	
	$curr_menu_group = $this->session->userdata("menu_group");
	$curr_menu_group = (empty($curr_menu_group))?"general":$curr_menu_group;
	
	$parent_menu_allowed = $this->user_access->get_menus_allowed($user_id," AND (`type` = '".$curr_menu_group."' or `type` = 'home') AND parent_menu = 0");

	$the_top_menu = "";
	// Get menu Level 0
	if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
	{
		$the_top_menu .= '<ul class="nav navbar-nav">';
		foreach($parent_menu_allowed as $index => $mn)
		{				
			$sub_menu_allowed2 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn['user_menu_id'] . "'");

			if($mn['type'] != $curr_menu_group or (!is_array($sub_menu_allowed2) or count($sub_menu_allowed2) == 0))
			{
				if($mn['controller'] == "#" and (is_array($sub_menu_allowed2) and count($sub_menu_allowed2) == 0))
				{}else{
					$the_top_menu .= '<li><a href="'.base_url() . $mn['controller'] . '/' . $mn['function'] . '">'.$mn['menu_title'].'</a></li>';			
				}
			}
			// Get menu Level 1
			if(is_array($sub_menu_allowed2) and count($sub_menu_allowed2) > 0)
			{
				foreach($sub_menu_allowed2 as $index2 => $mn2)
				{
					$sub_menu_allowed3 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn2['user_menu_id'] . "'");
					
					
					if($mn2['type'] != $curr_menu_group or (!is_array($sub_menu_allowed3) or count($sub_menu_allowed3) == 0))
					{
						if($mn2['controller'] == "#" and (is_array($sub_menu_allowed3) and count($sub_menu_allowed3) == 0))
						{}else{
							$the_top_menu .= '<li class="dropdown"><a href="'.base_url() . $mn2['controller'] . '/' . $mn2['function'] . '" class="dropdown-toggle" data-toggle="dropdown">'.$mn2['menu_title'].'</a>';	
						}
					}
					
					if(is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)
					{
						$the_top_menu .= '<ul class="dropdown-menu">';
						foreach($sub_menu_allowed3 as $index3 => $mn3)
						{
							$the_top_menu .= '<li><a href="'.base_url() . $mn3['controller'] . '/' . $mn3['function'] . '">'.$mn3['menu_title'].'</a>';	
						}
						$the_top_menu .= '</ul>';
					}
					
					$the_top_menu .= '</li>';
				}
			}
		}
		$the_top_menu .= '</ul>';
	}
	
	echo $the_top_menu;
