<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
		$is_login = $this->user_access->is_login();
    if($is_login)
    {
?>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="<?php echo base_url();?>"><?php echo WEB_NAME;?></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php
      $this->load->view('components/topmenu');
      ?>
    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Modal -->
  <div class="modal fade" id="popupoverlay" tabindex="-1" role="dialog" aria-labelledby="overlaycontainer" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content ajax_container" id="data_target-modal">
      </div>
    </div>
  </div>
<?php 
  }
} ?>
