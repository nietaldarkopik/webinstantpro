<?php

class blankadmin{
  var $CI;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setAssets()
  {
        $css = '    
                <link href="'.current_admin_theme_url().'dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
				
				<link href="'.current_admin_theme_url().'assets/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
				
				<!-- <link href="'.current_admin_theme_url().'assets/css/morris/morris.css" rel="stylesheet" type="text/css" /> -->
				
				<link href="'.current_admin_theme_url().'assets/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
				
				<link href="'.current_admin_theme_url().'assets/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
				
				<link href="'.current_admin_theme_url().'assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
				
				<link href="'.current_admin_theme_url().'assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
				
				<link href="'.current_admin_theme_url().'assets/css/styles.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>

                <link href="'.current_admin_theme_url().'assets/scripts/jquery/css/ui-darkblue/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'assets/css/style.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'assets/css/custom.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'assets/css/sb-admin-2.css" rel="stylesheet" type="text/css" />
                
              ';
        $this->CI->assets->add_css($css,"head");
        
        $js_head = '
            
            <script src="'.current_admin_theme_url().'assets/plugins/jquery-1.10.1.min.js"></script>
            <script src="'.current_admin_theme_url().'assets/scripts/jquery/js/jquery-ui-1.10.3.custom.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/bootstrap/js/bootstrap.min.js"></script>
            
            <script src="'.current_admin_theme_url().'assets/plugins/hover-dropdown.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.migrate.min.js"></script>
            
            <script src="'.current_admin_theme_url().'assets/plugins/modernizr.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.easing.min.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.mousewheel.js"></script>

            <script src="'.current_admin_theme_url().'assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
            <script src="'.current_admin_theme_url().'assets/scripts/holder.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/back-to-top.js"></script>
            
            <script src="'.current_admin_theme_url().'assets/js/custom/raphael-min.js"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/custom/app.js" type="text/javascript"></script>            
            <script src="'.current_admin_theme_url().'static/js/floatThead/jquery.floatThead.js"></script>            
            <script src="'.current_admin_theme_url().'static/js/tinymce/tinymce.min.js"></script>
            <script src="'.current_admin_theme_url().'static/js/tinymce/jquery.tinymce.min.js"></script>
            <script src="'.current_admin_theme_url().'static/js/jquery.form.js"></script>
            <script src="'.current_admin_theme_url().'static/js/ajax.js"></script>
            <script src="'.current_admin_theme_url().'static/js/data.js"></script>
            <script src="'.current_admin_theme_url().'static/js/blocks.js"></script>
            
            <!--[if lt IE 9]>
            <script src="'.current_admin_theme_url().'assets/plugins/respond.min.js"></script>  
            <![endif]-->
            
            <script src="'.current_admin_theme_url().'assets/plugins/bxslider/jquery.bxslider.min.js"></script>			
			<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->
			';
        $js = '
            
            <script type="text/javascript">
              $(document).ready(function() {
                $("a[rel=catalogue]").fancybox({
                  "transitionIn"		: "fade",
                  "transitionOut"		: "fade",
                  "padding"		      : "0",
                  "titlePosition" 	: "outside",
                  "titleFormat"		: function(title, currentArray, currentIndex, currentOpts) {
                    return \'<span id="fancybox-title-over">Image \' + (currentIndex + 1) + \' / \' + currentArray.length + (title.length ? \' &nbsp; \' + title : \'\') + \'</span>\';
                  }
                });
              });
            </script>
            
            
            <script type="text/javascript">
            $(".bxslider").bxSlider({
              minSlides: 1,
              maxSlides: 3,
              slideWidth: 165,
              slideMargin: 15,
              controls: true                     // true, false - previous and next controls
            });
            </script>
            
            <script type="text/javascript">
              $(function () {
                $("#mangstab").tab("show");
                var hash = document.location.hash;
                var prefix = "tab_";
                if (hash) {
                    $(".nav-tabs a[href="+hash.replace(prefix,"")+"]").tab("show");
                } 

                $(".nav-tabs a").on("shown", function (e) {
                    window.location.hash = e.target.hash.replace("#", "#" + prefix);
                });
                $(".table-responsive table.table").floatThead({scrollingTop:100});
                window.scrollTo(0,0);
              });
            </script>
            
            <script type="text/javascript">
              var head = document.getElementsByTagName("head")[0],
                  style = document.createElement("style");
              style.type = "text/css";
              if (style.styleSheet)
              {
                style.styleSheet.cssText = ":before,:after{content:none !important;}";
              } else {
                style.appendChild(document.createTextNode(":before,:after{content:none !important;}"));
              }
              head.appendChild(style);
              setTimeout(function(){
                  head.removeChild(style);
              }, 0);
            </script>
            
        ';
        $this->CI->assets->add_js($js,"body");
        $this->CI->assets->add_js($js_head,"head");
        $this->CI->hook->add_action('hook_create_form_filter_output',array($this,'_hook_create_form_filter_output'));
  }
  
  function setFrontAdminAssets()
  {
        $css = '    
				<link href="'.current_admin_theme_url().'dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/colorpicker/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/styles.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>
				<link href="'.current_admin_theme_url().'assets/css/ionslider/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
				<link href="'.current_admin_theme_url().'assets/css/ionslider/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css"/>

                <link href="'.current_admin_theme_url().'assets/scripts/jquery/css/ui-darkblue/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
				<link href="'.current_admin_theme_url().'assets/css/bootstrap-slider/slider.css" rel="stylesheet" type="text/css"/>
                <link href="'.current_admin_theme_url().'assets/css/style.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'assets/css/custom.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'assets/css/sb-admin-2.css" rel="stylesheet" type="text/css" />
                <link href="'.current_admin_theme_url().'static/css/bootstrap-theme-admin.css" rel="stylesheet" type="text/css" />                
              ';
        $this->CI->assets->add_css($css,"body");
        
        $js_head = '
            
            <!-- <script src="'.current_admin_theme_url().'assets/plugins/jquery-1.10.1.min.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/bootstrap/js/bootstrap.min.js"></script>  -->
            <script src="'.current_admin_theme_url().'assets/scripts/jquery/js/jquery-ui-1.10.3.custom.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/hover-dropdown.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.migrate.min.js"></script>            
            <script src="'.current_admin_theme_url().'assets/plugins/modernizr.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.easing.min.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/jquery.mousewheel.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
            <script src="'.current_admin_theme_url().'assets/scripts/holder.js"></script>
            <script src="'.current_admin_theme_url().'assets/plugins/back-to-top.js"></script>
				           
            <script src="'.current_admin_theme_url().'assets/js/custom/raphael-min.js"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>            
            <script src="'.current_admin_theme_url().'assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>            
            <script src="'.current_admin_theme_url().'assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>            
            <script src="'.current_admin_theme_url().'assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>            
            <script src="'.current_admin_theme_url().'assets/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'assets/js/custom/app.js" type="text/javascript"></script>
            <script src="'.current_admin_theme_url().'static/js/floatThead/jquery.floatThead.js"></script>
            
            <script src="'.current_admin_theme_url().'static/js/tinymce/tinymce.min.js"></script>
            <script src="'.current_admin_theme_url().'static/js/tinymce/jquery.tinymce.min.js"></script>
            <script src="'.current_admin_theme_url().'static/js/jquery.form.js"></script>
            
            <!--[if lt IE 9]>
            <script src="'.current_admin_theme_url().'assets/plugins/respond.min.js"></script>  
            <![endif]-->
            
            <script src="'.current_admin_theme_url().'assets/plugins/bxslider/jquery.bxslider.min.js"></script>			
            <script src="'.current_admin_theme_url().'assets/js/plugins/ionslider/ion.rangeSlider.min.js"></script>			
            <script src="'.current_admin_theme_url().'assets/js/plugins/bootstrap-slider/bootstrap-slider.js"></script>			
			<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->
			';
        $js = '
            <script src="'.current_admin_theme_url().'static/js/ajax.js"></script>
            <script src="'.current_admin_theme_url().'static/js/data.js"></script>
            <script src="'.current_admin_theme_url().'static/js/blocks.js"></script>
            <script src="'.current_admin_theme_url().'static/js/admin-helper.js"></script>
            <script src="'.current_admin_theme_url().'static/js/admin-trigger.js"></script>
            <script src="'.current_admin_theme_url().'static/js/admin.js"></script>
            <script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery("body").addClass("admin-editor");
					jQuery("body").addClass("skin-blue").addClass("fixed");
				});
				
				var head = document.getElementsByTagName("head")[0],
				style = document.createElement("style");
				style.type = "text/css";
				if (style.styleSheet)
				{
					style.styleSheet.cssText = ":before,:after{content:none !important;}";
				} else {
					style.appendChild(document.createTextNode(":before,:after{content:none !important;}"));
				}
					head.appendChild(style);
					setTimeout(function(){
					head.removeChild(style);
				}, 0);
            </script>
            
        ';
        $this->CI->assets->add_js($js,"body");
        $this->CI->assets->add_js($js_head,"head");
        $this->CI->hook->add_action('hook_create_form_filter_output',array($this,'_hook_create_form_filter_output'));
  }
  
  function _hook_create_form_filter_output($output = "")
  {
    $title = $this->getTextBetweenTags($output,'h3');
    $output = preg_replace('/<h3>(.*)<\/h3>/i', '', $output);
    $output = '<div class="col-lg-12">
                <div class="box box-solid box-primary collapsed-box">
                  <div data-original-title="Form Pencarian" class="box-header" data-toggle="tooltip" title="">
                      <h3 class="box-title">'.$title.'</h3>
                      <div class="box-tools pull-right">
                          <button class="btn btn-primary btn-xs" data-widget="collapse" id="box-collapse"><i class="fa fa-minus"></i></button>
                          <script>
                            $(document).ready(function(){
                              $("#box-collapse").click();
                            });
                          </script>
                          <!-- <button class="btn btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i></button> -->
                      </div>
                  </div>
                  <div style="display: block;" class="box-body">
                     '.$output.'
                  </div>
                  <div style="display: block;" class="box-footer">
                      &nbsp;
                  </div>
                </div>
              </div>';
    return $output;
  }
  
  function getTextBetweenTags($string, $tagname) {
    $pattern = "/<$tagname>([\w\W]*?)<\/$tagname>/";
    preg_match($pattern, $string, $matches);
    return (isset($matches[1]))?$matches[1]:"";
  }

}
