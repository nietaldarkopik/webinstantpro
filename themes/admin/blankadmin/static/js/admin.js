jQuery(document).ready(function(){
  function create_admin_menu()
  {
	$("body").css({"top":"50px"});
  }
  
  create_admin_menu();
  create_panel({
	modal_type: "modal-lg block-dragable",
	class:"containment",
	id:"setting-widget",
	body:"Hello World"
  });
  
  create_panel({
	modal_type: "ajax_container content_container modal-lg block-dragable",
	//class:"",
	id:"admin-menu-panel",
	body:"&nbsp;",
	footer: ""
  });
  
  create_panel({
	modal_type: "modal-lg modal-style-editor col-sm-12 col-md-6 col-lg-6 pull-right",
	class:"col-sm-12 container-style-editor col-md-12 col-lg-12 pull-right",
	id:"admin-style-editor",
	body:"&nbsp;",
	footer: ""
  });
  
  //$( ".block-sortable > *" ).append("<a href=\"javascript:void(0);\" title=\"Move this block\" class=\"sortable-handle glyphicon glyphicon-move\"></a>");
  //$( ".block-dragable" ).draggable({ scroll: true, scrollSpeed: 100});//,handle: $(this).find(".draggable-handle"),containment: $(this).parent(".containment"),cursor: "move",});
  //$( ".inline_editable" ).after("<a href=\"javascript:void(0);\" title=\"Move this block\" class=\"sortable-handle glyphicon glyphicon-move\"></a>");
  
  /*
  $( ".inline_editable" ).each(function(i,k){
	if($(this).find(".editable").length == 0)
	{}else{
	  var id = $(this).find(".editable").attr("id");
	  $(this).find(".editable").after("<a class=\"toggle_editable glyphicon glyphicon-pencil\" id=\"toogle_"+id+"\" href=\"javascript:void(0);\"></a>");
	}
  });
 */
  
  /*
  $( ".block-sortable,.row" ).each(function(i,k){
	var connectwith = $(k).attr("connect-with");
	connectwith = (!connectwith)?false:connectwith;
	var orginalcolorgroup,orginalcolorsingle;
	$(k).sortable({
	  scroll: true,
	  connectWith: connectwith,
	  revert: true,
	  items: "> *:not(.admin-configuration)",
	  //placeholder: "ui-state-highlight-editor",
	  forcePlaceholderSize: false,
	  handle: ".sortable-handle",
	  tolerance: "pointer",
	  start: function( event, ui ) {
		var height = $(".ui-sortable-placeholder").css('height');
		height = height.replace("px","");
		height = parseInt(height);
		height = height - 2;
		$(".ui-sortable-placeholder").css('height',height);
		$(".ui-sortable-placeholder").css('margin',"0");
		orginalcolorgroup = $(ui.item).css('background-color');
		orginalcolorsingle = $(k).css('background-color');
		//$(ui.item).css("padding","-2px");
		$(connectwith).animate({
		  backgroundColor: "#ff9"
		});
		$(k).animate({
		  backgroundColor: "#ff9"
		});
	  },
	  stop: function( event, ui ) {
		$(connectwith).animate({
		  backgroundColor: orginalcolorgroup
		});
		$(k).animate({
		  backgroundColor: orginalcolorsingle
		});
		init_resizable();
	  },
		receive: function(event, ui) {
			var targetList = ui.item.closest(".container");
		}
	});
  });
  */
  
  /*
  $(".toggle_editable").on("click",function(){
	var id_tinymce = $(this).attr("id");
	id_tinymce = id_tinymce.replace("toogle_","");
	tinymce.execCommand('mceToggleEditor',false,id_tinymce);
  });
  */
  
  $(".block-widgets-container,body > .container,body > .container-fluid").each(function(k,v){
	  create_editor_container(v,k);
  });
  
  $(".block-widgets-container > .container-fluid > .row,body > .editor-wrap-con > .container > .row,body > .editor-wrap-con > .container > .editor-wrap-row > .row,body > .editor-wrap-con > .container-fluid > .row,body > .editor-wrap-con > .container-fluid > .editor-wrap-row > .rowbody > .container > .row,body > .container > .editor-wrap-row > .row,body > .container-fluid > .row,body > .container-fluid > .editor-wrap-row > .row").each(function(k,v){
	  create_editor_row(v,k);
  });
  
  $("[class*=col-][data-block-item-id]").each(function(k,v){
	  create_editor_col(v,k);
  });
  
  /*
  $(".admin-configuration *").on("mouseover",function(){
	$(this).parents(".block-widgets-container").css("background","#ff9").css("opacity","0.9");
  });
  
  $(".admin-configuration *").on("mouseout",function(){
	$(this).parents(".block-widgets-container").css("background","").css("opacity","1");
  });
  */
	
	jQuery("body").addClass("admin-editor");
	
	var containers = $("body > .container,body > .fluid-container");
	var add_container = '<button class="btn btn-primary add-new-container center-block text-center">Tambah Kontainer</button>';
	$("body").append(add_container);
	$("[data-block-item-id]").each(function(k,v){
		if($(v).children(":not(.admin-configuration-col)").length == 0)
		{
			var col_id = $(v).attr("data-block-item-id");
			$(v).find(".admin-configuration-col").before('<button btn-block-item-id="'+col_id+'" class="btn-add-new-widget btn btn-sm bg-orange col-lg-12 col-md-12 col-sm-12 col-xs-12">Masukan Widget</button>').end();
		}
	});
	$("[data-row-id]").trigger("do_sortable_col");
	$("body").trigger("do_sortable_container");
	$(".editor-wrap-con").trigger("do_sortable_row");
	$(".editor-wrap-row > .row > [class^=col-]").trigger("do_resizable_col");

	$("#admin-menu-panel").on('hidden.bs.modal', function (e) {
		$(document).ajaxSend(function( event, jqxhr, settings ){});
	});
	$("input[type='colorpicker']").colorpicker();
	//init_resizable();
	
	$(".slider").slider();

	$('#admin-style-editor').on('hide.bs.modal', function (e) {
		var r = confirm("Anda yakin akan menutup form Setting?");
		if (r == true) {
			return true;
		} else {
			return false;
		}
	});
});
