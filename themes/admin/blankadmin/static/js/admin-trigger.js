jQuery(document).on("click","#admin_right_tab li.active a",function (e) {
	var tab = $(this);
	jQuery(tab).parents("ul").next().find(".tab-pane.active").removeClass('active');
	jQuery(tab).parents('li').removeClass('active');
	jQuery(tab).blur();
});
	
jQuery(document).on("click", ".admin-configuration .setting-container", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var blockid = $(this).attr("id");
    blockid = blockid.replace("showblock-", "");
    	
    $.ajax({
        url: base_url + "admin/adminblocks/setting_container/" + blockid,
        type: "post",
        data: "is_ajax=1",
        success: function(result) {
            //var output = $("<div></div>").append($(result));
            $("#admin-style-editor .modal-dialog").html($(result).html());
            $("#admin-style-editor").modal('show');
            $(".colorpicker").each(function(k,v){
				var parent = $(v).parent();
				$(v).colorpicker({container:parent,align:'left',format:'rgba'});
			});
        }
    });
});

jQuery(document).on("click", ".admin-configuration-col .setting-col", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var this_btn = $(this);
    var container_id = $(this_btn).parents("[data-container-id]").attr("data-container-id");
    var row_id = $(this_btn).parents("[data-row-id]").attr("data-row-id");
    row_id = row_id.replace("addcolrow-", "");
    var blockid = $(this).attr("id");
    blockid = blockid.replace("showcol-", "");
    var block_col_id = blockid;
    
	var param_data = "page_id=" + ((!page_id)?0:page_id) + "&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id + "&row_id=" + row_id +"&block_col_id=" + block_col_id;
	var param_data_obj = [{"theme_id" : theme_id , "page_id" : ((!page_id)?0:page_id) , "template_id" : template_id , "con_id" : container_id , "row_id" : row_id, "block_col_id" : block_col_id}];

	$( document ).ajaxSend(function( event, jqxhr, settings ) {
	  if(typeof settings.data == "string")
	  {
		var data_tmp = settings.data;
		var data_output = "";
		data_tmp = data_tmp.split("&");
		$.each(data_tmp,function(k,v){
			var t_v = v.split("=");
			if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "row_id" || t_v[0] == "block_col_id" || t_v[0] == ""){}else{
				data_output = data_output + "&" + v;
			}
		});
		settings.data = data_output + "&" + param_data;
	  }else{
		//settings.data = $.extend(param_data_obj,settings.data);
	  }
	});
	
    $.ajax({
        url: base_url + "admin/adminblocks/setting_col/" + blockid,
        type: "post",
        data: "is_ajax=1&"+param_data,
        success: function(result) {
			$( document ).ajaxSend(function( event, jqxhr, settings ) {
			  if(typeof settings.data == "string")
			  {
				var data_tmp = settings.data;
				var data_output = "";
				data_tmp = data_tmp.split("&");
				$.each(data_tmp,function(k,v){
					var t_v = v.split("=");
					if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "row_id" || t_v[0] == "block_col_id" || t_v[0] == ""){}else{
						data_output = data_output + "&" + v;
					}
				});
				settings.data = data_output + "&" + param_data;
			  }else{
				//settings.data = $.extend(param_data_obj,settings.data);
			  }
			});
            //var output = $("<div></div>").append($(result));
            $("#admin-menu-panel .modal-dialog").html($(result).html());
            $("#admin-menu-panel").modal('show');
        }
    });
});

jQuery(document).on("click", ".admin-configuration .remove-container", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var id = $(this).attr("id");
    var blockid = id;
    blockid = blockid.replace("removeblock-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/remove_con/" + blockid,
        type: "post",
        data: "is_ajax=0&page_id="+((!page_id)?0:page_id),
        dataType: "json",
        success: function(result) {
            $('#' + id).parents(".editor-wrap-con").remove();
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".admin-configuration .remove-row", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var id = $(this).attr("id");
    var blockid = id;
    blockid = blockid.replace("removerow-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/remove_row/" + blockid,
        type: "post",
        data: "is_ajax=0&page_id="+((!page_id)?0:page_id),
        dataType: "json",
        success: function(result) {
            $('#' + id).parents(".editor-wrap-row").remove();
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".admin-configuration-col .remove-col", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var id = $(this).attr("id");
    var blockid = id;
    blockid = blockid.replace("removecol-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/remove_col/" + blockid,
        type: "post",
        data: "do_action=do_remove_col&is_ajax=0&page_id="+((!page_id)?0:page_id),
        dataType: "json",
        success: function(result) {
            $('#' + id).parents("[data-block-item-id=" + blockid + "]").remove();
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".admin-configuration .remove-widget", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var id = $(this).attr("id");
    var blockid = id;
    blockid = blockid.replace("removewidget-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/remove_widget/" + blockid,
        type: "post",
        data: "is_ajax=0&page_id="+((!page_id)?0:page_id),
        dataType: "json",
        success: function(result) {
            $('#' + id).parents("[data-block-item-id=" + blockid + "]").remove();
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".admin-configuration .copy-container", function() {
    $(".editor-wrap-con").css("z-index", "1");
    var blockid = $(this).attr("id");
    blockid = blockid.replace("showblock-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/setting_container/" + blockid,
        type: "post",
        data: "is_ajax=1",
        success: function(result) {
            //var output = $("<div></div>").append($(result));
            $("#admin-menu-panel .modal-dialog").html($(result).html());
            $("#admin-menu-panel").modal('show');
        }
    });
});

jQuery(document).on("click", ".admin-configuration-row .setting-row", function() {
    var blockid = $(this).attr("id");
    blockid = blockid.replace("showrow-", "");
    $.ajax({
        url: base_url + "admin/adminblocks/setting_row/" + blockid,
        type: "post",
        data: "is_ajax=1",
        success: function(result) {
            $("#admin-style-editor .modal-dialog").html($(result).html());
            $("#admin-style-editor").modal('show');
            $(".colorpicker").colorpicker();
        }
    });
});

jQuery(document).on("click", ".admin-configuration-col .setting-widget",function() {
	var blockid = $(this).attr("id");
	blockid = blockid.replace("showwidget-", "");
	$.ajax({
		url: base_url + "admin/adminblocks/setting_widget/" + blockid,
		type: "post",
		data: "is_ajax=1",
		success: function(result) {
			//var output = $("<div></div>").append($(result));
			$("#admin-menu-panel .modal-dialog").html($(result).html());
			$("#admin-menu-panel").modal('show');
		}
	});
});

jQuery(document).on("do_sortable_container", "body", function() {
    jQuery(this).sortable({
        connectWith: "body",
        placeholder: "ui-state-highlight ui-state-highlight-editor",
        tolerance: "intersect",
        items: '> .editor-wrap-con',
        axis: 'y',
        revert: true,
        opacity: 0.4,
		scroll: true,
		scrollSensitivity: 50,
        handle: '.move-container',
        update: function(event, ui) {
			save_sort_container();
        }
    }); //.disableSelection();
});

jQuery(document).on("do_sortable_row", ".editor-wrap-con", function() {
    jQuery(this)
	.sortable({
        connectWith: ".editor-wrap-con",
        placeholder: "ui-state-highlight ui-state-highlight-editor",
        tolerance: "pointer",
        items: '.editor-wrap-row',
		forceHelperSize: true,
        //appendTo: '.editor-wrap-con > .container',
        //containment: "parent",
		//accept: ".container,.fluid-container",
        axis: 'y',
		scroll: true,
		scrollSensitivity: 50,
        revert: true,
        opacity: 0.4,
        handle: '.move-row',
        drop: function (event, ui) {
			//$(ui.draggable).parents(".container")
            //$('.dropArea').append(ui.draggable);
        },
        update: function(event, ui) {
			var parent = $(ui.item).parents(".container");
			var superparent = $(ui.item).parents(".editor-wrap-con");
            var index = $(parent).find(".editor-wrap-row").index(ui.item);
			
			if(index == -1)
			{
				$(superparent).find(".container,.fluid-container").eq(0).append(ui.item);
			}
			save_sortorder();
            //alert(ui.item.html());
            //sort_menu();
			 //var row_id = $(ui.item[0]).attr("data-row-id");
			 //$(this).find(".container").append($(ui.item[0]));
			 //$(this).children("[data-row-id="+row_id+"]").remove();
			 //create_editor_row($(this).find("[data-row-id="+row_id+"]"),0);
        },
        start: function(event, ui) {
            //var index = ui.placeholder.index();
            //alert(ui.item.html());
            //sort_menu();
            var position = $(ui.item).position();
            $(ui.item).css({
                top: position.top * (-1)
            })
        },
        receive: function(event,ui){
		}
    }); //.disableSelection();;
	
});

jQuery(document).on("do_sortable_col", "[data-row-id]", function() {
	jQuery(this).sortable({
        connectWith: "[data-row-id]",
        placeholder: "ui-state-highlight ui-state-highlight-editor-col",
        tolerance: "pointer",
        items: '> [class^=col-]',
		forceHelperSize: true,
        //appendTo: '.editor-wrap-con > .container',
        //containment: "parent",
		//accept: ".container,.fluid-container",
        axis: 'yx',
		scroll: true,
		scrollSensitivity: 50,
        revert: true,
        opacity: 0.4,
        handle: '.move-col',
        drop: function (event, ui) {
			//$(ui.draggable).parents(".container")
            //$('.dropArea').append(ui.draggable);
        },
        update: function(event, ui) {
			var parent = $(ui.item).parents(".row");
			var superparent = $(ui.item).parents(".editor-wrap-row");
            var index = $(parent).find("[data-block-item-id]").index(ui.item);
			
			if(index == -1)
			{
				$(superparent).find(".editor-wrap-row").eq(0).append(ui.item);
			}
			save_sortorder();
            //alert(ui.item.html());
            //sort_menu();
			 //var row_id = $(ui.item[0]).attr("data-row-id");
			 //$(this).find(".container").append($(ui.item[0]));
			 //$(this).children("[data-row-id="+row_id+"]").remove();
			 //create_editor_row($(this).find("[data-row-id="+row_id+"]"),0);
        },
        start: function(event, ui) {
            //var index = ui.placeholder.index();
            //alert(ui.item.html());
            //sort_menu();,
			ui.placeholder.height(ui.item.height());
			ui.placeholder.width(ui.item.width());
            var position = $(ui.item).position();
            $(ui.item).css({
                top: position.top * (-1)
            })
        },
        receive: function(event,ui){
		}
    });

	/*	
	jQuery( this ).draggable({
		//connectToSortable: ".editor-wrap-con",
        placeholder: "ui-state-highlight-editor",
		revert: "invalid",
		handle: ".move-col"
	});
    jQuery(this).sortable({
        connectWith: ".editor-wrap-row",
        placeholder: "ui-state-highlight-editor",
        tolerance: "pointer",
        items: '> .editor-wrap-col',
        //axis: 'y',
        revert: true,
        opacity: 0.4,
		scroll: true,
		scrollSensitivity: 50,
        update: function(event, ui) {
            //var index = ui.placeholder.index();
            //alert(ui.item.html());
            //sort_menu();
        }
    }); //.disableSelection();
	*/
});

jQuery(document).on("do_resizable_col", ".editor-wrap-row > .row > [class^=col-]", function() {
   init_resizable(jQuery(this));
});

jQuery(document).on("click", ".add-new-container", function() {
    var this_btn = $(this);
    $.ajax({
        url: base_url + "admin/adminblocks/add_con/",
        type: "post",
        data: "do_action=do_add_container&theme_id=" + theme_id + "&template_id=" + template_id + "&page_id=" + page_id,
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

            var container_id = $("<div></div>").html(content).find("[data-container-id]").attr("data-container-id");
            $(this_btn).before($(content));
            create_editor_container($("[data-container-id=" + container_id + "]"), 0);
			$("body").trigger("do_sortable_container");
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".add-new-row", function() {
    var this_btn = $(this);
    var container_id = $(this_btn).attr("id");
    container_id = container_id.replace("addrow-", "");
	
	var param_data = "page_id=" + ((!page_id)?0:page_id) + "&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id;
	$( document ).ajaxSend(function( event, jqxhr, settings ) {
	  if(typeof settings.data == "string")
	  {
		var data_tmp = settings.data;
		var data_output = "";
		data_tmp = data_tmp.split("&");
		$.each(data_tmp,function(k,v){
			var t_v = v.split("=");
			if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == ""){}else{
				data_output = data_output + "&" + v;
			}
		});
		settings.data = data_output + "&" + param_data;
	  }else{
		//settings.data = $.extend(param_data_obj,settings.data);
	  }
	});
	
    $.ajax({
        url: base_url + "admin/adminblocks/add_row/",
        type: "post",
        data: "do_action=do_add_row" + "&" + param_data,
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

            var row_id = $("<div></div>").html(content).find("[data-row-id]").attr("data-row-id");
            $("[data-container-id=" + container_id + "]").append($(content));
            create_editor_row($("[data-row-id=" + row_id + "]"), 0);
			$(".editor-wrap-con").trigger("do_sortable_row");
            show_alert(type, message)
        }
    });
});

jQuery(document).on("click", ".add-new-col", function() {
    var this_btn = $(this);
    var container_id = $(this_btn).parents("[data-container-id]").attr("data-container-id");
    var row_id = $(this_btn).attr("id");
    row_id = row_id.replace("addcolrow-", "");
	
	var param_data = "page_id=" + ((!page_id)?0:page_id) + "&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id + "&row_id=" + row_id;
	$( document ).ajaxSend(function( event, jqxhr, settings ) {
	  if(typeof settings.data == "string")
	  {
		var data_tmp = settings.data;
		var data_output = "";
		data_tmp = data_tmp.split("&");
		$.each(data_tmp,function(k,v){
			var t_v = v.split("=");
			if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "" || t_v[0] == "row_id"){}else{
				data_output = data_output + "&" + v;
			}
		});
		settings.data = data_output + "&" + param_data;
	  }else{
		//settings.data = $.extend(param_data_obj,settings.data);
	  }
	});
    $.ajax({
        url: base_url + "admin/adminblocks/add_col_options/",
        type: "post",
        data: "do_action=do_add_col_options&" + param_data,
        //dataType: "json",
        success: function(result) {
			$("#admin-menu-panel .modal-dialog").html($(result).html());
            $("#admin-menu-panel").modal('show');
        }
    });
});

jQuery(document).on("click", ".btn-add-new-widget", function() {
    var this_btn = $(this);
    var container_id = $(this_btn).parents("[data-container-id]").attr("data-container-id");
    var row_id = $(this_btn).parents("[data-row-id]").attr("data-row-id");
    row_id = row_id.replace("addcolrow-", "");
    var block_col_id = $(this_btn).attr("btn-block-item-id");
	
	var param_data = "page_id=" + ((!page_id)?0:page_id) + "&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id + "&row_id=" + row_id +"&block_col_id=" + block_col_id;
	var param_data_obj = [{"theme_id" : theme_id , "page_id" : ((!page_id)?0:page_id) , "template_id" : template_id , "con_id" : container_id , "row_id" : row_id, "block_col_id" : block_col_id}];
	
	
    $.ajax({
        url: base_url + "admin/adminblocks/add_widget_options/",
        type: "post",
        data: "do_action=do_add_widget_options&"+param_data,
        //dataType: "json",
        success: function(result) {
			$( document ).ajaxSend(function( event, jqxhr, settings ) {
			  if(typeof settings.data == "string")
			  {
				var data_tmp = settings.data;
				var data_output = "";
				data_tmp = data_tmp.split("&");
				$.each(data_tmp,function(k,v){
					var t_v = v.split("=");
					if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "row_id" || t_v[0] == "block_col_id" || t_v[0] == ""){}else{
						data_output = data_output + "&" + v;
					}
				});
				settings.data = data_output + "&" + param_data;
			  }else{
				//settings.data = $.extend(param_data_obj,settings.data);
			  }
			});
			$("#admin-menu-panel .modal-dialog").html($(result).html());
            $("#admin-menu-panel").modal('show');
            if($(".add-widgets.active").length == 0)
            {
				$(".save-widgets").removeClass("btn-primary").addClass("bg-gray");
			}else{
				$(".save-widgets").removeClass("bg-gray").addClass("btn-primary");
			}
        }
    });
});

jQuery(document).on("click", ".add-columns", function() {
    $(".add-columns").removeClass("active");
    $(this).addClass("active");
});

jQuery(document).on("click", ".add-widgets", function() {
    $(".add-widgets").removeClass("active");
    $(this).addClass("active");
    if($(".add-widgets.active").length == 0)
	{
		$(".config-widgets,.save-widgets").removeClass("btn-primary").addClass("save-widget-disabled").addClass("bg-gray");
	}else{
		$(".config-widgets,.save-widgets").removeClass("bg-gray").removeClass("save-widget-disabled").addClass("btn-primary");
	}

});

jQuery(document).on("click", ".save-columns", function() {
    jQuery(this).html("Sedang Menambahkan...");
    var this_btn = $(".add-columns.active");
    var container_id = $(this_btn).attr("con_id");
    var row_id = $(this_btn).attr("row_id");
    var block_col_id = $(this_btn).attr("btn-block-item-id");
    var theme_id = $(this_btn).attr("theme_id");
    var template_id = $(this_btn).attr("template_id");
    var number_columns = $(this_btn).attr("number_columns");
    var block_name = $(".add-block-name").val();
    
	var param_data = "page_id=" + ((!page_id)?0:page_id) + "&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id+"&block_name"+block_name + "&row_id=" + row_id;
	$( document ).ajaxSend(function( event, jqxhr, settings ) {
	  if(typeof settings.data == "string")
	  {
		var data_tmp = settings.data;
		var data_output = "";
		data_tmp = data_tmp.split("&");
		$.each(data_tmp,function(k,v){
			var t_v = v.split("=");
			if(t_v[0] == "theme_id" || t_v[0] == "page_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "row_id" || t_v[0] == "block_name" || t_v[0] == "" ){}else{
				data_output = data_output + "&" + v;
			}
		});
		settings.data = data_output + "&" + param_data;
	  }else{
		//settings.data = $.extend(param_data_obj,settings.data);
	  }
	});
	
    $.ajax({
        url: base_url + "admin/adminblocks/add_col/"+number_columns,
        type: "post",
        data: "do_action=do_add_cols&" + param_data,
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

            var rows = $("<div></div>").html(content).find("[data-block-item-id]");
            if($(rows).length > 0)
            {
				$(rows).each(function(k,v){
					var col_id = $(v).attr("data-block-item-id");
					$(v).find("bscol").replaceWith('<button btn-block-item-id="'+col_id+'" class="btn-add-new-widget btn btn-sm bg-orange col-lg-12 col-md-12 col-sm-12 col-xs-12">Masukan Widget</button>').end();
					$("[data-row-id=" + row_id + "]").append($(v));
					//var block_col_id = $(v).find("bscol").attr("data-block-item-id");
					//$(v).find("bscol").replaceWith('<button class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12">Masukan Widget</button>';
					create_editor_col($("[data-block-item-id=" + col_id + "]"), 0);
					$("[data-row-id]").trigger("do_sortable_col");
					$(".editor-wrap-row > .row > [class^=col-]").trigger("do_resizable_col");
				});
			}
            show_alert(type, message);
			$("#admin-menu-panel").modal('hide');
			$(".editor-wrap-row > .row > [class^=col-]").trigger("do_resizable_col");
        }
    });
});

jQuery(document).on("click", ".config-widgets", function() {
    jQuery(this).html("Loading ...");
    var this_btn = $(".add-widgets.active");
    var container_id = $(this_btn).attr("con_id");
    var row_id = $(this_btn).attr("row_id");
    var block_col_id = $(this_btn).attr("block_col_id");
    var theme_id = $(this_btn).attr("theme_id");
    var template_id = $(this_btn).attr("template_id");
    var number_columns = $(this_btn).attr("number_columns");
    var widget_url = $(this_btn).attr("widget_url");
    
    $.ajax({
        url: widget_url,
        type: "post",
        data: "is_ajax=1&theme_id=" + theme_id + "&template_id=" + template_id + "&con_id=" + container_id + "&row_id=" + row_id +"&block_col_id=" + block_col_id,
        //dataType: "json",
        success: function(result) {
			$("#admin-menu-panel .modal-dialog").html($(result).html());
        }
    });
});

jQuery(document).on("click", ".save-widgets", function() {
    jQuery(this).html("Loading ...");
    var this_btn = $(".add-widgets.active");
    var container_id = $(this_btn).attr("con_id");
    var row_id = $(this_btn).attr("row_id");
    var theme_id = $(this_btn).attr("theme_id");
    var template_id = $(this_btn).attr("template_id");
    var block_name = $(".add-block-name").val();
    var block_col_id = $(this_btn).attr("block_col_id");
    var widget_id = $(this_btn).attr("widget_id");
    var widget_content_id = $(this_btn).attr("widget_content_id");
    
    $.ajax({
        url: base_url + "admin/adminblocks/add_widget/"+block_col_id,
        type: "post",
        data: "do_action=do_save_widget&is_ajax=1&con_id="+container_id+"&row_id="+row_id+"&theme_id="+theme_id+"&template_id="+template_id +"&block_col_id=" + block_col_id+"&widget_id="+widget_id+"&widget_content_id="+widget_content_id,
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

			if($(".btn-add-new-widget[btn-block-item-id="+block_col_id+"]").length > 0)
			{
				$(".btn-add-new-widget[btn-block-item-id="+block_col_id+"]").replaceWith($(content));
			}else{
				$("[data-block-item-id="+block_col_id+"]").html($(content));
				create_editor_col($("[data-block-item-id=" + block_col_id + "]"), 0);
			}
			
			$("[data-row-id]").trigger("do_sortable_col");
			$(".editor-wrap-row > .row > [class^=col-]").trigger("do_resizable_col");
			//$(".editor-wrap-row > .row > [class^=col-]").trigger("do_resizable_col");
            $("#admin-menu-panel").modal('hide');
            $.ajax({
				url:base_url + "admin/adminblocks/callback_widget/"+block_col_id,
				type: "post",
				//dataType: "script",
				data:"do_action=do_save_widget&is_ajax=1&con_id="+container_id+"&row_id="+row_id+"&theme_id="+theme_id+"&template_id="+template_id +"&block_col_id=" + block_col_id+"&widget_id="+widget_id+"&widget_content_id="+widget_content_id,
				success: function(script)
				{
					if(!script){
					}else{
						eval(script);
					}
				}
			});
            show_alert(type, message);
        }
    });
});
