function save_sort_container()
{
	var container_element = jQuery("body > .editor-wrap-con");
	var data_order = [];
	
	jQuery.each(jQuery(container_element),function(k,v){
		var con_id = $(v).find("[data-container-id]").attr("data-container-id");
		data_order[k] = con_id;
	});
	
    $.ajax({
        url: base_url + "admin/adminblocks/sortorder_con/",
        type: "post",
        data: {"do_action":"do_sort_order_container","data_containers":data_order,"theme_id": theme_id,"template_id":template_id,"page_id":page_id},
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

            show_alert(type, message)
        }
    });
}

function save_sortorder()
{
	var container_element = jQuery("body > .editor-wrap-con");
	var data_order = [];
	
	jQuery.each(jQuery(container_element),function(k,v){
		var con_id = $(v).find("[data-container-id]").attr("data-container-id");
		var row_element = jQuery(v).find("[data-row-id]");
		
		var data_order_row = [];
		jQuery.each(jQuery(row_element),function(krow,vrow){
			var row_id = $(vrow).attr("data-row-id");
			var block_item_element = jQuery(vrow).find("[data-block-item-id]");
				
			var data_order_block_item = [];
			jQuery.each(jQuery(block_item_element),function(kblock_item,vblock_item){
				var block_col_id = $(vblock_item).attr("data-block-item-id");
				data_order_block_item[kblock_item] = {"block_col_id" : block_col_id};
			});
			data_order_row[krow] = {"row_id" : row_id,"children" : data_order_block_item};
		});
		data_order[k] = {"con_id" : con_id,"children" : data_order_row};
	});
	
    $.ajax({
        url: base_url + "admin/adminblocks/sortorder_all/",
        type: "post",
        data: {"do_action":"do_sort_order_all","data_order":data_order,"theme_id": theme_id,"template_id":template_id,"page_id":page_id},
        dataType: "json",
        success: function(result) {
            var content = (!result.content) ? "" : result.content;
            var message = (!result.message) ? "" : result.message;
            var type = (!result.status) ? "" : result.status;

            show_alert(type, message)
        }
    });
}

function create_editor_container(v,k)
{
	var block_name = $(v).attr("data-container-id");
	block_name = (typeof block_name != "undefined")?block_name:"id-xx-"+k;
	$(v).wrap('<div class="editor-wrap-con"></div>');
	$(v).parents(".editor-wrap-con")
			.append('<div class="admin-configuration admin-configuration-con" style="display:none;">'+
					'	<div class="admin-container-conf">'+
					'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-warning btn-xs menutools move-container" id="moveblock-'+block_name+'">'+
					'			<span class="glyphicon glyphicon-resize-vertical pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Pindahkan</span>'+
					'		</a>'+
					'		<a href="javascript:void(0);" title="Klik disini untuk menambahkan baris" class="btn btn-warning btn-xs menutools add-new-row" id="addrow-'+block_name+'">'+
					'			<span class="glyphicon glyphicon-plus pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Tambah Baris</span>'+
					'		</a>'+
					'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-warning btn-xs menutools copy-container" id="copyblock-'+block_name+'">'+
					'			<span class="glyphicon glyphicon-duplicate pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Duplikat</span>'+
					'		</a>'+
					'		<a href="javascript:void(0);" title="Klik disini untuk konfigurasi Blok Konten" class="btn btn-warning btn-xs menutools setting-container" id="showblock-'+block_name+'">'+
					'			<span class="glyphicon fa-desktop pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Setting</span>'+
					'		</a>'
					+
					//(($(v).hasClass("container-fixed"))?'':
					'		<a href="javascript:void(0);" title="Klik untuk menghapus kontainer" class="btn btn-danger btn-xs menutools remove-container" id="removeblock-'+block_name+'">'+
					'			<span class="glyphicon glyphicon-trash pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Hapus</span>'+
					'		</a>'
					//)
					+
					'	</div>'+
					'</div>')
					.on("mouseenter",function(){
					  $(this).children(".admin-configuration").show();
					})
					.on("mouseleave",function(){
					  $(this).children(".admin-configuration").hide();
					})
					.on("click",function(){
					});
}

function create_editor_row(v,k)
{

	var block_name = $(v).attr("data-row-id");
	block_name = (typeof block_name != "undefined")?block_name:"id-xx-"+k;
	$(v).wrap('<div class="editor-wrap-row"></div>');
	$(v).parents(".editor-wrap-row")
		.append('<div class="admin-configuration admin-configuration-row" style="display:none;">'+
				'	<div class="admin-container-conf">'+
				'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-primary btn-xs menutools move-row" id="moverow-'+block_name+'">'+
				'			<span class="glyphicon glyphicon-resize-vertical pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Pindahkan</span>'+
				'		</a>'+
				'		<a href="javascript:void(0);" title="Klik disini untuk menambahkan baris" class="btn btn-primary btn-xs menutools add-new-col" id="addcolrow-'+block_name+'">'+
				'			<span class="glyphicon glyphicon-plus pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Tambah Kolom</span>'+
				'		</a>'+
				'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-primary btn-xs menutools copy-row" id="copyrow-'+block_name+'">'+
				'			<span class="glyphicon glyphicon-duplicate pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Duplikat</span>'+
				'		</a>'+
				'		<a href="javascript:void(0);" title="Klik disini untuk konfigurasi Blok Konten" class="btn btn-primary btn-xs menutools setting-row" id="showrow-'+block_name+'">'+
				'			<span class="glyphicon fa-desktop pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Setting</span>'+
				'		</a>'+
				//(($(v).hasClass("row-fixed"))?'':
				'		<a href="javascript:void(0);" title="Klik disini untuk menghapus Blok Konten" class="btn btn-danger btn-xs menutools remove-row" id="removerow-'+block_name+'">'+
				'			<span class="glyphicon glyphicon-trash pull-left"></span> <span class="visible-lg visible-md pull-right">&nbsp;Hapus</span>'+
				'		</a>'
				//)
				+
				'	</div>'+
				'</div>')
				.on("mouseenter",function(){
				  $(this).children(".admin-configuration").show();
				})
				.on("mouseleave",function(){
				  $(this).children(".admin-configuration").hide();
				})
				.on("click",function(){
				});  
}

function create_editor_col(v,k)
{

	var block_name = $(v).attr("data-block-item-id");
	block_name = (typeof block_name != "undefined")?block_name:"id-xx-"+k;

	$(v)
	.addClass("editor-hovered")
	.append('<div class="admin-configuration-col" style="display:none;">'+
			'	<div class="admin-container-conf">'+
			'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-primary btn-xs menutools move-col" id="movecol-'+block_name+'">'+
			'			<span class="glyphicon glyphicon-move pull-left"></span> <span class="hidden pull-right">&nbsp;Pindahkan</span>'+
			'		</a>'+
			'		<a href="javascript:void(0);" title="Geser disini untuk mengatur posisi Blok Konten" class="btn btn-primary btn-xs menutools copy-col" id="copycol-'+block_name+'">'+
			'			<span class="glyphicon glyphicon-duplicate pull-left"></span> <span class="hidden pull-right">&nbsp;Duplikat</span>'+
			'		</a>'+
			'		<a href="javascript:void(0);" title="Klik disini untuk konfigurasi Widget" class="btn btn-primary btn-xs menutools setting-col" id="showcol-'+block_name+'">'+
			'			<span class="glyphicon glyphicon-cog"></span>  <span class="hidden pull-right">&nbsp;Setting Widget</span>'+
			'		</a>'+
			'		<a href="javascript:void(0);" title="Klik disini untuk menghapus Blok Konten" class="btn btn-danger btn-xs menutools remove-col" id="removecol-'+block_name+'">'+
			'			<span class="glyphicon glyphicon-trash pull-left"></span> <span class="hidden pull-right">&nbsp;Hapus</span>'+
			'		</a>'+
			'	</div>'+
			'</div>')
	.on("mouseenter",function(){
	  $(this).children(".admin-configuration-col").show();
	})
	.on("mouseleave",function(){
	  $(this).children(".admin-configuration-col").hide();
	});  
}

function setup_tinymce(selector,options)
{
	selector = (typeof selector != "undefined")?selector:".inline_editable";
	var configs = $.extend({
							  theme: "modern",
							  plugins: [
								  "advlist autolink lists link image charmap print preview hr anchor pagebreak",
								  "searchreplace wordcount visualblocks visualchars code fullscreen",
								  "insertdatetime media nonbreaking save table contextmenu directionality",
								  "emoticons template paste textcolor template responsivefilemanager image"
							  ],
							  toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
							  toolbar2: "print preview responsivefilemanager image media | forecolor backcolor emoticons | template",
							  
							relative_urls: false,
							convert_urls : true,
							remove_script_host: false,
							//document_base_url: base_url+"uploads/",

							filemanager_title:"Responsive Filemanager",
							filemanager_crossdomain: true,
							external_filemanager_path:base_url+"uploads/filemanager/",
							external_plugins: { "filemanager" : base_url+"uploads/filemanager/plugin.min.js"},

							image_advtab: true,
							  templates: [
								  {title: 'Test template 1', content: 'Test 1'},
								  {title: 'Test template 2', content: 'Test 2'}
							  ]
							}, options );
	$(selector).tinymce(configs);
}

function setup_bs_wysihtml5(selector)
{
	selector = (typeof selector != "undefined")?selector:".inline_editable";
	var script_wysihtml5 = '<link rel="stylesheet" href="'+current_theme_url+'assets/js/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css"/>';
	script_wysihtml5 = script_wysihtml5+'<script type="text/javascript" src="'+current_theme_url+'assets/js/plugins/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>';
	script_wysihtml5 = script_wysihtml5+'<script type="text/javascript" src="'+current_theme_url+'assets/js/plugins/bootstrap-wysihtml5/src/bootstrap3-wysihtml5.js"></script>';
	$("body").append(script_wysihtml5);
	$(selector).wysihtml5();
}

function create_panel(config)
{
	var setting = {
					modal_type: "modal-sm",
					class:"col-sm-12 col-md-12",
					id:"theme-panel",
					body:"",
					footer: '<button type="button" class="btn btn-default modal-close" data-dismiss="modal">Close</button>'+
							'<button type="button" class="btn btn-primary modal-save">Save changes</button>',
					label:"Setting Widget"
				  };
	var config = (!config)?settng:$.extend(setting,config);

	var panel = '<div class="modal fade '+config.class+'" id="' + config.id + '" tabindex="-1" role="modal" aria-labelledby="' + config.id + 'Label" aria-hidden="true">'+
				'  <div class="modal-dialog ' + config.modal_type + '">'+
				'    <!--<div class="modal-content panel panel-default">-->'+
				'      <!--<div class="modal-header draggable-handle panel-heading">'+
				'        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
				'        <h4 class="modal-title" id="' + config.id + 'Label">' + config.label + '</h4>'+
				'      </div>-->'+
				'      <div class="modal-body">'+
						  config.body +
				'      </div>'+
				'      <!-- <div class="modal-footer">'+
						 config.footer +
				'      </div>'+
				'    </div> -->'+
				'  </div>'+
				'</div>';
				
	$("body").append(panel);
}

function helper_class_text(element)
{
	var txtclass = $(element).attr("class");
	var style = $(element).attr("style");
	txtclass = txtclass.replace("editor-hovered","");
	txtclass = txtclass.replace("editor-resizable","");
	txtclass = txtclass.replace("ui-resizable","");
	txtclass = txtclass.replace("  "," ");
	var output = txtclass;//+"<br/>"+style;
	return output; 
}

function float2int(value) {
	return value | 0;
}

function init_draggable()
{
	$( ".block-draggable,[class*='col-']" ).each(function(i,k){
		$(k).append("<a href=\"javascript:void(0);\" title=\"Move this block\" class=\"sortable-handle glyphicon glyphicon-move\"></a>");
		var connectwith = $(k).attr("connect-with");
		connectwith = (!connectwith)?$(k).parent():connectwith;
		var orginalcolorgroup,orginalcolorsingle;
		if($(connectwith).hasClass("ui-sortable"))
		{}else{
			$(connectwith).each(function(i2,k2){
				$(k2).sortable({
					scroll: true,
					//connectWith: connectwith,
					revert: true,
					forcePlaceholderSize: false,
					handle: ".sortable-handle",
					tolerance: "pointer",
					start: function( event, ui ) {
						var height = $(".ui-sortable-placeholder").css('height');
						height = height.replace("px","");
						height = parseInt(height);
						height = height - 2;
						$(".ui-sortable-placeholder").css('height',height);
						$(".ui-sortable-placeholder").css('margin',"0");
						orginalcolorgroup = $(ui.item).css('background-color');
						orginalcolorsingle = $(k).css('background-color');
						$(ui.item).css("position","relative");
						$(k2).animate({
						  backgroundColor: "#ff9"
						});
						$(k).animate({
						  backgroundColor: "#ff9"
						});
					},
					stop: function( event, ui ) {
						$(k2).animate({
						  backgroundColor: orginalcolorgroup
						});
						$(k).animate({
						  backgroundColor: orginalcolorsingle
						});
					}
				});
			});
		}
		$(k).draggable({
		  connectToSortable: connectwith,
		  //helper: "clone",
		  revert: true
		});
	});
}

//init_draggable();

function class_switcher(prefix_class,parent_width,item_width,item)
{
  var gridrange = parent_width/12;
  var percent_item = item_width/gridrange;
  var col_number = [1,2,3,4,5,6,7,8,9,10,11,12];
  var removeClasses = "";
  var setnumberclass = Math.round(percent_item);
  
  setnumberclass = (setnumberclass >= 4 && setnumberclass < 6)?4:setnumberclass;
  setnumberclass = (setnumberclass >= 6 && setnumberclass < 8)?6:setnumberclass;
  setnumberclass = (setnumberclass >= 9 && setnumberclass < 12)?9:setnumberclass;
  setnumberclass = (setnumberclass >= 12)?12:setnumberclass;
  
  var addClass = prefix_class + setnumberclass;
  $(col_number).each(function(i,k){
	  if(k != setnumberclass)
	  {
		removeClasses = removeClasses + " " + prefix_class + k;
	  }
  });
  //alert(parent_width/item_width);
  //alert(removeClasses);
  //alert(addClass);
  
  $(item).switchClass(removeClasses,addClass,100);
  if($(item).hasClass(addClass))
  {
  }else{
	  $(item).addClass(addClass)
  }
}

function init_resizable(selector)
{
	var maxwidth = $(window).width();
	selector = (!selector)?".resizable,[class*='col-']":selector;

	$(selector).each(function(i,k)
	{
		var maxwidth = $(k).parent().width();
		var gridrange = float2int(maxwidth/12);
		//alert(gridrange + " " + maxwidth);
		$(k).css("min-height",30).addClass("editor-resizable");
		$(k).resizable({
			//containment: "parent",
			//minHeight: 50,
			minWidth: gridrange,
			maxWidth: maxwidth,
			grid: [gridrange,1],
			ghost: true,
			helper: "ui-resizable-helper",
			handles : "e",
			start: function( event, ui )
			{
				$(ui.element).css("position","relative");
				$(ui.element).css("top","auto");
				$(ui.element).css("left","auto");
			},
			stop: function( event, ui )
			{
				//$(ui.element).css("height",ui.originalSize.height);				
				class_switcher("col-lg-",maxwidth,ui.size.width,ui.element);
				$(ui.element).css("position","");
				$(ui.element).css("top","");
				$(ui.element).css("left","");
				$(ui.element).css("width","");
				$(ui.element).css("height","");
				var all_class = helper_class_text(ui.element);
				var block_col_id = $(ui.element).attr("data-block-item-id");
				var content = '<div class="'+all_class+'" data-block-item-id="'+block_col_id+'"><bscol id="'+block_col_id+'"></bscol></div>';
				
							
				
				var param_data = "block_col_id=" + block_col_id;
				$( document ).ajaxSend(function( event, jqxhr, settings ) {
				  if(typeof settings.data == "string")
				  {
					var data_tmp = settings.data;
					var data_output = "";
					data_tmp = data_tmp.split("&");
					$.each(data_tmp,function(k,v){
						var t_v = v.split("=");
						if(t_v[0] == "theme_id" || t_v[0] == "template_id" || t_v[0] == "con_id" || t_v[0] == "block_col_id" || t_v[0] == ""){}else{
							data_output = data_output + "&" + v;
						}
					});
					settings.data = data_output + "&" + param_data;
				  }else{
					//settings.data = $.extend(param_data_obj,settings.data);
				  }
				});
				
				$.ajax({
					data: {"do_action":"do_save_content","page_id":page_id,"post_content" : content,"block_col_id" : block_col_id},
					url: base_url + "admin/adminblocks/update_block_col_content/"+block_col_id,
					type: "post",
					dataType: "json",
					success: function(result)
					{
						var message = (!result.message) ? "" : result.message;
						var type = (!result.status) ? "" : result.status;
						show_alert(type, message)
					}
				});
				//init_draggable();
			}
		});
	});
}


function show_alert(type,message)
{
	var type = (!type)?'warning':type;
	var message = (!message)?'':message;
	var output = '	<div class="alert alert-'+type+' alert-dismissible admin-notification" role="alert">'+
			  '	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
			  '	  '+message+
			  '	</div>';
	setTimeout(function(){
							$("body").find(".admin-notification").eq(0).hide("slow").remove();
							},5000);
	$("body").append(output);
}
