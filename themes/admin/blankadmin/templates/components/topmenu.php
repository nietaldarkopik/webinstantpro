<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
$menu = $this->load->view("components/menu",array(),true);
echo $menu;
/*
<ul class="nav navbar-nav">
	<li>
		<a href="<?php echo base_url();?>">
			<span class="glyphicon glyphicon-home"></span>
		</a>
	</li>
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-globe"></span> Site Management
		</a>
		<ul class="dropdown-menu">
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/appearence_menus/index"><span class="glyphicon glyphicon-menu-hamburger"></span> Menu Management</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/appearence_pages/index"><span class="glyphicon glyphicon-file"></span> Page Management</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/appearence_plugins/index"><span class="glyphicon glyphicon-log-in"></span> Plugins</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/appearence_themes/index"><span class="glyphicon glyphicon-sunglasses"></span> Themes</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-user"></span> Administration
		</a>
			<ul class="dropdown-menu">
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/administration_user_levels/index"><span class="glyphicon glyphicon-king"></span> User Levels</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/administration_users/index"><span class="glyphicon glyphicon-pawn"></span> User Accounts</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/administration_user_roles/index"><span class="glyphicon glyphicon-knight"></span> User Roles</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/administration_my_profile/index"><span class="glyphicon glyphicon-tower"></span> My Profile</a></li>
			</ul>
	</li>
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-cog"></span> Configuration
		</a>
		<ul class="dropdown-menu">
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/setting_general/index"><span class="glyphicon glyphicon-cog"></span> Site Configuration</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/setting_social_media/index"><span class="glyphicon glyphicon-thumbs-up"></span> Sosial Media</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/setting_social_media/index"><span class="glyphicon glyphicon-thumbs-up"></span> Google Analitics</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" href="<?php echo base_url();?>admin/setting_smtp/index"><span class="glyphicon glyphicon-envelope"></span> SMTP</a></li>
		</ul>
	</li>
	<li>
		<a href="<?php echo base_url();?>admin/dashboard/do_logout">
			<span class="glyphicon glyphicon-log-out"></span> Logout
		</a>
	</li>
</ul>
*/
?>