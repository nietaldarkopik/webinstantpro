<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	#echo $this->user_access->get_menus_allowed_structured();
		
	$parent_menu_allowed = $this->user_access->get_menus_allowed($user_id," AND user_custom_menus.parent_menu = 0 AND user_custom_menus.status = 'active'");
	
	$the_top_menu = "";
	// Get menu Level 0
	if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
	{
		$the_top_menu .= '<ul class="nav navbar-nav">';
		foreach($parent_menu_allowed as $index => $mn)
		{				
			$sub_menu_allowed2 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn['user_menu_id'] . "' AND user_custom_menus.status = 'active'");

			$url = base_url($mn['path'] . $mn['controller'] . '/' . $mn['function']);
			if($mn['type'] == 'plugin')
			{
				$url = base_url($mn['url']);
			}
			
			$is_submenu_exist1 = ((is_array($sub_menu_allowed2) and count($sub_menu_allowed2) > 0))?' class="dropdown-toggle" data-toggle="dropdown" ':'';
			$is_submenu_exist1_li = ((is_array($sub_menu_allowed2) and count($sub_menu_allowed2) > 0))?' class="dropdown" ':'';
			$the_top_menu .= '<li'.$is_submenu_exist1_li.'><a href="'. $url . '" '.$mn['attributes'].' '.$is_submenu_exist1.'>'.$mn['menu_title'].'</a>';			
			
			// Get menu Level 1
			if(is_array($sub_menu_allowed2) and count($sub_menu_allowed2) > 0)
			{
				$the_top_menu .= '<ul class="dropdown-menu">';
				foreach($sub_menu_allowed2 as $index2 => $mn2)
				{
					$sub_menu_allowed3 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn2['user_menu_id'] . "' AND user_custom_menus.status = 'active'");
							
					$url = base_url($mn2['path'] . $mn2['controller'] . '/' . $mn2['function']);
					if($mn2['type'] == 'plugin')
					{
						$url = base_url($mn2['url']);
					}
					
					$is_submenu_exist2 = "";
					if((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0))
					{
						$mn2['attributes'] = str_replace('is_modal"','dropdown-toggle is_modal"',$mn2['attributes']);
						$is_submenu_exist2 = ' data-toggle="dropdown"';
					}
					
					$is_submenu_exist2_li = ((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0))?' class="dropdown-submenu" ':'';
					$the_top_menu .= '<li'.$is_submenu_exist2_li.'><a href="'. $url . '" '.$mn2['attributes'].' '.$is_submenu_exist2.'>'.$mn2['menu_title'].'</a>';	
					
					if(is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)
					{
						$the_top_menu .= '<ul class="dropdown-menu">';
						foreach($sub_menu_allowed3 as $index3 => $mn3)
						{
							$url = base_url($mn3['path'] . $mn3['controller'] . '/' . $mn3['function']);
							if($mn3['type'] == 'plugin')
							{
								$url = base_url($mn3['url']);
							}
							$the_top_menu .= '<li><a href="' . $url . '" '.$mn3['attributes'].'>'.$mn3['menu_title'].'</a></li>';	
						}
						$the_top_menu .= '</ul>';
					}
					
					$the_top_menu .= '</li>';
				}
				$the_top_menu .= '</ul>';
			}
			$the_top_menu .= '</li>';
		}
		$the_top_menu .= '</ul>';
	}
	
	echo $the_top_menu;
