<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>

  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
						<div class="pull-right">
						  <?php
							echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
						  ?>
						</div>
                    </div>
                  </div>
                  <div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<?php
						  $this->data->create_listing($this->init);
						  $data_rows = $this->data->data_rows;
						  if(is_array($data_rows) and count($data_rows) > 0)
						  {
							  echo '
								  <ul class="list-group">
									<li class="list-group-item">
										<div class="row">
											<div class="col-lg-1 col-md-1 col-sm-1 text-center"><strong>Posisi</strong></div>
											<div class="col-lg-7 col-md-7 col-sm-11 text-center"><strong>Nama Baris</strong></div>
											<div class="col-lg-2 col-md-2 col-sm-12 text-right"><strong>Aksi</strong></div>
										</div>
									</li>
								  </ul>';
							  echo "<ul class='sortable list-group'>";
							  foreach($data_rows as $i => $data_row)
							  {
								  echo '<li class="list-group-item">
											<div class="row">
												<div class="col-lg-1 col-md-1 col-sm-1"><i class="glyphicon glyphicon-move icon-move pull-left"></i></div>
												<div class="col-lg-7 col-md-7 col-sm-11">'.$data_row['block_title'].'</div>
												<div class="col-lg-2 col-md-2 col-sm-12">
													<div class="btn-group pull-right">
														<button type="button" class="btn btn-primary btn-sm">Choose Action</button>
														<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
														  <span class="caret"></span>
														  <span class="sr-only">Toggle Dropdown</span>
														</button>
														<ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
															<li role="presentation" class=""><a role="menuitem" tabindex="-1" href="http://localhost/wipro/admin/appearence_plugins/edit/12" class="glyphicon-share icon glyphicon ajax is_modal" data-target-ajax=".ajax_container"> Edit</a></li><li role="presentation" class=""><a role="menuitem" tabindex="-1" href="http://localhost/wipro/admin/appearence_plugins/view/12" class="glyphicon-share icon glyphicon ajax is_modal" data-target-ajax=".ajax_container"> View</a></li><li role="presentation" class=""><a role="menuitem" tabindex="-1" href="http://localhost/wipro/admin/appearence_plugins/delete/12" class="glyphicon-cog icon glyphicon ajax is_modal" data-target-ajax=".ajax_container"> Delete</a></li>
														</ul>
													</div>
												</div>
											</div>
										</li>';
							  }
							  echo "</ul>";
						  }
						?>
						<script type="text/javascript">
						  jQuery(document).ready(function(){
							jQuery("ul.sortable").sortable({handle: '.icon-move'});//("do_sortable");
						  });
						</script>
					</div>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
						<div class="pull-right">
						  <?php
							echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
						  ?>
						</div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

