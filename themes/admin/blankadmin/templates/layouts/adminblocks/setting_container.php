<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
	$is_ajax = $this->input->post('is_ajax');
	$is_modal = $this->input->post('is_modal');
	$pagination_data_target = "#data_target-".$is_ajax;
	$container_id = (isset($data_setting['container_id']))?$data_setting['container_id']:0;
?>

  <div class="content-container col-sm-12 col-md-12 no-padding nomargin">
    <div class="panel panel-default">
      <div class="panel-heading clearfix bg-blue">
        <h3 class="panel-title pull-left padding-top-5"><?php echo $page_title;?></h3>
        <a class="glyphicon fa-close pull-right text-white hidden" href="javascript:void(0);"></a>
        <a class="glyphicon fa-arrow-circle-down pull-right text-white hidden" href="javascript:void(0);"></a>
      </div>
      <div class="panel-body no-padding" id="setting_container_form">
			<form class="form-horizontal ajax" enctype="multipart/form-data" method="post" data-target="#setting_container_form" data-target-ajax="#setting_container_form" action="<?php echo base_url("admin/adminblocks/setting_container/".$container_id);?>">
				<div id="accordion">
				  <h3>Properties</h3>
				  <div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="exampleInputEmail3">Container Name</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="properties_container_name" placeholder="Container Name" name="properties[container_name]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="exampleInputEmail3">Container ID</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" value="container-<?php echo $container_id;?>" class="form-control" readonly="readonly" id="properties_container_id" placeholder="Container ID" name="properties[container_id]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="exampleInputEmail3">Class</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="properties_container_class" placeholder="Custom Class" name="properties[container_class]">
							</div>
						</div>
					</div>
				  </div>
				  <h3>Text</h3>
				  <div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_font_family">Font Family</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="text_font_family" placeholder="Font Family" name="text[font_family]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_font_size">Font Size</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="text_font_size" placeholder="Font Size" name="text[font_size]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_font_size">Font Style</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="text_font_style" name="text[font_style]">
									<option value="inherit">inherit</option>
									<option value="initial">initial</option>
									<option value="normal">normal</option>
									<option value="italic">italic</option>
									<option value="oblique">oblique</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_font_weight">Font Weight</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="text_font_weight" name="text[font_weight]">
									<option value="normal">normal</option>
									<option value="bold">bold</option>
									<option value="bolder">bolder</option>
									<option value="lighter">lighter</option>
									<option value="initial">initial</option>
									<option value="inherit">inherit</option>
									<option value="100">100</option>
									<option value="200">200</option>
									<option value="300">300</option>
									<option value="400">400</option>
									<option value="500">500</option>
									<option value="600">600</option>
									<option value="700">700</option>
									<option value="800">800</option>
									<option value="900">900</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_text_color">Text Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="colorpicker form-control" id="text_text_color" placeholder="Text Color" name="text[text_color]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_text_alignment">Text Alignment</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="text_text_alignment" name="text[text_alignment]">
									<option value="left">left</option>
									<option value="center">center</option>
									<option value="right">right</option>
									<option value="justify">justify</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_text_decoration">Text Decoration</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="text_text_decoration" name="text[text_decoration]">
									<option value="none">none</option>
									<option value="overline">overline</option>
									<option value="line-through">line-through</option>
									<option value="underline">underline</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="text_text_transformation">Text Transformation</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="text_text_transformation" name="text[text_transformation]">
									<option value="none">none</option>
									<option value="uppercase">uppercase</option>
									<option value="lowercase">lowercase</option>
									<option value="capitalize">capitalize</option>
								</select>
							</div>
						</div>
					</div>
				  </div>
				  <h3>Block</h3>
				  <div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_background_color">Background Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="colorpicker form-control" id="block_background_color" placeholder="Background Color" name="block[background_color]">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_background_image">Background Image</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="file" class="form-control" id="block_background_image" placeholder="Background Image" name="block[background_image]">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_border_size">Border Width</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="slider form-control" id="block_border_width" placeholder="Border Width" name="block[border_width]"  data-slider-min="0" data-slider-max="50" data-slider-step="1" data-slider-value="0" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_border_style">Border Style</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<select class="form-control" id="block_border_style" name="block[border_style]">
									<option value="none">none</option>
									<option value="dotted">dotted</option>
									<option value="dashed">dashed</option>
									<option value="solid">solid</option>
									<option value="double">double</option>
									<option value="groove">groove</option>
									<option value="ridge">ridge</option>
									<option value="inset">inset</option>
									<option value="outset">outset</option>
								</select>
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_border_color">Border Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="colorpicker form-control" id="block_border_color" placeholder="Border Color" name="block[border_color]">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="exampleInputEmail3">Padding</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_left" name="block[margin][left]" placeholder="Left">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_top" name="block[margin][top]" placeholder="Top">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_right" name="block[margin][right]" placeholder="Right">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_bottom" name="block[margin][bottom]" placeholder="Bottom">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_margin_left">Margin</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_left" name="block[margin][left]" placeholder="Left">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_top" name="block[margin][top]" placeholder="Top">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_right" name="block[margin][right]" placeholder="Right">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-6 col-xs-6" id="block_margin_bottom" name="block[margin][bottom]" placeholder="Bottom">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="block_height">Height</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="slider form-control col-lg-12 col-md-12" id="block_height" placeholder="Height"  data-slider-min="0" data-slider-max="1000" data-slider-step="5" data-slider-value="0" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<div class="margin20">
					<input type="hidden" name="is_ajax" value="1"/>
					<input type="hidden" name="container_id" value="<?php echo $container_id;?>"/>
					<input type="hidden" name="ajax_target" value="#setting_container_form">
					<button type="submit" class="btn btn-sm bg-primary btn-block" ><span class="fa fa-save text-white"></span>Save</button>
				</div>
			</form>
		  <script>
		  $(function() {
			$( "#accordion" ).accordion({
			  collapsible: true,
				heightStyle: "content"
			});
			$("#block_height").bootstrapSlider({
				tooltip: 'always',
				step: 5,
				formatter: function(val) {
					return val + " px";
				},
				tooltip_position: 'bottom'
			});
			$("#block_border_width").bootstrapSlider({
				tooltip: 'always',
				step: 1,
				formatter: function(val) {
					return val + " px";
				},
				tooltip_position: 'bottom'
			});
		  });
		  </script>
      </div>
    </div>
  </div>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

