<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
	$is_ajax = $this->input->post('is_ajax');
	$is_modal = $this->input->post('is_modal');
	$pagination_data_target = "#data_target-".$is_ajax;

?>

	<div class="ajax_container content-container col-sm-12 col-md-12 no-padding nomargin">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<h3 class="panel-title pull-left"><?php echo $page_title;?></h3>
				<a class="glyphicon fa-close pull-right hidden" href="javascript:void(0);"></a>
				<a class="glyphicon fa-arrow-circle-down pull-right hidden" href="javascript:void(0);"></a>
			</div>
			<div class="panel-body no-padding">
			<form class="form-horizontal" action="<?php echo base_url("admin/adminblocks/");?>">
				<div id="accordion">
					<h3>Properties</h3>
					<div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Container Name">Container Name</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Container Name" placeholder="Container Name">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="ID">ID</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="ID" placeholder="ID">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Class">Class</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Class" placeholder="Class">
							</div>
						</div>
					</div>
					</div>
					<h3>Text</h3>
					<div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Font Family">Font Family</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Font Family" placeholder="Font Family">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Font Size">Font Size</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Font Size" placeholder="Font Size">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Font Style">Font Style</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Font Style" placeholder="Font Style">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Font Weight">Font Weight</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Font Weight" placeholder="Font Weight">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Text Color">Text Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="color" class="form-control" id="Email" placeholder="Email">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Text Alignment">Text Alignment</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Text Alignment" placeholder="Text Alignment">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Text Decoration">Text Decoration</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Text Decoration" placeholder="Text Decoration">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Text Transformation">Text Transformation</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Text Transformation" placeholder="Text Transformation">
							</div>
						</div>
					</div>
					</div>
					<h3>Block</h3>
					<div>
					<div class="row">
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Background Color">Background Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Background Color" placeholder="Background Color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Background Image">Background Image</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Background Image" placeholder="Background Image">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Border Size">Border Size</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Border Size" placeholder="Border Size">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Border Style">Border Style</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Border Style" placeholder="Border Style">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Border Color">Border Color</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Border Color" placeholder="Border Color">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Padding">Padding</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Left" placeholder="Left">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Top" placeholder="Top">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Right" placeholder="Right">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Bottom" placeholder="Bottom">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Margin">Margin</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Left" placeholder="Left">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Top" placeholder="Top">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Right" placeholder="Right">
								<input type="text" class="form-control-static col-lg-3 col-md-4 col-sm-4 col-xs-4" id="Bottom" placeholder="Bottom">
							</div>
						</div>
						<div class="form-group form-group-sm">
							<label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-static" for="Height">Height</label>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<input type="text" class="form-control" id="Bottom" placeholder="Bottom">
							</div>
						</div>
					</div>
					</div>
				</div>
			</form>
			<script>
			$(function() {
			$( "#accordion" ).accordion({
				collapsible: true,
				heightStyle: "content"
			});
			});
			</script>
			</div>
		</div>
	</div>

<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

