<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


				<div class="pad_left1"><h2 class="pad_bot1"><?php echo $page_title;?></h2></div>
				<?php
					echo $this->data->show_panel_allowed("","",array("listing"));
				?>
					<br class="fclear"/><br/>
				<?php
					echo $this->data->create_form($this->init);
					echo $response;
				?>

<script type="text/javascript">
	jQuery("select#controller").on("change",function(){
		jQuery.ajax({
			type: "post",
			url: base_url + "menu/listing",
			data: "get_functions=" + jQuery("select#controller").val(),
			success:function(msg){
				jQuery("select#function").html(msg);
			}
		});
	});
	jQuery(document).ready(function(){
	});
</script>
