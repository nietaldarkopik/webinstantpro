<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16 hidden-print">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
              </ul>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <?php
                      echo $this->data->create_form_filter((isset($config_form_filter) and is_array($config_form_filter) and count($config_form_filter) > 0)?$config_form_filter:$this->init);
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8 hidden-print">
                      <?php
						
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
						<div class="pull-right">
						  <?php
							echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
						  ?>
						</div>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      echo $this->data->create_listing($this->init);
                    ?>
                  </div>
                  <div class="row hidden-print">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4 hidden-print">
						<div class="pull-right">
						  <?php
							echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array()));
						  ?>
						</div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form((isset($config_form_add) and is_array($config_form_add) and count($config_form_add) > 0)?$config_form_add:$this->init);
                  ?>
					  
				  <script type="text/javascript">
					$(document).ready(function(){
						function check_page_name(page_name)
						{
							$.ajax({
								url: base_url + "admin/appearence_pages/check_page_name",
								type: "post",
								data: "page_name=" + page_name,
								success: function(msg){
									$("#form #page_name").val(msg);
								}
							});
						}
						$("#form #page_title").focusout(function(){
							var page_title = $(this).val();
							var page_name = page_title.replace(/ /g,'-');
							page_name = page_name.replace(/[^a-z0-9\-]/gi,'');
							page_name = page_name.toLowerCase();
							$("#form #page_name").val(page_name);
							check_page_name(page_name);
						});
						
						$("#form #page_name").change(function(){
							var page_name = $(this).val();
							check_page_name(page_name);
						});
					});
				  </script>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

