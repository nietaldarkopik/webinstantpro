<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
		  <div class="row data_target<?php echo $is_modal;?>">
			<div class="col-lg-12">
			  <ul class="nav nav-tabs margintop-1 marginleft-16">
			  <?php
				echo $this->data->show_panel_allowed("","admin","",array(array('title' => 'Listing','name' => 'listing', 'class' => 'glyphicon-list')),"",false);
			  ?>
			  </ul>
			  <br class="fclear"/><br/>
			  <?php 
				echo $response;
			  ?>
			  <?php
				echo $this->data->create_form($this->init);
			  ?>
				  <script type="text/javascript">
					$(document).ready(function(){
						var page_id = "<?php echo (isset($this->data->data_row['page_id']))?$this->data->data_row['page_id']:0;?>
						function check_page_name(page_name)
						{
							$.ajax({
								url: base_url + "admin/appearence_pages/check_page_name",
								type: "post",
								data: "page_name=" + page_name + "&page_id=" + page_id,
								success: function(msg){
									$("#form #page_name").val(msg);
								}
							});
						}
						$("#form #page_title").focusout(function(){
							var page_title = $(this).val();
							var page_name = page_title.replace(/ /g,'-');
							page_name = page_name.replace(/[^a-z0-9\-]/gi,'');
							page_name = page_name.toLowerCase();
							$("#form #page_name").val(page_name);
							check_page_name(page_name);
						});
						
						$("#form #page_name").change(function(){
							var page_name = $(this).val();
							check_page_name(page_name);
						});
					});
				  </script>
			</div>
		  </div>
      </div>
	</div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>
