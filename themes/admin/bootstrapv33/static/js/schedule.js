				function generate_input()
				{
					var days = jQuery(".days");
					var hours = jQuery(".hours");
					var instructure_rows = jQuery(".instructure_rows");
					
					jQuery(hours).each(function(k,v){
						var no_hours = k;
						jQuery(v).find(".hour_from").attr("name","hour_from["+no_hours+"]");
						jQuery(v).find(".hour_to").attr("name","hour_to["+no_hours+"]");
						jQuery(days).each(function(kday,vday){
							jQuery(vday).find(".event_colomn").eq(no_hours).find("select").attr("name","material["+kday+"]["+no_hours+"]");
							jQuery(vday).find(".event_colomn").eq(no_hours).find("textarea").attr("name","textarea_acara["+kday+"]["+no_hours+"]");
						});
						jQuery(instructure_rows).each(function(kins,vins){
							jQuery(vins).find(".instructure_colomn").eq(no_hours).find(".instructure_name").attr("name","instructure_name["+kins+"]["+no_hours+"]");
							jQuery(vins).find(".instructure_colomn").eq(no_hours).find(".instructure_id").attr("name","instructure_id["+kins+"]["+no_hours+"]");
						});
					});
				}
				
				generate_input();
				jQuery(document).on("click",".add_colomn_schedule",function(){
					var table = jQuery(this).parents('table');
					var no_col = jQuery(table).find("tr").eq(2).find("th").index(jQuery(this).parents("th"));
					var curr_th = jQuery(this).parents("th");
					var no_col = curr_th.index(".table_report tr:eq(2) th");
					
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var td = jQuery(v).find("td");
						var td_curr = jQuery(v).find("td").eq(jQuery(v).find("td").length-2);
						var colspan = jQuery(td).attr("colspan");
						if(!colspan)
						{
							if(k >= 4)
							{
								jQuery(td_curr).after(jQuery(v).find("td").eq(jQuery(v).find("td").length-2).clone());
							}
						}else{
							colspan = parseInt(colspan);
							colspan = colspan + 1;
							jQuery(td).attr("colspan",colspan);
						}
					});
					
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var th = jQuery(v).find("th");
						var th_curr = jQuery(v).find("th").eq(no_col);
						var colspan = jQuery(th).attr("colspan");
						if(!colspan){}else{
							colspan = parseInt(colspan);
							colspan = colspan + 1;
							jQuery(th).attr("colspan",colspan);
						}
						var new_col = 	jQuery(v).find("th").eq(no_col-1).clone();
						if(jQuery(new_col).find("a.remove_colomn_schedule").length == 0 && jQuery(new_col).find("input").length == 0)
						{
							new_col = jQuery(new_col).prepend('<a href="javascript:void(0)" class="icon delete remove_colomn_schedule">remove</a><br/>');
						}
						jQuery(th_curr).before(new_col);
						
						var curr_th_hours = jQuery(v).find("th.hours:last");
						jQuery(curr_th_hours).before(jQuery(curr_th_hours).clone());
					});
					var curr_scroll = jQuery(".container_schedule").scrollLeft();
					jQuery(".container_schedule").scrollLeft(800+curr_scroll);
					generate_input();
				});
				
				jQuery(document).on("click","a.remove_colomn_schedule",function(){
					var table = jQuery(this).parents('table');
					var no_col = jQuery(table).find("tr").eq(2).find("th").index(jQuery(this).parents("th"));
					var curr_th = jQuery(this).parents("th");
					var no_col = curr_th.index(".table_report tr:eq(2) th");
					jQuery.each(jQuery(table).find("tr"),function(k,v){
						var th = jQuery(v).find("th");
						jQuery(th).each(function(k2,v2){
							var th_curr = jQuery(v).find("th").eq(no_col);
							var colspan = jQuery(th).eq(k2).attr("colspan");
							if(!colspan){}else{
								colspan = parseInt(colspan);
								colspan = colspan - 1;
								jQuery(th).eq(k2).attr("colspan",colspan);
							}
							
							if(jQuery(th_curr).hasClass("hours") && no_col-1 == k2)
							{
								alert(jQuery(th_curr).html());
								jQuery(v).find("th").eq(no_col-1).remove();
							}
							
							if(!jQuery(th_curr).hasClass("hours") && no_col == k2)
							{
								jQuery(v).find("th").eq(no_col).remove();
							}
						});
						
						var is_days_rows = jQuery(v).hasClass("days");
						var td = jQuery(v).find("td");
						jQuery(td).each(function(k2,v2){
							var colspan = jQuery(td).eq(k2).attr("colspan");
							if(!colspan){}else{
								colspan = parseInt(colspan);
								colspan = colspan - 1;
								jQuery(td).eq(k2).attr("colspan",colspan);
							}
							if(is_days_rows)
							{
								if(no_col == k2)
								{
									jQuery(td).eq(k2).remove();
								}
							}else{								
								if(no_col == k2)
								{
									jQuery(td).eq(k2).remove();
								}
							}
						});
					});
					generate_input();
				});
				
				var no_rows_after = 0;
				jQuery(document).on("change","select.material",function(){
					var table = jQuery(this).parents('table');
					var no_row = jQuery(table).find("tr").index(jQuery(this).parents("tr"));
					var curr_th = jQuery(this).parents("td");
					var no_col = curr_th.index(".table_report tr:eq("+no_row+") td");
					
					var text = jQuery(this).find("option:selected").text();
					var value = jQuery(this).find("option:selected").attr("value");
					var instructure_id = jQuery(this).find("option:selected").attr("instructure_id");
					var instructure_name = jQuery(this).find("option:selected").attr("instructure_name");
					
					//jQuery(table).find("tr").eq(no_row).find("td").eq(no_col-1).find("input.instructure_id").val(instructure_id);
					//jQuery(table).find("tr").eq(no_row).find("td").eq(no_col-1).find("input.instructure_name").val(instructure_name);
					
					jQuery(table).find("tr").eq(no_row+1).find("td").eq(no_col-1).find("input.instructure_id").val(instructure_id);
					jQuery(table).find("tr").eq(no_row+1).find("td").eq(no_col-1).find("input.instructure_name").val(instructure_name);
					if(value == 'custom')
					{
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").remove();
						}
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").replaceWith('<textarea style="width:90%;font-size:10px;font-style:italic;" name="textarea_acara">Ketikan Acara disini</textarea>');
						}else{
							jQuery(this).parent().append('<textarea style="width:90%;font-size:10px;font-style:italic;">Ketikan Acara disini</textarea>');
						}
					}else if(value == '')
					{
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").remove();
						}
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").remove();
						}
					}else{
						if(jQuery(this).parent().find("textarea").length > 0)
						{
							jQuery(this).parent().find("textarea").remove();
						}
						if(jQuery(this).parent().find("div.text_materi").length > 0)
						{
							jQuery(this).parent().find("div.text_materi").replaceWith('<div class="text_materi">'+text+'</div>');
						}else{
							jQuery(this).parent().append('<div class="text_materi">'+text+'</div>');
						}
					}
					generate_input();
				});
