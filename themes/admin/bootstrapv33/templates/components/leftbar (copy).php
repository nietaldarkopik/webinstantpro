<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>				
          <div class="col-sm-4 col-md-3 sidebar-container hidden-print">
					
						<form action="#" method="get" class="form form-search clearfix">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" />
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                </span>
              </div><!-- /input-group -->
            </form>
                    
            <div class="panel-group list-group control-panel marginbottom4px">
              <a href="#" class="list-group-item"><i class="glyphicon glyphicon-home">&nbsp;</i>Home</a>
            </div><!-- .panel-user -->
            
            <div class="panel-group marginbottom4px" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#member-management"><i class="glyphicon glyphicon-user">&nbsp;</i>Member Management</a>
                  </h4>
                </div>
                <div id="member-management" class="panel-collapse collapse">
                  <div class="panel-body padding0">
                    <div class="list-group control-panel marginbottom4px">
                      <a href="<?php echo base_url('admin/members');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>List Members</a>
                      <a href="<?php echo base_url('admin/green_wallet_members');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>Green Wallet Members</a>
                    </div><!-- .panel-user -->
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#certificate-management"><i class="glyphicon glyphicon-bookmark">&nbsp;</i>Certificates Management</a>
                  </h4>
                </div>
                <div id="certificate-management" class="panel-collapse collapse">
                  <div class="panel-body padding0">
                    <div class="list-group control-panel marginbottom4px">
                      <a href="<?php echo base_url('admin/certificate_members');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>Certificates Members</a>
                      <a href="<?php echo base_url('admin/certificates');?>" class="list-group-item active"><i class="glyphicon glyphicon-user">&nbsp;</i>List Certificates</a>
                    </div><!-- .panel-user -->
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#transaction"><i class="glyphicon glyphicon-random">&nbsp;</i>Transaction</a>
                  </h4>
                </div>
                <div id="transaction" class="panel-collapse collapse">
                  <div class="panel-body padding0">
                    <div class="list-group control-panel marginbottom4px">
                      <a href="<?php echo base_url('admin/bidding');?>" class="list-group-item"><i class="glyphicon glyphicon glyphicon-screenshot">&nbsp;</i>Data Bidding</a>
                      <a href="<?php echo base_url('admin/selling');?>" class="list-group-item"><i class="glyphicon glyphicon-shopping-cart">&nbsp;</i>Data Selling</a>
                      <a href="<?php echo base_url('admin/transaction_approval');?>" class="list-group-item"><i class="glyphicon glyphicon-ok">&nbsp;</i>Transaction Approval</a>
                      <a href="<?php echo base_url('admin/history_bidding');?>" class="list-group-item"><i class="glyphicon glyphicon-time">&nbsp;</i>History Bidding</a>
                      <a href="<?php echo base_url('admin/history_selling');?>" class="list-group-item"><i class="glyphicon glyphicon-time">&nbsp;</i>History Selling</a>
                      <a href="<?php echo base_url('admin/history_green_wallet');?>" class="list-group-item"><i class="glyphicon glyphicon-time">&nbsp;</i>History Green Wallet</a>
                      <a href="<?php echo base_url('admin/history_certificates');?>" class="list-group-item"><i class="glyphicon glyphicon-time">&nbsp;</i>History Certificates</a>
                    </div><!-- .panel-user -->
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#user-management"><i class="glyphicon glyphicon-user">&nbsp;</i>User Management</a>
                  </h4>
                </div>
                <div id="user-management" class="panel-collapse collapse">
                  <div class="panel-body padding0">
                    <div class="list-group control-panel marginbottom4px">
                      <a href="<?php echo base_url('admin/user_level');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>User Groups</a>
                      <a href="<?php echo base_url('admin/user_level_role');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>User Roles</a>
                      <a href="<?php echo base_url('admin/users');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>User Lists</a>
                    </div><!-- .panel-user -->
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-settings"><i class="glyphicon glyphicon-wrench">&nbsp;</i>Settings</a>
                  </h4>
                </div>
                <div id="collapse-settings" class="panel-collapse collapse">
                  <div class="panel-body padding0">
                    <div class="list-group control-panel marginbottom4px">
                      <a href="<?php echo base_url('admin/jabon_price');?>" class="list-group-item"><i class="glyphicon glyphicon-usd">&nbsp;</i>Jabon Price</a>
                      <a href="<?php echo base_url('admin/custom_menus');?>" class="list-group-item"><i class="glyphicon glyphicon glyphicon-th-list">&nbsp;</i>Menu Manager</a>
                    </div><!-- .panel-user -->
                  </div>
                </div>
              </div>
            </div>
    
            <div class="list-group control-panel">
              <a href="<?php echo base_url('admin/dashboard/do_logout');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>Logout</a>
            </div><!-- .panel-user -->
            
					</div><!-- .sidebar-container -->
          
