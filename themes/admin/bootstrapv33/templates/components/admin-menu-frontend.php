<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
		$is_login = $this->user_access->is_login();
    if($is_login)
    {
?>

    <div class="container-fluid" id="admin-nav">
      <div class="col-md-12 col-lg-12">
        <div class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="banner">
            <div class="container">
              <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target-ajax=".admin-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand2" href="index.html"><img src="<?php echo current_theme_url();?>assets/images/logo_c2mspro.png" alt="logo" class="img-responsive"></a>
                </div>
                <div class="collapse admin-navbar-collapse navbar-collapse navbar-right" id="admin-menu">
					<?php
					$this->load->view('components/topmenu');
					?>
                  <ul class="nav navbar-nav">
                    <li>
                      <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminthemes" title="Templates Management">Templates</a>
                    </li>	
                    <li>
                      <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminblocks" title="Widget Management">Widget</a>
                    </li>	
                    <li>
                      <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/adminplugins" title="Plugins Management">Plugins</a>
                    </li>	
                    <li class="dropdown">
                      <a data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="#" class="dropdown-toggle" data-toggle="dropdown">Setting <i class="glyphicon glyphicon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li>
                          <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>page_block" title="General">General</a>
                          </li>
                        <li>
                          <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>page_block" title="Header">Header</a>
                          </li>
                        <li>
                          <a class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>page_block" title="Menu">Menu</a>
                        </li>											
                      </ul>
                    </li>
                    <li>
                      <a href="<?php echo base_url();?>dashboard/do_logout" title="Logout">Logout</a>
                    </li>	
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
<?php 
  }
?>
