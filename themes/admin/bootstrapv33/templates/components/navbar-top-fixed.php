<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
		$is_login = $this->user_access->is_login();
    if($is_login)
    {
?>
					<!-- Header Navbar -->
					<nav class="navbar navbar-static-top" role="navigation">
						<a href="#" class="logo">
							<!-- Add the class icon to your logo image or logo icon to add the margining
							<img class="icon" src="img/logo.png" /> -->
							<h1 class="pull-left">Project Management</h1>
							<h2 class="pull-left">PLDT</h2>
						</a>
						<!-- Sidebar toggle button-->
						<button class="navbar-btn btn btn-lg navbar-toggle visible-lg visible-md visible-sm visible-xs sidebar-toggle" data-toggle="offcanvas" role="button">
							<span class="glyphicon glyphicon-transfer"></span>
						</button>
						<button type="button" class="navbar-toggle sidebar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="navbar-right hide">
							<ul class="nav navbar-nav">
								<!-- Notifications -->
								<li class="dropdown notifications-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-warning"></i>
										<span class="label label-warning">10</span>
									</a>
									<ul class="dropdown-menu">
										<li class="header">You have 10 notifications</li>
										<li>
											<!-- inner menu: contains the actual data -->
											<ul class="menu">
												<li>
													<a href="#">
														<i class="ion ion-ios7-people info"></i> 5 new members joined today
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-warning danger"></i> Very long description here that may not fit into the page and may cause design problems
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-users warning"></i> 5 new members joined
													</a>
												</li>

												<li>
													<a href="#">
														<i class="ion ion-ios7-cart success"></i> 25 sales made
													</a>
												</li>
												<li>
													<a href="#">
														<i class="ion ion-ios7-person danger"></i> You changed your username
													</a>
												</li>
											</ul>
										</li>
										<li class="footer"><a href="#">View all</a></li>
									</ul>
								</li>
								
								<!-- Tasks -->
								<li class="dropdown tasks-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-tasks"></i>
										<span class="label label-danger">9</span>
									</a>
									<ul class="dropdown-menu">
										<li class="header">You have 9 tasks</li>
										<li>
											<!-- inner menu: contains the actual data -->
											<ul class="menu">
												<li><!-- Task item -->
													<a href="#">
														<h3>
															Design some buttons
															<small class="pull-right">20%</small>
														</h3>
														<div class="progress xs">
															<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">20% Complete</span>
															</div>
														</div>
													</a>
												</li><!-- end task item -->
												<li><!-- Task item -->
													<a href="#">
														<h3>
															Create a nice theme
															<small class="pull-right">40%</small>
														</h3>
														<div class="progress xs">
															<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">40% Complete</span>
															</div>
														</div>
													</a>
												</li><!-- end task item -->
												<li><!-- Task item -->
													<a href="#">
														<h3>
															Some task I need to do
															<small class="pull-right">60%</small>
														</h3>
														<div class="progress xs">
															<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">60% Complete</span>
															</div>
														</div>
													</a>
												</li><!-- end task item -->
												<li><!-- Task item -->
													<a href="#">
														<h3>
															Make beautiful transitions
															<small class="pull-right">80%</small>
														</h3>
														<div class="progress xs">
															<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">80% Complete</span>
															</div>
														</div>
													</a>
												</li><!-- end task item -->
											</ul>
										</li>
										<li class="footer">
											<a href="#">View all tasks</a>
										</li>
									</ul>
								</li>
								
								<!-- User Account -->
								<li class="dropdown user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="glyphicon glyphicon-user"></i>
										<span>Dhasamoeka <i class="caret"></i></span>
									</a>
									<ul class="dropdown-menu">
										<!-- User image -->
										<li class="user-header">
											<img src="img/avatar3.png" class="img-circle" alt="User Image" />
											<p>
												Dhasamoeka - Web Makeover
												<small>Member since Nov. 2012</small>
											</p>
										</li>
										<!-- Menu Body -->
										<li class="user-body">
											<div class="col-xs-4 text-center">
												<a href="#">Followers</a>
											</div>
											<div class="col-xs-4 text-center">
												<a href="#">Sales</a>
											</div>
											<div class="col-xs-4 text-center">
												<a href="#">Friends</a>
											</div>
										</li>
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-left">
												<a href="#" class="btn btn-default btn-flat">Profile</a>
											</div>
											<div class="pull-right">
												<a href="#" class="btn btn-default btn-flat">Sign out</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
					<!-- Content Header (Page header) -->
					<nav class="navbar navbar-static-top navbar-pages">
						<div class="container-fluid navbar-pages-container">
							<div class="row">
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<?php
								$this->load->view('components/topmenu');
								?>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</header>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
			<?php $this->load->view("components/leftbar");?>
				<!--
                <section class="content-header">
                    <h1>Dashboard <small>Control panel</small></h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
				-->
				<!-- Modal -->
				<div class="modal fade" id="popupoverlay" tabindex="-1" role="dialog" aria-labelledby="overlaycontainer" aria-hidden="true">
					<div class="modal-dialog modal-lg">
					  <div class="modal-content ajax_container" id="data_target-modal">
					  </div>
					</div>
				</div>
				
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
<?php 
  }
} ?>
