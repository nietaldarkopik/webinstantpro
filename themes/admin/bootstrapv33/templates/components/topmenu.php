<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	#echo $this->user_access->get_custom_menus_allowed_structured();
	
	$curr_menu_group = $this->session->userdata("menu_group");
	$curr_menu_group = (empty($curr_menu_group))?"general":$curr_menu_group;
	
	$parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (`type` = 'home') AND parent_menu = 0 AND user_custom_menus.status = 'active'");

	$the_top_menu = "";
	// Get menu Level 0
	if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
	{
		$the_top_menu .= '<ul class="nav navbar-nav">
							<li>
								<a href="'.base_url().'">
									<span class="glyphicon glyphicon-home"></span>
								</a>
							</li>';
		foreach($parent_menu_allowed as $index => $mn2)
		{
      $path = $this->uri->segment(1);
      $controller = $this->uri->segment(2);
      $function = $this->uri->segment(3);
      $active = ($path == $mn2['path'] and $controller == $mn2['controller'] and $function == $mn2['function'])?' topmenu-list active ':'';
		  $sub_menu_allowed3 = $this->user_access->get_custom_menus_allowed($user_id," AND parent_menu = '" . $mn2['user_menu_id'] . "' AND user_custom_menus.status = 'active'");
		  
		  if($mn2['type'] != $curr_menu_group or (!is_array($sub_menu_allowed3) or count($sub_menu_allowed3) == 0))
		  {
        if($mn2['controller'] == "#" and (is_array($sub_menu_allowed3) and count($sub_menu_allowed3) == 0))
        {}else{
          $the_top_menu .= '<li class="'.((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)?' dropdown ':'') . $active . '">
                              <a href="'.base_url() . $mn2['path'] . '/'  . $mn2['controller'] . '/' . $mn2['function'] . '" '.((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)?' class="dropdown-toggle" data-toggle="dropdown"':'').'>
                                '.((!empty($mn2['attributes']))?'<span class="'.$mn2['attributes'].'"></span> ':'').$mn2['menu_title'].'
                              </a>';	
        }
		  }
		  
		  if(is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)
		  {
        $the_top_menu .= '<ul class="dropdown-menu">';
        foreach($sub_menu_allowed3 as $index3 => $mn3)
        {
          $active2 = ($path == $mn3['path'] and $controller == $mn3['controller'] and $function == $mn3['function'])?'topmenu-list active ':'';
          $the_top_menu .= '<li class="'.$active2.'"><a href="'.base_url() . $mn3['path'] . '/' . $mn3['controller'] . '/' . $mn3['function'] . '">'.((!empty($mn3['attributes']))?'<span class="'.$mn3['attributes'].'"></span> ':'').$mn3['menu_title'].'</a>';	
        }
        $the_top_menu .= '</ul>';
		  }
		}
		$the_top_menu .= '
						</ul>';
	}
	
	#echo $the_top_menu;
?>
<ul class="nav navbar-nav">
	<li>
		<a href="<?php echo base_url();?>">
			<span class="glyphicon glyphicon-home"></span>
		</a>
	</li>
	<?php
	/*
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-briefcase"></span> Catalog Management
		</a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url();?>admin/catalog_categories/index"><span class="glyphicon glyphicon-th-list"></span> Product Categories</a></li>
			<li><a href="<?php echo base_url();?>admin/catalog_products/index"><span class="glyphicon glyphicon-gift"></span> Products</a></li>
			<li><a href="<?php echo base_url();?>admin/catalog_attributes/index"><span class="glyphicon glyphicon-paperclip"></span> Attributes</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-shopping-cart"></span> Shop Management
		</a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url();?>admin/store_orders/index"><span class="glyphicon glyphicon-phone-alt"></span> Orders</a></li>
			<li><a href="<?php echo base_url();?>admin/store_customers/index"><span class="glyphicon glyphicon-king"></span> Customers</a></li>
			<li><a href="<?php echo base_url();?>admin/store_payment_methods/index"><span class="glyphicon glyphicon-credit-card"></span> Payment Methods</a></li>
			<li><a href="<?php echo base_url();?>admin/store_delivery_methods/index"><span class="glyphicon glyphicon-plane"></span> Delivery Methods</a></li>
			<li><a href="<?php echo base_url();?>admin/store_price_manager/index"><span class="glyphicon glyphicon-usd"></span> Price Management</a></li>
			<li><a href="<?php echo base_url();?>admin/store_reports/index"><span class="glyphicon glyphicon-list-alt"></span> Reports</a></li>
		</ul>
	</li>
	*/
	?>
	<li class="dropdown">
		<a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-globe"></span> Site Management
		</a>
		<ul class="dropdown-menu">
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/appearence_menus/index"><span class="glyphicon glyphicon-menu-hamburger"></span> Menu Management</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/appearence_pages/index"><span class="glyphicon glyphicon-file"></span> Page Management</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/appearence_plugins/index"><span class="glyphicon glyphicon-log-in"></span> Plugins</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/appearence_themes/index"><span class="glyphicon glyphicon-sunglasses"></span> Themes</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-user"></span> Administration
			<ul class="dropdown-menu">
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/administration_user_levels/index"><span class="glyphicon glyphicon-king"></span> User Levels</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/administration_users/index"><span class="glyphicon glyphicon-pawn"></span> User Accounts</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/administration_user_roles/index"><span class="glyphicon glyphicon-knight"></span> User Roles</a></li>
				<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/administration_my_profile/index"><span class="glyphicon glyphicon-tower"></span> My Profile</a></li>
			</ul>
		</a>
	</li>
	<li class="dropdown">
		<a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
			<span class="glyphicon glyphicon-cog"></span> Configuration
		</a>
		<ul class="dropdown-menu">
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_general/index"><span class="glyphicon glyphicon-cog"></span> Site Configuration</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_social_media/index"><span class="glyphicon glyphicon-thumbs-up"></span> Sosial Media</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_social_media/index"><span class="glyphicon glyphicon-thumbs-up"></span> Google Analitics</a></li>
			<li><a  class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-body" href="<?php echo base_url();?>admin/setting_smtp/index"><span class="glyphicon glyphicon-envelope"></span> SMTP</a></li>
		</ul>
	</li>
	<li>
		<a href="<?php echo base_url();?>">
			<span class="glyphicon glyphicon-log-out"></span> Logout
		</a>
	</li>
</ul>