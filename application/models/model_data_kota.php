<?php
class Model_data_kota extends Master_model {
  var $table = 'data_kota';
  var $primary_key = 'data_kota_id';
  var $orderby = 'nama_kota';
  var $order = 'ASC';

}
