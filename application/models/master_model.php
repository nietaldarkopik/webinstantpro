<?php
class Master_model extends CI_Model {
  var $table = '';
  var $primary_key = '';
  var $fields = '';
  var $limit = 0;
  var $offset = 0;
  var $orderby = '';
  var $order = '';

  function add($fields)
  {
    if(!empty($fields)){
        $do = $this->db->insert($this->table, $fields);
        if($do)
            return array('success' => true);
        else
            return array('success' => false);
    }

    return array('success' => false);
  }

  function edit($data_edit, $where = array())
  {
    if(!empty($data_edit)){
        if(!empty($where))
            $this->db->where($where);

        $do = $this->db->update($this->table, $data_edit);
        if($do)
            return array('success' => true);
        else
            return array('success' => false);            

    }

    return array('success' => false);
        
  }

  function delete($where)
  {
    if(!empty($where))
    {
        $this->db->where($where);
        $do = $this->db->delete($this->table);
        if($do)
            return array('success' => true);
        else
            return array('success' => false);            

    }
    return array('success' => false);            

  }

  function get($limit = 0, $offset = 0, $where = array(), $orderby = '', $order = '')
  {
    $limit = (!empty($limit)) ? $limit : $this->limit;
    $offset = (!empty($offset)) ? $offset : $this->offset;
    $orderby = (!empty($orderby)) ? $orderby : $this->orderby;
    $order = (!empty($order)) ? $order : $this->order;
    $return = array('total' => 0, 'data' => array());

    if(!empty($orderby) && !empty($order))
        $this->db->order_by($orderby, $order);

    if($limit > 0)
        $this->db->limit($limit, $offset);

    if(!empty($where))
        $this->db->where($where);

    $get = $this->db->get($this->table);
    $return = array(
        'total' => $get->num_rows(),
        'data' => $get->result(),
    );
    return $return;
  }
  
}
