<?php
class Model_data_propinsi extends Master_model {
  var $table = 'data_propinsi';
  var $primary_key = 'data_propinsi_id';
  var $orderby = 'nama_propinsi';
  var $order = 'ASC';

}
