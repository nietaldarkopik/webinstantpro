<?php

class Controller_configs extends CI_Model 
{
  
  var $controllers = array();
  var $params = array();
  
  function __construct()
  {
    
    $data_tables = array(
                          "addon_exchange_point" => 
                                array(
                                    "mainapp_exchange_points",
                                    "mainapp_product_points",
                                    "mainapp_bonus_temporary"
                                ),
                          "addon_card_iw2" => array("mainapp_card_bonus"),
                          "addon_qualification_elc_lc" => array("mainapp_qualified_member"),
                          "addon_card_webforinstant" => array("mainapp_card_bonus"),
                          "addon_bonus_generation_cycle" => array("mainapp_bonus_temporary"),
                          "addon_bonus_apig" => array("mainapp_bonus"),
                          "addon_bonus_roi" => array("mainapp_bonus"),
                          "addon_bonus_royalty" => array("mainapp_bonus"),
                          "addon_topup_installment_sponsor" => 
                                array(
                                    "mainapp_sponsor_installments",
                                    "mainapp_bonus_temporary",
                                    "mainapp_installment_jabon"
                                ),
                          "addon_bonus_sponsor_gba" => 
                                array("mainapp_sponsor_gba",
                                    "mainapp_bonus_temporary"
                                    ),
                          "addon_bonus_roll_installment" => 
                                array(
                                      "mainapp_bonus_jabon",
                                      "mainapp_data_installments",
                                      "mainapp_installment_jabon"
                                ),
                          "addon_topup_daily" => array("mainapp_data_topup"),
                          "addon_network_growing" => 
                                array(
                                    "mainapp_sale_point",
                                    "mainapp_cpp",
                                    "mainapp_cps",
                                    "mainapp_bonus_temporary"
                                ),
                          "addon_bonus_roll" => array("mainapp_bonus_jabon"),
                          "addon_topup_sponsor" => array("mainapp_bonus_temporary"),
                          "addon_log_bonus_instant" => array("mainapp_log_api_albarkah"),
                          "addon_qualification_infinity" => array("mainapp_qualification_infinity"),
                          "addon_data_bonus" => array("mainapp_bonus_temporary"),
                          "addon_content_management" => array("mainapp_cms"),
                          "addon_edit_information" => array("mainapp_content"),
                          "addon_create_new_pspa" => array("mainapp_custom_pspa"),
                        );
    $result = array();
    foreach($data_tables as $i => $d)
    {
        foreach($d as $i2 => $v)
        {
          #$data_tables[$i][$i2] = array($v => $this->db->field_data($v));
          $fields = $this->db->field_data($v);
          $fields = json_decode(json_encode($fields),true);
          if(is_array($fields) and count($fields) > 0)
          {
            $primary_key = "";
            foreach($fields as $index => $field)
            {
              
              if(isset($field['primary_key']) and $field['primary_key'] == 1)
              {
                $primary_key = $field['name'];
                unset($fields[$index]);
                continue;
              }
              
              $type = "input_text";
              
              if($field['type'] == 'datetime')
                $type = 'input_datetime';
                
              if($field['type'] == 'date')
                $type = 'input_date';
                
              if($field['type'] == 'text')
                $type = 'input_textarea';
                
              $append_attributes = array(
                                            'name' => $field['name'],
                                            'label' => ucwords(str_replace("_"," ",$field['name'])),
                                            'id' => $field['name'],
                                            'value' => '',
                                            'type' => $type,
                                            'use_search' => 'true',
                                            'use_listing' => 'true',
                                            'rules' => 'required'
                                          );
                                          
              $field = array_merge($field,$append_attributes);
              $fields[$index] = $field;
            }
          }
          $result[$i][$v] = array("table" => $v,"fields" => $fields,'primary_key' => $primary_key);
        }
    }
    
    $this->controllers =  $result;
  }
  
  function set_param_config($controller,$params = array())
  {
    if(!isset($controller) or empty($controller))
      return false;
      
    $this->params[$controller] = $params;
    return true;
  }
  
  function get_config($control_name = "")
  {
    if(isset($this->controllers[$control_name]) and is_array($this->controllers[$control_name]) and count($this->controllers[$control_name]) > 0)
    {
      $return =  $this->controllers[$control_name];
      $append_params = (isset($this->params[$control_name]))?$this->params[$control_name]:array();
      if(is_array($append_params) and count($append_params) > 0)
      {
        $return = array_merge_recursive($return,$append_params);
      }
      return $return;
    }
    
    return array();
  }
}
