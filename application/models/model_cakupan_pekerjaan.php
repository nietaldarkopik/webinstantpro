<?php
class Model_cakupan_pekerjaan extends CI_Model 
{
  function get_weeks($master_kontrak_id = "")
  {
		date_default_timezone_set('Europe/London');
		$q = $this->db->query("SELECT * FROM mk_master_kontrak WHERE mk_master_kontrak_id = '".$master_kontrak_id."'");
		$data = $q->row_array();
		
		$weeks = 0;
		if(is_array($data) and count($data) > 0)
		{
			#echo $data['tanggal_mulai'] . " " . $data['tanggal_berakhir'];
			$weeks = $this->date_diff_weeks($data['tanggal_mulai'],$data['tanggal_berakhir']);
		}
		return $weeks;
	}
	
	function date_diff_weeks($date1="", $date2="")
	{
			$first = new DateTime($date1);
			$second = new DateTime($date2);
			if($date1 > $date2) return $this->date_diff_weeks($date2, $date1);
			return floor($first->diff($second)->days/6);
	}
  
  function get_week_number_by_date($date1 = "",$date2 = "",$work_date = "",$master_kontrak_id = "")
  {
    $strdate1 = strtotime($date1);
    $strdate2 = strtotime($date2);
    $strworkdate = strtotime($work_date);
  }
  
  function get_date_range_by_number_week($date1 = "",$date2 = "",$week_number = "")
  {
    $week_days = $week_number * 7;
    $strdate1 = strtotime($date1);
    $strdate2 = strtotime($date2);
    
    $start_date = date('Y-m-d', strtotime("+".$week_days." days", strtotime($date1)));
    $end_date = date('Y-m-d', strtotime("+6 days", strtotime($start_date)));
    
    return array("start_date" => $start_date,"end_date" => $end_date);
  }
  
  function get_date_number_by_week($date1 = "",$date2 = "",$week_number = "",$master_kontrak_id = "",$spesifikasi_id = "")
  {
    $week_days = $week_number * 7;
    $strdate1 = strtotime($date1);
    $strdate2 = strtotime($date2);
    
    $start_date = date('Y-m-d', strtotime("+".$week_days." days", strtotime($date1)));
    $end_date = date('Y-m-d', strtotime("+6 days", strtotime($start_date)));
    
    $where = "";
    if(!empty($spesifikasi_id))
    {
      $where .= " AND spesifikasi_id = '".$spesifikasi_id."' ";
    }
    
    $q = $this->db->query("SELECT * FROM mk_cakupan_kerja WHERE master_kontrak_id = '".$master_kontrak_id."' AND tanggal_rencana >= '".$start_date."' AND tanggal_rencana <= '".$end_date."' " . $where . " ");
    $data_cakupan_kerja = $q->result_array();
    return $data_cakupan_kerja;
  }
  
  function get_total_work_location($date1 = "",$date2 = "",$master_kontrak_id = "",$spesifikasi_id = "")
  {
    $where = "";
    if(!empty($spesifikasi_id))
    {
      $where .= " AND spesifikasi_id = '".$spesifikasi_id."' ";
    }
    
    $q = $this->db->query("SELECT * FROM mk_cakupan_kerja WHERE master_kontrak_id = '".$master_kontrak_id."' AND tanggal_rencana >= '".$date1."' AND tanggal_rencana <= '".$date2."' ".$where." ");
    $data_cakupan_kerja = $q->result_array();
    return $data_cakupan_kerja;
  }
  
  function get_total_product($date1 = "",$date2 = "",$master_kontrak_id = "",$spesifikasi_id = "")
  {
    $where = "";
    if(!empty($spesifikasi_id))
    {
      $where .= " AND spesifikasi_id = '".$spesifikasi_id."' ";
    }
    
    $q = $this->db->query("SELECT SUM(nilai) total_qty FROM mk_cakupan_kerja WHERE master_kontrak_id = '".$master_kontrak_id."' AND tanggal_rencana >= '".$date1."' AND tanggal_rencana <= '".$date2."' ".$where." GROUP BY spesifikasi_id");
    $data_cakupan_kerja = $q->row_array();
    return (isset($data_cakupan_kerja['total_qty']))?$data_cakupan_kerja['total_qty']:0;
  }
}
