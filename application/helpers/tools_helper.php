<?php
/**
 *
 * Safely encrypts data for POST transport
 * URL issues
 *  transforms + to spaces
 *  / are param value separators
 *  = are param value separators
 *
 *  we process the string on reverse
 * @param string $string
 */
function my_urlsafe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_','.'),$data);
    return $data;
}

/**
 *
 * The encoding string had to be specially encoded to be transported
 * over the HTTP
 *
 * The reverse function has to be executed on the client
 *
 * @param string $string
 */
function my_urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

function is_date( $str ){
    $stamp = strtotime( $str );
    if (!is_numeric($stamp) || !preg_match("^\d{1,2}[.-/]\d{2}[.-/]\d{4}^", $str))
        return FALSE;
    $month = date( 'm', $stamp );
    $day   = date( 'd', $stamp );
    $year  = date( 'Y', $stamp );
    if (checkdate($month, $day, $year))
        return TRUE;
    return FALSE;
}


function format_currency($nominal = 0,$currency = "")
{
  $nominal = number_format($nominal,0,",",".");
  return $currency.$nominal;
}

function image_thumb($method = "resize", $image_path, $height = "", $width = "" ) {
    // Get the CodeIgniter super object
    $CI =& get_instance();

    // Path to image thumbnail
    $info = pathinfo($image_path);
    $image_thumb = dirname( $image_path ) . '/thumbs/' . basename($image_path,'.'.$info['extension']) . '_' . ((empty($height))?"auto":$height) . '_' . ((empty($width))?"auto":$width) . '_' . $method . '.' . $info['extension'];

    if ( !file_exists( $image_thumb ) ) {
        // LOAD LIBRARY
        $CI->load->library( 'image_lib' );

        // CONFIGURE IMAGE LIBRARY
        $config['image_library']    = 'gd2';
        $config['source_image']     = $image_path;
        $config['new_image']        = $image_thumb;
        $config['maintain_ratio']   = false;
        if(!empty($height))
			$config['height']           = $height;
		if(!empty($width))
			$config['width']            = $width;
        $CI->image_lib->initialize( $config );
        $CI->image_lib->$method();
        $CI->image_lib->clear();
    }

    return $image_thumb;
}
