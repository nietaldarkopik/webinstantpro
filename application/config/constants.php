<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('ADMIN_EMAIL',		'nietaldarkopik@gmail.com');
define('ADMIN_NAME',		'ADMIN SIMBINTEK');

define('YEAR_START',2000);


define('TABLE_SYS_TABLE_CONFIG','system_table_config');
define('TABLE_SYS_SETTINGS','system_settings');
define('TABLE_SYS_LOG','system_log');
define('TABLE_SYS_RECYCLE_BIN_DATA','system_recycle_bin_data');
define('TABLE_SYS_RECYCLE_BIN_FILE','system_recycle_bin_file');
define('TABLE_SYS_PLUGINS','system_plugins');
define('TABLE_SYS_WIDGETS','system_widgets');
define('TABLE_SYS_THEMES','system_themes');
define('TABLE_SYS_BLOCKS','system_blocks');
define('TABLE_SYS_BLOCK_GROUPS','system_block_groups');
define('TABLE_SYS_BLOCK_GROUP_RELS','system_block_group_rels');
define('TABLE_SYS_BLOCK_PLUGINS','system_block_plugins');
define('TABLE_SESSION','system_sessions');
define('TABLE_USERS','user_accounts');
define('TABLE_USR_DATA','user_data');
define('TABLE_USR_LEVELS','user_levels');
define('TABLE_USR_LEVEL_ROLES','user_level_roles');
#define('TABLE_USR_MENUS','user_menus');
define('TABLE_USR_MENUS','user_custom_menus');
define('TABLE_USR_MENU_GROUPS','user_menu_groups');
define('TABLE_USR_PAGES','user_pages');
define('TABLE_USR_ROLES','user_roles');
define('TABLE_USR_THEMES','user_themes');

define('WEB_NAME','Webinstantpro.com');
/* End of file constants.php */
/* Location: ./application/config/constants.php */
