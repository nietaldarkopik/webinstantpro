<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class MY_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $is_login = $this->user_access->is_login();

        $this->load->set_object_path('view',array(FCPATH.'themes/user/'.CURRENT_THEME.'/templates/'	=> TRUE));
        
        if(file_exists(FCPATH.'themes/user/'.CURRENT_THEME.'/'.CURRENT_THEME . '.php'))
        {
			require_once(FCPATH.'themes/user/'.CURRENT_THEME.'/'.CURRENT_THEME . '.php');
			$theme_config = CURRENT_THEME;
			$theme_loader = new $theme_config();
			$theme_loader->setAssets();

			if($this->user_access->is_login())
			{
				$theme_loader->setAdminAssets();
			}
        }
        
		
        if(ENVIRONMENT == 'production')
        {
          $this->output->cache(5);
        }
		
		$this->assets->add_css('<link rel="stylesheet" href="'.base_url().'assets/core/css/core-theme.css" rel="stylesheet">',"head");

    }
}

class Admin_Controller extends CI_Controller {

	var $config_import = array();
	
	function __construct()
	{
			parent::__construct();
			$is_login = $this->user_access->is_login();
			if(!$is_login and $this->uri->segment(1) != "admin" and $this->uri->segment(2) != "dashboard" and $this->uri->segment(3) != "login")
			{
				header("location: ".base_url()."admin/dashboard/login");
			}
			
			$this->load->set_object_path('view',array(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/templates/' => true),TRUE);
			$this->assets->reset();
			if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php'))
			{
				require_once(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php');
				$theme_config = CURRENT_ADMIN_THEME;
				$theme_loader = new $theme_config();
				$theme_loader->setAssets();
			}

			$user_id = $this->user_access->current_user_id;
			$controller = $this->uri->segment(1);
			$function = $this->uri->segment(2,"index");
			
			if(!$is_login and $this->uri->segment(3) != "login" and $this->uri->segment(3) != "do_login")
			{
				if($this->uri->segment(1) != "dashboard" and $this->uri->segment(1) != "")
				{
					if(!$this->user_access->is_page_allowed($user_id,$controller,$function))
					{
						redirect(base_url()."admin/dashboard/login");
						#show_error('Anda tidak memiliki ijin untuk mengakses halaman ini',500,'404 Page Not Found');
					}
				}
			}
			if(ENVIRONMENT == 'production')
			{
				$this->output->cache(5);
			}
			
	}

}

class Plugins_Controller extends CI_Controller {

	function __construct()
  {
    parent::__construct();
    $this->load->set_object_path('library',array(FCPATH.'plugins/'	=> TRUE),false);
    $this->load->set_object_path('helper',array(FCPATH.'plugins/'	=> TRUE),false);
    $this->load->set_object_path('model',array(FCPATH.'plugins/'	=> TRUE),false);
    $this->load->set_object_path('view',array(FCPATH.'plugins/'	=> TRUE),false);
    $this->assets->reset();
  }
}

// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */
