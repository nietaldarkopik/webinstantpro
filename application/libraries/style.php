<?php
class Style{
	var $CI;
  
	function Style(){
		$this->CI =& get_instance();
		$this->CI->load->library("css_parser");
	}
	
	function extract_css($css = '')
	{
		$output = array();
				
		$cssIndex = $this->CI->css_parser->ParseCSS($css);
		return $this->CI->css_parser->GetCSSArray($cssIndex);
	}
}
