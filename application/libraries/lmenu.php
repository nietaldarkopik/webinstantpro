<?php

class Lmenu{
	var $CI;
	
	function Lmenu(){
		$this->CI =& get_instance();
	}
	
	function mapTree($dataset = array(),$parent = 0,$key = 'parent_menu') {
		$tree = array();
		
		if(isset($dataset) and is_array($dataset) and count($dataset) > 0)
		{
			foreach($dataset as $index => $arrays)
			{
				if($arrays[$key] == $parent)
				{
					#$the_array[$arrays['id']] = $arrays;
					$arrays['children'] = $this->mapTree($dataset,$arrays['user_menu_id'],$key);
					$tree[] = $arrays;
				}
			}
		}
		return $tree;
	}
	
	function display_structure($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = "\n".'<ul>'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output_tmp = $this->display_structure($node['children'],$indent+1, $max_depth);
					$output_tmp2 = str_replace("\n","",$output_tmp);
					if($output_tmp2 == "<ul></ul>" and $href == '#')
					{}else{
						$output .= '<li><a href="'.$href.'">'.$node['title'].'<span class="ui-icon ui-icon-arrowthick-1-e"></span></a>';
						$output .= $output_tmp;
						$output .= '</li>'."\n";
					}
				}else{
					if($href != '#')
					{
						$output .= '<li><a href="'.$href.'">'.$node['title'].'</a>';
						$output .= '</li>'."\n";
					}
				}
				
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
	
	function display_structure2($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul>'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li><a href="'.$href.'"><span class="ui-icon  '. $node['icon']. '"></span>'.$node['title'].'<span class="ui-icon ui-icon-arrowthick-1-e"></span></a>';
					$output .= $this->display_structure2($node['children'],$indent+1, $max_depth);
				}else{
					if($href != '#')
						$output .= '<li><a href="'.$href.'"><span class="ui-icon  '. $node['icon']. '"></span>'.$node['title'].'</a>';
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}

	function get_menus_allowed_structured($user_id = "",$depth = "") {		
		$a = $this->get_menus_allowed($user_id);
		$mapped = array();
		$mapped = $this->mapTree($a);
		$output = $this->display_structure($mapped,0,$depth);
		return $output;
	}
	
	function get_menu($where = array()){
		if(!empty($where))
			$this->CI->db->where($where);
		$q = $this->CI->db->get("user_menus");
		$user_menus = $q->row_array();
		return $user_menus;
	}
	
	function get_menu_by_parent($parent_menu = "0",$user_id = ""){
		$user_level = $this->get_level($user_id);
		$query = $this->CI->db->query("SELECT * FROM user_level_roles,user_menus WHERE user_menus.parent_menu = '".$parent_menu."' AND ((user_level_roles.controller = user_menus.controller AND user_level_roles.function = 'index' AND user_level_roles.user_level_id = '$user_level') or user_menus.controller = '#') GROUP BY user_menus.controller,user_menus.function ORDER BY sort_order ASC");
		$user_menuss = $query->result_array();		
		return $user_menuss;
	}
		
	function get_url($path = "",$controller = "",$function = "index"){
    $path = trim($path,"/");
		$function = (empty($function))?"index":$function;
    $url = $path.'/'.$controller.'/'.$function;
    $url = str_replace("//","/",$url);
    $url = ltrim($url,"/");
		return base_url().$url;
	}

}
