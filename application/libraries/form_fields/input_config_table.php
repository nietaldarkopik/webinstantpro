<?php

class Input_config_table{
  var $CI;
  var $form_field = "";

  function Input_config_table()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "text";
    
    $desc = (isset($field['desc']))?$field['desc']:"";
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }
    $input_types = $this->CI->form_fields->get_fields();
    
    $input_types_options = array();
    foreach($input_types as $i => $t)
    {
      $input_types_options += array(strtolower($t['class_name']) => $t['class_name']);
    }
    $mainfields = array(
                        "name" => array(
                                          'name' => 'name',
                                          'label' => 'Name',
                                          'id' => 'name',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => 'required'
                                        ),
                        "id" => array(
                                          'name' => 'id',
                                          'label' => 'ID',
                                          'id' => 'id',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => 'required'
                                        ),
                        "label" => array(
                                          'name' => 'label',
                                          'label' => 'Label',
                                          'id' => 'label',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => 'required'
                                        ),
                        "value" => array(
                                          'name' => 'value',
                                          'label' => 'Value',
                                          'id' => 'value',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "type" => array(
                                          'name' => 'type',
                                          'label' => 'Input Type',
                                          'id' => 'type',
                                          'value' => '',
                                          'type' => 'input_selectbox',
                                          'options' => $input_types_options,
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "desc" => array(
                                          'name' => 'desc',
                                          'label' => 'Text Description',
                                          'id' => 'desc',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "rules" => array(
                                          'name' => 'rules',
                                          'label' => 'Rules',
                                          'id' => 'rules',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "use_search" => array(
                                          'name' => 'use_search',
                                          'label' => 'Use Search',
                                          'id' => 'use_search',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => 'required'
                                        ),
                        "use_listing" => array(
                                          'name' => 'use_listing',
                                          'label' => 'Use Listing',
                                          'id' => 'use_listing',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => 'required'
                                        )/*,
                        "star_required" => array(
                                          'name' => 'star_required',
                                          'label' => 'Star Required',
                                          'id' => 'star_required',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "maxlength" => array(
                                          'name' => 'maxlength',
                                          'label' => 'Maxlength',
                                          'id' => 'maxlength',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "size" => array(
                                          'name' => 'size',
                                          'label' => 'Size',
                                          'id' => 'size',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "style" => array(
                                          'name' => 'style',
                                          'label' => 'Style',
                                          'id' => 'style',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "class" => array(
                                          'name' => 'class',
                                          'label' => 'Class',
                                          'id' => 'class',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        ),
                        "parent_name" => array(
                                          'name' => 'parent_name',
                                          'label' => 'Parent Input Name',
                                          'id' => 'parent_name',
                                          'value' => '',
                                          'type' => 'input_text',
                                          'use_search' => 'true',
                                          'use_listing' => 'true',
                                          'rules' => ''
                                        )*/
                      );

    $this->CI->db->where(array($this->CI->data->primary_key => $this->CI->data->primary_key_value));
    $get_data_table = $this->CI->db->get($this->CI->data->table)->row_array();
    $fields = array();
    
    if(isset($get_data_table['table_name']))
      $fields = $this->CI->db->field_data($get_data_table['table_name']);
    
    $other_fields = '
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Other Field Config</h3>
              </div>
              <div class="panel-body panelbody-fieldconfig">
                <div class="row">
                  <div class="col-lg-12 hide field-config-row">
                    <!-- div class="col-lg-4">
                      <div class="row">
                        <input type="text" name="other_fields_label[__field_name__][]" value="" class="form-control" placeholder="Field Label">
                      </div>
                    </div -->
                    <div class="col-lg-6">
                      <div class="row">
                        <input type="text" name="other_fields_name[__field_name__][]" value="" class="form-control" placeholder="Field Name">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="row">
                        <div class="input-group">
                          <input type="text" name="other_fields_value[__field_name__][]" value="" class="form-control" placeholder="Field Value">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-danger remove-config" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Config Field" data-message="Are you sure you want to delete this config?">
                              <span class="glyphicon glyphicon-remove"></span>
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> <!-- end row -->
                <div class="col-lg-12 clearfix">
                  <div class="row">
                    <button name="input_submit" type="button" value="" class="add_new_field_config required btn btn-primary btn-block">Add new Field Config</button>
                  </div>
                </div>
              </div> <!-- end panel-body -->
            </div> <!-- end panel-primary -->';
    #echo "<pre>";
    #print_r($fields);
    #echo "</pre>";
    $tabs_nav = "";
    $tabs_pane = "";
    if(is_array($fields) and count($fields) > 0)
    {
      foreach($fields as $i => $t)
      {
        $f = (array) $t;
            
        $the_form = "";
        $parent_name = "fields";
        foreach($mainfields as $ifield => $field)
        {
          
          $parent_name = (isset($f['name']) and $ifield == 'name')?$parent_name.'['.$f['name'].']':$parent_name;
          $field['parent_name'] = $parent_name;
          $field['value'] = (isset($f['name']) and $ifield == 'name')?$f['name']:"";
          $field['value'] = (isset($f['name']) and $ifield == 'id')?$f['name']:$field['value'];
          $field['value'] = (isset($f['name']) and $ifield == 'label')?ucwords(str_replace("_"," ",$f['name'])):$field['value'];
          $field['value'] = (isset($f['name']) and $ifield == 'value')?$f['default']:$field['value'];
          /*
          $field['name'] = (isset($f['name']))?$f['name']:"";
          $field['id'] = (isset($f['name']))?$f['name']:"";
          $field['label'] = (isset($f['name']))?ucwords(str_replace("_"," ",$f['name'])):"";
          $field['value'] = (isset($f['default']))?$f['default']:"";
          $field['maxlength'] = (isset($f['max_length']))?$f['max_length']:"";
          $field['primary_key'] = (isset($f['primary_key']) and $f['primary_key'] == 1)?true:"";
          */
          $the_form .= $this->CI->data->create_form_field($field);//,$value = "",$show_star = true,$only_input = false);
        }
        $tabs_nav .= '<li role="presentation"  class="wrapword"><a href="#'.$f['name'].'" aria-controls="'.$f['name'].'" role="tab" data-toggle="tab">'.$f['name'].'</a></li>';
        $tabs_pane .= '
        <div role="tabpanel" class="tab-pane'.(($i == 0)?' active':'').'" id="'.$f['name'].'">
          <div class="panel panel-primary">
          
            <div class="panel-heading">
              <h3 class="panel-title">Config Field "'.$f['name'].'"</h3>
            </div>
              
            <div class="panel-body">
              <div class="row data_target">
                <div class="col-lg-12">
                  '.$the_form.'
                </div>
              </div>
            </div>
            
          </div>
          
          '.str_replace("__field_name__",$f['name'],$other_fields).'
      </div>';
      }
    }
    
    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $form = '
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Table Parameter</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12 table-parameter-row hide">
                    <!-- div class="col-lg-4">
                      <div class="row">
                        <input type="text" name="parameter_label[]" value="" placeholder="Parameter Label" class="form-control">
                      </div>
                    </div -->
                    <div class="col-lg-6">
                      <div class="row">
                        <input type="text" name="parameter_name[]" value="" placeholder="Parameter Name" class="form-control">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="row">
                        <div class="input-group">
                          <input type="text" name="parameter_value[]" value="" placeholder="Parameter Value" class="form-control">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-danger remove-parameter" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Parameter" data-message="Are you sure you want to delete this data?">
                              <span class="glyphicon glyphicon-remove"></span>
                            </button>
                          </span>
                        </div>
                      </div>
                    </div><!-- /.col-lg-12 -->
                  </div><!-- /.row -->
                  <br class="clearfix"/><br/>
                  <div class="col-lg-12">
                    <button name="input_button" type="button" id="add_new_parameter" value="" class=" required btn btn-primary btn-block">Add new Parameter</button>
                  </div>
                  
                </div><!-- /.row -->
              </div><!-- /.panel-body -->
            </div><!-- /.panel panel-primary -->
            
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Config Table</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12 form_config_table">
                    <div role="tabpanel" class="input_config_table row">
                      <!-- Nav tabs -->
                      <ul class="nav nav-pills nav-tabs nav-stacked col-lg-4 col-sm-4 col-xs-12" role="tablist">
                        '.$tabs_nav.'
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content col-lg-8 col-sm-8 col-xs-12">
                        '.$tabs_pane.'
                      </div>
                    </div>
                  </div><!-- /.col-lg-12 -->
                </div><!-- /.row -->
              </div><!-- /.panel-body -->
            </div><!-- /.panel panel-primary -->
              ';
    $output .= '  
                  <br/>
                  <div class="row">
                    <div class="col-lg-3">
                      <label for="'. $name .'">'. $label .'</label>';
                      #$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($field_attributes));
                  
    $this->form_field = $form;
    $output .= '    </div>
                    <div class="col-lg-9">'.$form . 
                      ((!empty($desc))?'
                      <span class="help-block">'.$desc.'</span>':'').'
                    </div><!-- /.col-lg-6 -->
                  </div><!-- /.row -->';
                  
                  
                  
    $js_inline = '
      <script>
        $(document).ready(function(){
          $("#add_new_parameter").on("click",function(){
            var new_row = $("<div class=\"col-lg-12 table-parameter-row\"></div>").append($(".table-parameter-row:last").html());
            $(".table-parameter-row:last").after($(new_row).removeClass("hide"));
          });
          $(".add_new_field_config").on("click",function(){
            $(".panelbody-fieldconfig").removeClass("selected");
            $(this).parents(".panelbody-fieldconfig").addClass("selected");
            var cur_panel = $(this).parents(".panelbody-fieldconfig");
            var new_row_c = $("<div class=\"col-lg-12 field-config-row\"></div>").append($(cur_panel).find(".field-config-row:last").html());
            $(cur_panel).find(".field-config-row:last").after($(new_row_c).removeClass("hide"));
          });
          
        $("body").on("click",".remove-parameter", function(e) {
            e.preventDefault();
            var el = $(this);
            var title = $(el).attr("data-title");
            var msg = $(el).attr("data-message");
            var dataForm = $(el).attr("data-form");
            
            $("#config-table-confirm")
            .find("#frm_body").html(msg)
            .end().find("#frm_title").html(title)
            .end().modal("show");
            
            //$("#config-table-confirm").find("#frm_submit").attr("data-form", dataForm);
            $("#config-table-confirm").find("#row_type").val("parameter");
            $(".remove-parameter").removeClass("clicked");
            $(this).addClass("clicked");
        });
        
        $("body").on("click",".remove-config", function(e) {
            e.preventDefault();
            var el = $(this);
            var title = $(el).attr("data-title");
            var msg = $(el).attr("data-message");
            var dataForm = $(el).attr("data-form");
            
            $("#config-table-confirm")
            .find("#frm_body").html(msg)
            .end().find("#frm_title").html(title)
            .end().modal("show");
            
            //$("#config-table-confirm").find("#frm_submit").attr("data-form", dataForm);
            $("#config-table-confirm").find("#row_type").val("config");
            $(".remove-config").removeClass("clicked");
            $(this).addClass("clicked");
        });

        $("#config-table-confirm").on("click", "#frm_submit", function(e) {
            var row_type = $("#config-table-confirm").find("#row_type").val();
            if(row_type == "config")
            {
              $(".remove-config.clicked").parents(".field-config-row").remove();
            }else{
              $(".remove-parameter.clicked").parents(".table-parameter-row").remove();
            }
            $("#config-table-confirm").modal("hide");
            $(".remove-parameter").removeClass("clicked");
        });
        
        $("#config-table-confirm").on("hidden.bs.modal", function(e) {
            $(".panelbody-fieldconfig").removeClass("selected");
            $(".remove-parameter").removeClass("clicked");
            $(".remove-config").removeClass("clicked");
        });
      });
    </script>
    ';
    
    $is_ajax = $this->CI->input->post("is_ajax");
    $output .= '<div class="modal fade" id="config-table-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="frm_title">Delete</h4>
                      </div>
                      <div class="modal-body" id="frm_body"></div>
                      <div class="modal-footer">
                        <input type="hidden" value="config" id="row_type"/>
                        <button type="button" class="btn btn-primary col-sm-2 pull-right" id="frm_submit">Yes</button>
                        <button type="button" class="btn btn-danger col-sm-2 pull-right" data-dismiss="modal" id="frm_cancel">No</button>
                      </div>
                    </div>
                  </div>
                </div>';
    if($is_ajax == 1)
    {
      $output .= $js_inline;
    }else
    {
      $this->CI->assets->add_js($js_inline,"body");
    }
    
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

