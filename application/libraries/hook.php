<?php

class Hook{
	var $CI;
	var $hooks = array();
	
	function Hook(){
		$this->CI =& get_instance();
	}
	
	function do_action($hook_name = "",$params = "")
	{
		$output = $params;
		if(isset($this->hooks[$hook_name]) and !empty($this->hooks[$hook_name]))
		{
			$the_hook = $this->hooks[$hook_name];
			if(is_array($the_hook) and count($the_hook) > 1)
			{
				#if(isset($the_hook[0]->{$the_hook[1]}))
					$output = $the_hook[0]->{$the_hook[1]}($params);
			}else{
				#if(function_exists($the_hook[0]))
					$output = $$the_hook[0]($params);
			}
		}
		return $output;
	}
	
	function add_action($hook_name = "",$callback = "")
	{
		$this->hooks[$hook_name] = $callback;
	}
}
