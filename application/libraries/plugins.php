<?php
class Plugins{
	var $CI;
	var $hooks = array();
	var $plugins = array();
  
	function Plugins(){
		$this->CI =& get_instance();
	}
	
  function set_all_path()
  {
    $plugins = $this->get_plugins();
    $this->load_plugins();
    if(is_array($this->plugins))
    {
      foreach($this->plugins as $i => $p)
      {
        $p->setPath();
      }
    }
  }
	
  function set_path($plugin_name = "")
  {
    $this->load_plugins($plugin_name);
    if(is_array($this->plugins))
    {
      foreach($this->plugins as $i => $p)
      {
        $p->setPath();
      }
    }
  }
  
  function get_plugins()
  {
    $dir            = FCPATH.'plugins/';
    $files          = scandir($dir);
    
    $list_plugins = array_filter($files, function($filename) use ($dir) {
      return (is_dir($dir . $filename) and $filename != '.' and $filename != '..') ? true : false;
    });

    $plugins = array();
    if(is_array($list_plugins) and count($list_plugins) > 0)
    {
      $this->CI->load->library('parser');
      foreach($list_plugins as $i)
      {
        $config = array();
        if(function_exists('simplexml_load_file') and file_exists(FCPATH.'plugins/'.$i.'/config/'.$i.'.xml'))
        {
          $config = simplexml_load_file(FCPATH.'plugins/'.$i.'/config/'.$i.'.xml');
          $config = (array) $config;
        }else{
          if(file_exists(FCPATH.'plugins/'.$i.'/config/'.$i.'.php') and !class_exists($i))
            require_once(FCPATH.'plugins/'.$i.'/config/'.$i.'.php');
        }
        
        if(is_array($config) and count($config) == 0)
          continue;
          
        $config['logo'] = '';
        if(file_exists(FCPATH.'plugins/'.$i.'/views/assets/images/logo.gif'))
          $config['logo'] = base_url().'plugins/'.$i.'/views/assets/images/logo.gif';
          
        $plugins[$i] = array("configs" => $config);
      }
    }
	
    return $plugins;
  }
  
  function get_data_plugins($where = "")
  {
    if(!empty($where))
      $this->CI->db->where($where);
    $q = $this->CI->db->get(TABLE_SYS_PLUGINS);
    $data = $q->result_array();
    
    return $data;
  }
  
  function load_plugins($plugin_name = "")
  {
    $where['status'] = 'active';
    if(!empty($plugin_name))
      $where['plugin_name'] = $plugin_name;
      
    $list_plugins = $this->get_data_plugins($where);
    $plugins = array();
    if(is_array($list_plugins) and count($list_plugins) > 0)
    {
      foreach($list_plugins as $i => $p)
      {
        $plugin_name = $p['plugin_name'];
        if(!file_exists(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php'))
          continue;
        
        if(!class_exists($plugin_name))
          require_once(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php');
        $plugins[$plugin_name] = new $plugin_name();
      }
    }
    
    $this->plugins = $plugins;
  }
  
  function load_plugin($plugin_name = "")
  {
    #$where['status'] = 'active';
	$where = array();
    if(!empty($plugin_name))
      $where['plugin_name'] = $plugin_name;
      
    $list_plugins = $this->get_data_plugins($where);
    $plugin = array();
    if(is_array($list_plugins) and count($list_plugins) > 0)
    {
      foreach($list_plugins as $i => $p)
      {
        $plugin_name = $p['plugin_name'];
        if(!file_exists(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php'))
          continue;
        
        if(!class_exists($plugin_name))
          require_once(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php');
        $plugin = new $plugin_name();
      }
    }
    
    return $plugin;
  }
  
  function call_plugins($plugin_name = "")
  {
    $list_plugins = $this->get_data_plugins("plugin_name = '".$plugin_name."'");
    $plugins = array();
    if(is_array($list_plugins) and count($list_plugins) > 0)
    {
      foreach($list_plugins as $i => $p)
      {
        $plugin_name = $p['plugin_name'];
        if(!file_exists(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php'))
          continue;
          
        require_once(FCPATH.'plugins/'.$plugin_name.'/'.$plugin_name.'.php');
        $plugins[$plugin_name] = new $plugin_name();
      }
    }
    
    return (isset($plugins[$plugin_name]))?$plugins[$plugin_name]:false;
  }
  
  function call_plugin_controller($plugin_name = "",$controller = "")
  {
    $controller = (empty($controller))?$plugin_name:$controller;
    $list_plugins = $this->get_data_plugins("plugin_name = '".$plugin_name."'");
    $plugins = array();
    if(is_array($list_plugins) and count($list_plugins) > 0)
    {
      foreach($list_plugins as $i => $p)
      {
        $plugin_name = $p['plugin_name'];
        if(!file_exists(FCPATH.'plugins/'.$plugin_name.'/controllers/'.$controller.'controller.php'))
          continue;
          		
        $plugin_controller = ucfirst(strtolower($controller."controller"));
        if(!class_exists($plugin_controller))
			require_once(FCPATH.'plugins/'.$plugin_name.'/controllers/'.$controller.'controller.php');

        if(class_exists($plugin_controller))
        {
          $plugins[$plugin_name] = new $plugin_controller();
        }else{
          die("Plugin Controller not found");
          break;
        }
      }
    }
    
    return (isset($plugins[$plugin_name]))?$plugins[$plugin_name]:false;
  }
  
  function segment($number)
  {
    $segments = $this->uri->uri_string();
    $segments = explode("page_plugin/plugin_controller",$segments);
    $segments = trim($segments[1],"/");
    $segments = explode("/",$segments);
    return (isset($segments[$number]))?$segments[$number]:"";
  }
  
  function segment_array()
  {
    $segments = $this->uri->uri_string();
    $segments = explode("page_plugin/plugin_controller",$segments);
    $segments = trim($segments[1],"/");
    $segments = explode("/",$segments);
    return $segments;
  }
  
  function uri_string()
  {
    $segments = $this->uri->uri_string();
    $segments = explode("page_plugin/plugin_controller",$segments);
    $segments = trim($segments[1],"/");
    return $segments;
  }
  
}
