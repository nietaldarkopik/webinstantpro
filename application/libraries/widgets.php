<?php
class Widgets{
	var $CI;
	var $hooks = array();
	var $plugins = array();
  
	function Widgets(){
		$this->CI =& get_instance();
	}
	
  function install($plugin_name = "",$widget_data = array())
  {
    $this->CI->db->where(array("plugin_name" => $plugin_name));
    $data_plugin = $this->CI->db->get("system_plugins")->row_array();
	
    $where = array();
    $where['plugin_name'] = $plugin_name;
    $where['widget_name']  = $widget_data['widget_name'];
    
    $data = array();
    $data['plugin_id'] = (isset($data_plugin['plugin_id']))?$data_plugin['plugin_id']:"0";
    $data['plugin_name'] = $plugin_name;
    $data['widget_name']  = $widget_data['widget_name'];
    $data['widget_title'] = $widget_data['widget_title'];
    $data['description']  = $widget_data['description'];
    $data['status']  = 'active';
    $data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
    $data['author']  = $widget_data['author'];
    $data['version']  = $widget_data['version'];
    $this->CI->db->where($where);
    $q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);
    
    if($q_check_widget->num_rows() == 0)
    {
      $q = $this->CI->db->insert(TABLE_SYS_WIDGETS,$data);
      if($q)
      {
        return 1;
      }else{
        return 0;
      }
    }else{
      return -1;
    }
  }
  
  function update($plugin_name = "",$widget_data = array())
  {
    $where = array();
    $where['plugin_name'] = $plugin_name;
    $where['widget_name']  = $widget_data['widget_name'];
    
    $data = array();
    $data['plugin_name'] = $plugin_name;
    $data['widget_name']  = $widget_data['widget_name'];
    $data['widget_title'] = $widget_data['widget_title'];
    $data['description']  = $widget_data['description'];
    $data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
    $data['author']  = $widget_data['author'];
    $data['version']  = $widget_data['version'];
    $this->CI->db->where($where);
    $q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);
    
    if($q_check_widget->num_rows() == 0)
    {
      $q = $this->install($plugin_name,$widget_data);
      if($q)
      {
        return 1;
      }else{
        return 0;
      }
    }else{
      $this->CI->db->where($where);
      $q = $this->CI->db->update(TABLE_SYS_WIDGETS,$data);
      if($q)
      {
        return 1;
      }else{
        return -1;
      }
    }
  }
  
  function callback_widgets($block_col="")
  {
	global $assets_loaded;
	  //$this->load_widget($widget_name,$widget_data);
	$widget_content_id =  (isset($block_col['widget_content_id']))?$block_col['widget_content_id']:0;
	$widget_id =  (isset($block_col['widget_id']))?$block_col['widget_id']:0;
	$data_widget = $this->CI->db->where(array("widget_id" => $widget_id))->get("system_widgets")->row_array();

	$output = "";
	if(isset($data_widget['widget_name']) and !empty($data_widget['widget_name']))
	{
		$widget_name = $data_widget['widget_name'];
		$get_plugin = $this->CI->db->where(array("plugin_name" => $data_widget['plugin_name']))->get("system_plugins")->row_array();
		$plugin_name = (isset($get_plugin['plugin_name']))?$get_plugin['plugin_name']:"";
		if(!empty($plugin_name))
		{
		  if(!class_exists($plugin_name))
		  {
			  require_once(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php');
			  $class_widget_name = ucwords('widget_'.$widget_name);
			  $class_widget = new $class_widget_name;
			  $class_widget->init($block_col);
			  if(method_exists($class_widget,"show_callback_widget"))
				$output = $class_widget->show_callback_widget($block_col);
		  }else{
			  $class_widget_name = ucwords('widget_'.$widget_name);
			  $class_widget = new $class_widget_name;
			  $class_widget->init($block_col);
			  if(method_exists($class_widget,"show_callback_widget"))
				$output = $class_widget->show_callback_widget($block_col);
		  }
		}
	}
	return $output;
  }
  
  function show_widgets($block_col="")
  {
	global $assets_loaded;
	  //$this->load_widget($widget_name,$widget_data);
	$widget_content_id =  (isset($block_col['widget_content_id']))?$block_col['widget_content_id']:0;
	$widget_id =  (isset($block_col['widget_id']))?$block_col['widget_id']:0;
	$data_widget = $this->CI->db->where(array("widget_id" => $widget_id))->get("system_widgets")->row_array();

	$output = "";
	if(isset($data_widget['widget_name']) and !empty($data_widget['widget_name']))
	{
		$widget_name = $data_widget['widget_name'];
		$get_plugin = $this->CI->db->where(array("plugin_name" => $data_widget['plugin_name']))->get("system_plugins")->row_array();
		$plugin_name = (isset($get_plugin['plugin_name']))?$get_plugin['plugin_name']:"";

		if(!empty($plugin_name))
		{
		  if(!class_exists($plugin_name))
		  {
			  require_once(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php');
			  $class_widget_name = ucwords('widget_'.$widget_name);
			  $class_widget = new $class_widget_name;
			  $class_widget->init($block_col);
			  $output = $class_widget->show_widget($block_col);
			  
			  if(is_array($assets_loaded) and count($assets_loaded) > 0 and in_array($class_widget_name,$assets_loaded))
			  {}else{
				$class_widget->show_assets_widget($block_col);
				$assets_loaded[] = $class_widget_name;
			  }
		  }else{
			  $class_widget_name = ucwords('widget_'.$widget_name);
			  $class_widget = new $class_widget_name;
			  $class_widget->init($block_col);
			  $output = $class_widget->show_widget($block_col);
			  if(is_array($assets_loaded) and count($assets_loaded) > 0 and in_array($class_widget_name,$assets_loaded))
			  {}else{
				$class_widget->show_assets_widget($block_col);
				$assets_loaded[] = $class_widget_name;
			  }
		  }
		}
	}
	return $output;
  }
  
  function load_widget($widget_name = "",$widget_data="")
  {
  }
  

  function fetch_widgets($path = "",$filter_file_name = ".php")
  {
		$path            = rtrim($path,"/")."/";
		$dir            = "/".trim($path,"/")."/";
		$url            = str_replace("./","",$path);
		$url            = str_replace(FCPATH,"",$url);
		$files          = scandir($dir);
		
		$list_widgets = array_filter($files, function($filename) use ($dir,$filter_file_name) {
			$file_size = filesize($dir.$filename);
			return (strpos($filename,$filter_file_name) !== false and $file_size > 0) ? true : false;
		});
		
		$widgets = array();
		if(is_array($list_widgets) and count($list_widgets) > 0)
		{
		  foreach($list_widgets as $i)
		  {
			$config = array();
			$base_name_widget = basename($i,$filter_file_name);
			$config['data']['widget_name'] = $base_name_widget;
			$config['data']['screenshot'] = '';
			
			if(file_exists($path.$base_name_widget.'.gif'))
			  $config['data']['screenshot'] = $url.$base_name_widget.'.gif';
			
			$path = rtrim($path,"/");
			if(!is_dir($path."/".$i) and file_exists($path."/".$i))
			{
				require_once($path.'/'.$i);
				$class_name_widget = ucfirst(strtolower($base_name_widget));
				if(class_exists($class_name_widget))
				{
					$class_widget = new $class_name_widget;
					if(method_exists($class_widget,'config'))
					{
						$the_config = $class_widget->config();
						$config['data'] = array_merge($config['data'],$the_config);
					}
					$config['class'] = $class_widget;
				}
			}
			$widgets[$i] = array("configs" => $config);
		  }
		}
		return $widgets;
  }

  function get_widgets($where = "")
  {
	if(!empty($where))
		$this->CI->db->where($where);
	$data_widgets = $this->CI->db->get("system_widgets")->result_array();
	$widgets = array();
	foreach($data_widgets as $i => $data_widget)
	{
		if(isset($data_widget['widget_name']) and !empty($data_widget['widget_name']))
		{
			$widget_name = (isset($data_widget['widget_name']))?$data_widget['widget_name']:"";;
			$plugin_name = (isset($data_widget['plugin_name']))?$data_widget['plugin_name']:"";

			if(!empty($plugin_name) and !empty($widget_name))
			{
			  if(!class_exists($widget_name))
			  {
				  if(file_exists(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php'))
				  {
					  require_once(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php');
					  $class_widget_name = ucwords('widget_'.$widget_name);
					  if(class_exists($class_widget_name))
					  {
						$class_widget = new $class_widget_name;
						$widgets[strtolower($class_widget_name)] = array("class" => $class_widget,"data" => $data_widget);
					  }
				  }
			  }else{
				    $class_widget_name = ucwords('widget_'.$widget_name);
					$class_widget = new $class_widget_name;
					$widgets[strtolower($class_widget_name)] = array("class" => $class_widget,"data" => $data_widget);
			  }
			}
		}
	}

	return $widgets;
  }

  function get_data_widget($where = array(),$plugin_active = false)
  {
	$this->CI->db->where($where);
	$data_widget = $this->CI->db->get("system_widgets")->row_array();

	$output = array();
	if($plugin_active)
	{
		$widget_name = (isset($data_widget['widget_name']))?$data_widget['widget_name']:"";
		$get_plugin = $this->CI->db->where(array("plugin_name" => $data_widget['plugin_name'],"status" => "active"))->get("system_plugins")->row_array();
		$plugin_name = (isset($get_plugin['plugin_name']))?$get_plugin['plugin_name']:"";
		if(!empty($plugin_name))
		{
			$output = $data_widget;
		}

	}
	return $output;
  }

  function call_widget_controller($widget_name = "",$function = "")
  {
    $function = (empty($function))?$widget_name:$function;
    $list_widgets = $this->get_widgets("widget_name = '".$widget_name."'");
    $widgets = array();
    if(is_array($list_widgets) and count($list_widgets) > 0)
    {
      foreach($list_widgets as $i => $p)
      {
		$d = (isset($p['data']))?$p['data']:array();
        $plugin_name = (isset($d['plugin_name']))?$d['plugin_name']:"";
        $widget_name = (isset($d['widget_name']))?$d['widget_name']:"";
        
        if(!file_exists(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php'))
          continue;
        
        $widget_controller = strtolower('widget_'.$widget_name);
        if(!class_exists($widget_controller))
			require_once(FCPATH.'plugins/'.$plugin_name.'/controllers/widgets/widget_'.$widget_name.'.php');
		
        if(class_exists($widget_controller))
        {
          $widgets[$widget_name] = new $widget_controller();
        }else{
          die("Widget Controller not found");
          break;
        }
      }
    }
    
    return (isset($widgets[$widget_name]))?$widgets[$widget_name]:false;
  }
}
