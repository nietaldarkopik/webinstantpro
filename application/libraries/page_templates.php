<?php
class Page_templates{
	var $CI;
	var $hooks = array();
	var $plugins = array();
  
	function Page_templates(){
		$this->CI =& get_instance();
	}
	
	function view($layouts = "layouts/dashboard",$data = array(), $header = "header",$footer = "footer",$show = true)
	{
		$output = "";
        $is_ajax = $this->CI->input->post('is_ajax');

		if(!empty($header) and (empty($is_ajax) or $is_ajax === false ))
		{
			$output .= $this->CI->load->view($header,$data,false);
		}
		if(empty($is_ajax) or $is_ajax === false )
		{
			#$output .= $this->CI->load->view('components/top',$data,false);
			#$output .= $this->CI->load->view('components/navbar-top-fixed',$data,false);
		}
		if(!empty($layouts))
		{
			$output .= $this->CI->load->view($layouts,$data,false);
		}
		if(empty($is_ajax) or $is_ajax === false )
		{
			#$output .= $this->CI->load->view('components/container-main-close',$data,false);
			#$output .= $this->CI->load->view('components/bottom',$data,false);
		}
		if(!empty($footer) and (empty($is_ajax) or $is_ajax === false ))
		{
			$output .= $this->CI->load->view($footer,$data,false);
		}
		
		if($show)
		{
			echo $output;
		}else{
			return $output;
		}
	}

	
	function fetch_templates($path = "",$filter_file_name = ".php")
	{
		$path            = rtrim($path,"/")."/";
		$dir            = trim($path,"/")."/";
		$url            = str_replace("./","",$path);
		$url            = str_replace(FCPATH,"",$url);
		$files          = scandir($dir);
		
		$list_templates = array_filter($files, function($filename) use ($dir,$filter_file_name) {
			$file_size = filesize($dir.$filename);
			return (strpos($filename,$filter_file_name) !== false and $file_size > 0) ? true : false;
		});
		
		$templates = array();
		if(is_array($list_templates) and count($list_templates) > 0)
		{
		  foreach($list_templates as $i)
		  {
			$config = array();
			$base_name_template = basename($i,$filter_file_name);
			$config['data']['template_name'] = $base_name_template;
			$config['data']['screenshot'] = '';
			$config['data']['template_type'] = 'fixed';
			
			if(file_exists($path.$base_name_template.'.gif'))
			  $config['data']['screenshot'] = $url.$base_name_template.'.gif';
			  
			$templates[$i] = array("configs" => $config);
		  }
		}
		return $templates;
	}
	
	function fetch_template($path = "",$filter_file_name = ".php")
	{
		$path            = rtrim($path,"/")."/";
		$dir            = trim($path,"/")."/";
		$url            = str_replace("./","",$path);
		$url            = str_replace(FCPATH,"",$url);
		$files          = scandir($dir);
		
		$list_templates = array_filter($files, function($filename) use ($dir,$filter_file_name) {
			$file_size = filesize($dir.$filename);
			return (strpos($filename,$filter_file_name) !== false and $file_size > 0) ? true : false;
		});
		
		$templates = array();
		if(is_array($list_templates) and count($list_templates) > 0)
		{
		  foreach($list_templates as $i)
		  {
			$config = array();
			$base_name_template = basename($i,$filter_file_name);
			$config['data']['template_name'] = $base_name_template;
			$config['data']['screenshot'] = '';
			
			if(file_exists($path.$base_name_template.'.gif'))
			  $config['data']['screenshot'] = $url.$base_name_template.'.gif';
			  
			$templates[$i] = array("configs" => $config);
		  }
		}
		return $templates;
	}

	function update_templates($path = "",$theme_name = "",$theme="",$extension_template = ".php")
	{
		$first_theme = (empty($first_theme))?$theme_name:$first_theme;
		$check_theme = $this->CI->db->where(array("theme_name" => $theme_name))->get("system_themes")->num_rows();
		$theme_config = (isset($theme['configs']['data']))?$theme['configs']['data']:false;

		$theme_id = "";
		
		if($check_theme == 0)
		{
			if($theme_config !== false)
			{
				$this->CI->db->insert("system_themes",$theme_config);
				$theme_id = $this->CI->db->insert_id();
			}
		}else{
			if($theme_config !== false)
			{
				$this->CI->db->where(array("theme_name" => $theme_name))->update("system_themes",$theme_config);
				$data_theme = $this->CI->db->where(array("theme_name" => $theme_name))->get("system_themes")->row_array();
				$theme_id = (isset($data_theme['theme_id']))?$data_theme['theme_id']:"";
			}
		}
		
		if(!empty($theme_id) and !empty($theme_name))
		{
			$templates = $this->fetch_templates("./themes/".$path."/".$theme_name."/templates/layouts",$extension_template);

			$template_names = array();
			if(is_array($templates) and count($templates) > 0)
			{
				foreach($templates as $file_name => $template)
				{
					$template_id = "";
					
					$template_config = (isset($template['configs']['data']))?$template['configs']['data']:false;
					$template_name = (isset($template_config['template_name']))?$template_config['template_name']:"";
					$template_names[] = "'".$template_name."'";
					
					$check_template = $this->CI->db->where(array("template_name" => $template_name/*,"theme_id" => $theme_id*/))->get("system_templates")->num_rows();

					/*if($template_config !== false)
						$template_config['theme_id'] = $theme_id;*/
								
					if(empty($template_name))
						continue;
					
					if($check_template == 0)
					{
						if($template_config !== false)
						{
							$this->CI->db->insert("system_templates",$template_config);
							$template_id = $this->CI->db->insert_id();
						}
					}else{
						if($template_config !== false)
						{
							$this->CI->db->where(array("template_name" => $template_name/*,"theme_id" => $theme_id*/))->update("system_templates",$template_config);
							$data_template = $this->CI->db->where(array("template_name" => $template_name/*,"theme_id" => $theme_id*/))->get("system_templates")->row_array();
							$template_id = (isset($data_template['template_id']))?$data_template['template_id']:"";
						}
					}
				}
			}
			
			
			$in_template_names = (is_array($template_names) and count($template_names) > 0)?"template_name NOT IN (". implode(",",$template_names) .") ":"";
			$template_removed = $this->CI->db->query("SELECT * FROM system_templates WHERE ".$in_template_names)->result_array();
			if(is_array($template_removed) and count($template_removed) > 0)
			{
				foreach($template_removed as $i => $t)
				{
					$where_template = array(/*"theme_id" => $theme_id,*/"template_id" => $t['template_id']);
					
					$this->CI->db->where($where_template);
					$this->CI->db->delete("system_page_containers");
					
					$this->CI->db->where($where_template);
					$this->CI->db->delete("system_page_rows");
					
					$this->CI->db->where($where_template);
					$this->CI->db->delete("system_page_cols");
					
					$this->CI->db->where($where_template);
					$this->CI->db->delete("system_templates");
				}
			}
		}
	}
	
	function generate_theme($path = "",$filter_file_name = ".php",$theme_name = "")
	{
		$theme = $this->CI->themes->fetch_theme($path,$theme_name);
		$this->update_templates($path,$theme_name,$theme,$filter_file_name);
		
		/*
		if(!class_exists(""))
		{
			require_once(APPPATH."libraries/ganon.php");
		}
		*/
		
		if(!class_exists("phpQuery"))
		{
			require_once(APPPATH."libraries/PhpQuery.php");
		}
		
		$templates =  $this->fetch_templates($path,$filter_file_name);
		if(!file_exists($path."/tmp") or !is_dir($path."/tmp"))
		{
			mkdir($path."/tmp");
		}
		if(!file_exists($path."/cache") or !is_dir($path."/cache"))
		{
			mkdir($path."/cache");
		}
		
		$this->CI->db->where(array("theme_name" => $theme_name));
		$data_theme = $this->CI->db->get("system_themes")->row_array();
		
		if(!isset($data_theme['theme_id']))
		{
			return false;
		}
		
		$header_str = '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');?>
<?php
$this->load->view("header");
?>';
		$footer_str = '
<?php
$this->load->view("footer");
?>';

		if(is_array($templates) and count($templates) > 0)
		{
			$dashboard_content = "";
			$dashboard_file_name = "";
			
			$class_widgets = $this->CI->widgets->get_widgets();
			
			foreach($templates as $i => $t)
			{
				if($i == "index.php")
				{
					continue;
				}
				$file_template = file_get_contents($path."/".$i);
				$file_template = str_replace('"assets/','"'.current_theme_url().'assets/theme/',$file_template);
				$file_template = str_replace('\'assets/','\''.current_theme_url().'assets/theme/',$file_template);
				$body_content = $this->get_content_template($file_template);
				$file_name = $t['configs']['data']['template_name'];
								
				$this->CI->db->where(array("template_name" => $file_name/*,"theme_id" => $data_theme['theme_id']*/));
				$data_template = $this->CI->db->get("system_templates")->row_array();
				
				if(empty($body_content))
					continue;
				$body_content_dom = phpQuery::newDocumentHTML("<body>".$body_content."</body>");
				$container_htmls = $body_content_dom['.container'];
				
				$container_html_output = "";
				
				$no_con = 0;
				foreach($container_htmls as $con => $h)
				{
					$rows_htmls = pq('.row',$h);
				
					$data_block_containers = array(
												 //"theme_id" => $data_theme['theme_id'],
												 "template_id" => $data_template['template_id'],
												 "group_name" => "container-" . $data_theme['theme_id'] . "-" . $data_template['template_id'] . "-" . $con,
												 "group_title" => "Container " . $data_theme['theme_id'] . " " . $data_template['template_id'] . " " . $con,
												 "sort_order" => $no_con
												 );
					
					$data_block_containers_where = $data_block_containers;
					$this->CI->db->where($data_block_containers);
					$q_check = $this->CI->db->get("system_page_containers")->num_rows();
					
					$page_container_id = "";
					$page_col_id = "";
					
					$data_block_container = "";
					if($q_check == 0)
					{
						$this->CI->db->insert("system_page_containers",$data_block_containers);
						$page_container_id = $this->CI->db->insert_id();
						
						$this->CI->db->where(array("page_container_id" => $page_container_id));
						$this->CI->db->update("system_page_containers",array("content" => pq($h)->clone()->attr("data-container-id",$page_container_id)->html("<bscontainer id='".$page_container_id."'/>")->htmlOuter()));
						
						$this->CI->db->where(array("page_container_id" => $page_container_id));
						$q_get = $this->CI->db->get("system_page_containers")->row_array();
						$data_block_container = $q_get;
					}else{
						$this->CI->db->where($data_block_containers_where);
						$q_get = $this->CI->db->get("system_page_containers")->row_array();
						$data_block_container = $q_get;
						$page_container_id = $q_get['page_container_id'];
					}
					
					$no_rows = 0;
					foreach($rows_htmls as $rows => $row)
					{
						$cols_htmls = pq('[class^="col-"]',$row);
						$cols_others_htmls = pq('*',$row);
						
						$data_blocks = array(
													 //"theme_id" => $data_theme['theme_id'],
													 "template_id" => $data_template['template_id'],
													 "page_container_id" => $page_container_id,
													 "page_row_name" => "row-" . $data_theme['theme_id'] . "-" . $data_template['template_id'] . "-" . $rows,
													 "page_row_title" => "Row " . $data_theme['theme_id'] . " " . $data_template['template_id'] . " " . $rows,
													 "sort_order" => $no_rows
													 );
						$data_blocks_where = $data_blocks;
						
						$this->CI->db->where($data_blocks);
						$q_check = $this->CI->db->get("system_page_rows")->num_rows();
						
						$page_row_id = 0;
						$data_block = "";
						
						if($q_check == 0)
						{
							$this->CI->db->insert("system_page_rows",$data_blocks);
							$page_row_id = $this->CI->db->insert_id();
							
							$this->CI->db->where(array("page_row_id" => $page_row_id));
							$this->CI->db->update("system_page_rows",array("content" => pq($row)->clone()->attr("data-row-id",$page_row_id)->html("<bsrow id='".$page_row_id."'/>")->htmlOuter()));

							$this->CI->db->where(array("page_row_id" => $page_row_id));
							$data_block = $this->CI->db->get("system_page_rows")->row_array();

						}else{
							$this->CI->db->where($data_blocks);
							$q_get = $this->CI->db->get("system_page_rows")->row_array();
							$page_row_id = $q_get['page_row_id'];
							
							$this->CI->db->where(array("page_row_id" => $page_row_id));
							$data_block = $this->CI->db->get("system_page_rows")->row_array();
						}
					
						if($cols_htmls->count() > 0)
						{
							$no_cols = 0;
							foreach($cols_htmls as $cols => $col)
							{
								$template_parsed = 0;
								
								if(is_array($class_widgets) and count($class_widgets) > 0)
								{
									foreach($class_widgets as $i => $class_widget)
									{
										$data_widget = $class_widget['data'];
										$the_class_widget = $class_widget['class'];
										
										if(method_exists($the_class_widget,"parse_template"))
										{
											$do_parse_template = $the_class_widget->parse_template($col,$data_theme,$data_template,$data_block_container,$data_block,$no_cols);
											$template_parsed = ($do_parse_template)?1:$template_parsed;
										}
										
									}
								}
								
								if($template_parsed == 0)
								{
									if(isset($class_widgets['custom_text']))
									{
										$class_widget = $class_widgets['custom_text'];
										
										$data_widget_custom_text = (isset($class_widget['data']))?$class_widget['data']:"";
										$the_class_widget_custom_text = (isset($class_widget['class']))?$class_widget['class']:"";
										$do_parse_template = $the_class_widget_custom_text->parse_template($col,$data_theme,$data_template,$data_block_container,$data_block,0);
									}
								}
								$no_cols++;
							}
						}else
						{
							if($cols_others_htmls->count() > 0)
							{								
								$template_parsed = 0;
								
								if(is_array($class_widgets) and count($class_widgets) > 0)
								{
									if(is_array($class_widgets) and count($class_widgets) > 0)
									{
										foreach($class_widgets as $i => $class_widget)
										{
											$data_widget = $class_widget['data'];
											$the_class_widget = $class_widget['class'];
											
											if(method_exists($the_class_widget,"parse_template"))
											{
												$do_parse_template = $the_class_widget->parse_template($row,$data_theme,$data_template,$data_block_container,$data_block,0);
												$template_parsed = ($do_parse_template)?1:$template_parsed;
											}
										}
									}
								}
								if($template_parsed == 0)
								{
									if(isset($class_widgets['custom_text']))
									{
										$class_widget = $class_widgets['custom_text'];
										
										$data_widget_custom_text = (isset($class_widget['data']))?$class_widget['data']:"";
										$the_class_widget_custom_text = (isset($class_widget['class']))?$class_widget['class']:"";
										$do_parse_template = $the_class_widget_custom_text->parse_template($row,$data_theme,$data_template,$data_block_container,$data_block,0);
									}
								}
							}
							
						}
						$no_rows++;
					}
			
					$no_con++;
				}
			
				
				$container_html_output = '<?php echo $this->page_templates->show_containers(((isset($template[\'template_id\']))?$template[\'template_id\']:"'.$data_template['template_id'].'"));?>';
				$file_content = $header_str;
				$file_content .= $container_html_output;
				$file_content .= $footer_str;
				$file = fopen($path."/cache/".$file_name.".php","w+");
				fwrite($file,$file_content);
				fclose($file);
				
				@chmod($path."/cache/".$file_name.".php",0777);
				
				$dashboard_file_name = ($dashboard_file_name == "" and $file_name == "index")?$file_name:$dashboard_file_name;
				if($file_name == "index" or $file_name == "dashboard")
				{
					$dashboard_content = $file_content;
				}
				
				//rename($path."/".$i, $path."/tmp/".$i);
			}
			
			if(!empty($dashboard_content))
			{
				$file = fopen($path."/cache/dashboard.php","w+");
				fwrite($file,$dashboard_content);
				fclose($file);
				@chmod($path."/cache/dashboard.php",0777);
			}
			
			$templates =  $this->fetch_templates($path."/cache/",".php");
			
			if(is_array($templates) and count($templates) > 0)
			{
				foreach($templates as $i => $t)
				{
					rename($path."/cache/".$i, $path."/".$i);
				}
			}
		}
		
		return $templates;
	}
	
	function show_container($container_id = "",$template_id = "",$theme_id = "")
	{
		global $assets_loaded;
		#$data_container = $this->CI->db->where(array("page_container_id" => $container_id))->get("system_page_containers")->row_array();
		#$page_container_id = (isset($data_container['page_container_id']))?$data_container['page_container_id']:0;
		
		$page_container_id = $container_id;
		$data_blocks = $this->CI->db->where(array("page_container_id" => $page_container_id))->order_by("sort_order ASC")->get("system_page_rows")->result_array();

		$output = "";
		if(is_array($data_blocks) and count($data_blocks) > 0)
		{
			foreach($data_blocks as $i => $data_block)
			{
				$page_row_id =  (isset($data_block['page_row_id']))?$data_block['page_row_id']:0;
				
				$data_page_cols = $this->CI->db->where(array("page_row_id" => $page_row_id))->order_by("sort_order ASC")->get("system_page_cols")->result_array();

				$widgets_output = "";
				if(is_array($data_page_cols) and count($data_page_cols) > 0)
				{
					foreach($data_page_cols as $i2 => $page_col)
					{
						$widgets_col = $page_col['content'];
						$show_widget = $this->CI->widgets->show_widgets($page_col);
						$widgets_output_cols = str_replace('<bscol id="'.$page_col['page_col_id'].'"></bscol>',$show_widget,$widgets_col);
						$widgets_output .= $widgets_output_cols;
					}
				}
				
				$widgets_row = $data_block['content'];
				$widgets_output_rows = str_replace('<bsrow id="'.$data_block['page_row_id'].'"></bsrow>',$widgets_output,$widgets_row);
				$output .= $widgets_output_rows;
				
			}
		}
		
		return $output;
	}
	
	function show_containers($page = array())
	{
		global $assets_loaded;
		$data_theme = $this->CI->db
								->where(array("status" => "active"))
								->get("system_themes")
								->row_array();
		$data_page_template = $this->CI->db
								->where(array("page_id" => (isset($page['user_page_id'])?$page['user_page_id']:'-1')))
								->get("system_page_templates")
								->row_array();
		$data_template = $this->CI->db
								->where(array("template_id" => ((isset($data_page_template['template_id']))?$data_page_template['template_id']:'-1')))
								->get("system_templates")
								->row_array();
		$data_block_containers = $this->CI->db
											->where(array("object_id" => (isset($page['user_page_id'])?$page['user_page_id']:'-1')))
											->order_by("sort_order ASC")
											->get("system_page_containers")
											->result_array();
		
		$assets_loaded = array();
		if(is_array($data_page_template) and count($data_page_template) > 0 and is_array($data_block_containers) and count($data_block_containers) > 0 and is_array($data_template) and count($data_template) > 0)
		{
			foreach($data_block_containers as $i => $container)
			{
				$content_container = $this->show_container($container['page_container_id'],$data_template['template_id'],$data_theme['theme_id']);
				$output_container = str_replace('<bscontainer id="'.$container['page_container_id'].'"></bscontainer>',$content_container,$container['content']);
				echo $output_container;
			}
		}
	}
	
	function get_content_template($string = "", $tagname="body",$pattern = "#<{{tagname}}\b[^>]*>(.*?)</{{tagname}}\b[^>]*>#s",$no_index = 1)
	{
		$pattern = str_replace("{{tagname}}",$tagname,$pattern);
		preg_match($pattern, $string, $matches);
		return (isset($matches[$no_index]))?$matches[$no_index]:"";
	}
	
	function sortorder_container($data_containers = array(),$template_id = 0)
	{
		$output = false;
		if(is_array($data_containers) and count($data_containers) > 0 and (int) $template_id > 0)
		{
			foreach($data_containers as $i => $c)
			{
				$where = array(	//"theme_id" => $theme_id,
								"template_id" => $template_id,
								"page_container_id" => $c
							  );
		
				$this->CI->db->where($where);
				$output = $this->CI->db->update("system_page_containers",array("sort_order" => $i));
			}
		}
		return $output;
	}
	
	function sortorder_all($data_containers = array(),$template_id = 0)
	{
		$output = false;
		if(is_array($data_containers) and count($data_containers) > 0 and (int) $template_id > 0)
		{
			foreach($data_containers as $i => $c)
			{
				$where = array(	//"theme_id" => $theme_id,
								"template_id" => $template_id,
								"page_container_id" => $c['con_id']
							  );
		
				$this->CI->db->where($where);
				$output = $this->CI->db->update("system_page_containers",array("sort_order" => $i));
				
				$data_rows = (isset($c['children']) and is_array($c['children']) and count($c['children']) > 0)?$c['children']:array();
				if(is_array($data_rows) and count($data_rows) > 0)
				{
					foreach($data_rows as $irow => $crow)
					{
						$where = array(	//"theme_id" => $theme_id,
										"template_id" => $template_id,
										"page_row_id" => $crow['row_id']
									  );
				
						$this->CI->db->where($where);
						$q_insert_row = $this->CI->db->update("system_page_rows",array("page_container_id" => $c['con_id'],"sort_order" => $irow));
						
								
						$data_block_cols = (isset($crow['children']) and is_array($crow['children']) and count($crow['children']) > 0)?$crow['children']:array();
						if(is_array($data_block_cols) and count($data_block_cols) > 0)
						{
							foreach($data_block_cols as $iblock_col => $cblock_col)
							{
								$where = array(	//"theme_id" => $theme_id,
												"template_id" => $template_id,
												"page_col_id" => $cblock_col['block_col_id']
											  );
						
								$this->CI->db->where($where);
								$q_insert_block_col = $this->CI->db->update("system_page_cols",array(
																									"page_container_id" => $c['con_id'],
																									"page_row_id" => $crow['row_id'],
																									"sort_order" => $iblock_col));
							}
						}
					}
				}
			}
		}
				
		return $output;
	}
	
	function create_container($template_id = 0,$object_id = 0,$content = "")
	{
		$output = false;
		if((int) $template_id > 0 and (int) $object_id > 0)
		{
			$where = array(	//"theme_id" => $theme_id,
							"template_id" => $template_id
						  );
						  
			$last_data = $this->CI->db->order_by("page_container_id DESC")->get("system_page_containers")->row_array();
			$last_sort_data = $this->CI->db->order_by("sort_order DESC")->where($where)->get("system_page_containers")->row_array();
			$data_template = $this->CI->db->where($where)->get("system_templates")->row_array();
			
			$sort_order = (isset($last_sort_data['sort_order']))?$last_sort_data['sort_order']:0;
			$last_page_container_id = (isset($last_data['page_container_id']))?$last_data['page_container_id']+1:0;
			$template_name = (isset($data_template['template_name']))?$data_template['template_name']:$template_id;
			$group_title = "Kontainer ".($sort_order+1)." Template ".$template_name;
			$group_name = url_title($group_title,"_",true)."_".$sort_order;
			
			$content = ' ';
			
			$data_container = array(
									"object_id" => $object_id,
									"template_id" => $template_id,
									"content" => $content,
									"group_title" => $group_title,
									"group_name" => $group_name,
									"sort_order" => $sort_order
								  );
			$q_insert = $this->CI->db->insert("system_page_containers",$data_container);
			$last_insert_id = $this->CI->db->insert_id();
			
			$content = '<div class="container" data-container-id="'.$last_insert_id.'"><bscontainer id="'.$last_insert_id.'"></bscontainer></div>';
			$this->CI->db->where(array("page_container_id" => $last_insert_id));
			$this->CI->db->update("system_page_containers",array("content" => $content));
			$output = $content;
		}
		return $output;
	}
	
	function create_row($template_id = 0,$con_id = 0,$content = "")
	{
		$output = false;
		if((int) $template_id > 0 and (int) $con_id > 0)
		{
			$where = array(	//"theme_id" => $theme_id,
							"template_id" => $template_id
						  );
			
			$where_block = array(	//"theme_id" => $theme_id,
									"template_id" => $template_id,
									"page_container_id" => $con_id
								  );
			$last_data = $this->CI->db->order_by("page_row_id DESC")->get("system_page_rows")->row_array();
			$last_sort_data = $this->CI->db->order_by("sort_order DESC")->where($where_block)->get("system_page_rows")->row_array();
			$data_template = $this->CI->db->where($where)->get("system_templates")->row_array();
			
			$sort_order = (isset($last_sort_data['sort_order']))?$last_sort_data['sort_order']:0;
			$last_page_row_id = (isset($last_data['page_row_id']))?$last_data['page_row_id']+1:0;
			$template_name = (isset($data_template['template_name']))?$data_template['template_name']:$template_id;
			$block_title = "Baris ".($sort_order+1)." Template ".$template_name;
			$block_name = url_title($block_title,"_",true)."_".$sort_order;
			
			$content = ' ';
			
			$data_row = array(
									//"theme_id" => $theme_id,
									"template_id" => $template_id,
									"page_container_id" => $con_id,
									"content" => $content,
									"page_row_title" => $block_title,
									"page_row_name" => $block_name,
									"sort_order" => $sort_order
								  );
			$q_insert = $this->CI->db->insert("system_page_rows",$data_row);
			$last_insert_id = $this->CI->db->insert_id();
			$content = '<div class="row" data-row-id="'.$last_insert_id.'"><bsrow id="'.$last_insert_id.'"></bsrow></div>';
			$this->CI->db->where(array("page_row_id" => $last_insert_id));
			$output = $this->CI->db->update("system_page_rows",array("content" => $content));
			$output =  $content;
		}
		return $output;
	}
	
	function create_column($template_id = 0,$con_id = 0,$row_id = 0,$number_coloumns = 1,$block_title = "")
	{
		$return = false;
		$content_output = "";
		if((int) $template_id > 0 and (int) $con_id > 0 and (int) $row_id >= 0)
		{
			$class_col = (is_numeric($number_coloumns))?(12 / $number_coloumns):$number_coloumns;
			$tag_column = array();
			if($number_coloumns == 'left_bar')
			{
				$tag_column[0] = '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-block-item-id="#id_block_col#"><bscol id="#id_block_col#"></bscol></div>';
				$tag_column[1] = '<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" data-block-item-id="#id_block_col#"><bscol id="#id_block_col#"></bscol></div>';
			}elseif($number_coloumns == 'right_bar')
			{
				$tag_column[0] = '<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" data-block-item-id="#id_block_col#"><bscol id="#id_block_col#"></bscol></div>';
				$tag_column[1] = '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" data-block-item-id="#id_block_col#"><bscol id="#id_block_col#"></bscol></div>';
			}else{
				for($i = 0;$i < $number_coloumns; $i++)
				{
					$tag_column[] = '<div class="col-lg-'.$class_col.' col-md-'.$class_col.' col-sm-12 col-xs-12" data-block-item-id="#id_block_col#"><bscol id="#id_block_col#"></bscol></div>';
				}
			}
			
			if(is_array($tag_column) and count($tag_column) > 0)
			{
				$block_name = url_title($block_title,"-",true);
				
				$last_sort_order = array(	//'theme_id' => $theme_id,
											'template_id' => $template_id,
											'page_container_id' => $con_id,
											'page_row_id' => $row_id
											);
				$last_order = $this->CI->db->where($last_sort_order)->order_by("sort_order DESC")->get("system_page_cols")->row_array();
				foreach($tag_column as $i => $c)
				{
					$sort_order = (isset($last_order['sort_order']))?$last_order['sort_order']+1:0;
					$data_col = array(	//'theme_id' => $theme_id,
										'template_id' => $template_id,
										'page_container_id' => $con_id,
										'page_row_id' => $row_id,
										//'block_name' => $block_name,
										//'block_title' => $block_title,
										'content' => $c,
										'sort_order' => $sort_order
										);
					$q_insert = $this->CI->db->insert("system_page_cols",$data_col);
					$insert_id = $this->CI->db->insert_id();
					$content = str_replace('#id_block_col#',$insert_id,$c);
					$this->CI->db->where(array("page_col_id" => $insert_id));
					$this->CI->db->update("system_page_cols",array('content' => $content));
					$content_output .= $content;
					$return = ($q_insert)?true:$return;
				}
			}
			$return = $content_output;
		}
		return $return;
	}
	
	function append_widget($template_id = 0,$con_id = 0,$row_id = 0,$page_col_id = 0,$widget_id = 0,$widget_content_id = 0)
	{
		$content = false;
		if((int) $template_id > 0 and (int) $con_id > 0 and (int) $row_id > 0 and (int) $page_col_id > 0 and (int) $widget_id > 0)
		{
			$this->CI->db->where(array("page_col_id" => $page_col_id));
			$q_insert = $this->CI->db->update("system_page_cols",array("widget_id" => $widget_id,"widget_content_id" => $widget_content_id));
			if($q_insert)
			{
				$block_col_data = array("widget_id" => $widget_id,"widget_content_id" => $widget_content_id);
				$content = $this->CI->widgets->show_widgets($block_col_data);
			}
		}
		return $content;
	}
}

