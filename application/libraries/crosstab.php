<?php

/*******************************************************

Class to display mysql data in cross-tabular format.
Made By member - http://www.naukri.com tech team.
On April 20, 2002
Please go through the Readme.txt file provided with this pack.

*******************************************************/

class crosstab {

var $CI;
var $rowfield;
var $rowfieldalias;
var $columnfield;
var $columnfieldalias;
var $reffield;
var $reffieldalias;
var $crosstable;
var $row;
var $column;
var $element;

function crosstab(){
	$this->CI =& get_instance();
}

function init($row_field,$column_field,$ref_field,$cross_table){
	$this->CI =& get_instance();
	$this->rowfield=$row_field;
	$this->rowfieldalias=$row_field;
	$this->columnfield=$column_field;
	$this->columnfieldalias=$column_field;
	$this->reffield=$ref_field;
	$this->reffieldalias=$ref_field;
	$this->crosstable=$cross_table;
	
	#$this->rowfieldalias = end(explode(".",$this->rowfield));
	#$this->columnfieldalias = end(explode(".",$this->columnfield));
	#$this->reffieldalias = end(explode(".",$this->reffield));
}


function get()
{
	if(empty($this->rowfield) or empty($this->rowfieldalias) or empty($this->columnfield) or empty($this->columnfieldalias) or empty($this->reffield) or empty($this->crosstable))
		return false;
	$sqlrow="SELECT  distinct $this->rowfield as ROW from $this->crosstable ORDER BY $this->rowfield ASC";
	$sqlcolumn="SELECT distinct $this->columnfield as COLUMN1 from $this->crosstable ORDER BY $this->columnfield ASC";
	$sqlelement="SELECT count($this->reffield) as CROSSVALUE,$this->rowfield as $this->rowfieldalias,$this->columnfield as $this->columnfieldalias from $this->crosstable group by $this->rowfield,$this->columnfield";

	$this->row = $this->CI->db->query($sqlrow)->result_array();
	$this->column = $this->CI->db->query($sqlcolumn)->result_array();
	$element = $this->CI->db->query($sqlelement)->result_array();	
	
	foreach($element as $index => $welement)	{
		$this->element[$welement[$this->rowfieldalias]][$welement[$this->columnfieldalias]]=$welement['CROSSVALUE'];
	}
}

function set($col=array(),$row=array(),$ref=array())
{
	
}

function show()
{	
	
	if(empty($this->rowfield) or empty($this->rowfieldalias) or empty($this->columnfield) or empty($this->columnfieldalias) or empty($this->reffield) or empty($this->crosstable))
		return false;
	
	$ttotal = 0;
	$total = array();
	echo "<table border=1><thead><tr><th>Field</th>";
	for ($n=0;$n<=(count($this->column)-1);$n++){
		echo "<th align='center'>".$this->column[$n]['COLUMN1']."&nbsp;</th>";
	}
		echo "<th align='center'>Total</th>";
	echo "</tr></thead><tbody>";
	
	foreach($this->element as $key => $the_rowfield)
	{
		echo "<tr>";
		echo "<td nowrap='nowrap'><b>".$key."</b>&nbsp;</td>";
		$totalperrow = 0;
		foreach($this->column as $n => $col){
			if(isset($the_rowfield[$col['COLUMN1']]))
			{
				$total[$n] 	= 	((isset($total[$n]))?$total[$n]:0) + $the_rowfield[$col['COLUMN1']];
				$ttotal 	=	$ttotal+$the_rowfield[$col['COLUMN1']];
				$totalperrow += $the_rowfield[$col['COLUMN1']];
				echo "<td align='center'>".$the_rowfield[$col['COLUMN1']]."</td>";
			}else{
				echo "<td align='center'>0</td>";
			}
		}
		echo "<td align='center'><b>".$totalperrow."</b></td>";
		echo "</tr>";
	}
	
	echo "<tr>";
	echo "<td align='center'><b>Total</b></td>";
	for ($n=0;$n<=(count($this->column)-1);$n++){
		echo "<td align='center'><b><font size=-1>".$total[$n]."</font></b></td>";
	}
	echo "<td align='center'><b>".$ttotal."</b></td></tr>";
	
	echo "<tr><td colspan='".count($this->column)."'><b>Total Records $ttotal</b></td></tr";
	echo "</tbody></table>";
	
	
	
	
// End of method	
}


// End of Class
}

?>
