<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Basic_widget{

  var $name = '';
  var $title = '';
  var $description = '';
  var $author = '';
  var $version = '';
  var $CI;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
	  return true;
  }
  
  function uninstall()
  {
	  return true;
  }
  
  function query_element_template($block_html = "")
  {
	  
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col="0")
  {
	return "";
  }
  
  public function show_widget($data_widget="")
  {
	return "";
  }
  
  function show_assets_widget($data_widget="")
  {
  }
  
  function init($data_widget="")
  {
    
  }
  
}
