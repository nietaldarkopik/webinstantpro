<?php

class Filter_input_datetime{
  var $CI;
  var $form_field = "";
  var $assets = "";
  function Filter_input_datetime()
  {
		$this->CI =& get_instance();
  }

  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "text";
    
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control addon_input_datetime';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= '  <div class="row">
                    <br/><div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
    $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($field_attributes));
                  $form = 
                  '<div class="input-group">
                    '.$form.'
                    '.$error.'
                    '.
                    (($star_required == 1)?'
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                    </span>':'')
                    .
                    ((!empty($stardesc_required))?'
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-warning-sign"></span>
                      '.$stardesc_required.'
                    </span>':'')
                    .'
                    <span class="input-group-addon">
                      <span class="glyphicon">&nbsp;</span>
                    </span>
                  </div>';
    $this->form_field = $form;
    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">'.$form.'
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';      
    $this->config_field = array();
    $this->output_field = $output;
    
    $assets_head['css'] = '<link href="'.current_admin_theme_url().'static/js/jquery/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />';
    $assets_head['js'] =  "";
    $assets_head['css_inline'] =  "";
    $assets_head['js_inline'] =  "";
    
    $assets_body['css'] =  '';
    $assets_body['js'] =  '<script type="text/javascript" src="'.current_admin_theme_url().'static/js/jquery-ui-timepicker-addon.js"></script>';
    $assets_body['css_inline'] =  "";
    $assets_body['js_inline'] =  "$(document).ready(function(){
                                      $('.addon_input_datetime').datetimepicker({
                                        dateFormat: 'yy-mm-dd',
                                        timeFormat: 'HH:mm:ss'
                                      });
                                    });";
    
    $this->assets['head'] = $assets_head;
    $this->assets['body'] = $assets_body;
  }
  
  function output()
  {
    return $this->output_field;
  }
  
  function get_assets()
  {
    return $this->assets;
  }
}

