<?php

class Filter_input_radio{
  var $CI;
  var $form_field = "";
  function Filter_input_radio()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
      $output = "";
      $value = "";
      $type = "";
      $star_required = "";
      $name = "";
      $label = (isset($field['label']))?$field['label']:$field;
      $name = (isset($field['name']))?$field['name']:$field;
      $id = (isset($field['id']))?$field['id']:$name;
      $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
      $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
      $size = (isset($field['size']))?$field['size']:"";
      $style = (isset($field['style']))?$field['style']:"";
      $class = (isset($field['class']))?$field['class']:"input_".$type;
      $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
      $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
      $rules = (isset($field['rules']))?$field['rules']:"";
      $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
      
      $data = array(
        'name'        => $input_name,
        'id'          => $id,
        'value'       => $value,
        'maxlength'   => $maxlength,
        'size'        => $size,
        'style'       => $style,
        'type'       => $type,
        'class'		=> $class
      );
      
      $desc = "";
      $error = form_error($input_name);
      if($type == "file")
      {
        $this->CI->load->library('upload');
        if(isset($field['config_upload']) and isset($field['config_upload']['allowed_types']) and !empty($field['config_upload']['allowed_types']))
        {
          $format_file = str_replace("|",",",$field['config_upload']['allowed_types']);
          $desc .= "<span class='desc_field'><i>Format file : (".$format_file.")</i></span>";
        }
        $error = $this->CI->upload->display_errors('<p class="error">','</p>');
      }

      $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
      $output .= '<label for="'. $name .'">'. $label .'</label>';
      if($type == "textarea")
      {
        $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_textarea($data));
        $output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
      }else{
        $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($data));
        $output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
      }  
  }
  
  function output()
  {
    $action = '';
    $form_title = '';
    $nonce = '';
    $ajax_target = '';
    $button_action = '';
    $output = '';
    $output .= '
    		<div class="row">
				  <br/><div class="col-lg-3">
					<div class="input-group">
						<label for="user_name">Pilih Radiobutton</label>
					</div>
				  </div>
				  <div class="col-lg-9">
						<div class="input-group">
						  <span class="input-group-addon">
							<input type="radio">
						  </span>
						  <span class="form-control">Jawaban 1</span>
						</div>
						<br/>
						<div class="input-group">
						  <span class="input-group-addon">
							<input type="radio">
						  </span>
						  <span class="form-control">Jawaban 2</span>
						</div>
						<br/>
					</div>
				</div>';
    return $output;
  }
}

				
