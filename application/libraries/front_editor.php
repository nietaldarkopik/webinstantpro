<?php
class Front_editor{
	var $CI;
	var $hooks = array();
	var $plugins = array();

	function Front_editor(){
		$this->CI =& get_instance();
	}

	function setAdminAssets()
	{
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
			$css = '<link rel="stylesheet" href="'.base_url().'assets/core/css/bootstrap-theme-admin.css">';
			$this->CI->assets->add_css($css,"head");
			$js = '<script type="text/javascript" src="'.base_url().'assets/core/js/script.js"></script>';
					
			$this->CI->assets->add_js($js,"body");
			
			if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php'))
			{
			  require_once(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php');
			  $this->CI->load->set_object_path('view',array(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/templates/'	=> 1),false);
			  #$this->CI->assets->reset();
			  $theme_config = CURRENT_ADMIN_THEME;
			  $theme_loader = new $theme_config();
			  $theme_loader->setFrontAdminAssets();
			}
			

			if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/templates/components/admin-menu-frontend.php'))
			{
				
				$this->CI->load->view('components/admin-menu-frontend');

			}
		}
	}
}
