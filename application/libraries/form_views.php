<?php

class Form_views{
	var $CI;
	
	function Form_views(){
		$this->CI =& get_instance();
	}
  
	function get_fields()
	{
		$controllers    = array();

        $dir            = APPPATH.'/libraries/form_views/';
        $files          = scandir($dir);

        $controller_files = array_filter($files, function($filename) {
            return (substr(strrchr($filename, '.'), 1)=='php') ? true : false;
        });

        foreach ($controller_files as $filename)
        {
            require_once(APPPATH.'/libraries/form_views/'.$filename);

            $classname = ucfirst(substr($filename, 0, strrpos($filename, '.')));
            $controller = new $classname();
            $methods = get_class_methods($controller);

            foreach ($methods as $index => $method)
            {
							if((strpos($method,'_') <= 0 and strpos($method,'_') === 0) or $method == 'get_instance')
							{
								unset($methods[$index]);
							}
            }

            $controller_info = array(
                'filename' => $filename,
                'class_name' => $classname,
                'methods'  => $methods
            );
            array_push($controllers,$controller_info);
        }
				return $controllers;
	}
  
  function get_field_names()
  {
    $fields = $this->get_fields();
    $field_names = array();
    foreach($fields as $i => $d)
    {
      $field_names[] = strtolower($d['class_name']);
    }
    return $field_names;
  }
  
  function field($type = "",$config = array())
  {
    $the_input_type = new $type;
    $the_input_type->config($config);
    $output = $the_input_type->output();
    $output = $this->CI->hook->do_action('hook_form_field_output',$output);
    return $output;
  }
}
