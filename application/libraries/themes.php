<?php
class Themes{
	var $CI;
	var $hooks = array();
	var $plugins = array();
	var $current_theme = array();
  
	function Themes(){
		$this->CI =& get_instance();
		$check_activate = $this->CI->db->query("SELECT * FROM system_themes WHERE status = 'active'")->row_array();
		$current_theme = (isset($check_activate['theme_name']))?$check_activate['theme_name']:'blank';
		$this->current_theme = $check_activate;
		
		define('CURRENT_THEME', $current_theme);
	}
	
	function install($plugin_name = "",$widget_data = array())
	{
		$where = array();
		$where['plugin_name'] = $plugin_name;
		$where['widget_name']  = $widget_data['widget_name'];

		$data = array();
		$data['plugin_name'] = $plugin_name;
		$data['widget_name']  = $widget_data['widget_name'];
		$data['widget_title'] = $widget_data['widget_title'];
		$data['description']  = $widget_data['description'];
		$data['status']  = 'publish';
		$data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
		$data['author']  = $widget_data['author'];
		$data['version']  = $widget_data['version'];
		$this->CI->db->where($where);
		$q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);

		if($q_check_widget->num_rows() == 0)
		{
		  $q = $this->CI->db->insert(TABLE_SYS_WIDGETS,$data);
		  if($q)
		  {
			return 1;
		  }else{
			return 0;
		  }
		}else{
		  return -1;
		}
	}

	function update($plugin_name = "",$widget_data = array())
	{
		$where = array();
		$where['plugin_name'] = $plugin_name;
		$where['widget_name']  = $widget_data['widget_name'];

		$data = array();
		$data['plugin_name'] = $plugin_name;
		$data['widget_name']  = $widget_data['widget_name'];
		$data['widget_title'] = $widget_data['widget_title'];
		$data['description']  = $widget_data['description'];
		$data['widget_data']  = (isset($widget_data['widget_data']) and is_array($widget_data['widget_data']))?json_encode($widget_data['widget_data']):json_encode($widget_data);
		$data['author']  = $widget_data['author'];
		$data['version']  = $widget_data['version'];
		$this->CI->db->where($where);
		$q_check_widget = $this->CI->db->get(TABLE_SYS_WIDGETS);

		if($q_check_widget->num_rows() == 0)
		{
		  $q = $this->install($plugin_name,$widget_data);
		  if($q)
		  {
			return 1;
		  }else{
			return 0;
		  }
		}else{
		  $this->CI->db->where($where);
		  $q = $this->CI->db->update(TABLE_SYS_WIDGETS,$data);
		  if($q)
		  {
			return 1;
		  }else{
			return -1;
		  }
		}
	}

	function fetch_themes($path = "")
	{
		$path            = rtrim($path,"/")."/";
		$dir            = rtrim($path,"/")."/";
		$url            = str_replace("./","",$path);
		$url            = str_replace(FCPATH,"",$url);
		$files          = scandir($dir);
		
		$list_themes = array_filter($files, function($filename) use ($dir) {
		  return (is_dir($dir . "/" . $filename) and $filename != '.' and $filename != '..') ? true : false;
		});
		
		$themes = array();
		if(is_array($list_themes) and count($list_themes) > 0)
		{
		  $this->CI->load->library('parser');
		  foreach($list_themes as $i)
		  {
			$config = $this->fetch_theme($path,$i);
			if(!$config)
				continue;
			$themes[$i] = $config;
		  }
		}
		return $themes;
	}
	
	function fetch_theme($path = "",$theme_name = "")
	{
		
		$path            = rtrim($path,"/")."/";
		$dir            = rtrim($path,"/")."/";
		$url            = str_replace("./","",$path);
		$url            = str_replace(FCPATH,"",$url);
		$files          = scandir($dir);
		
		$config = array();
		if(function_exists('simplexml_load_file') and file_exists($path.$theme_name.'/configs/'.$theme_name.'.xml'))
		{
		  $config = simplexml_load_file($path.$theme_name.'/configs/'.$theme_name.'.xml');
		  $config = (array) $config;
		}else{
		  if(file_exists($path.$theme_name.'/configs/'.$theme_name.'.php') and !class_exists(ucwords($theme_name)))
			require_once($path.$theme_name.'/configs/'.$theme_name.'.php');
		}
		
		if(is_array($config) and count($config) == 0)
		  return false;
		  
		$config['data']['screenshot'] = '';
		if(file_exists($path.$theme_name.'/screenshot.gif'))
		  $config['data']['screenshot'] = $url.$theme_name.'/screenshot.gif';
	  
		$config = array("configs" => $config);
		return $config;
	}
}
