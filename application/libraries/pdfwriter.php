<?php

class Pdfwriter{
	var $CI;
	var $inputfilename;
	var $html;
	var $objreader;
	
	function __construct(){
		$this->CI =& get_instance();
		$this->objphpexcel = new PHPExcel();
	}
  
  function set_html($html = "")
  {
    $this->html = $html;
  }
  
  function download($file_name = "",$paper = "A4",$orientation = "landscape")
  {
    if(!class_exists('TCPDF'))
    {
      require_once(APPPATH."libraries/tcpdf/tcpdf_include.php");
    }

    #header('Content-Type: application/pdf; charset=utf-8');
    #header('Content-Disposition: attachment;filename="'.$file_name.'.pdf"');
    #header('Cache-Control: max-age=0');
    $file_name = (empty($file_name))?'pdf-'.date("Y-m-d"):$file_name;
    $pdf = new TCPDF($orientation, 'pt', $paper);
    $pdf->setFontSubsetting(FALSE);
    //    Set margins, converting inches to points (using 72 dpi)
    #$pdf->SetMargins($printMargins->getLeft() * 72, $printMargins->getTop() * 72, $printMargins->getRight() * 72);
    #$pdf->SetAutoPageBreak(TRUE, $printMargins->getBottom() * 72);

    $pdf->setPrintHeader(FALSE);
    $pdf->setPrintFooter(FALSE);

    $pdf->AddPage();

    //  Set the appropriate font
    #$pdf->SetFont($this->getFont());
    $pdf->SetFont('times', '', 10);
    $pdf->writeHTML($this->html);

    //  Document info
    #$pdf->SetTitle($this->_phpExcel->getProperties()->getTitle());
    #$pdf->SetAuthor($this->_phpExcel->getProperties()->getCreator());
    #$pdf->SetSubject($this->_phpExcel->getProperties()->getSubject());
    #$pdf->SetKeywords($this->_phpExcel->getProperties()->getKeywords());
    #$pdf->SetCreator($this->_phpExcel->getProperties()->getCreator());

    //  Write to file
    #fwrite($fileHandle, $pdf->output($pFilename, 'S'));
    //Close and output PDF document
    $pdf->Output($file_name.'.pdf', 'I');
  }

  function mpdf($file_name = "",$paper = "A4",$orientation = "landscape")
  { 
    if(!class_exists('mPDF'))
    {
      require_once(APPPATH."libraries/mpdf60/mpdf.php");
    }
    $file_name = (empty($file_name))?'pdf-'.date("Y-m-d"):$file_name;
    $mpdf=new mPDF(); 
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML(file_get_contents(base_url()."themes/admin/bootstrapv33/dist/css/bootstrap.css"),1);
    $mpdf->WriteHTML(file_get_contents(base_url()."themes/admin/bootstrapv33/assets/css/styles.css"),1);
    $mpdf->WriteHTML(file_get_contents(base_url()."themes/admin/bootstrapv33/assets/css/print.css"),1);
    $mpdf->WriteHTML($this->html);

    $mpdf->Output();

    exit;
  }
  
  function dompdf($file_name = "",$paper = "A2",$orientation = "landscape")
  { 
    if(!class_exists('DOMPDF'))
    {
      require_once(APPPATH."libraries/dompdf/dompdf_config.inc.php");
    }
    $file_name = (empty($file_name))?'pdf-'.date("Y-m-d"):$file_name;
    $pdf = new DOMPDF();
    $pdf->set_paper(strtolower($paper), $orientation);

    $pdf->load_html($this->html);
    $pdf->render();

    //  Write to file
    #$pdf->output($file_name.".pdf");
    $pdf->stream($file_name.".pdf");
    exit;
  }

}
