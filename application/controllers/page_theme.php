<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_theme extends MY_Controller {
  
  function __construct()
  {
    parent::__construct();
  }
  
  function index()
  {
    
  }
  
  function page()
  {
    
  }
  
  function fetch_list()
  {
    
  }
  
  function installer()
  {
    
  }
  
  function uninstaller()
  {
    
  }
  
  function updater()
  {
    
  }
  
  function configuration()
  {
    
  }
  
  function activator()
  {
    
  }
  
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
