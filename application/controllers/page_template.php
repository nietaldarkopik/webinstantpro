<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_template extends My_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
	
	public function show($template_id = "")
	{
		$this->db->where(array("template_id" => $template_id));
		$get_template = $this->db->get("system_templates")->row_array();
		if(is_array($get_template) and count($get_template) > 0)
		{
			$current_template = $get_template;
			$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
			
			$data = array();
			$data['theme'] = $this->themes->current_theme;
			$data['template'] = $get_template;
			$script = 
			'
			var theme_id = "'.$this->themes->current_theme['theme_id'].'";
			var template_id = "'.$get_template['template_id'].'";
			var page_id = "0";
			';
			$this->assets->add_js_inline($script,"head");
			$this->templates->view("layouts/template-maker",$data, $header = "",$footer = "",$show = true);
			/*
			if(file_exists(FCPATH . "themes/user/".$this->themes->current_theme['theme_name'].'/layouts/'.$template_name.".php"))
				$this->templates->view("layouts/".$template_name,$data, $header = "",$footer = "",$show = true);
			else
				$this->templates->view("layouts/template-maker",$data, $header = "",$footer = "",$show = true);
			*/
		}
	}
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
