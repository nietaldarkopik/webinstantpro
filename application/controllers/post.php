<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
  }
  
	public function index($post_type = "",$page_name = "")
	{
		
	}
	
	public function listing($post_type = "",$page_number = "")
	{
		
	}
  
  
	public function show($post_type = "",$page_name = "")
	{
		
		$this->db->where(array("menu_type_name" => $post_type));
		$get_type = $this->db->get("widget_menu_types")->row_array();
		
		$table_name = (isset($get_type['object_table']))?$get_type['object_table']:"";
		$primary_key = (isset($get_type['primary_key']))?$get_type['primary_key']:"";
		$index_key = (isset($get_type['index_key']))?$get_type['index_key']:"";
		$template_name = (isset($get_type['template_name']) and !empty($get_type['template_name']))?$get_type['template_name']:"blog-list";
	
		$this->db->where(array("template_name" => $template_name));
		$get_template = $this->db->get("system_templates")->row_array();
		
		if(!empty($primary_key))
			$this->db->where(array($primary_key => $page_name));
		if(!empty($index_key))
			$this->db->or_where(array($index_key => $page_name));
		
		$get_page = $this->db->get($table_name)->row_array();
		
		if($table_name == "user_pages")
		{
			$this->db->where(array("page_id" => $get_page['user_page_id']));
			$get_template = $this->db->get("system_page_templates")->row_array();
			if(is_array($get_template) and count($get_template) > 0)
			{
				$this->db->where(array("template_id" => $get_template['template_id']));
				$current_template = $this->db->get("system_templates")->row_array();
				$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
			}
		}
		#echo $template_name."<br/>";
		if(!empty($template_name))
		{
					
			$this->db->where(array("template_name" => $template_name));
			$get_template = $this->db->get("system_templates")->row_array();
			if(is_array($get_template) and count($get_template) > 0)
			{
				
				$data = array();
				$data['page'] = $get_page;
				$data['theme'] = $this->themes->current_theme;
				$data['template'] = $get_template;
				$script = 
				'
				var theme_id = "'.$this->themes->current_theme['theme_id'].'";
				var template_id = "'.$get_template['template_id'].'";
				var page_id = "0";
				';
				$this->assets->add_js_inline($script,"head");
				$this->templates->view("layouts/template",$data, $header = "",$footer = "",$show = true);
			}
		}
	}
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
