<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_block extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
  }
  
	public function index()
	{
  }
  
	public function page($block_name="")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
    /*
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_page_block_listing',array($this,'_hook_show_panel_allowed'));
		*/
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('components/default/listing',array('response' => '','page_title' => 'Block Management','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter));
		else
			$this->load->view('layouts/login');
	}
    
	public function plugin_list($block_name="")
	{
    $config = $this->_config_plugin_list($block_name);
    $subcontents = "";
    if(is_array($config) and count($config) > 0)
    {
      $this->data->init($config);
      $this->data->set_filter();
      $block_plugin_lists = $this->block->get_plugins_list($block_name);
      $subcontents = $this->load->view('components/list',array('config' => $config,'plugin_lists' => $block_plugin_lists),true);
    }

    $is_ajax = $this->input->post('is_ajax');
    
    $param_view = array('content' => $subcontents);
    $this->load->view('layouts/full-width',$param_view);
	}
  
	public function plugin_configuration($block_name="")
	{
    
	}
  
	public function plugin_page_visibility($block_name="")
	{
    
	}
  
	public function edit($block_name="")
	{
    
	}
  
	public function publish($block_name="")
	{
    
	}
  
	public function unpublish($block_name="")
	{
    
	}
  
	public function delete($block_name="")
	{
    
	}
  
	public function visibility_pages($block_name="")
	{
    
	}
  
	public function setting_container()
	{
		
	}
  
  function _config()
  {
    $init = array(	'table' => TABLE_SYS_BLOCKS,
                    'fields' => array(
                                  array(
                                    'name' => 'block_name',
                                    'label' => 'Block Name',
                                    'id' => 'block_name',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'block_title',
                                    'label' => 'Block Title',
                                    'id' => 'block_title',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  )
                            ),
                            'primary_key' => 'block_id',
                            'path' => "/",
                            'controller' => 'page_block',
                            'function' => 'index',
                            'panel_function' => array('edit','delete','publish','unpublish','config_plugin')
                    );
		$this->init = $init;
    return $init;
  }
  
	function _config_plugin_list($blockname="")
	{
    $this->db->where(array("block_name" => $blockname));
    $q_block = $this->db->get(TABLE_SYS_BLOCKS);
    
    if($q_block->num_rows() == 0)
      return array();
      
    $block = $q_block->row_array();
    
    $init = array(	/*'table' => TABLE_SYS_BLOCK_PLUGINS,*/
                    'query'  => "SELECT * FROM ".TABLE_SYS_BLOCK_PLUGINS." bp,".TABLE_SYS_PLUGINS." p ",
                    'fields' => array(
                                  array(
                                    'name' => 'plugin_id',
                                    'label' => 'Plugin',
                                    'id' => 'plugin_id',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'block_id',
                                    'label' => 'Block Widget',
                                    'id' => 'block_id',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'position',
                                    'label' => 'Position',
                                    'id' => 'position',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required|decimal'
                                  ),
                                  array(
                                    'name' => 'status',
                                    'label' => 'Status',
                                    'id' => 'status',
                                    'value' => '',
                                    'type' => 'input_selectbox',
                                    'options' =>  array(
                                                    'Published' => 'Published',
                                                    'Unpublished' => 'Unpublished'
                                                  ),
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  ),
                                  array(
                                    'name' => 'hide_in_pages',
                                    'label' => 'Hide in Pages',
                                    'id' => 'hide_in_pages',
                                    'value' => '',
                                    'type' => 'input_text',
                                    'use_search' => 'true',
                                    'use_listing' => 'true',
                                    'rules' => 'required'
                                  )
                            ),
                            'primary_key' => 'block_plugin_id',
                            'where' => " bp.block_id = '".$block['block_id']."' AND bp.plugin_id = p.plugin_id AND p.status = 'active' ",
                            'path' => "/",
                            'controller' => 'page_block',
                            'function' => 'block',
                            'panel_function' => array('edit','delete','publish','unpublish','config_plugin')
                    );
		$this->init = $init;
    return $init;
	}
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
