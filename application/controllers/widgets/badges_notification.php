<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Badges_notification extends MY_Controller {
  
  public function __construct()
  {
    parent::__construct();  
    $this->load->library("gts/gts_certificates");
    $this->load->library("gts/gts_members");
    $this->load->library("gts/gts_greenwallet");;
  }
  
	public function index()
	{
    $this->load->view("components/badges_notification");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
