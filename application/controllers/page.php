<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
	
	public function show($page_name = "")
	{
		$this->db->where(array("page_name" => $page_name));
		$get_page = $this->db->get("user_pages")->row_array();
		$page_id = (isset($get_page['user_page_id']))?$get_page['user_page_id']:0;

		if(isset($get_page['user_page_id']) and isset($this->themes->current_theme['theme_id']))
		{
			$this->db->where(array("page_id" => $get_page['user_page_id']/*,"theme_id" => $this->themes->current_theme['theme_id']*/));
			$get_page_template = $this->db->get("system_page_templates")->row_array();
			$this->db->where(array("template_id" => $get_page_template['template_id']));
			$current_template = $this->db->get("system_templates")->row_array();
			
			if(is_array($current_template) and count($current_template) > 0)
			{
				$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
				$template_id = (isset($current_template['template_id']))?$current_template['template_id']:"dashboard";
				$data = array();
				$data['page'] = $get_page;
				$data['theme'] = $this->themes->current_theme;
				$data['template'] = $current_template;
				$script = 
				'
				var theme_id = "'.$this->themes->current_theme['theme_id'].'";
				var template_id = "'.$current_template['template_id'].'";
				var page_id = "'.$page_id.'";
				';
				$this->assets->add_js_inline($script,"head");
				$this->templates->view("layouts/template",$data, $header = "",$footer = "",$show = true);
			}
		}
	}
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
