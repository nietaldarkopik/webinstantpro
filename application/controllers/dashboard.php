<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
  }
  
	public function index()
	{
		
		$this->db->where(array("is_frontpage" => 1));
		$get_page = $this->db->get("user_pages")->row_array();
		$page_id = (isset($get_page['user_page_id']))?$get_page['user_page_id']:0;
		
		if(isset($get_page['user_page_id']) and isset($this->themes->current_theme['theme_id']))
		{
			$this->db->where(array("page_id" => $get_page['user_page_id']/*,"theme_id" => $this->themes->current_theme['theme_id']*/));
			$get_template = $this->db->get("system_page_templates")->row_array();
			
			if(is_array($get_template) and count($get_template) > 0)
			{
				$this->db->where(array("template_id" => $get_template['template_id']));
				$current_template = $this->db->get("system_templates")->row_array();
				$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
				$template_id = (isset($current_template['template_id']))?$current_template['template_id']:"0";
				$theme_id = (isset($current_template['theme_id']))?$current_template['theme_id']:"dashboard";
				
				if($this->themes->current_theme != $theme_id)
				{
					$this->db->where(array(/**/"template_name" => $template_name));
					$current_template = $this->db->get("system_templates")->row_array();
					$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
					$template_id = (isset($current_template['template_id']))?$current_template['template_id']:"0";
					$theme_id = (isset($current_template['theme_id']))?$current_template['theme_id']:$theme_id;
				}
				
				$data = array();
				$data['page'] = $get_page;
				$data['theme'] = $this->themes->current_theme;
				$data['template'] = $get_template;
				$script = 
				'
				var theme_id = "'.$this->themes->current_theme['theme_id'].'";
				var template_id = "'.$template_id.'";
				var page_id = "'.$page_id.'";
				';
				
				$this->assets->add_js_inline($script,"head");
				$this->templates->view("layouts/template",$data, $header = "",$footer = "",$show = true);
			}else{
				$this->db->where(array("template_name" => 'dashboard'));
				$get_template = $this->db->get("system_templates")->row_array();
				$template_name = (isset($get_template['template_name']))?$get_template['template_name']:"dashboard";
				$template_id = (isset($get_template['template_id']))?$get_template['template_id']:"0";
				$theme_id = (isset($get_template['theme_id']))?$get_template['theme_id']:"dashboard";
				
				if($this->themes->current_theme != $theme_id)
				{
					$this->db->where(array("template_name" => $template_name));
					$current_template = $this->db->get("system_templates")->row_array();
					$template_name = (isset($current_template['template_name']))?$current_template['template_name']:"dashboard";
					$template_id = (isset($current_template['template_id']))?$current_template['template_id']:"0";
					$theme_id = (isset($current_template['theme_id']))?$current_template['theme_id']:$theme_id;
				}
				
				$data = array();
				$data['page'] = $get_page;
				$data['theme'] = $this->themes->current_theme;
				$data['template'] = $get_template;
				$script = 
				'
				var theme_id = "'.$this->themes->current_theme['theme_id'].'";
				var template_id = "'.$template_id.'";
				var page_id = "'.$page_id.'";
				';
				
				$this->assets->add_js_inline($script,"head");
				$this->templates->view("layouts/template",$data, $header = "",$footer = "",$show = true);
			}
		
		}else{
			require_once(FCPATH . 'themes/user/default/default_theme.php');
			$this->load->set_object_path('view',array(FCPATH."themes/user/" => 1),false);
			$default_theme = new Default_theme();
			$default_theme->setAssets();
			$default_theme->setAdminAssets();
		
			$script = 
			'
				jQuery(document).ready(function(){
					create_panel({
						modal_type: "modal-lg",
						class:"",
						id:"create-first-page",
						body:"",
						footer: \'<button type="button" class="btn btn-default modal-close" data-dismiss="modal">Close</button>\'+
								\'<button type="button" class="btn btn-primary modal-save">Save changes</button>\',
						label:"Setting Widget"
					});
					create_panel({
						modal_type: "modal-lg",
						class:"",
						id:"install-content",
						body:"",
						footer: \'<button type="button" class="btn btn-default modal-close" data-dismiss="modal">Close</button>\'+
								\'<button type="button" class="btn btn-primary modal-save">Save changes</button>\',
						label:"Setting Widget"
					});
					
					$("#create-first-page-btn").on("click",function(){
						$.ajax({
							data: "",
							type: "post",
							url: base_url+"admin/adminwizard/create_page",
							success: function(msg)
							{
								$("#create-first-page .modal-body").html(msg);
								$("#create-first-page").modal("show");
							}
						});
					});
					
					$("#install-content-btn").on("click",function(){
						$("#install-content").modal("show");
					});
				});
			';
			$this->assets->add_js_inline($script,"head");
			//$this->assets->add_js('<script src="'.current_admin_theme_url().'static/js/admin.js"></script>',"body");
			$this->load->view("default/templates/layouts/page-wizard");
		}
		
	}
      
	function login()
	{
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
		  $error = "";
		  $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
		  $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
		  header("location: ".base_url($user_level_name."dashboard"));
		  exit;
		}
		$is_ajax = $this->input->post('is_ajax');
		
		$error= "";
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		$is_login = $this->user_access->is_login();
		
		if(!empty($user_name) || !empty($password))
		{
			$password = md5($password);
			
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
				$user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
				$user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
				header("location: ".base_url($user_level_name."dashboard"));
			}else{
				$error = "Username atau password tidak valid";
			}
		}
		
		$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
	}
	
	function do_login()
	{
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		
		if($password == "" and $user_name == "")
		{
			$is_login = $this->user_access->is_login();
			if($is_login)
			{
				$user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
				$user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
				header("location: ".base_url());
				exit;
			}else{
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}else{
			$password = md5($password);
			
			$error= "";
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
				$user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
				$user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
				header("location: ".base_url());
				exit;
			}else{
				$error = "Username atau password tidak valid";
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}
	}
	
	function is_login()
	{
			$is_login = $this->user_access->is_login();
			echo $is_login;
	}
	
	function do_logout()
	{
		$this->user_access->do_logout();
		header("location: ".base_url());
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
