<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_options extends Admin_Controller {
  
  function index()
  {
	
	$str_params = $this->input->post("params");
	$str_params = base64_decode($str_params);
	parse_str($str_params,$params);
    $table = (isset($params["table"]) and !empty($params["table"]))?$params["table"]:"";
    $select = (isset($params["select"]) and !empty($params["select"]))?$params["select"]:"";
    $where = (isset($params["where"]) and !empty($params["where"]))?$params["where"]:"";
    $fk = $this->input->post("fk");
    $primary_key = (isset($params["primary_key"]) and !empty($params["primary_key"]))?$params["primary_key"]:"";
    $foreign_key = (isset($params["foreign_key"]) and !empty($params["foreign_key"]))?$params["foreign_key"]:"";
    $relation = array($foreign_key => $fk);

    $q = "";
    if(!empty($select))
      $this->db->select($select);
    if(!empty($where))
      $this->db->where($where);
    if(implode("",$relation) != "")
      $this->db->where($relation);
    if(!empty($table))
      $q = $this->db->get($table);
    
    $output = "";
    if(!empty($q))
    {
      if($q->num_rows() > 0)
      {
        $data = $q->result_array();
        if(is_array($data) and count($data) > 0)
        {
          foreach($data as $index => $value)
          {
            if(isset($value['value']) and isset($value['label']))
            {
              $value['value'] = isset($value[$primary_key])?$value[$primary_key]:$value['value'];
              $output .= '<option value="'.$value['value'].'">'.$value['label'].'</option>';
            }
          }
        }
      }
    }
    echo $output;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
