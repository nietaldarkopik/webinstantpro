<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugin_messages extends MY_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/plugin_messages/listing',array('response' => '','page_title' => 'Data Message','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/plugin_messages/listing',array('response' => '','page_title' => 'Data Message','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function reply($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/plugin_messages/edit',array('response' => $response,'page_title' => 'Data Message'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function do_send()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$response = $this->data->add("",$this->init['fields']);
		
		$errors = $this->data->errors;
		$response = (empty($errors))?"Pesan Anda berhasil dikirim. Kami akan membalas pesan anda sesegera mungkin.":$errors;
		echo $response;
		exit;
	}
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/plugin_messages/view',array('response' => '','page_title' => 'Data Message'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_plugin_messages_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/plugin_messages/listing',array('response' => '','page_title' => 'Data Message','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
		$init = array(	'table' => "plugin_messages",
						'fields' => array(
										array(
											'name' => 'name',
											'label' => 'Name',
											'id' => 'name',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'email',
											'label' => 'Email',
											'id' => 'email',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'phone',
											'label' => 'Phone',
											'id' => 'phone',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'message',
											'label' => 'Message',
											'id' => 'message',
											'value' => '',
											'type' => 'input_mce',
											'use_search' => false,
											'use_listing' => true,
											'rules' => 'required'
										)
                    ),
                    'path' => "/",
                    'controller' => 'plugin_messages',
                    'function' => 'index',
                    'primary_key' => 'plugin_message_id',
                    'panel_function' => array(
                                                array('title' => 'Reply','name' => 'reply', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Delete','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "ADD NEW WIDGET";
  }
  
  function _hook_create_form_title_edit($title){
    return "EDIT WIDGET";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".tab-content #add";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".tab-content #search";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
