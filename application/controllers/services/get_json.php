<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get_json extends Admin_Controller {
  
  function index()
  {
  }
  
  function key($sess_key = "")
  { 
    $output = "";
    $data = array();

    $sess_value = $this->session->userdata($sess_key);
    $sess_value_fk = $this->session->userdata('fk_'.$sess_key);
    if(!empty($sess_value))
    {
      $term = $this->input->post("term");
      $term = str_replace(" ","%",$term);
      if($sess_value_fk != "")
      {
        #$sess_value .= " AND label " . $sess_value_fk .' LIKE "%'.$term.'%"';
        $sess_value .= ' HAVING label LIKE "%'.$term.'%"';
      }else{
        $sess_value .= ' HAVING label LIKE "%'.$term.'%"';
      }
      
      $q = $this->db->query($sess_value . " LIMIT 0,30");
      $data = $q->result_array();
    }
    $output = json_encode($data);
    echo $output;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
