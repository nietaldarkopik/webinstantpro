<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appearence_themes extends Admin_Controller {

  var $init = array();
  var $page_title = "";
  
  function index()
  {
  //	$this->fetch_themes("user");
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    if($is_login)
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Themes','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    else
      $this->load->view('layouts/login');
      
  }
  
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  }
  
  function activate($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    
	$check_activate = $this->db->query("SELECT * FROM system_themes WHERE theme_id = '".$object_id."'");
	if($check_activate->num_rows() > 0)
	{
		$this->db->query("UPDATE system_themes SET status = 'not active' WHERE theme_id != '".$object_id."'");
		$this->db->query("UPDATE system_themes SET status = 'active' WHERE theme_id = '".$object_id."'");
	}
	echo "<script type='text/javascript'>window.location.reload();</script>";
	exit;
  } 
  
  function edit($object_id = "")
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_themes_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Themes'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_themes_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Themes'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_listing',array($this,'_hook_show_panel_allowed'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Themes'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_themes_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID provinsi
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_themes_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Themes','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
    else
      $this->load->view('layouts/login');
    
  }
  
  function _config($id_object = "")
  {
    $init = array(  'table' => 'system_themes',
            'fields' => array(
                           array(
                            'name' => 'theme_title',
                            'label' => 'Theme Title',
                            'id' => 'theme_title',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'theme_name',
                            'label' => 'Theme Name',
                            'id' => 'theme_name',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'author',
                            'label' => 'Author',
                            'id' => 'author',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'version',
                            'label' => 'Version',
                            'id' => 'version',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'description',
                            'label' => 'Description',
                            'id' => 'description',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'status',
                            'label' => 'Status',
                            'id' => 'status',
                            'value' => '',
                            'type' => 'input_selectbox',
							'options' => array('' => '---- Choose Status ----','active' => 'Active', 'not active' => 'Not Active'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'appearence_themes',
                    'function' => 'index',
                    'primary_key' => 'theme_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog'),
                                              //array('title' => 'Generate Theme','name' => 'generate_theme_user', 'class' => 'glyphicon-cog'),
                                              array('title' => 'Activate','name' => 'activate', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'with_numbering' => false
          );
    $this->init = $init;
  }
  
  function fetch_themes($path = "user")
  {
	  
	$themes = $this->themes->fetch_themes("./themes/".$path);
	$active_theme = $this->db->where(array("status" => "active"))->get("system_themes")->row_array();
	
	if(is_array($themes) and count($themes) > 0)
	{
		$exist_theme = array();
		$first_theme = "";
		foreach($themes as $theme_name => $theme)
		{
			$this->fetch_theme($path,$theme_name,$theme);
			$exist_theme[] = $theme_name;
		}
		
		if(is_array($exist_theme) and count($exist_theme) > 0)
		{
			#$this->db->where_not_in("theme_name",$exist_theme)->delete("system_themes");
		}
		
		if(is_array($active_theme) and count($active_theme) > 0 and isset($active_theme['theme_name']))
		{
			$this->db->where(array("theme_name" => $active_theme['theme_name']))->update("system_themes",array("status" => "active"));
		}else{
			$this->db->where(array("theme_name" => $first_theme))->update("system_themes",array("status" => "active"));
		}
	}
  }
  
  function fetch_theme($path = "",$theme_name = "",$theme="",$extension_template = ".php")
  {
	  $this->templates->update_templates($path,$theme_name,$theme,$extension_template);
  }
  
  function generate_theme_user($theme_id = "")
  {
	  $theme = $this->db->where(array("theme_id" => $theme_id))->get("system_themes")->row_array();
	  $theme_name = (isset($theme['theme_name']))?$theme['theme_name']:"";
	  if(!empty($theme_name))
	  {
		$this->generate_themes("user/",$theme_name);
	  }
	  $this->index();
  }
  
  function generate_themes($path = "user/",$theme_name = "")
  {
	  $data_theme = $this->themes->fetch_theme("./themes/".$path."/",$theme_name);
	  $this->fetch_theme($path,$theme_name,$data_theme,".html");
	  $templates = $this->templates->generate_theme("./themes/".$path."/".$theme_name."/templates/layouts",".html",$theme_name);
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Add New Themes";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Themes";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return 1;
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
