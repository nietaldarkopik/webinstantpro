<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appearence_plugins extends Admin_Controller {

  var $init = array();
  var $page_title = "";
  
  function index()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    if($is_login)
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Appearence plugins','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    else
      $this->load->view('layouts/login');
      
  }
  
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_plugins_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Appearence plugins'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_plugins_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Appearence plugins'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_listing',array($this,'_hook_show_panel_allowed'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Appearence plugins'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_plugins_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID provinsi
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_plugins_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Appearence plugins','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
    else
      $this->load->view('layouts/login');
    
  }
  

  function fetch_plugins($path = "user")
  {
	  
	$plugins = $this->plugins->get_plugins();
	
	if(is_array($plugins) and count($plugins) > 0)
	{
		$exist_plugin = array();
		$first_plugin = "";
		foreach($plugins as $plugin_name => $plugin)
		{
			
			$first_plugin = (empty($first_plugin))?$plugin_name:$first_plugin;
			$check_plugin = $this->db->where(array("plugin_name" => $plugin_name))->get("system_plugins")->num_rows();
			$plugin_config = (isset($plugin['configs']))?$plugin['configs']:false;
			
			$plugin_id = "";
			
			if($check_plugin == 0)
			{
				if($plugin_config !== false)
				{
					$plugin_config['status'] = 'not active';
					$this->db->insert("system_plugins",$plugin_config);
					$plugin_id = $this->db->insert_id();
				}
			}else{
				if($plugin_config !== false)
				{
					$this->db->where(array("plugin_name" => $plugin_name))->update("system_plugins",$plugin_config);
					$data_plugin = $this->db->where(array("plugin_name" => $plugin_name))->get("system_plugins")->row_array();
					$plugin_id = (isset($data_plugin['plugin_id']))?$data_plugin['plugin_id']:"";
				}
			}
			
			
			$exist_plugin[] = $plugin_name;
		}
	}
	
	$this->index();
  }
  
  function install_plugin($plugin_id = "")
  {
	$data_plugin = $this->db->where(array("plugin_id" => $plugin_id))->get("system_plugins")->row_array();
	if(isset($data_plugin['plugin_name']) and !empty($data_plugin['plugin_name']))
	{
		$class_plugin = $this->plugins->load_plugin($data_plugin['plugin_name']);
		$status_installed = $class_plugin->install();
		if($status_installed)
		{
			$this->db->where(array("plugin_id" => $plugin_id))->update("system_plugins",array("status" => "active"));
		}else{
			$this->db->where(array("plugin_id" => $plugin_id))->update("system_plugins",array("status" => "not active"));
		}
	}
	$this->index();
  }
  
  function uninstall_plugin($plugin_id = "")
  {
	$data_plugin = $this->db->where(array("plugin_id" => $plugin_id))->get("system_plugins")->row_array();
	$this->db->where(array("plugin_id" => $plugin_id))->update("system_plugins",array("status" => "not active"));
	$this->index();
  }

	function activate(){}
	function block_controller(){}
	function configuration(){}
	function fetch_list(){}
	function front_controller(){}
	function install_widgets(){}
	function update(){}
	function widgets(){}
	function checker(){}

	function plugin_controller($plugin_id="",$controller="",$function="",$param="")
	{
		$function = (empty($function))?"index":$function;
		
		if(is_numeric($plugin_id))
			$this->db->where(array('plugin_id' => $plugin_id));
		else
			$this->db->where(array('plugin_name' => $plugin_id));
		$q = $this->db->get(TABLE_SYS_PLUGINS);
		$data_plugin = $q->row_array();
		$plugin_name = (isset($data_plugin['plugin_name']))?$data_plugin['plugin_name']:"";
		$this->plugins->set_path($plugin_name);
		
		$plugin = $this->plugins->call_plugin_controller($plugin_name,$controller,$param,true);
		
		$output = $plugin->$function($param);
		echo $output;
	}

  function _config($id_object = "")
  {
    $init = array(  'table' => 'system_plugins',
            'fields' => array(
                          array(
                            'name' => 'plugin_title',
                            'label' => 'Plugin Title',
                            'id' => 'plugin_title',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                           array(
                            'name' => 'plugin_name',
                            'label' => 'Plugin Name',
                            'id' => 'plugin_name',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'description',
                            'label' => 'Description',
                            'id' => 'description',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'status',
                            'label' => 'Status',
                            'id' => 'status',
                            'value' => '',
                            'type' => 'input_selectbox',
							'options' => array('active' => 'Active','not active' => 'Not Active'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'author',
                            'label' => 'Author',
                            'id' => 'author',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'version',
                            'label' => 'Version',
                            'id' => 'version',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          )
                    ),
                    'path' => "/admin/",
                    'controller' => 'appearence_plugins',
                    'function' => 'index',
                    'primary_key' => 'plugin_id',
                    'panel_function' => array(
                                              array('title' => 'Install','name' => 'install_plugin', 'class' => 'glyphicon-share'),
                                              array('title' => 'Uninstall','name' => 'uninstall_plugin', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'with_numbering' => false,
                    'sess_keyname' => "appearence_plugins"
          );
    $this->init = $init;
	$this->data->sess_keyname = $init['sess_keyname'];
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Add New Appearence plugins";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Appearence plugins";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
