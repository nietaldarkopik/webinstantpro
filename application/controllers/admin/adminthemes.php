<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminthemes extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	var $response = "";
  
  function __construct()
  {
      parent::__construct();
  }
  
	function index()
	{    
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/adminthemes/listing',array('response' => '','page_title' => 'Data Themes','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'addon_bonus_apig/listing','uri_segment' => 5);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminthemes/edit',array('response' => $response,'page_title' => 'Data Themes'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminthemes/add',array('response' => $response,'page_title' => 'Data Themes'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminthemes/view',array('response' => '','page_title' => 'Data Themes'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
			
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminthemes/listing',array('response' => '','page_title' => 'Data Themes','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function block_controller(){}
	function configuration(){}
	function fetch_list(){}
	function activate(){}
	function install(){}
	function page(){}
	function uninstall(){}
	function update(){}
	function checker(){}

	function _config($id_object = "")
	{
		$init = array(	'table' => TABLE_SYS_THEMES,
						'fields' => array(
											array(
													'name' => 'theme_title',
													'label' => 'Theme Title',
													'id' => 'theme_title',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'theme_name',
													'label' => 'Theme Name',
													'id' => 'theme_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'author',
													'label' => 'Author',
													'id' => 'author',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'version',
													'label' => 'Version',
													'id' => 'version',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'description',
													'label' => 'Description',
													'id' => 'description',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => 'required'
												)
										),
										'primary_key' => 'theme_id',
										'action' => base_url($this->uri->segment(2).'/add'),
										'action_search' => base_url($this->uri->segment(2).'/listing'),
										'path' => "/admin/",
										'controller' => 'adminthemes',
										'function' => 'index',
										'panel_function' => array(
																	array('title' => 'Install','name' => 'install', 'class' => 'glyphicon-edit'),
																	array('title' => 'Uninstall','name' => 'uninstall', 'class' => 'glyphicon-share'),
																	array('title' => 'Widget Manager','name' => 'block_controller', 'class' => 'glyphicon-share'),
																	array('title' => 'Activate','name' => 'activate', 'class' => 'glyphicon-share'),
																	array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
																  ),
										'bulk_options' => array(
																	array('title' => 'Uninstall','name' => 'uninstall', 'class' => 'glyphicon-share')
																  )
					);
		$this->init = $init;
	}
  
	function _hook_do_add($param = "")
	{
    $config = array();
    $config['fields'] = $this->input->post("fields");
    $parameters = array();
    $parameter_name = $this->input->post("parameter_name");
    //$parameter_label = $this->input->post("parameter_label");
    $parameter_value = $this->input->post("parameter_value");
    
    $other_fields_name = $this->input->post("other_fields_name");
    //$other_fields_label = $this->input->post("other_fields_label");
    $other_fields_value = $this->input->post("other_fields_value");
    
    if(is_array($parameter_name) and count($parameter_name) > 1)
    {
      foreach($parameter_name as $i => $p)
      {
        if($i == 0)
          continue;
        $p_name = $p;
        if(empty($p_name))
          continue;
          //$p_label = $parameter_label[$i];
          $p_value = $parameter_value[$i];
          
          $config[$p_name] = $p_value;
      }  
      
      $other_fields = $other_fields_name;
      if(is_array($other_fields) and count($other_fields) > 0)
      {
        foreach($other_fields as $if => $f)
        {
          if(is_array($f) and count($f) > 0)
          {
            foreach($f as $i => $fi)
            {
              if($i == 0)
                continue;
              
              $f_name = $fi;
              if(empty($f_name))
                continue;
              //$p_label = $other_fields_label[$no_field];
              $p_value = $other_fields_value[$if][$i];
              
              $config['fields'][$if] += array($f_name => $p_value);
            }
          }
        }
      }
    }
    
		return $config;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Themes";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Themes";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
