<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_categories extends Admin_Controller {

	var $init = array();
	var $page_title = "";

	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_listing',array($this,'_hook_show_panel_allowed'));

		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$init_add = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $init_add;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
				
		$do_save_menu = $this->input->post("do_save_menu");
		if(!empty($do_save_menu))
		{
			$save_menu = $this->_save_parent_menu();
			echo $save_menu;
			exit;
		}
		
		$data_tree = "";
		/*
		$is_login = $this->user_access->is_login();
		$this->db->order_by("sort_order","ASC");
		$categories = $this->db->get("plugin_product_categories");
		$categories = $categories->result_array();
		*/
		$this->data->create_listing($this->init);
		$categories = $this->data->data_rows;
		$the_categories = array();
		foreach($categories as $i => $c)
		{
			$the_cat = array();
			$the_cat = $c;
			$the_cat['title'] = $c['category_name'];
			$the_categories[] = $the_cat;
		}
		$categories = $this->_mapTree($the_categories,0,'parent_category_id','plugin_product_category_id');
		$data_tree = $this->_display_structure($categories);

		if($is_login)
		{
			$data = array(	'data_tree' => $data_tree,
									'response' => '',
									'page_title' => 'Data Catalog categories',
									'config_form_add' => $config_form_add,
									'config_form_filter' => $config_form_filter,
									'listing_config' => $this->init
								);
			$this->templates->view('layouts/tree/listing',$data);
		}
		else
		  $this->load->view('layouts/login');
	  
	}
  
	function _save_parent_menu($key = 'parent_category_id',$primary_key = 'plugin_product_category_id')
	{
		$output = 0;
		$structure = $this->input->post('menu');
		if(is_array($structure) and count($structure))
		{
			$sort_order = array();
			foreach($structure as $menu_id => $parent)
			{
				$sort_index = (isset($sort_order[$parent]))?$sort_order[$parent]:0;
				$this->db->where(array($primary_key => $menu_id));
				$output = (int) $this->db->update('plugin_product_categories',array($key => $parent,"sort_order" => $sort_index));
				if(isset($sort_order[$parent]))
				{
					$sort_order[$parent] += 1;
				}else{
					$sort_order[$parent] = 1;
				}
			}
		}
		return $output;
	}
	
	function _mapTree($dataset = array(),$parent = 0,$key = 'parent_category_id',$primary_key = 'plugin_product_category_id') {
		$tree = array();
		
		if(isset($dataset) and is_array($dataset) and count($dataset) > 0)
		{
			foreach($dataset as $index => $arrays)
			{
				if($arrays[$key] == $parent)
				{
					#$the_array[$arrays['id']] = $arrays;
					$arrays['children'] = $this->_mapTree($dataset,$arrays[$primary_key],$key,$primary_key);
					$tree[] = $arrays;
				}
			}
		}
		return $tree;
	}
	
	function _display_structure($nodes, $indent=0, $max_depth = "",$key = 'parent_category_id',$primary_key = 'plugin_product_category_id')
	{
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul class="sortables fleft">'."\n";
			foreach ($nodes as $node) {
				$href = '#';
				#$action = $this->show_panel_allowed("",$path,$controller,$panel_function,$nodes[$primary_key]);
				$action = $this->data->show_panel_allowed("","admin","catalog_categories",$this->init['panel_function'],$node[$primary_key],false,false);
				$action = str_replace("btn-sm","btn-xs",$action);
				
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node[$primary_key].'">
									<input type="hidden" name="menu['.$node[$primary_key].']" alt="'.$node['sort_order'].'" value="'.$node[$key].'" class="inp_menu"/>
									<span class="ui-icon ui-icon-triangle-1-n fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= $this->_display_structure($node['children'],$indent+1, $max_depth,$key,$primary_key);
				}else{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node[$primary_key].'">
								<input type="hidden" name="menu['.$node[$primary_key].']" alt="'.$node['sort_order'].'" value="'.$node[$key].'" class="inp_menu"/>
								<span class="ui-icon ui-icon-triangle-1-s fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= '<ul class="sortables fleft"></ul>'."\n";
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
	
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_listing',array($this,'_hook_show_panel_allowed'));
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Catalog categories'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'catalog_categories_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Catalog categories'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_catalog_categories_listing',array($this,'_hook_show_panel_allowed'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Catalog categories'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing()
  {
	  $this->index();
  }
  
  function _config($id_object = "")
  {
    $init = array(  'table' => 'plugin_product_categories',
            'fields' => array(
                          array(
                            'name' => 'category_name',
                            'label' => 'Category Name',
                            'id' => 'category_name',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                           array(
                            'name' => 'category_url',
                            'label' => 'Category URL',
                            'id' => 'category_url',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'description',
                            'label' => 'Description',
                            'id' => 'description',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'meta_title',
                            'label' => 'Meta Title',
                            'id' => 'meta_title',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'meta_keywords',
                            'label' => 'Meta Keywords',
                            'id' => 'meta_keywords',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'meta_description',
                            'label' => 'Meta Description',
                            'id' => 'meta_description',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'status',
                            'label' => 'Status',
                            'id' => 'status',
                            'value' => '',
                            'type' => 'input_selectbox',
							'options' => array('' => 'Choose Status','active' => 'Active','not active' => 'Not Active'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          )
                    ),
                    'path' => "/admin/",
                    'controller' => 'catalog_categories',
                    'function' => 'index',
                    'primary_key' => 'plugin_product_category_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'with_numbering' => false
          );
    $this->init = $init;
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Add New Catalog categories";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Catalog categories";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
