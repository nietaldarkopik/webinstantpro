<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appearence_filemanager extends Admin_Controller{

	var $init = array();
	var $page_title = "";
	
	function file_browser()
	{
		$segment_array = $this->uri->segment_array();
	 
		// first and second segments are the controller and method
		$controller = array_shift( $segment_array );
		$method = array_shift( $segment_array );
	 
		// absolute path using additional segments
		$path_in_url = '';
		foreach ( $segment_array as $segment ) $path_in_url.= $segment.'/';
		$absolute_path = getcwd().'/'.$path_in_url;
		$absolute_path = rtrim( $absolute_path ,'/' );
	 
		// check if it is a path or file
		if ( is_dir( $absolute_path ))
		{
			// link generation helper
			$this->load->helper('url');
	 
			$dirs = array();
			$files = array();
			// fetching directory
			if ( $handle = @opendir( $absolute_path ))
			{
				while ( false !== ($file = readdir( $handle )))
				{
					if (( $file != "." AND $file != ".." ))
					{
						if ( is_dir( $absolute_path.'/'.$file ))
						{
							$dirs[]['name'] = $file;
						}
						else
						{
							$files[]['name'] = $file;
						}
					}
				}
				closedir( $handle );
				sort( $dirs );
				sort( $files );
	 
			}
			// parent folder
			// ensure it exists and is the first in array
			if ( $path_in_url != '' )
				array_unshift ( $dirs, array( 'name' => '..' ));
	 
			// view data
			$data = array(
				'controller' => $controller,
				'method' => $method,
				'virtual_root' => getcwd(),
				'path_in_url' => $path_in_url,
				'dirs' => $dirs,
				'files' => $files,
				);
			$this->load->view( 'layouts/filebrowser/filebrowser', $data );
		}
		else
		{
			// is it a file?
			if ( is_file($absolute_path) )
			{
				// open it
				header ('Cache-Control: no-store, no-cache, must-revalidate');
				header ('Cache-Control: pre-check=0, post-check=0, max-age=0');
				header ('Pragma: no-cache');
	 
				$text_types = array(
					'php', 'css', 'js', 'html', 'txt', 'htaccess', 'xml'
					);
				$path_parts = pathinfo($absolute_path);
				// download necessary ?
				if( isset($path_parts['extension']) && in_array( $path_parts['extension'], $text_types) ) {
					header('Content-Type: text/plain');
				} else {
					header('Content-Type: application/x-download');
					header('Content-Length: ' . filesize( $absolute_path ));
					header('Content-Disposition: attachment; filename=' . basename( $absolute_path ));
				}
	 
				@readfile( $absolute_path );
			}
			else
			{
				show_404();
			}
		}
	}
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/appearence_filemanager/listing',array('response' => '','page_title' => 'Data Message','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/appearence_filemanager/listing',array('response' => '','page_title' => 'Data Message','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function reply($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/appearence_filemanager/edit',array('response' => $response,'page_title' => 'Data Message'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/appearence_filemanager/add',array('response' => $response,'page_title' => 'Data Message'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/appearence_filemanager/view',array('response' => '','page_title' => 'Data Message'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_filemanager_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/appearence_filemanager/listing',array('response' => '','page_title' => 'Data Message','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{
		$init = array(	'table' => "appearence_filemanager",
						'fields' => array(
										array(
											'name' => 'name',
											'label' => 'Name',
											'id' => 'name',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'email',
											'label' => 'Email',
											'id' => 'email',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'phone',
											'label' => 'Phone',
											'id' => 'phone',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'message',
											'label' => 'Message',
											'id' => 'message',
											'value' => '',
											'type' => 'input_mce',
											'use_search' => false,
											'use_listing' => true,
											'rules' => 'required'
										),
										array(
											'name' => 'datetime',
											'label' => 'Date',
											'id' => 'datetime',
											'value' => '',
											'type' => 'input_hidden',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										)
                    ),
                    'path' => "/admin/",
                    'controller' => 'appearence_filemanager',
                    'function' => 'index',
                    'primary_key' => 'plugin_message_id',
                    'panel_function' => array(
                                                array('title' => 'Reply','name' => 'reply', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Delete','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah WIDGET";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit WIDGET";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
