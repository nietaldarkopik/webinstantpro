<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admintemplates extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	var $response = "";
  
  function __construct()
  {
      parent::__construct();
  }
  
	function index()
	{    
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));
		//$this->hook->add_action('hook_create_listing_value_theme_id',array($this,'_hook_create_listing_value_theme_id'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Templates','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'addon_bonus_apig/listing','uri_segment' => 5);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Templates'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		$this->init['action'] = base_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');

		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Templates'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_addon_bonus_apig_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Templates'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function configuration(){}

	function _config($id_object = "")
	{
		$current_theme = $this->themes->current_theme;
		
		$init = array(	'table' => 'system_templates',
						//'where' => ' theme_id = "'.$current_theme['theme_id'].'" ',
						'fields' => array(
											/*
											array(
													'name' => 'theme_id',
													'label' => 'Nama Theme',
													'id' => 'theme_id',
													'value' => '',
													'type' => 'input_selectbox',
													//'table'	=> 'system_themes themes',
													//'select' => array('theme_id AS value','theme_name AS label'),
													'options' => array($current_theme['theme_id'] => $current_theme['theme_name']),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											*/
											array(
													'name' => 'template_name',
													'label' => 'Nama Template',
													'id' => 'template_name',
													'value' => '',
													'type' => 'input_text',
													'desc' => '(Karakter yang diperbolehkan "a-z","0-9","_" dan "-")',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'template_description',
													'label' => 'Keterangan',
													'id' => 'template_description',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => false,
													'rules' => ''
												),
											/*
											array(
													'name' => 'template_type',
													'label' => 'Jenis Template',
													'id' => 'template_type',
													'value' => '',
													'type' => 'input_selectbox',
													'options' => array('fixed' => 'fixed','custom' => 'custom'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											*/
											array(
													'name' => 'screenshot',
													'label' => 'Screenshot',
													'id' => 'screenshot',
													'value' => '',
													'type' => 'input_file',
													'config_upload' => array( 
																			'upload_path' => './themes/user/'.$current_theme['theme_name'].'/',
																			'encrypt_name' => false,
																			'allowed_types' =>  'jpg|png|gif'
																			),
													'use_search' => false,
													'use_listing' => true,
													'rules' => ''
												)
										),
										'primary_key' => 'template_id',
										'action' => base_url($this->uri->segment(2).'/add'),
										'action_search' => base_url($this->uri->segment(2).'/listing'),
										'path' => "/admin/",
										'controller' => 'admintemplates',
										'function' => 'index',
										'panel_function' => array(
																	array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-cog'),
																	array('title' => 'View','name' => 'view', 'class' => 'glyphicon-cog'),
																	array('title' => 'Modify Template','name' => 'frontend', 'class' => 'glyphicon-cog')
																  ),
										'bulk_options' => array(
																	array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-trashs')
																  )
					);
		$this->init = $init;
	}


	function frontend($object_id = "")
	{
	  echo '<script>';
	  echo 'window.location.href="'.base_url("page_template/show/".$object_id).'";';
	  echo '</script>';
	  exit;
	}
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah Data Templates";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Data Templates";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
	function _hook_create_listing_value_theme_id($default_value = "")
	{		
		$theme = $this->db->where(array("theme_id" => $default_value))->get("system_themes")->row_array();
		$theme_id = (isset($theme['theme_id']))?$theme['theme_id']:"";
		$theme = $this->db->where(array("theme_id" => $theme_id))->get("system_themes")->row_array();
		
		$output = (isset($theme['theme_name']))?$theme['theme_name']: '';
		return $output;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
