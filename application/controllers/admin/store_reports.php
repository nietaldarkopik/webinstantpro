<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_reports extends Admin_Controller {

  var $init = array();
  var $page_title = "";
  
  function index()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID provinsi
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'store_reports_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    if($is_login)
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Store reports','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    else
      $this->load->view('layouts/login');
      
  }
  
  function delete($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'store_reports_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data Store reports'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'store_reports_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data Store reports'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_listing',array($this,'_hook_show_panel_allowed'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data Store reports'));
    else
      $this->load->view('layouts/login');
    
  }
    
  function listing()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_store_reports_listing',array($this,'_hook_show_panel_allowed'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID provinsi
    $init_add = $this->init;
    foreach($init_add['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'store_reports_id'){
        unset($init_add['fields'][$idx]);
      }
    }

    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data Store reports','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
    else
      $this->load->view('layouts/login');
    
  }
  

  function ajax_get_propinsi_data($propinsi_id = '')
  {
    $this->load->model('model_store_reports');
    $where = array();
    if(!empty($propinsi_id))
      $where = array('store_reports_id' => $propinsi_id);

    $return = $this->model_store_reports->get(0,0,$where);
    echo json_encode($return);
    die;
  }

  function _config($id_object = "")
  {
    $init = array(  'table' => 'store_reports',
            'fields' => array(
                          array(
                            'name' => 'store_reports_id',
                            'label' => 'ID Store reports',
                            'id' => 'store_reports_id',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                           array(
                            'name' => 'kode_propinsi',
                            'label' => 'Kode Store reports',
                            'id' => 'kode_propinsi',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'nama_propinsi',
                            'label' => 'Nama Store reports',
                            'id' => 'nama_propinsi',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                    ),
                    'path' => "/admin/",
                    'controller' => 'store_reports',
                    'function' => 'index',
                    'primary_key' => 'store_reports_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'with_numbering' => false
          );
    $this->init = $init;
  }
  
  function _hook_do_add($param = "")
  {
    return $param;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Add New Store reports";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Store reports";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
