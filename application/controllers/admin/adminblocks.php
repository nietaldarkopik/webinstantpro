<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminblocks extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/adminblocks/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function widgets()
	{
		$this->_config_widgets();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		if($is_login)
			$this->load->view('layouts/adminblocks/listing',array('response' => '','page_title' => 'Data Widget','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function sortorder_con()
	{
		$do_action = $this->input->post("do_action");
		if($do_action == "do_sort_order_container")
		{
			$data_containers = $this->input->post("data_containers");
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$page_id = $this->input->post("page_id");

			$q_insert = false;
			if((int) $page_id > 0)
			{
				$q_insert = $this->page_templates->sortorder_container($data_containers,$template_id);
			}else{
				$q_insert = $this->templates->sortorder_container($data_containers,$template_id);
			}
			
			if($q_insert)
			{
				echo json_encode(array("status" => "success","message" => "Posisi Block berhasil diubah","content" => ""));
			}else{
				echo json_encode(array("status" => "danger","message" => "Posisi Block berhasil diubah","content" => ""));
			}
		}
	}
	
	function sortorder_all()
	{
		$do_action = $this->input->post("do_action");
		if($do_action == "do_sort_order_all")
		{
			$data_containers = $this->input->post("data_order");
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$page_id = $this->input->post("page_id");
			
			$q_insert = false;
			if((int) $page_id > 0)
			{
				$q_insert = $this->page_templates->sortorder_all($data_containers,$template_id);
			}else{
				$q_insert = $this->templates->sortorder_all($data_containers,$template_id);
			}
			
			if($q_insert)
			{
				echo json_encode(array("status" => "success","message" => "Posisi Block berhasil diubah","content" => ""));
			}else{
				echo json_encode(array("status" => "danger","message" => "Posisi Block berhasil diubah","content" => ""));
			}
		}
	}
	
	function add_con()
	{
		$do_action = $this->input->post("do_action");
		if($do_action == "do_add_container")
		{
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$content = $this->input->post("content");
			$page_id = $this->input->post("page_id");
			
			$content = false;
			if((int) $page_id > 0)
			{
				$content = $this->page_templates->create_container($template_id,$page_id,$content);
			}else{
				$content = $this->templates->create_container($template_id,$content);
			}
			
			if(!empty($content) and $content !== false)
			{
				echo json_encode(array("status" => "success","message" => "Data Block berhasil ditambahkan","content" => $content));
			}else{
				echo json_encode(array("status" => "danger","message" => "Data Block gagal ditambahkan","content" => ""));
			}
		}
	}
	
	function add_row()
	{
		$do_action = $this->input->post("do_action");
		if($do_action == "do_add_row")
		{
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$content = $this->input->post("content");
			$con_id = $this->input->post("con_id");
			$page_id = $this->input->post("page_id");
			
			$content = false;
			if((int) $page_id > 0)
			{
				$content = $this->page_templates->create_row($template_id,$con_id,$content);
			}else{
				$content = $this->templates->create_row($template_id,$con_id,$content);
			}
			
			if($content !== false)
			{
				echo json_encode(array("status" => "success","message" => "Data Baris berhasil ditambahkan","content" => $content));
			}else{
				echo json_encode(array("status" => "danger","message" => "Data Baris gagal ditambahkan","content" => ""));
			}
		}
	}
	
	function add_col($number_coloumns = 1)
	{
		$theme_id = $this->input->post("theme_id");
		$template_id = $this->input->post("template_id");
		$con_id = $this->input->post("con_id");
		$row_id = $this->input->post("row_id");
		$block_title = $this->input->post("block_name");
		$page_id = $this->input->post("page_id");
	
		$content = false;
		if((int) $page_id > 0)
		{
			$content = $this->page_templates->create_column($template_id,$con_id,$row_id,$number_coloumns,$block_title);
		}else{
			$content = $this->templates->create_column($template_id,$con_id,$row_id,$number_coloumns,$block_title);
		}
		
		if($content !== false)
		{
			echo json_encode(array("status" => "success","message" => "Data Kolom berhasil ditambahkan","content" => $content));
		}else{
			echo json_encode(array("status" => "danger","message" => "Data Kolom gagal ditambahkan","content" => ""));
		}
	}
	
	function add_col_options()
	{
		$do_action = $this->input->post("do_action");
		$theme_id = $this->input->post("theme_id");
		$template_id = $this->input->post("template_id");
		$con_id = $this->input->post("con_id");
		$row_id = $this->input->post("row_id");
		$output = "";
		
		if($do_action == "do_add_col_options")
		{
			$attributes = 'theme_id="'.$theme_id.'" template_id="'.$template_id.'" con_id="'.$con_id.'"  row_id="'.$row_id.'" ';
			$output =  '
					  <div class="ajax_container content-container col-sm-12 col-md-12">
						<div class="panel panel-default">
						  <div class="panel-heading">
							<h3 class="panel-title">Pilih Kolom</h3>
						  </div>
						  <div class="panel-body paddingtop0">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 1 Kolom</span>
									</button>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-right btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="2" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 2 Kolom</span>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="3" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 3 Kolom</span>
									</button>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-right btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="4" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 4 Kolom</span>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="6" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 6 Kolom</span>
									</button>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-right btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns" number_columns="12" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok 12 Kolom</span>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns add-col-leftbar" number_columns="left_bar" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok Sidebar Kiri</span>
									</button>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button type="button" class="pull-right btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-columns add-col-rightbar" number_columns="right_bar" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Tambah Blok Sidebar Kanan</span>
									</button>
								</div>
							</div>
							<div class="row hide">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<input type="text" placeholder="Masukan Nama Blok" value="" class="form-control add-block-name margintop20"/>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<button type="button" class="btn btn-lg btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 save-columns" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="center-block">Tambahkan ke halaman</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				  </div>
				</div>';
		}
		echo $output;
	}
	
	function add_widget_options()
	{
		$do_action = $this->input->post("do_action");
		$theme_id = $this->input->post("theme_id");
		$template_id = $this->input->post("template_id");
		$con_id = $this->input->post("con_id");
		$row_id = $this->input->post("row_id");
		$block_col_id = $this->input->post("block_col_id");
		$output = "";
		
		if($do_action == "do_add_widget_options")
		{
			$data_widgets = $this->db->order_by("widget_name ASC")->get("system_widgets")->result_array();
			$attributes = 'theme_id="'.$theme_id.'" template_id="'.$template_id.'" con_id="'.$con_id.'"  row_id="'.$row_id.'" block_col_id="'.$block_col_id.'" ';
			
			$block_widgets = "";
			if(is_array($data_widgets) and count($data_widgets) > 0)
			{
				foreach($data_widgets as $i => $widget)
				{
					$widget_url = base_url().'admin/adminblocks/setting_widget/'.$widget['widget_name'].'/configuration/';
					$block_widgets .= '
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<button widget_url="'.$widget_url.'" type="button" class="pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-widgets" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off" '.$attributes.'>
										<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">'.$widget['widget_title'].'</span>
									</button>
								</div>
								';
				}
			}
			$output =  '
					  <div class="ajax_container content-container col-sm-12 col-md-12">
						<div class="panel panel-default">
						  <div class="panel-heading">
							<h3 class="panel-title">Pilih Widget</h3>
						  </div>
						  <div class="panel-body paddingtop0">
							<div class="row">
							'.$block_widgets.'
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<button type="button" class="btn-disabled btn btn-lg bg-gray col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 config-widgets" data-toggle="button" aria-pressed="false"  autocomplete="off" '.$attributes.'>
										<span class="center-block"><span class="glyphicon glyphicon-cog"></span> Konfigurasi Widget </span>
									</button>
								</div>
							</div>
						</div>
					</div>
				  </div>
				</div>';
		}
		echo $output;
	}
	
	function add_widget($block_col_id = "")
	{
		$do_action = $this->input->post("do_action");
		$content = false;
		
		if($do_action == "do_save_widget")
		{
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$con_id = $this->input->post("con_id");
			$row_id = $this->input->post("row_id");
			$block_col_id = $this->input->post("block_col_id");
			$widget_id = $this->input->post("widget_id");
			$widget_content_id = $this->input->post("widget_content_id");
			$page_id = $this->input->post("page_id");
			
			if((int) $page_id > 0)
			{
				$content = $this->page_templates->append_widget($template_id,$con_id,$row_id,$block_col_id,$widget_id,$widget_content_id);
			}else{
				$content = $this->templates->append_widget($template_id,$con_id,$row_id,$block_col_id,$widget_id,$widget_content_id);
			}
		}
		
		if($content !== false)
		{
			echo json_encode(array("status" => "success","message" => "Data Widget berhasil ditambahkan","content" => $content));
		}else{
			echo json_encode(array("status" => "danger","message" => "Data Widget gagal ditambahkan","content" => ""));
		}
	}
	
	function update_block_col_content($block_col_id = "")
	{
		$do_action = $this->input->post("do_action");
		$q_insert = false;
		$content = "";
		if($do_action == "do_save_content")
		{
			$post_content = $this->input->post("post_content");
			$theme_id = $this->input->post("theme_id");
			$template_id = $this->input->post("template_id");
			$con_id = $this->input->post("con_id");
			$row_id = $this->input->post("row_id");
			$block_col_id = $this->input->post("block_col_id");
			$widget_id = $this->input->post("widget_id");
			$widget_content_id = $this->input->post("widget_content_id");
			$page_id = $this->input->post("page_id");
			
			if((int) $page_id > 0)
			{
				$this->db->where(array("page_col_id" => $block_col_id));
				$q_insert = $this->db->update("system_page_cols",array("content" => $post_content));
			}else{
				$this->db->where(array("block_col_id" => $block_col_id));
				$q_insert = $this->db->update("system_block_cols",array("content" => $post_content));
			}
		}
		
		if($q_insert)
		{
			echo json_encode(array("status" => "success","message" => "Ukuran Block telah disimpan","content" => $content));
		}else{
			echo json_encode(array("status" => "danger","message" => "Ukuran Block gagal disimpan","content" => ""));
		}
	}	
	
	
	function callback_widget($block_col_id = "")
	{
		$do_action = $this->input->post("do_action");
		$q_insert = false;
		$content = "";
		$theme_id = $this->input->post("theme_id");
		$template_id = $this->input->post("template_id");
		$con_id = $this->input->post("con_id");
		$row_id = $this->input->post("row_id");
		$block_col_id = $this->input->post("block_col_id");
		$widget_id = $this->input->post("widget_id");
		$widget_content_id = $this->input->post("widget_content_id");
			
		$block_col_data = array("block_col_id" => $block_col_id,"widget_id" => $widget_id,"widget_content_id" => $widget_content_id);
		$content = $this->widgets->callback_widgets($block_col_data);
		
		echo $content;
	}	
	
	function remove_con($object_id = "")
	{
		$page_id = $this->input->post("page_id");
		$q_delete = false;
		if((int) $page_id > 0)
		{
			$this->db->where(array("page_container_id" => $object_id));
			$q_delete = $this->db->delete("system_page_cols");
			$this->db->where(array("page_container_id" => $object_id));
			$q_delete = $this->db->delete("system_page_rows");
			$this->db->where(array("page_container_id" => $object_id));
			$q_delete = $this->db->delete("system_page_containers");
		}else{
			$this->db->where(array("block_container_id" => $object_id));
			$q_delete = $this->db->delete("system_block_cols");
			$this->db->where(array("block_container_id" => $object_id));
			$q_delete = $this->db->delete("system_block_rows");
			$this->db->where(array("block_container_id" => $object_id));
			$q_delete = $this->db->delete("system_block_containers");
		}
		if($q_delete)
		{
			echo json_encode(array("status" => "success","message" => "Data Block berhasil dihapus"));
		}else{
			echo json_encode(array("status" => "danger","message" => "Data Block gagal dihapus"));
		}
	}
	
	function remove_row($object_id = "")
	{
		$page_id = $this->input->post("page_id");
		$q_delete = false;
		if((int) $page_id > 0)
		{
			$this->db->where(array("page_row_id" => $object_id));
			$q_delete = $this->db->delete("system_page_cols");
			$this->db->where(array("page_row_id" => $object_id));
			$q_delete = $this->db->delete("system_page_rows");
		}else{
			$this->db->where(array("block_container_id" => $object_id));
			$q_delete = $this->db->delete("system_block_cols");
			$this->db->where(array("block_container_id" => $object_id));
			$q_delete = $this->db->delete("system_block_rows");
		}
		
		if($q_delete)
		{
			echo json_encode(array("status" => "success","message" => "Data Baris berhasil dihapus"));
		}else{
			echo json_encode(array("status" => "danger","message" => "Data Baris gagal dihapus"));
		}
	}
	
	
	function remove_col($object_id = "")
	{
		$do_action = $this->input->post("do_action");
		$page_id = $this->input->post("page_id");
		if($do_action == "do_remove_col")
		{
			$q_delete = false;
			if((int) $page_id > 0)
			{
				$this->db->where(array("page_col_id" => $object_id));
				$q_delete = $this->db->delete("system_page_cols");
			}else
			{
				$this->db->where(array("block_col_id" => $object_id));
				$q_delete = $this->db->delete("system_block_cols");
			}
			if($q_delete)
			{
				echo json_encode(array("status" => "success","message" => "Data Kolom berhasil dihapus"));
			}else{
				echo json_encode(array("status" => "danger","message" => "Data Kolom gagal dihapus"));
			}
		}
	}
	
	function remove_widget($object_id = "")
	{
		$page_id = $this->input->post("page_id");
		if((int) $page_id > 0)
		{
			$this->db->where(array("page_col_id" => $object_id));
			$q_delete = $this->db->delete("system_page_cols");
		}else{
			$this->db->where(array("block_col_id" => $object_id));
			$q_delete = $this->db->delete("system_block_cols");
		}
		if($q_delete)
		{
			echo json_encode(array("status" => "success","message" => "Data Kolom berhasil dihapus"));
		}else{
			echo json_encode(array("status" => "danger","message" => "Data Kolom gagal dihapus"));
		}
	}	
	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/edit',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/add',array('response' => $response,'page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/view',array('response' => '','page_title' => 'Data Widget'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $this->init;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/listing',array('response' => '','page_title' => 'Data Widget','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function setting_container_list_rows($container_id = "")
	{
		$this->_config();
		
		$this->init['table'] = "system_block_rows";
		$this->init['where'] = " block_container_id = '".$container_id."'";
		$this->init['sort_order'] = " sort_order ASC";
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/setting_container',array('response' => '','page_title' => 'Data Baris','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
		
	function setting_container($container_id = "")
	{
		
		$is_login = $this->user_access->is_login();
		$data = array();
		$data['container_id'] = $container_id;
		
		if($is_login)
			$this->load->view('layouts/adminblocks/setting_container',array('data_setting' => $data,'response' => '','page_title' => 'Setting Container'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function setting_row($block_row_id = "")
	{
		$this->_config();
		$this->init['table'] = "system_block_cols";
		$this->init['where'] = " block_row_id = '".$block_row_id."'";
		$this->init['sort_order'] = " sort_order ASC";
		$this->init['primary_key'] = "block_col_id";
		$this->init['fields'] = array(
									array(
										'name' => 'block_row_id',
										'label' => 'Block ID',
										'id' => 'block_row_id',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => 'required'
									),
									array(
										'name' => 'widget_id',
										'label' => 'Widget ID',
										'id' => 'widget_id',
										'value' => '',
										'type' => 'input_text',
										'use_search' => true,
										'use_listing' => true,
										'rules' => 'required'
									)
								);
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminblocks/setting_row',array('response' => '','page_title' => 'Data Widget','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function setting_widget($widget_id = "",$function="",$param="")
	{
		$function = (empty($function))?"index":$function;
		
		if(is_numeric($widget_id))
			$this->db->where(array('widget_id' => $widget_id));
		else
			$this->db->where(array('widget_name' => $widget_id));
		$q = $this->db->get("system_widgets");
		$data_widget = $q->row_array();
		$plugin_name = (isset($data_widget['plugin_name']))?$data_widget['plugin_name']:"";
		$widget_name = (isset($data_widget['widget_name']))?$data_widget['widget_name']:"";
		$this->plugins->set_path($plugin_name);		
		$widget = $this->widgets->call_widget_controller($widget_name,$function,$param,true);
		
		$output = "Page not found";
		
		if(method_exists($widget,$function))
			$output = $widget->$function($param);
			
		echo $output;
	}
	
	function setting_col($block_row_id = "")
	{
		$output = "";
		if(empty($block_row_id))
		{
			$this->add_widget_options();
			exit;
		}
		
		
		$page_id = $this->input->post("page_id");
		
		if(!empty($page_id))
		{
			$page_row_id = $block_row_id;
			$data_col = $this->db->where(array("page_col_id" => $page_row_id))->get("system_page_cols")->row_array();
			if(isset($data_col['widget_id']) and !empty($data_col['widget_id']) and $data_col['widget_id'] > 0)
			{
				$data_widget = $this->db->where(array("widget_id" => $data_col['widget_id']))->get("system_widgets")->row_array();
					
				$plugin_name = (isset($data_widget['plugin_name']))?$data_widget['plugin_name']:"";
				$widget_name = (isset($data_widget['widget_name']))?$data_widget['widget_name']:"";
				$this->plugins->set_path($plugin_name);		
				$widget = $this->widgets->call_widget_controller($widget_name,'configuration',"",true);
				
				$output = "Page not found";
				
				if(method_exists($widget,"configuration"))
					$output = $widget->{"configuration"}();
			}
			
		}else{
			$data_col = $this->db->where(array("block_col_id" => $block_row_id))->get("system_block_cols")->row_array();
			if(isset($data_col['widget_id']) and !empty($data_col['widget_id']) and $data_col['widget_id'] > 0)
			{
				$data_widget = $this->db->where(array("widget_id" => $data_col['widget_id']))->get("system_widgets")->row_array();
					
				$plugin_name = (isset($data_widget['plugin_name']))?$data_widget['plugin_name']:"";
				$widget_name = (isset($data_widget['widget_name']))?$data_widget['widget_name']:"";
				$this->plugins->set_path($plugin_name);		
				$widget = $this->widgets->call_widget_controller($widget_name,'configuration',"",true);
				
				$output = "Page not found";
				
				if(method_exists($widget,"configuration"))
					$output = $widget->{"configuration"}();
			}
		}
		echo $output;
	}
	
	function get_style()
	{
		$this->load->library("style");
		$css = file_get_contents('http://localhost/wipro/themes/user/virtuoso_simple/assets/theme/css/nexus.css');
		$this->style->extract_css($css);
	}
	
	function page(){}
	function plugin_list(){}
	function plugin_configuration(){}
	function plugin_page_visibility(){}

	function _config($id_object = "")
	{
    $init = array(	'table' => "system_block_containers",
						'fields' => array(
													array(
														'name' => 'group_name',
														'label' => 'Nama Block Group',
														'id' => 'group_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'group_title',
														'label' => 'Judul Block Group',
														'id' => 'group_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													)
                    ),
                    'path' => "/admin/",
                    'controller' => 'adminblocks',
                    'function' => 'index',
                    'primary_key' => 'block_container_id',
                    'panel_function' => array(
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _config_widgets($id_object = "")
	{
    $init = array(	
            //'table' => TABLE_SYS_BLOCKS,
            'query' => "SELECT p.*,b.* FROM ".TABLE_SYS_BLOCKS." b,".TABLE_SYS_BLOCK_PLUGINS." bp ,".TABLE_SYS_PLUGINS." p ",
            'where' => " b.block_row_id = bp.block_row_id AND p.plugin_id = bp.plugin_id ",
						'fields' => array(
													array(
														'name' => 'plugin_title',
														'label' => 'Widget Title',
														'id' => 'plugin_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'plugin_name',
														'label' => 'Widget Name',
														'id' => 'plugin_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'block_name',
														'label' => 'Block Name',
														'id' => 'block_name',
														'value' => '',
														'type' => 'input_text',
														'use_search' => false,
														'use_listing' => false,
														'rules' => 'required'
													),
													array(
														'name' => 'block_title',
														'label' => 'Display on Block',
														'id' => 'block_title',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'status',
														'label' => 'Status',
														'id' => 'status',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													),
													array(
														'name' => 'position',
														'label' => 'Order Position',
														'id' => 'position',
														'value' => '',
														'type' => 'input_text',
														'use_search' => true,
														'use_listing' => true,
														'rules' => 'required'
													)
                    ),
                    'primary_key' => 'block_row_id',
                    'path' => "/",
                    'controller' => 'adminblocks',
                    'function' => 'widgets',
                    'panel_function' => array(
                                                array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
                                                array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              ),
                    'bulk_options' => array(
                                                array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
                                                array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
                                                array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah WIDGET";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit WIDGET";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "1";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
