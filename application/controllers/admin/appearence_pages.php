<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appearence_pages extends Admin_Controller {

  var $init = array();
  var $page_title = "";
  
  function index()
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
    $this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
    $this->hook->add_action('hook_do_after_add',array($this,'_hook_do_after_add'));
    $this->hook->add_action('hook_do_after_edit',array($this,'_hook_do_after_edit'));
    $this->hook->add_action('hook_do_after_delete',array($this,'_hook_do_after_delete'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_edit',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_view',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_delete',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_listing',array($this,'_hook_show_panel_allowed'));
	$this->hook->add_action('hook_create_listing_value_is_frontpage',array($this,'_hook_create_listing_value_is_frontpage'));
	$this->hook->add_action('hook_create_listing_value_template_id',array($this,'_hook_create_listing_value_template_id'));
    
    $is_login = $this->user_access->is_login();

    //hilangkan ID provinsi
    $init_add = $this->init;
	$init_add['table'] = 'user_pages';
    $config_form_filter = $this->init;
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
    $config_form_add = $init_add;
    $config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    $config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
    if($is_login)
      $this->load->view('layouts/appearence_pages/listing',array('response' => '','page_title' => 'Data Halaman','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
    else
      $this->load->view('layouts/login');
      
  }
  
  function delete($object_id = "")
  {
    $this->_config();
	$this->init['table'] = 'user_pages';
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete')); 
    $response = $this->data->delete("",$this->init['fields']);
    $paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
    $this->data->init_pagination($paging_config);
    $this->listing();
  } 
  
  function edit($object_id = "")
  {
    $this->_config();

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'appearence_pages_id'){
        unset($this->init['fields'][$idx]);
      }
    }

    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
    $this->hook->add_action('hook_do_after_edit',array($this,'_hook_do_after_edit'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_listing',array($this,'_hook_show_panel_allowed'));

    
    $init = (isset($this->init['fields']))?$this->init['fields']:array();
    if(is_array($init) and count($init) > 0)
    {
      foreach($init as $index => $i)
      {
        if(isset($i['name']) and $i['name'] == 'password')
        {
          $init[$index]['rules'] = "";
        }
      }
    }
    $this->init['fields'] = $init;
    
    $response = $this->data->edit("",$this->init['fields']);
    
    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/appearence_pages/edit',array('response' => $response,'page_title' => 'Data Halaman'));
    else
      $this->load->view('layouts/login');
    
  }
  
  function add()
  {
    $this->_config();
	$init_add = $this->init;
	
	$this->init['table'] = 'user_pages';

    //hilangkan ID propinsi
    foreach($this->init['fields'] as $idx => $dtfield){
      if($dtfield['name'] == 'template_id'){
        unset($this->init['fields'][$idx]);
      }
    }
	
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_do_after_add',array($this,'_hook_do_after_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
    $response = $this->data->add("",$this->init['fields']);
    
    $this->init = $init_add;
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
    $this->hook->add_action('hook_do_after_add',array($this,'_hook_do_after_add'));
    $this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
    $this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/appearence_pages/add',array('response' => $response,'page_title' => 'Data Halaman'));
    else
      $this->load->view('layouts/login');
    
  }
  
  
  function frontend($object_id = "")
  {
	  $page = $this->db->where(array("user_page_id" => $object_id))->get("user_pages")->row_array();
	  echo '<script>';
	  echo 'window.location.href="'.base_url("page/show/".$page['page_name']).'";';
	  echo '</script>';
	  exit;
  }
  
  function view($object_id = "")
  {
    $this->_config();
    $this->data->init($this->init);
    $this->data->set_filter();
    $this->data->primary_key_value = $object_id;
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_index',array($this,'_hook_show_panel_allowed'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_appearence_pages_listing',array($this,'_hook_show_panel_allowed'));
	$this->hook->add_action('hook_create_form_view_value_is_frontpage',array($this,'_hook_create_listing_value_is_frontpage'));
	$this->hook->add_action('hook_create_form_view_value_template_id',array($this,'_hook_create_listing_value_template_id'));

    
    $is_login = $this->user_access->is_login();
    if($is_login)     
      $this->load->view('layouts/appearence_pages/view',array('response' => '','page_title' => 'Data Halaman'));
    else
      $this->load->view('layouts/login');
  }
    
  function listing()
  {
	  $this->index();
  }
  
  function _config($id_object = "")
  {
	$current_theme = $this->themes->current_theme;
	$theme_name = (isset($current_theme['theme_name']))?$current_theme['theme_name']:"";
	$templates = $this->db->get("system_templates")->result_array();
	$option_templates = array('' => '------Pilih Template-----');
	
	foreach($templates as $i => $template)
	{
		$option_templates[$template['template_id']] = $template['template_name'];
	}
    $init = array(
			'table' => '(user_pages LEFT JOIN system_page_templates on user_pages.user_page_id = system_page_templates.page_id)',
            'fields' => array(
                          array(
                            'name' => 'page_title',
                            'label' => 'Judul Halaman',
                            'id' => 'page_title',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'page_name',
                            'label' => 'Nama Halaman',
                            'id' => 'page_name',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required',
							'desc' => 'Harus unik digunakan sebagai url, karakter yang boleh dimasukan adalah "0-9","A-z","-" atau "_"'
                          ),
                          /*
                          array(
                            'name' => 'page_description',
                            'label' => 'Page Description',
                            'id' => 'page_description',
                            'value' => '',
                            'type' => 'input_mce',
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => 'required'
                          ),
                          */
                          array(
                            'name' => 'meta_title',
                            'label' => 'Meta Title',
                            'id' => 'meta_title',
                            'value' => '',
                            'type' => 'input_text',
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'meta_keywords',
                            'label' => 'Meta Keywords',
                            'id' => 'meta_keywords',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'meta_description',
                            'label' => 'Meta Description',
                            'id' => 'meta_description',
                            'value' => '',
                            'type' => 'input_textarea',
                            'use_search' => true,
                            'use_listing' => false,
                            'rules' => ''
                          ),
                          array(
                            'name' => 'status',
                            'label' => 'Status',
                            'id' => 'status',
                            'value' => '',
                            'type' => 'input_selectbox',
							'options' => array('' => '---- Choose Status ----','active' => 'Publish','not active' => 'Draft'),
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
                          array(
                            'name' => 'template_id',
                            'label' => 'Template',
                            'id' => 'template_id',
                            'value' => '',
                            'type' => 'input_selectbox',
							'options' => $option_templates,
                            'use_search' => true,
                            'use_listing' => true,
                            'rules' => 'required'
                          ),
							array(
									'name' => 'is_frontpage',
									'label' => 'Set Sebagai Halaman Home',
									'id' => 'is_frontpage',
									'value' => '',
									'type' => 'input_selectbox',
									'use_search' => false,
									'use_listing' => true,
									'options' => array(	'0' => 'Not Active','1' => 'Active'),
									'rules' => ''
								)
                    ),
                    'path' => "/admin/",
                    'controller' => 'appearence_pages',
                    'function' => 'index',
                    'primary_key' => 'user_page_id',
                    'panel_function' => array(
                                              array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
                                              array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
                                              array('title' => 'Show Page','name' => 'frontend', 'class' => 'glyphicon-share'),
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'bulk_options' => array(
                                              array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
                                            ),
                    'with_numbering' => false
          );
    $this->init = $init;
  }
  
  function _hook_do_add($param = "")
  {
	if(isset($param['template_id']))
		unset($param['template_id']);
    return $param;
  }
  
  function _hook_do_after_add($param = "")
  {
	$data = $this->input->post("data");
	$template_id = (isset($data['template_id']))?$data['template_id']:"";
	$is_frontpage = (isset($data['is_frontpage']) and $data['is_frontpage'] == 1)?1:0;
	$current_theme = $this->themes->current_theme;
	$theme_id = (isset($current_theme['theme_id']))?$current_theme['theme_id']:"";
	$input_data = array("page_id"=>$param/*,"theme_id" => $theme_id*/,"template_id" => $template_id);
	$where_data = $input_data;
	if(isset($where_data['template_id']))
		unset($where_data['template_id']);
	
	$q_check = $this->db->where($where_data)->get("system_page_templates");
	if($q_check->num_rows() > 0)
	{
		$this->db->where($where_data)->update("system_page_templates",$input_data);
	}else{
		$this->db->insert("system_page_templates",$input_data);
	}
	if($is_frontpage == 1)
	{
		$this->db->query("UPDATE user_pages SET is_frontpage = '0' WHERE user_page_id != '".$param."'");
	}
	
	$this->templates->copy_to_page_template($template_id,$param);
    return $param;
  }
  
  function _hook_do_after_edit($param = "")
  {
	  return $this->_hook_do_after_add($param);
  }
  
  function _hook_do_after_delete($param = "")
  {
		$where_data = array("page_id"=>$param);
		$this->db->where($where_data)->delete("system_page_templates");
		return true;
  }
  
  function _hook_do_edit($param = "")
  {
    return $param;
  }
  
  function _hook_do_delete($param = "")
  {
    return $param;
  }
  
  function _hook_create_form_title_add($title){
    return "Tambah Halaman Baru";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit Halaman";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return 1;
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
	function _hook_create_listing_value_is_frontpage($default_value = "")
	{
		return ($default_value == 1)?"Home Page":"-";
	}
	function _hook_create_listing_value_template_id($default_value = "")
	{		
		$template = $this->db->where(array("template_id" => $default_value))->get("system_templates")->row_array();
		$theme_id = (isset($template['theme_id']))?$template['theme_id']:"";
		$theme = $this->db->where(array("theme_id" => $theme_id))->get("system_themes")->row_array();
		
		$output = (isset($theme['theme_name']))?$theme['theme_name'] . ' - ' : '';
		$output .= (isset($template['template_name']))?$template['template_name'] : $default_value;
		return $output;
	}
	
	function check_page_name()
	{
		$page_name = $this->input->post("page_name");
		$check = $this->db->where(array("page_name" => $page_name))->get("user_pages")->row_array();
		$output = $page_name;
		if(is_array($check) and count($check) > 0)
		{
			$last_page = $this->db->query("SELECT * FROM user_pages ORDER BY user_page_id DESC")->row_array();
			$output = $page_name . '-' . $last_page['user_page_id'];
		}
		
		echo $output;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
