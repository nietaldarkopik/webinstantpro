<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminplugins extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();

		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');

		if($is_login)
			$this->load->view('layouts/adminplugins/listing',array('response' => '','page_title' => 'Data Plugin','config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'listing_config' => $this->init));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
    $this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_listing',array($this,'_hook_show_panel_allowed'));

		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminplugins/edit',array('response' => $response,'page_title' => 'Data Plugin'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
    
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminplugins/add',array('response' => $response,'page_title' => 'Data Plugin'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_listing',array($this,'_hook_show_panel_allowed'));

		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminplugins/view',array('response' => '','page_title' => 'Data Plugin'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_edit',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_view',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_delete',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_index',array($this,'_hook_show_panel_allowed'));
		$this->hook->add_action('hook_show_panel_allowed_panel_/_Data Plugin_listing',array($this,'_hook_show_panel_allowed'));
		
		$is_login = $this->user_access->is_login();


		$config_form_filter = $this->init;
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2));
		$config_form_add = $this->init;
		$config_form_add['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
		$config_form_filter['action'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
			
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/adminplugins/listing',array('response' => '','page_title' => 'Data Plugin','config_form_filter' => $config_form_filter,'config_form_add' => $config_form_add));
		else
			$this->load->view('layouts/login');
		
	}
	
	function activate(){}
	function block_controller(){}
	function checker(){}
	function configuration(){}
	function fetch_list(){}
	function front_controller(){}
	function hook(){}
	function install(){}
	function install_widgets(){}
	function page(){}
	function uninstall(){}
	function update(){}
	function widget(){}
	
	function plugin_controller($plugin_id="",$controller="",$function="")
	{
		$function = (empty($function))?"index":$function;
		$this->db->where(array('plugin_id' => $plugin_id));
		$q = $this->db->get(TABLE_SYS_PLUGINS);
		$data_plugin = $q->row_array();
		$plugin_name = (isset($data_plugin['plugin_name']))?$data_plugin['plugin_name']:"";
		$this->plugins->set_path($plugin_name);
		$plugin = $this->plugins->call_plugin_controller($plugin_name,$controller,true);
		$output = $plugin->$function();
		echo $output;
	}

	function _config($id_object = "")
	{
		$init = array(	'table' => TABLE_SYS_PLUGINS,
						'fields' => array(
											array(
												'name' => 'plugin_title',
												'label' => 'Plugin Title',
												'id' => 'plugin_title',
												'value' => '',
												'type' => 'input_text',
												'use_search' => 'true',
												'use_listing' => 'true',
												'rules' => 'required'
											),
											array(
												'name' => 'plugin_name',
												'label' => 'Plugin Name',
												'id' => 'plugin_name',
												'value' => '',
												'type' => 'input_text',
												'use_search' => 'true',
												'use_listing' => 'true',
												'rules' => 'required'
											),
											array(
												'name' => 'status',
												'label' => 'Status',
												'id' => 'status',
												'value' => '',
												'type' => 'input_text',
												'use_search' => 'true',
												'use_listing' => 'true',
												'rules' => ''
											),
											array(
												'name' => 'description',
												'label' => 'Description',
												'id' => 'description',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => 'true',
												'use_listing' => 'true',
												'rules' => ''
											),
											array(
												'name' => 'plugin_data',
												'label' => 'Plugin Data',
												'id' => 'plugin_data',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => 'false',
												'use_listing' => 'false',
												'rules' => ''
											)
                    ),
                    'primary_key' => 'plugin_id',
                    'path' => "/admin/",
                    'controller' => 'adminplugins',
                    'function' => 'index',
                    'panel_function' => array(
                                                array('title' => 'Install','name' => 'install', 'class' => 'glyphicon-edit'),
                                                array('title' => 'Uninstall','name' => 'uninstall', 'class' => 'glyphicon-share'),
                                                array('title' => 'Data Manager','name' => 'plugin_controller', 'class' => 'glyphicon-share'),
                                                array('title' => 'Widget Manager','name' => 'block_controller', 'class' => 'glyphicon-share'),
                                                array('title' => 'Configuration','name' => 'configuration', 'class' => 'glyphicon-cog'),
                                                array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-share')
                                              ),
                    'bulk_options' => array(
                                                array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share')
                                              )
          );
		$this->init = $init;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
  
  function _hook_create_form_title_add($title){
    return "Tambah PLUGIN";
  }
  
  function _hook_create_form_title_edit($title){
    return "Edit PLUGIN";
  }
  
  function _hook_create_form_ajax_target_add(){
    return ".ajax_container";
  }
  
  function _hook_create_form_filter_ajax_target(){
    return ".ajax_container";
  }
  
  function _hook_ajax_false(){
    return "";
  }
  
  function _hook_ajax_true(){
    return "ajax";
  }
  
  function _hook_show_panel_allowed($panel = "")
  {
    #$panel = str_replace(".ajax_container",".content-container",$panel);
    return $panel;
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
