<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration_user_roles extends Admin_Controller {

	var $init = array();
	
	function index()
	{
		$is_ajax = $this->input->post('is_ajax');
		$user_level_id = $this->input->post('user_level_id');
		
		$this->_do_save();
		
		$user_access = array();
		$user_levels = array();
		$controllers = array();
		if(!empty($user_level_id))
		{
			$this->db->where(array("user_level_id" => $user_level_id));
			$q = $this->db->get("user_level_roles");
			$t_user_access = $q->result_array();
			foreach($t_user_access as $index => $user_a)
			{
        $user_a['path'] = ($user_a['path'] == '/')?'user':$user_a['path'];
				$user_access[trim(strtolower($user_a['path']),"/")][$user_a['controller']][$user_a['function']] = $user_a['function'];
			}
			
			
			$controllers = $this->_get_controllers();
		}
		
		$is_login = $this->user_access->is_login();
		if($is_login)
		{
		  $inline_js ='';
			
      $this->assets->add_js_inline($inline_js,'body');
			$user_levels = $this->db->get("user_levels");
			$user_levels = $user_levels->result_array();
			$this->load->view('layouts/administration_user_roles/listing',array(	'response' => '',
																'controllers' => $controllers,
																'user_levels' => $user_levels,
																'user_access' => $user_access,
																'user_level_id' => $user_level_id,
																'page_title' => 'Right User Access'
																));
		}
		else
			$this->load->view('layouts/login');
	}
	
	function _do_save()
	{
		$is_ajax = $this->input->post('is_ajax');
		$user_level_id = $this->input->post('user_level_id_save');
		$post_method = $this->input->post("method");
		$do_process = $this->input->post("do_process");
		
		if($do_process == "do_save" and !empty($user_level_id))
		{
			$this->db->where(array("user_level_id" => $user_level_id));
			$query = $this->db->delete("user_level_roles");
			
			if(is_array($post_method) and count($post_method) > 0)
			{
				foreach($post_method as $path => $p_controller)
				{
          $path = ($path == 'user')?'/':'/'.$path.'/';
					if(is_array($p_controller) and count($p_controller) > 0)
					{
            foreach($p_controller as $controller => $p_method)
            {
              if(is_array($p_method) and count($p_method) > 0)
              {
                foreach($p_method as $function => $value)
                {
                  $this->db->where(array(	"user_level_id" => $user_level_id,
                                          "path" => $path,
                                          "controller" => $controller,
                                          "function" => $function
                                        ));
                  $q_check = $this->db->get("user_level_roles");
                  if($q_check->num_rows() == 0)
                    $this->db->insert("user_level_roles",array( "user_level_id" => $user_level_id,
                                                                "path" => $path,
                                                                "controller" => $controller,
                                                                "function" => $function,
                                                                "user_role_name" => $controller.'_'.$function));
                    
                }
              }
            }
					}
				}
			}
			return true;
		}
		
		return false;
	}

	function _config($id_object = "")
	{			
		$init = array(	'table' => 'users',
						'fields' => array(	
											array(
													'name' => 'user_name',
													'label' => 'Username',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
											array(
													'name' => 'password',
													'label' => 'Password',
													'id' => 'password',
													'value' => '',
													'type' => 'password',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'user_name',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|valid_email'
												),
											array(
													'name' => 'user_level_id',
													'label' => 'User Level',
													'id' => 'user_level_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_levels',
													'select' => array('user_level_id AS value','user_level_name AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_level_id',
													'rules' => 'required'
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_selectbox',
													'options' => array('' => '---- Select Option ----','active' => 'Active','not active' => 'Not Active'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'user_id'
					);
		$this->init = $init;
	}
	
	function _get_controllers()
	{
		$controllers         = array();
    $dir                 = APPPATH.'controllers/';
    $controller_files    = scandir_r($dir);
    
    $controllers = get_class_methods_r_path($controller_files,$dir);
    return $controllers;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
