<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminwizard extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		
	}
	
	function create_page()
	{
		require_once(FCPATH . 'themes/user/default/default_theme.php');
		$this->load->set_object_path('view',array(FCPATH."themes/user/" => 1),false);
		$default_theme = new Default_theme();
		$default_theme->setAssets();
		$default_theme->setAdminAssets();
		$this->load->view("default/templates/layouts/create-page");
	}
	
	function choose_template()
	{
		require_once(FCPATH . 'themes/user/default/default_theme.php');
		$this->load->set_object_path('view',array(FCPATH."themes/user/" => 1),false);
		$default_theme = new Default_theme();
		$default_theme->setAssets();
		$default_theme->setAdminAssets();
		$this->load->view("default/templates/layouts/choose-template");
	}
	
	function choose_theme()
	{
		require_once(FCPATH . 'themes/user/default/default_theme.php');
		$this->load->set_object_path('view',array(FCPATH."themes/user/" => 1),false);
		$default_theme = new Default_theme();
		$default_theme->setAssets();
		$default_theme->setAdminAssets();
		$this->load->view("default/templates/layouts/choose-theme");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
