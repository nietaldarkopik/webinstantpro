<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$output_widget = "";
	$do_action = $this->input->post("do_action");
	$theme_id = $this->input->post("theme_id");
	$template_id = $this->input->post("template_id");
	$con_id = $this->input->post("con_id");
	$row_id = $this->input->post("row_id");
	$block_col_id = $this->input->post("block_col_id");						
	$widget_id = (isset($current_data_widget['widget_id']))?$current_data_widget['widget_id']:"";

	$attributes = 'theme_id="'.$theme_id.'" template_id="'.$template_id.'" con_id="'.$con_id.'"  row_id="'.$row_id.'" block_col_id="'.$block_col_id.'" widget_id="'.$widget_id.'" ';
	if(!empty($block_col_id) and !empty($row_id) and !empty($con_id))
	{
		$this->session->set_userdata("attr_widget_custom_text",$attributes);
		$sess_attributes = $this->session->userdata("attr_widget_custom_text");
	}
	$attributes = (!empty($sess_attributes))?$sess_attributes:$attributes;
?>
<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
?>
<div class="ajax_container content-container col-sm-12 col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $page_title;?></h3>
		</div>
		<div class="panel-body no-padding">
			<div class="row data_target<?php echo $is_modal;?>">
				<div class="col-lg-12">
					<ul class="nav nav-tabs margintop-1">
						<li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
						<li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
					</ul>
					<br class="clearfix"/>
					<div class="col-lg-12 col-md-12">
						<?php 
						echo (isset($response))?$response:'';
						?>
					</div>
					<br/>
					<div id='content' class="tab-content">
					
						<div class="tab-pane active" id="listing<?php echo $is_modal;?>">
							<div class="row">
								<div class="col-lg-12 col-md-12 hidden-print clearfix">
									<div class="col-lg-12 col-md-12 hidden-print clearfix">
										<h3 class="box-title">Pilih Dari Yang Sudah Ada</h3>
									</div>
									<?php 
									  echo $this->data->create_form_filter($this->init);
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 hidden-print">
									<div class="col-lg-12 col-md-12 hidden-print">
									  <?php
										
										$paging_config = (isset($paging_config))?$paging_config:array();
										$paging_config['base_url'] = base_url().'admin/adminblocks/setting_widget/news_small/configuration/listing/';
										$paging_config['uri_segment'] = 7;
										
										$this->data->init_pagination($paging_config);
										$pagination =  $this->data->create_pagination($paging_config);
										$pagination =  str_replace('<a ','<a data-target-ajax=".ajax_container" ',$pagination);
										echo $pagination;
										$listing = $this->data->create_listing($this->init);
										$data_rows = $this->data->data_rows;
									  ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<form method="post" action="<?php echo current_url();?>" form="sort_menu" id="sort_menu" class="ajax" data-target-ajax=".ajax_container">
										<?php

										if(is_array($data_rows) and count($data_rows) > 0)
										{
											foreach($data_rows as $i => $row)
											{
												$widget_url = '';//base_url().'admin/adminblocks/setting_widget/'.$widget['widget_name'].'/configuration/';
												$output_widget .= '
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<button widget_url="'.$widget_url.'" type="button" class="btn-checkbox pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-widgets" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off" '.$attributes.' widget_content_id="'.$row['news_category_id'].'">
																	<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">'.$row['category_name'].'</span>
																</button>
															</div>
															';
											}
										}					
										$output_widget .= '
															<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
																<button widget_url="'.$widget_url.'" type="button" class="btn-checkbox pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-widgets" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off" '.$attributes.' widget_content_id="0">
																	<span class="pull-left glyphicon glyphicon-plus"></span> <span class="pull-right">Semua Kategori</span>
																</button>
															</div>
															';
										echo $output_widget;
										?>
										<div class="col-lg-12 col-md-12">
											<br/>
											<?php
												echo $pagination;
											?>
										</div>
										<input type="hidden" name="do_save_menu" value="do_save_menu"/>
										<input type="hidden" name="is_ajax" value="1"/>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<button type="button" class="center-block pull-left btn btn-lg col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 save-widgets" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off">
												<span class="glyphicon glyphicon-plus"></span> Tambahkan ke halaman
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						
						<div class="tab-pane" id="add<?php echo $is_modal;?>">
							<div class="col-lg-12 col-md-12">
								<?php 
								//$add_config['base_url'] = base_url().'admin/adminblocks/setting_widget/news_small/configuration/listing/';
								echo $this->data->create_form($add_config);
								?>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<br/>
		</div>
	</div>
</div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
