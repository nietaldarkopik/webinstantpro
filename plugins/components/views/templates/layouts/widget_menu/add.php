<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
		  <div class="row data_target<?php echo $is_modal;?>">
			<div class="col-lg-12">
			  <ul class="nav nav-tabs margintop-1 marginleft-16">
			  <?php
				echo $this->data->show_panel_allowed("","admin","",array(array('title' => 'Listing','name' => 'listing', 'class' => 'glyphicon-list')),"",false);
			  ?>
			  </ul>
			  <br class="fclear"/><br/>
			  <?php
				if(isset($response) and !empty($response))
				{
				  echo $response;
				}
			  ?>
				<?php
					echo $this->data->create_form($this->init);
				?>

					<script type="text/javascript">
						jQuery(document).ready(function(){
							jQuery("[name=\"data[type]\"]").on("change",function(){
								var menu_type = jQuery(this).val();
								
								jQuery.ajax({
									url: "<?php echo base_url("admin/appearence_plugins/plugin_controller/components/menu_items/get_object_tables");?>",
									data:"menu_type="+menu_type,
									type: "post",
									success: function(msg)
									{
										var first_option = $("#menu_type_name option").eq(0).clone();
										$("#menu_type_name").html(first_option);
										$("#menu_type_name").append(msg);
									}
								});
							});
						});
					</script>
			</div>
		  </div>
      </div>
	</div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>
