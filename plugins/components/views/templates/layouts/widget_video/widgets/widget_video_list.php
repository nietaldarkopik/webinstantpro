<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$output_widget = "";
	$do_action = $this->input->post("do_action");
	$theme_id = $this->input->post("theme_id");
	$template_id = $this->input->post("template_id");
	$con_id = $this->input->post("con_id");
	$row_id = $this->input->post("row_id");
	$block_col_id = $this->input->post("block_col_id");						
	$widget_id = (isset($current_data_widget['widget_id']))?$current_data_widget['widget_id']:"";

	$attributes = 'theme_id="'.$theme_id.'" template_id="'.$template_id.'" con_id="'.$con_id.'"  row_id="'.$row_id.'" block_col_id="'.$block_col_id.'" widget_id="'.$widget_id.'" ';
	if(!empty($block_col_id) and !empty($row_id) and !empty($con_id))
	{
		$this->session->set_userdata("attr_widget_custom_text",$attributes);
		$sess_attributes = $this->session->userdata("attr_widget_custom_text");
	}
	$attributes = (!empty($sess_attributes))?$sess_attributes:$attributes;
?>
<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
?>
<div class="ajax_container content-container col-sm-12 col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $page_title;?></h3>
		</div>
		<div class="panel-body no-padding">
			<div class="row data_target<?php echo $is_modal;?>">
				<div class="col-lg-12">
					<ul class="nav nav-tabs margintop-1">
						<li class="active"><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-cog"></span></a></li>
					</ul>
					<br class="clearfix"/>
					<div class="col-lg-12 col-md-12">
						<?php 
						echo (isset($response))?$response:'';
						?>
					</div>
					<br/>
					<div id='content' class="tab-content">
						<div class="tab-pane active" id="config<?php echo $is_modal;?>">
							<form method="post" class="ajax" action="<?php echo str_replace("index.php/","",current_url());?>" id="form_video_list">
								<div class="col-lg-12 col-md-12">
									<div class="form-group">
									  <label for="config_widget_title">Judul Widget</label>
									  <input type="text" class="form-control" name="config[widget_title]" id="config_widget_title" value="Video">
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_amount_data">Jumlah data yang ditampilkan</label>
									  <input type="number" class="form-control" name="config[amount_data]" id="config_amount_data" value="5">
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_sort_by">Urutkan berdasarkan</label>
									  <select class="form-control" name="config[sort_by]" id="config_sort_by">
										<option value="title">Judul Video</option>
										<option value="date_inserted">Tanggal Input</option>
									  </select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_sorting">Urutan</label>
									  <select class="form-control" name="config[sorting]" id="config_sorting">
										<option value="asc">A-Z</option>
										<option value="desc">Z-A</option>
										<option value="rand">Random</option>
									  </select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_amount_char_title">Jumlah karakter judul</label>
									  <input type="number" class="form-control" name="config[amount_char_title]" id="config_amount_char_title" value="20">
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_amount_char_description">Jumlah karakter deskripsi</label>
									  <input type="number" class="form-control" name="config[amount_char_description]" id="config_amount_char_description" value="150">
									</div>
								</div>
								<div class="col-lg-4 col-md-4">
									<div class="form-group">
									  <label for="config_text_readmore">Text ke link detail</label>
									  <input type="text" class="form-control" name="config[text_readmore]" id="config_text_readmore" value="Detail">
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<button type="button" class="hidden active btn-checkbox pull-left btn btn-lg bg-light-blue col-lg-12 col-md-12 col-sm-12 col-xs-12 margintop20 add-widgets" number_columns="1" data-toggle="button" aria-pressed="false" autocomplete="off" <?php echo $attributes;?> widget_content_id="0"></button>
									</div>
									<button class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12 save-widgets" type="button">
										<span class="glyphicon glyphicon-plus"></span> Simpan dan Masukan Widget
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<br/>
		</div>
	</div>
</div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
