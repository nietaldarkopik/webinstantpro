<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
      <div class="row data_target<?php echo $is_modal;?>">
        <div class="col-lg-12">
			<ul class="nav nav-tabs margintop-1 marginleft-16">
				<?php
					echo $this->data->show_panel_allowed("","admin","",array("listing"),"",false);
				?>
			</ul>
			<br class="fclear"/><br/>
			<?php
				echo $response;
				echo $this->data->create_form($this->init);
			?>

			<script type="text/javascript">
				jQuery("select#controller").on("change",function(){
					jQuery.ajax({
						type: "post",
						url: base_url + "admin/menus/listing",
						data: "get_functions=" + jQuery("select#controller").val(),
						success:function(msg){
							jQuery("select#function").html(msg);
						}
					});
				});
				jQuery(document).ready(function()
				{
					var current_function = jQuery("select#function").val();
					jQuery.ajax({
						type: "post",
						url: base_url + "admin/menus/listing",
						data: "get_functions=" + jQuery("select#controller").val() + "&current_function=" + current_function,
						success:function(msg){
							jQuery("select#function").html(msg);
						}
					});
				});
			</script>
        </div>
      </div>
    </div>
  </div>
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
      </div>
    </div>
  </div>
<?php
}
?>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php #$this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>

