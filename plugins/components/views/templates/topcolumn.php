<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
            <div id="carousel-slider" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators visible-xs">
                <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-slider" data-slide-to="1"></li>
                <li data-target="#carousel-slider" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">
                <div class="item active">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div>
                 <div class="item">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div> 
                 <div class="item">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div> 
              </div>
              
              <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
                <i class="glyphicon glyphicon-chevron-left"></i> 
              </a>
              
              <a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
                <i class="glyphicon glyphicon-chevron-right"></i> 
              </a>
            </div> <!--/#carousel-slider-->
          
