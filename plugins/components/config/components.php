<?php

$config = array();
$config['plugin_name']  = 'components';
$config['plugin_title'] = 'Bootstrap Components';
$config['description']  = 'Collections of Bootstrap Components';
$config['author']  = 'nietaldarkopik@gmail.com';
$config['version']  = '1.0';
