<?php

$widgets = array();


$components_row['widget_name']  = 'Components Row';
$components_row['widget_title'] = 'Element Row';
$components_row['description']  = 'Bootstrap Element Row';
$components_row['author']  = 'nietaldarkopik@gmail.com';
$components_row['version']  = '1.0';

$widgets['components_row'] = $components_row;

$components_column['widget_name']  = 'Components Column';
$components_column['widget_title'] = 'Element Column';
$components_column['description']  = 'Bootstrap Element Column';
$components_column['author']  = 'nietaldarkopik@gmail.com';
$components_column['version']  = '1.0';

$widgets['components_column'] = $components_column;

$components_heading_text['widget_name']  = 'Components Heading Text';
$components_heading_text['widget_title'] = 'Element Heading Text';
$components_heading_text['description']  = 'Bootstrap Element Heading Text';
$components_heading_text['author']  = 'nietaldarkopik@gmail.com';
$components_heading_text['version']  = '1.0';

$widgets['components_heading_text'] = $components_row;

$components_text['widget_name']  = 'Components Text';
$components_text['widget_title'] = 'Element Text';
$components_text['description']  = 'Bootstrap Element Text';
$components_text['author']  = 'nietaldarkopik@gmail.com';
$components_text['version']  = '1.0';

$widgets['components_text'] = $components_text;

$components_menu['widget_name']  = 'Components Menu';
$components_menu['widget_title'] = 'Element Menu';
$components_menu['description']  = 'Bootstrap Element Menu';
$components_menu['author']  = 'nietaldarkopik@gmail.com';
$components_menu['version']  = '1.0';

$widgets['components_menu'] = $components_menu;

$components_page_content['widget_name']  = 'Components Page Content';
$components_page_content['widget_title'] = 'Element Page Content';
$components_page_content['description']  = 'Bootstrap Element Page Content';
$components_page_content['author']  = 'nietaldarkopik@gmail.com';
$components_page_content['version']  = '1.0';

$widgets['components_page_content'] = $components_page_content;

$components_image['widget_name']  = 'Components Image';
$components_image['widget_title'] = 'Element Image';
$components_image['description']  = 'Bootstrap Element Image';
$components_image['author']  = 'nietaldarkopik@gmail.com';
$components_image['version']  = '1.0';

$widgets['components_image'] = $components_image;

