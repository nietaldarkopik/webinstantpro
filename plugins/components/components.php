<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class components{
  var $CI;
  var $plugin_name = "components";
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function install()
  {
    return ($this->install_widget() and $this->install_menu_admin() and $this->install_db())?true:false;
  }
  
  function install_widget()
  {
    $widgets = $this->CI->widgets->fetch_widgets(FCPATH.'plugins/'.$this->plugin_name.'/controllers/widgets/',".php");
    
    if(is_array($widgets) and count($widgets) > 0)
    {
      foreach($widgets as $i => $w)
      {
		$widget_config = (isset($w['configs']['data']))?$w['configs']['data']:array();
		echo "<pre>";
		print_r($widget_config);
		echo "</pre>";
        $this->CI->widgets->install($this->plugin_name,$widget_config);
		if(isset($widget_config['class']) and is_object($widget_config['class']))
		{
			$widget_config_class = $widget_config['class'];
			if(method_exists($widget_config_class,'install'))
			{
				$widget_config_class->install();
			}
		}
      }
    }
	return true;
  }
  
  function install_menu_admin()
  {
	  $last_insert_id = 2;
	  //admin menu Root Menu
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_menu"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Menu',
					'menu_name' => 'admin_plugin_components_menu',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/menu/index',
					'attributes' => ' class="ajax dropdown-toggle is_modal"',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
		  $last_insert_id = $this->CI->db->insert_id();
	  }
	  
	  //admin sub menu, Group menu 
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_menu_group"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => $last_insert_id,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Menu Groups',
					'menu_name' => 'admin_plugin_components_menu_group',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/menu/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin sub menu, menu items
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_menu_item"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => $last_insert_id,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Data Menu',
					'menu_name' => 'admin_plugin_components_menu_item',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/menu_items/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu Images
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_image"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Images',
					'menu_name' => 'admin_plugin_components_image',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/image/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu Videos
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_video"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Videos',
					'menu_name' => 'admin_plugin_components_video',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/video/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu sliders
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_slider"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Sliders',
					'menu_name' => 'admin_plugin_components_slider',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/slider/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu custom text
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_custom_text"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Text',
					'menu_name' => 'admin_plugin_components_custom_text',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/custom_text/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  	  
	  //admin menu News/Blog
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_news"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Blog',
					'menu_name' => 'admin_plugin_components_news',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/news/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu Portfolio
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_portfolio"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Portfolio',
					'menu_name' => 'admin_plugin_components_portfolio',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/portfolio/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  
	  //admin menu Contact
	  if($this->CI->db->where(array("menu_name" => "admin_plugin_components_contact"))->get("user_custom_menus")->num_rows() == 0)
	  {
		  $data_menu = array('user_menu_group_id' => 2,
					'parent_menu' => 2,
					'menu_title' => '<span class="glyphicon glyphicon-cog"></span> Contact',
					'menu_name' => 'admin_plugin_components_contact',
					'path' => '/admin/',
					'controller' => 'appearence_plugins',
					'function' => 'plugin_controller',
					'url' => 'admin/appearence_plugins/plugin_controller/components/contact/index',
					'attributes' => ' class="ajax is_modal" data-target="#admin-menu-panel" data-target-ajax="#admin-menu-panel .modal-dialog" data-content=".ajax_container" ',
					'type' => 'plugin',
					'status' => 'active');
		  $this->CI->db->insert("user_custom_menus",$data_menu);
	  }
	  return true;
  }
  
  function install_db()
  {
	  return true;
  }
  
  function setPath()
  {
    $this->CI->load->set_object_path('view',array(FCPATH.'plugins/'.$this->plugin_name.'/views/templates/' => true),false);
    $this->CI->load->set_object_path('library',array(FCPATH.'plugins/'.$this->plugin_name.'/library/' => true),false);
    $this->CI->load->set_object_path('controller',array(FCPATH.'plugins/'.$this->plugin_name.'/controller/' => true),false);
    $this->CI->load->set_object_path('helper',array(FCPATH.'plugins/'.$this->plugin_name.'/helper/' => true),false);
    $this->CI->load->set_object_path('config',array(FCPATH.'plugins/'.$this->plugin_name.'/config/' => true),false);
    $this->CI->load->set_object_path('model',array(FCPATH.'plugins/'.$this->plugin_name.'/model/' => true),false);
  }
    
}
