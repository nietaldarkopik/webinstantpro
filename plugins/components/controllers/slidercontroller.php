<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slidercontroller{

  var $name = 'slider';
  var $title = 'Slider';
  var $description = 'Plugin slider';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_slider_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_slider_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_slider_view'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_slider_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_slider_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_slider_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		
		$do_save_menu = $this->CI->input->post("do_save_menu");
		if(!empty($do_save_menu))
		{
			$save_menu = $this->_save_parent_menu();
			echo $save_menu;
			exit;
		}
		
		$get_functions = $this->CI->input->post("get_functions");
		$current_function = $this->CI->input->post("current_function");
		if(!empty($get_functions))
		{
			$this->_get_functions($get_functions,$current_function);
			exit;
		}
		
    $the_menus = "";
    $is_login = $this->CI->user_access->is_login();
    if(isset($filter['slider_id']))
    {		
      $this->CI->db->where($filter);
      $this->CI->db->order_by("sort_order","ASC");
      $appearence_menus = $this->CI->db->get("widget_menus");
      $appearence_menus = $appearence_menus->result_array();
      $appearence_menus = $this->CI->user_access->mapTree($appearence_menus,0);
      $the_menus = $this->_display_structure($appearence_menus);
    }
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/slider/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/default/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Slider",'the_menus' => $the_menus));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->CI->init['fields']);
		$paging_config = array('base_url' => base_url().'menu/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->CI->init['fields']))?$this->CI->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->CI->init['fields'] = $init;
		
		$response = $this->CI->data->edit("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/edit',array('response' => $response,'page_title' => $this->title));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$response = $this->CI->data->add("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah Slider'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/view',array('response' => '','page_title' => $this->title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config_menu_item($id_object = "")
	{			
		$this->CI->data->_get_controllers();
		$data_methods_arr = $this->CI->data->methods;
		$data_methods = array();
		foreach($data_methods_arr as $i => $v)
		{
			$data_methods[$v] = $v;
		}
        
		$init = array(	'table' => "widget_menus",
						'sort_order' => 'parent_menu ASC,sort_order ASC',
						'fields' => array(
											array(
													'name' => 'slider_id',
													'label' => 'Slider Group',
													'id' => 'slider_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> "widget_sliders".' menu2',
													'select' => array('slider_id AS value','name AS label'),
													'options' => array('' => '---- Choose Slider Group ----'),
													'primary_key' => 'slider_id',
													'rules' => 'required'
												),
											array(
													'name' => 'parent_menu',
													'label' => 'Parent Slider',
													'id' => 'parent_menu',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> "widget_menus".' menu2',
													'select' => array('menu_id AS value','menu_title AS label'),
													'options' => array('' => '---- Choose Feature ----','0' => '---- No Parent ----'),
													'primary_key' => 'menu_id',
													'rules' => 'required',
                                                    'js_connect_to' => array( 
                                                    'id_field_parent' => '"select[name=\"data[slider_id]\"]"',
                                                    'table' => "widget_menus",
                                                    'where' => '',
                                                    'select' => 'menu_id AS value,menu_title AS label',
                                                    'primary_key' => 'menu_id',
                                                    'foreign_key' => 'slider_id')
												),
											array(
													'name' => 'menu_title',
													'label' => 'Slider Title',
													'id' => 'menu_title',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required',
													'list_style' => 'width="150"'
												),
											array(
													'name' => 'sort_order',
													'label' => 'Nomor Urutan',
													'id' => 'sort_order',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'numeric',
													'list_style' => 'width="100" align="center"'
												),
											array(
													'name' => 'type',
													'label' => 'Type Slider',
													'id' => 'type',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> "widget_menu_types".' menu_type',
													'select' => array('menu_type_name AS value','menu_type_title AS label'),
													'options' => array('' => '---- Pilih Jenis Slider ----'),
													'primary_key' => 'menu_type_id',
													'rules' => 'required',
                                                    'js_connect_to' => array(
																			'id_field_parent' => '"select[name=\"data[menu_type_name]\"]"',
																			'table' => "widget_menu_types",
																			'where' => '',
																			'select' => 'menu_id AS value,menu_title AS label',
																			'primary_key' => 'menu_id',
																			'foreign_key' => 'menu_type_name'
																			)
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array(	'active' => 'Active','not active' => 'Not Active'),
													'rules' => ''
												)
										),
									'primary_key' => 'menu_id',
									'path' => "/admin/",
									'controller' => 'appearence_menus',
									'function' => 'index',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'sess_keyname' => 'components_slidercontroller'
					);
		$this->CI->init = $init;
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_sliders",
							'fields' => array(
														array(
															'name' => 'title',
															'label' => 'Title',
															'id' => 'title',
															'value' => '',
															'type' => 'input_text',
															'use_search' => true,
															'use_listing' => true,
															'rules' => 'required'
														),
														array(
															'name' => 'description',
															'label' => 'description',
															'id' => 'description',
															'value' => '',
															'type' => 'input_textarea',
															'use_search' => true,
															'use_listing' => true,
															'rules' => ''
														)
						),
						'primary_key' => 'widget_slider_id',
						'path' => "/admin/",
						'controller' => 'appearence_plugins',
						'function' => 'plugin_controller',
						'panel_function' => array(
												  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
												  array('title' => 'List Image','name' => 'view', 'class' => 'glyphicon-share'),
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'bulk_options' => array(
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'sess_keyname' => 'components_slidercontroller_slider',
						'action' => base_url("admin/appearence_plugins/plugin_controller/components/slider/add"),
						'uri_segment' => 7
					);
		$this->CI->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Slider";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Slider";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_slider_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/slider/delete",$param);
		return $param;
	}
	function _components_slider_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/slider/edit",$param);
		return $param;
	}
	function _components_slider_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/slider_items/listing",$param);
		return $param;
	}
	function _components_slider_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/slider/listing",$param);
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
