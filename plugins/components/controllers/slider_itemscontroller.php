<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_itemscontroller{

  var $name = 'slider_items';
  var $title = 'Slider Images';
  var $description = 'Plugin Slider Image';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_slider_items_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_slider_items_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_slider_items_view'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_slider_items_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_slider_items_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_slider_items_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$this->CI->hook->add_action('hook_create_listing_value_image_url',array($this,'_hook_create_listing_value_image_url'));
		$this->CI->hook->add_action('hook_create_listing_value_widget_slider_id',array($this,'_hook_create_listing_value_widget_slider_id'));
		
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
				
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/slider_items/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/default/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Slider Image"));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->CI->init['fields']);
		$paging_config = array('base_url' => base_url().'menu/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->CI->init['fields']))?$this->CI->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->CI->init['fields'] = $init;
		
		$response = $this->CI->data->edit("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/edit',array('response' => $response,'page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_items_listing'));
		$response = $this->CI->data->add("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah Slider Image'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/view',array('response' => '','page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_slider_images",
							'fields' => array(
												array(
													'name' => 'widget_slider_id',
													'label' => 'Slider Group',
													'id' => 'widget_slider_id',
													'value' => '',
													'type' => 'input_selectbox',
													'query' => 'SELECT title label,widget_slider_id value FROM widget_sliders',
													'options' => array('' => '-----Pilih Group Widget Slider Image-----'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'title',
													'label' => 'Title',
													'id' => 'title',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'text1',
													'label' => 'Text',
													'id' => 'text1',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => ''
												),
												array(
													'name' => 'image_url',
													'label' => 'Image',
													'id' => 'image_url',
													'value' => '',
													'type' => 'input_file',
													'use_search' => false,
													'use_listing' => true,
													'config_upload' => array( 
																			'upload_path' => './uploads/sliders/',
																			'encrypt_name' => true,
																			'allowed_types' =>  'jpg|gif|png'
																			),
													'rules' => 'required_file|required'
												)
											),
						'primary_key' => 'widget_slider_image_id',
						'path' => "/admin/",
						'controller' => 'appearence_plugins',
						'function' => 'plugin_controller',
						'panel_function' => array(
												  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
												  array('title' => 'List Image','name' => 'view', 'class' => 'glyphicon-share'),
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'bulk_options' => array(
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'sess_keyname' => 'components_slider_itemscontroller_slider',
						'action' => base_url("admin/appearence_plugins/plugin_controller/components/slider_items/add"),
						'uri_segment' => 7
					);
		$this->CI->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Slider Image";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Slider Image";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_slider_items_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/slider_items/delete",$param);
		return $param;
	}
	function _components_slider_items_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/slider_items/edit",$param);
		return $param;
	}
	function _components_slider_items_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/slider_items/view",$param);
		return $param;
	}
	function _components_slider_items_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/slider_items/listing",$param);
		return $param;
	}
	function _hook_create_listing_value_image_url($default_value = "")
	{
		$default_value = str_replace("./",base_url(),$default_value);
		$default_value = str_replace("<img","<img class='thumbnail'",$default_value);
		$default_value = str_replace("height=\"50\"","height=\"100\"",$default_value);
		return $default_value;
	}
	function _hook_create_listing_value_widget_slider_id($default_value = "")
	{
		$d = $this->CI->db->query("SELECT * FROM widget_sliders WHERE widget_slider_id = '".$default_value."'")->row_array();
		return (isset($d['title']))?$d['title']:$default_value;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
