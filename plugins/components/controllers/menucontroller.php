<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menucontroller{

  var $name = 'menu';
  var $title = 'menu';
  var $description = 'Plugin menu';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_menu_group_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_menu_group_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_menu_group_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_menu_group_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_menu_group_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_menu_group_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		
		$do_save_menu = $this->CI->input->post("do_save_menu");
		if(!empty($do_save_menu))
		{
			$save_menu = $this->_save_parent_menu();
			echo $save_menu;
			exit;
		}
		
		$get_functions = $this->CI->input->post("get_functions");
		$current_function = $this->CI->input->post("current_function");
		if(!empty($get_functions))
		{
			$this->_get_functions($get_functions,$current_function);
			exit;
		}
		
    $the_menus = "";
    $is_login = $this->CI->user_access->is_login();
    if(isset($filter['menu_group_id']))
    {		
      $this->CI->db->where($filter);
      $this->CI->db->order_by("sort_order","ASC");
      $appearence_menus = $this->CI->db->get("widget_menus");
      $appearence_menus = $appearence_menus->result_array();
      $appearence_menus = $this->CI->user_access->mapTree($appearence_menus,0);
      $the_menus = $this->_display_structure($appearence_menus);
    }
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/menu/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/widget_menu_group/listing',array('config_form_add' => $config_form_add,'config_form_filter' => $config_form_filter,'response' => '','page_title' => "Data Menu",'the_menus' => $the_menus));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->CI->init['fields']);
		$paging_config = array('base_url' => base_url().'menu/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->CI->init['fields']))?$this->CI->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->CI->init['fields'] = $init;
		
		$response = $this->CI->data->edit("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/widget_menu_group/edit',array('response' => $response,'page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_slider_listing'));
		$response = $this->CI->data->add("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/widget_menu_group/add',array('response' => $response,'page_title' => 'Tambah menu'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/widget_menu_group/view',array('response' => '','page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config_menu_item($id_object = "")
	{			
		$this->CI->data->_get_controllers();
		$data_methods_arr = $this->CI->data->methods;
		$data_methods = array();
		foreach($data_methods_arr as $i => $v)
		{
			$data_methods[$v] = $v;
		}
        
		$init = array(	'table' => "widget_menus",
						'sort_order' => 'parent_menu ASC,sort_order ASC',
						'fields' => array(
											array(
													'name' => 'menu_group_id',
													'label' => 'Menu Group',
													'id' => 'menu_group_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> "widget_menu_groups".' menu2',
													'select' => array('menu_group_id AS value','name AS label'),
													'options' => array('' => '---- Choose Menu Group ----'),
													'primary_key' => 'menu_group_id',
													'rules' => 'required'
												),
											array(
													'name' => 'parent_menu',
													'label' => 'Parent Menu',
													'id' => 'parent_menu',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> "widget_menus".' menu2',
													'select' => array('menu_id AS value','menu_title AS label'),
													'options' => array('' => '---- Choose Feature ----','0' => '---- No Parent ----'),
													'primary_key' => 'menu_id',
													'rules' => 'required',
                                                    'js_connect_to' => array( 
                                                    'id_field_parent' => '"select[name=\"data[menu_group_id]\"]"',
                                                    'table' => "widget_menus",
                                                    'where' => '',
                                                    'select' => 'menu_id AS value,menu_title AS label',
                                                    'primary_key' => 'menu_id',
                                                    'foreign_key' => 'menu_group_id')
												),
											array(
													'name' => 'menu_title',
													'label' => 'Menu Title',
													'id' => 'menu_title',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required',
													'list_style' => 'width="150"'
												),
											array(
													'name' => 'sort_order',
													'label' => 'Nomor Urutan',
													'id' => 'sort_order',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'numeric',
													'list_style' => 'width="100" align="center"'
												),
											array(
													'name' => 'type',
													'label' => 'Type Menu',
													'id' => 'type',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> "widget_menu_types".' menu_type',
													'select' => array('menu_type_name AS value','menu_type_title AS label'),
													'options' => array('' => '---- Pilih Jenis Menu ----'),
													'primary_key' => 'menu_type_id',
													'rules' => 'required',
                                                    'js_connect_to' => array(
																			'id_field_parent' => '"select[name=\"data[menu_type_name]\"]"',
																			'table' => "widget_menu_types",
																			'where' => '',
																			'select' => 'menu_id AS value,menu_title AS label',
																			'primary_key' => 'menu_id',
																			'foreign_key' => 'menu_type_name'
																			)
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array(	'active' => 'Active','not active' => 'Not Active'),
													'rules' => ''
												)
										),
									'primary_key' => 'menu_id',
									'path' => "/admin/",
									'controller' => 'appearence_menus',
									'function' => 'index',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'sess_keyname' => 'components_menucontroller'
					);
		$this->CI->init = $init;
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_menu_groups",
						'fields' => array(
											array(
													'name' => 'title',
													'label' => 'Judul Group Menu',
													'id' => 'title',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => ''
												),
											array(
													'name' => 'name',
													'label' => 'Nama Group Menu',
													'id' => 'name',
													'value' => '',
													'type' => 'input_hidden',
													'use_search' => true,
													'use_listing' => true,
													'rules' => ''
												)
										),
									'primary_key' => 'menu_group_id',
									'path' => "/admin/",
									'controller' => 'appearence_plugins',
									'function' => 'plugin_controller',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  array('title' => 'List Menu','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'sess_keyname' => 'components_menucontroller_menu_group',
									'action' => base_url("admin/appearence_plugins/plugin_controller/components/menu/add"),
									'uri_segment' => 7
					);
		$this->CI->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _display_structure_table($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$no = 0;
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			foreach ($nodes as $index =>$node) 
			{
				/*
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<td><h3><a href="'.$href.'"><span class="ui-icon ui-icon-arrowthick-1-e"></span>'.$node['menu_title'].'</a></h3>';
					$output .= $this->_display_structure_table($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<td><a href="'.$href.'">'.$node['menu_title'].'</a>';
				}
				
				$output .= '</td>'."\n";
				*/
				$panel_function = array("edit","view","delete");
				$action = $this->CI->data->show_panel_allowed("","",$panel_function,$node[$this->CI->init['primary_key']]);
				$no++;
				$output .= '
							<tr class="" style="">
								<td align="center" style="width: 30px;">'. $no .'</td>
								<td style="width: 100px;">'.$node['parent_menu'].'</td>
								<td style="width: 150px;">'.$node['menu_title'].'</td>
								<td style="width: 108px;">'. base_url() . $node['controller'] . '/' . $node['function'] .'</td>
								<td align="center" style="width: 30px;">'.$node['sort_order'].'</td>
								<td>
									' . $action . '
								</td>
							</tr>'."\n";
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<tr style="border-left: none !important;border-right: none !important;">
									<td colspan="6" style="border-left: none !important;border-right: none !important;"> <table width="100%"><tbody>' . $this->_display_structure_table($node['children'],$indent+1, $max_depth) . '</tbody></table></td>
								</tr>';
				}
			}
		}
		
		return $output;
	}
	
	
	function _display_structure($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul class="sortables fleft">'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				$panel_function = array("edit","view","delete");
				$action = $this->CI->data->show_panel_allowed("",$this->CI->init['path'],"appearence_menus",$this->CI->init['panel_function'],$node[$this->CI->init['primary_key']],true);

				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node['menu_id'].'">
									<input type="hidden" name="menu['.$node['menu_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_menu'].'" class="inp_menu"/>
									<span class="ui-icon ui-icon-triangle-1-n fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['menu_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= $this->_display_structure($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node['menu_id'].'">
								<input type="hidden" name="menu['.$node['menu_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_menu'].'" class="inp_menu"/>
								<span class="ui-icon ui-icon-triangle-1-s fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['menu_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= '<ul class="sortables fleft"></ul>'."\n";
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
		
	function _save_parent_menu()
	{
		$output = 0;
		$structure = $this->CI->input->post('menu');
		if(is_array($structure) and count($structure))
		{
			$sort_order = array();
			foreach($structure as $menu_id => $parent_menu)
			{
				$sort_index = (isset($sort_order[$parent_menu]))?$sort_order[$parent_menu]:0;
				$this->CI->db->where(array("menu_id" => $menu_id));
				$output = (int) $this->CI->db->update("widget_menus",array("parent_menu" => $parent_menu,"sort_order" => $sort_index));
				if(isset($sort_order[$parent_menu]))
				{
					$sort_order[$parent_menu] += 1;
				}else{
					$sort_order[$parent_menu] = 1;
				}
			}
		}
		return $output;
	}
	
	function _get_functions($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->CI->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->CI->input->post("current_function"):$current_function;
		$conmeth = $this->CI->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		if(isset($conmeth) and is_array($conmeth))
		{
			foreach($conmeth as $i => $d)
			{
				$class_name = strtolower($d['class_name']);
				if($get_functions == $class_name)
				{
					$methods = $d['methods'];
					foreach($methods as $index => $method)
					{
						if(!is_array($method))
						{
							$selected = ($current_function == $method)?' selected="selected" ':'';
							echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
						}
					}
				}
			}
		}
		echo "";
	}
	
	function _get_functions_($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->CI->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->CI->input->post("current_function"):$current_function;
		$conmeth = $this->CI->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		if(isset($conmeth[$get_functions]) and isset($conmeth[$get_functions]['methods']) and is_array($conmeth[$get_functions]['methods']))
		{
			$methods = $conmeth[$get_functions]['methods'];
			foreach($methods as $index => $method)
			{
				if(!is_array($method))
				{
					$selected = ($current_function == $method)?' selected="selected" ':'';
					echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
				}
			}
		}
		echo "";
	}
	
	function _hook_do_add($param = "")
	{
        
		$param['name'] = (isset($param['title']))?url_title($param['title'],"-",true):"menu-group-".rand(0,999);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Menu";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Menu";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return 1;
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_menu_group_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/menu/delete",$param);
		return $param;
	}
	function _components_menu_group_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/menu/edit",$param);
		return $param;
	}
	function _components_menu_group_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/menu_items/listing",$param);
		return $param;
	}
	function _components_menu_group_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/menu/listing",$param);
		return $param;
	}

	function get_object_tables($object_id = "")
	{
		$output = "";
		$post_menu_type = $this->CI->input->post("menu_type");
		$menu_type = $this->CI->db->where(array("menu_type_name" => $post_menu_type))->get("widget_menu_types")->row_array();
		if(is_array($menu_type) and count($menu_type) > 0)
		{
			$table_name = (isset($menu_type['object_table']))?$menu_type['object_table']:"";
			$primary_key = (isset($menu_type['primary_key']))?$menu_type['primary_key']:"";
			$index_key = (isset($menu_type['index_key']))?$menu_type['index_key']:"";
			$object_label = (isset($menu_type['object_label']))?$menu_type['object_label']:"";
			
			if(!empty($table_name))
			{
				$data_objects = $this->CI->db->get($table_name)->result_array();
				foreach($data_objects as $i => $o)
				{
					$output .= '<option value="'.$o[$primary_key].'">'.$o[$object_label].'</option>';
				}
			}
		}
		echo $output;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
