<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newscontroller{

  var $name = 'news';
  var $title = 'news';
  var $description = 'Plugin Artikel/News';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $init;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_create_listing_value_thumbnail',array($this,'_hook_create_listing_value_thumbnail'));
	
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		
		$the_news = "";
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
            $config_form_filter = $this->init;
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/news/");
            $config_form_add = $this->init;
                        
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/default/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Artikel",'the_news' => $the_news));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'news/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$this->init['action'] = base_url("admin/appearence_plugins/plugin_controller/components/news/edit/".$object_id);
		
		$this->CI->init = $this->init;
		$response = $this->CI->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/edit',array('response' => $response,'page_title' => "Data Artikel"));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$response = $this->CI->data->add("",$this->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah news'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;		
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_news_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_news_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_news_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_news_listing'));

		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/view',array('response' => '','page_title' => $this->title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_news",
						'fields' => array(
										  array(
											'name' => 'page_title',
											'label' => 'Title',
											'id' => 'page_title',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										  ),
										  array(
											'name' => 'page_name',
											'label' => 'Page Name',
											'id' => 'page_name',
											'value' => '',
											'type' => 'input_hidden',
											'use_search' => false,
											'use_listing' => false,
											'rules' => ''
										  ),
										  array(
											'name' => 'page_description',
											'label' => 'Page Description',
											'id' => 'page_description',
											'value' => '',
											'type' => 'input_mce',
											'use_search' => true,
											'use_listing' => false,
											'rules' => 'required'
										  ),
										  array(
											'name' => 'thumbnail',
											'label' => 'Thumbnail',
											'id' => 'thumbnail',
											'value' => '',
											'type' => 'input_file',
											'config_upload' => array(
																		'upload_path' => './uploads/news/',
																		'encrypt_name' => false,
																		'allowed_types' => 'jpg|gif|png'
																	),
											'use_search' => false,
											'use_listing' => true,
											'rules' => '',
											'the_file_rules' => '',
											'list_style' => ' width="250" '
										  ),
										  array(
											'name' => 'meta_title',
											'label' => 'Meta Title',
											'id' => 'meta_title',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => false,
											'rules' => ''
										  ),
										  array(
											'name' => 'meta_keywords',
											'label' => 'Meta Keywords',
											'id' => 'meta_keywords',
											'value' => '',
											'type' => 'input_textarea',
											'use_search' => true,
											'use_listing' => false,
											'rules' => ''
										  ),
										  array(
											'name' => 'meta_description',
											'label' => 'Meta Description',
											'id' => 'meta_description',
											'value' => '',
											'type' => 'input_textarea',
											'use_search' => true,
											'use_listing' => false,
											'rules' => ''
										  ),
										  array(
											'name' => 'status',
											'label' => 'Status',
											'id' => 'status',
											'value' => '',
											'type' => 'input_selectbox',
											'options' => array('' => '---- Choose Status ----','active' => 'Publish','not active' => 'Draft'),
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										  )
									),
									'primary_key' => 'news_id',
									'path' => "/admin/",
									'controller' => 'appearence_plugins',
									'function' => 'plugin_controller',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'sess_keyname' => 'components_newscontroller_news',
									'action' => base_url("admin/appearence_plugins/plugin_controller/components/news/add"),
									'uri_segment' => 7
					);
		$this->init = $init;
		$this->CI->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _display_structure_table($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$no = 0;
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			foreach ($nodes as $index =>$node) 
			{
				/*
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<td><h3><a href="'.$href.'"><span class="ui-icon ui-icon-arrowthick-1-e"></span>'.$node['news_title'].'</a></h3>';
					$output .= $this->_display_structure_table($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<td><a href="'.$href.'">'.$node['news_title'].'</a>';
				}
				
				$output .= '</td>'."\n";
				*/
				$panel_function = array("edit","view","delete");
				$action = $this->CI->data->show_panel_allowed("","",$panel_function,$node[$this->init['primary_key']]);
				$no++;
				$output .= '
							<tr class="" style="">
								<td align="center" style="width: 30px;">'. $no .'</td>
								<td style="width: 100px;">'.$node['parent_news'].'</td>
								<td style="width: 150px;">'.$node['news_title'].'</td>
								<td style="width: 108px;">'. base_url() . $node['controller'] . '/' . $node['function'] .'</td>
								<td align="center" style="width: 30px;">'.$node['sort_order'].'</td>
								<td>
									' . $action . '
								</td>
							</tr>'."\n";
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<tr style="border-left: none !important;border-right: none !important;">
									<td colspan="6" style="border-left: none !important;border-right: none !important;"> <table width="100%"><tbody>' . $this->_display_structure_table($node['children'],$indent+1, $max_depth) . '</tbody></table></td>
								</tr>';
				}
			}
		}
		
		return $output;
	}
	
	
	function _display_structure($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul class="sortables fleft">'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				$panel_function = array("edit","view","delete");
				$action = $this->CI->data->show_panel_allowed("",$this->init['path'],"appearence_news",$this->init['panel_function'],$node[$this->init['primary_key']],true);

				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li class="fclear the_news" id="the_news-'.$node['news_id'].'">
									<input type="hidden" name="news['.$node['news_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_news'].'" class="inp_news"/>
									<span class="ui-icon ui-icon-triangle-1-n fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['news_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= $this->_display_structure($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<li class="fclear the_news" id="the_news-'.$node['news_id'].'">
								<input type="hidden" name="news['.$node['news_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_news'].'" class="inp_news"/>
								<span class="ui-icon ui-icon-triangle-1-s fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['news_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= '<ul class="sortables fleft"></ul>'."\n";
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
		
	function _save_parent_news()
	{
		$output = 0;
		$structure = $this->CI->input->post('news');
		if(is_array($structure) and count($structure))
		{
			$sort_order = array();
			foreach($structure as $news_id => $parent_news)
			{
				$sort_index = (isset($sort_order[$parent_news]))?$sort_order[$parent_news]:0;
				$this->CI->db->where(array("news_id" => $news_id));
				$output = (int) $this->CI->db->update("widget_news",array("parent_news" => $parent_news,"sort_order" => $sort_index));
				if(isset($sort_order[$parent_news]))
				{
					$sort_order[$parent_news] += 1;
				}else{
					$sort_order[$parent_news] = 1;
				}
			}
		}
		return $output;
	}
	
	function _get_functions($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->CI->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->CI->input->post("current_function"):$current_function;
		$conmeth = $this->CI->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		if(isset($conmeth) and is_array($conmeth))
		{
			foreach($conmeth as $i => $d)
			{
				$class_name = strtolower($d['class_name']);
				if($get_functions == $class_name)
				{
					$methods = $d['methods'];
					foreach($methods as $index => $method)
					{
						if(!is_array($method))
						{
							$selected = ($current_function == $method)?' selected="selected" ':'';
							echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
						}
					}
				}
			}
		}
		echo "";
	}
	
	function _get_functions_($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->CI->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->CI->input->post("current_function"):$current_function;
		$conmeth = $this->CI->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		if(isset($conmeth[$get_functions]) and isset($conmeth[$get_functions]['methods']) and is_array($conmeth[$get_functions]['methods']))
		{
			$methods = $conmeth[$get_functions]['methods'];
			foreach($methods as $index => $method)
			{
				if(!is_array($method))
				{
					$selected = ($current_function == $method)?' selected="selected" ':'';
					echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
				}
			}
		}
		echo "";
	}
	
	function _hook_do_add($param = "")
	{
        
		$param['page_name'] = (isset($param['page_title']))?url_title($param['page_title'],"-",true).'-'.date("ymdhis"):"news-".rand(0,999);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{	
		if(isset($_FILES) and is_array($_FILES) and count($_FILES) > 0)
		{}else{unset($param['thumbnail']);}
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Artikel";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Artikel";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_news_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/news/delete",$param);
		return $param;
	}
	function _components_news_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/news/edit",$param);
		return $param;
	}
	function _components_news_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/news/view",$param);
		return $param;
	}
	function _components_news_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/news/listing",$param);
		return $param;
	}
	function _hook_create_listing_value_thumbnail($param = "")
	{
		$param = str_replace('./',base_url(),$param);
		$param = str_replace('height="50"','width="250"',$param);
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
