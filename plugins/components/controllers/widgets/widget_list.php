<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_list extends Basic_widget{

  var $name = 'list';
  var $title = 'List';
  var $description = 'Widget List';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
	public function index()
	{
    $is_ajax = $this->input->post('is_ajax');
    $this->load->view('layouts/dashboard');
	}
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
