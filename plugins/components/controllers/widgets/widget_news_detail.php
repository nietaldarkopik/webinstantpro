<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_news_detail extends Basic_widget{

  var $name = 'news_detail';
  var $title = 'News Detail';
  var $description = 'WIdget News Detail';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
    //Install DB
    $q_install = $this->CI->db->query('CREATE TABLE IF NOT EXISTS `widget_news_detail` (
									  `news_detailt_id` int(11) NOT NULL AUTO_INCREMENT,
									  `text_title` char(255) NOT NULL,
									  `text_name` char(255) NOT NULL,
									  `text_content` text NOT NULL,
									  `sort_order` int(11) NOT NULL,
									  PRIMARY KEY (`news_detailt_id`)
									) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
								');
	return $q_install;
  }
  
  function uninstall()
  {
	  //uninstall db
    $q_uninstall = $this->db->query("DROP TABLE widget_news_detail");
	return $q_uninstall;
  }
  
  function query_element_template($block_html = "")
  {
	  
  }
  
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col="0")
  {
	$this->install();
	$block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	$block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	$query = $this->query_element_template();
	$data_widget = $this->current_data;
	$data_block_item_where = array(
							 "theme_id" => $data_theme['theme_id'],
							 "template_id" => $data_template['template_id'],
							 "block_container_id" => $block_group_id,
							 "block_row_id" => $block_id,
							 "widget_id" => $data_widget['widget_id'],
							 "sort_order" => $no_col
							 );
	
	$this->CI->db->where($data_block_item_where);
	$q_check = $this->CI->db->get("system_block_cols")->num_rows();
	$output = 0;
	if($q_check == 0)
	{
		$widget_content = pq($col)->html();
		#$widget_content = str_replace('assets/',current_theme_url()."assets/theme/",$widget_content);
		#$widget_content = str_replace(current_theme_url().current_theme_url().'assets/',current_theme_url()."assets/theme/",$widget_content);
		$insert_data_widget = array("text_title" => "","text_name" => "","text_content" => trim($widget_content));
		$this->CI->db->set($insert_data_widget,true);
		$this->CI->db->insert("widget_news_detail");
		
		$widget_content_id = $this->CI->db->insert_id();
		$data_block_item = array(
								 "theme_id" => $data_theme['theme_id'],
								 "template_id" => $data_template['template_id'],
								 "block_container_id" => $block_group_id,
								 "block_row_id" => $block_id,
								 "widget_id" => $data_widget['widget_id'],
								 "widget_content_id" => $widget_content_id,
								 "sort_order" => $no_col
								 );
		$this->CI->db->insert("system_block_cols",$data_block_item);
		$block_col_id = $this->CI->db->insert_id();
		
		$content = '<bscol id="'.$block_col_id.'"></bscol>';
		if(pq($col)->is("[class^=col-"))
			$content = pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter();
			
		$this->CI->db->where(array("block_col_id" => $block_col_id));
		$this->CI->db->update("system_block_cols",array("content" => $content));
		$output = 1;
	}
	return $output;
  }
  
  public function show_widget($data_widget="")
  {
	  return "";
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";
	
	if($this->CI->db->table_exists('widget_'.$widget_name))
	{
		$data_widget_contents = $this->CI->db->where(array($widget_name."_id" => $widget_content_id))->get('widget_'.$widget_name)->row_array();
		$output = $data_widget_contents['text_content'];
		if(!class_exists("phpQuery"))
		{
			require_once(APPPATH."libraries/PhpQuery.php");
		}
		$pq = phpQuery::newDocumentHTML('<div class="inline_editable" data-widget-name="news_detail" data-widget-item-id="'.$widget_content_id.'">'.$output.'</div>');
		$output = $pq->html();
		return $output;
	}
	return "";
  }
  
  function show_assets_widget($data_widget="")
  {
	$js = '$(document).ready(function(){
					function save_news_detail()
					{
						var widget_item_id = $(".mce-edit-focus").attr("data-widget-item-id");
						var content = tinyMCE.activeEditor.getContent({format : "raw"});
						$.ajax({
						  url: base_url+"admin/appearence_plugins/plugin_controller/components/news_detail/do_edit/"+widget_item_id,
						  type: "post",
						  data: "do_action=do_save_news_detail&theme_id="+theme_id+"&template_id="+template_id+"&item_id="+widget_item_id+"&content="+content,
						  dataType: "json",	
						  success: function(result)
						  {
							var message = (!result.message)?"":result.message;
							var type = (!result.status)?"":result.status;
							show_alert(type,message)
						  }
						});
					}
					setup_tinymce(".inline_editable",{
							theme: "modern",
							plugins: [
							  "advlist autolink lists link upload_image charmap print preview hr anchor pagebreak",
							  "searchreplace wordcount visualblocks visualchars code fullscreen",
							  "insertdatetime media nonbreaking save table contextmenu directionality",
							  "paste textcolor template textpattern"
							],
							toolbar1: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
							toolbar2: "link insertfile upload_image textpattern media | forecolor backcolor emoticons save",
							image_advtab: false,
							/*
							templates: [
							  {title: "Test template 1", content: "Test 1"},
							  {title: "Test template 2", content: "Test 2"}
							],*/
							inline: true,
							save_enablewhendirty: true,
							save_onsavecallback: function() {
								save_news_detail();
							}
						});
					});';
	$this->CI->assets->add_js_inline($js,'body');
  }
  
  function init($data_widget="")
  {
    
  }
  
}
