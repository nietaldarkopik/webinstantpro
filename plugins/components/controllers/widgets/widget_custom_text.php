<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_custom_text extends Basic_widget{

  var $name = 'custom_text';
  var $title = 'Custom Text';
  var $description = 'WIdget Custom Text';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $init;	
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
    //Install DB
    $q_install = $this->CI->db->query('CREATE TABLE IF NOT EXISTS `widget_custom_text` (
									  `custom_textt_id` int(11) NOT NULL AUTO_INCREMENT,
									  `text_title` char(255) NOT NULL,
									  `text_name` char(255) NOT NULL,
									  `text_content` text NOT NULL,
									  `sort_order` int(11) NOT NULL,
									  PRIMARY KEY (`custom_textt_id`)
									) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
								');
	return $q_install;
  }
  
  function uninstall()
  {
	  //uninstall db
    $q_uninstall = $this->db->query("DROP TABLE widget_custom_text");
	return $q_uninstall;
  }
  
  function query_element_template($block_html = "")
  {
	  
  }
  
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col="0")
  {
	$this->install();
	$block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	$block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	$query = $this->query_element_template();
	$data_widget = $this->current_data;
	$data_block_item_where = array(
							 "theme_id" => $data_theme['theme_id'],
							 "template_id" => $data_template['template_id'],
							 "block_container_id" => $block_group_id,
							 "block_row_id" => $block_id,
							 "widget_id" => $data_widget['widget_id'],
							 "sort_order" => $no_col
							 );
	
	$this->CI->db->where($data_block_item_where);
	$q_check = $this->CI->db->get("system_block_cols")->num_rows();
	$output = 0;
	if($q_check == 0)
	{
		$widget_content = pq($col)->html();
		#$widget_content = str_replace('assets/',current_theme_url()."assets/theme/",$widget_content);
		#$widget_content = str_replace(current_theme_url().current_theme_url().'assets/',current_theme_url()."assets/theme/",$widget_content);
		$insert_data_widget = array("text_title" => "","text_name" => "","text_content" => trim($widget_content));
		$this->CI->db->set($insert_data_widget,true);
		$this->CI->db->insert("widget_custom_text");
		
		$widget_content_id = $this->CI->db->insert_id();
		$data_block_item = array(
								 "theme_id" => $data_theme['theme_id'],
								 "template_id" => $data_template['template_id'],
								 "block_container_id" => $block_group_id,
								 "block_row_id" => $block_id,
								 "widget_id" => $data_widget['widget_id'],
								 "widget_content_id" => $widget_content_id,
								 "sort_order" => $no_col
								 );
		$this->CI->db->insert("system_block_cols",$data_block_item);
		$block_col_id = $this->CI->db->insert_id();
		
		$content = '<bscol id="'.$block_col_id.'"></bscol>';
		if(pq($col)->is("[class^=col-"))
			$content = pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter();
			
		$this->CI->db->where(array("block_col_id" => $block_col_id));
		$this->CI->db->update("system_block_cols",array("content" => $content));
		$output = 1;
	}
	return $output;
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";
	
	if($this->CI->db->table_exists('widget_'.$widget_name))
	{
		$data_widget_contents = $this->CI->db->where(array($widget_name."_id" => $widget_content_id))->get('widget_'.$widget_name)->row_array();
		$output = $data_widget_contents['text_content'];
		if(!class_exists("phpQuery"))
		{
			require_once(APPPATH."libraries/PhpQuery.php");
		}
		$pq = phpQuery::newDocumentHTML('<div class="inline_editable" data-widget-name="custom_text" data-widget-item-id="'.$widget_content_id.'">'.$output.'</div>');
		$output = $pq->html();
		return $output;
	}
	return "";
  }
  
  function show_assets_widget($data_widget="")
  {
	$js = '
				function save_custom_text()
				{
					var widget_item_id = $(".mce-edit-focus").attr("data-widget-item-id");
					if(typeof tinyMCE == "undefined" || !tinyMCE){}
					else{
						var content = tinyMCE.activeEditor.getContent({format : "raw"});
						//content = content.replace("&","#and#");
						$.ajax({
						  url: base_url+"admin/appearence_plugins/plugin_controller/components/custom_text/do_edit/"+widget_item_id,
						  type: "post",
						  data: {"do_action" : "do_save_custom_text","theme_id":theme_id,"template_id":template_id,"item_id":widget_item_id,"content":content},
						  dataType: "json",	
						  success: function(result)
						  {
							var message = (!result.message)?"":result.message;
							var type = (!result.status)?"":result.status;
							show_alert(type,message)
						  }
						});
					}
				}
				function callback_widget_custom_text(selector)
				{
					selector = (!selector)?".inline_editable":selector;
					if(typeof setup_tinymce == "undefined" || !setup_tinymce){}
					else{
						setup_tinymce(selector,{
								theme: "modern",
								//skin: "tundora",
								plugins: [
								  "advlist autolink lists link charmap print preview hr anchor pagebreak",
								  "searchreplace wordcount visualblocks visualchars code fullscreen",
								  "insertdatetime media nonbreaking save table contextmenu directionality",
								  "paste textcolor template textpattern responsivefilemanager  image"
								],
								toolbar1: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link insertfile responsivefilemanager image textpattern media | forecolor backcolor emoticons save",
								//toolbar2: "link insertfile responsivefilemanager image textpattern media | forecolor backcolor emoticons save",
								toolbar2: "",
								toolbar3: "",
								relative_urls: false,
								convert_urls : true,
								remove_script_host: false,
								//document_base_url: base_url+"uploads/",

								filemanager_title:"Responsive Filemanager",
								filemanager_crossdomain: true,
								external_filemanager_path:base_url+"uploads/filemanager/",
								external_plugins: { "filemanager" : base_url+"uploads/filemanager/plugin.min.js"},

								inline: true,
								fixed_toolbar_container: "#toolbar_mce",
								save_enablewhendirty: true,
								theme_advanced_toolbar_location : "external",
								theme_advanced_toolbar_align : "left",
								theme_advanced_statusbar_location : "bottom",
								theme_advanced_resizing : true,
								save_onsavecallback: function() {
									save_custom_text();
								}
							});
					}
				}
				$(document).ready(function(){
					if($("#toolbar_mce").length == 0)
					{
						$("body").append("<div id=\"toolbar_mce\"></div>");
					}
					callback_widget_custom_text(".inline_editable");
				});';
	$this->CI->assets->add_js_inline($js,'body');
  }
  
  function show_callback_widget($block_item = "")
  {
	  $block_col_id = (isset($block_item['block_col_id']))?$block_item['block_col_id']:"";
	  if(empty($block_col_id))
		return "";
		
	  return 'callback_widget_custom_text("[data-block-item-id='.$block_col_id.'] .inline_editable");';
  }
  
  function init($data_widget="")
  {
    
  }
  
  function _config()
  {
		$init = array(	'table' => "widget_custom_text",
						'fields' => array(
											array(
												'name' => 'text_title',
												'label' => 'Text Title',
												'id' => 'text_title',
												'value' => '',
												'type' => 'input_text',
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'text_content',
												'label' => 'Text Content',
												'id' => 'text_content',
												'value' => '',
												'type' => 'input_mce',
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											)
										),
					'primary_key' => 'custom_text_id',
					'path' => "/",
					'controller' => 'adminblocks',
					'function' => 'widgets',
					'panel_function' => array(
												array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
												array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
												array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
											  ),
					'bulk_options' => array(
												array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
												array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
												array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
											  ),
					'sess_keyname' => 'components_custom_text_widget_controller',
		);
		$this->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {			
			$this->_config();
			$this->CI->data->init($this->init);
			$this->CI->data->set_filter();
			$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
			$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
			$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
			$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_create_form_button_action',array($this,'_hook_create_form_filter_button_action'));
			$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
			

			$config_form_filter = $this->init;
			$add_config_form = $this->init;
			$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/custom_text/configuration/';
			$add_config_form['action'] = base_url().'admin/adminblocks/setting_widget/custom_text/add/';
			$view = "";
			
			$is_login = $this->CI->user_access->is_login();
			if($is_login)
				$view = $this->CI->load->view('layouts/custom_text/widgets/widget_custom_text',
																array(	'response' => '',
																		'page_title' => 'Data Text',
																		'config_form_filter' => $config_form_filter,
																		'listing_config' => $this->init,
																		'add_config' => $add_config_form,
																		'current_data_widget' => $this->current_data
																		),
																true
																	);
			else
				$view = $this->CI->load->view('layouts/login',array(),true);
				
		  return $view;
	}
	
	function add()
	{
		
		$this->_config();
		$this->CI->data->init($this->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
		$this->CI->hook->add_action('hook_create_form_button_action',array($this,'_hook_create_form_filter_button_action'));
		$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
		$response = $this->CI->data->add("",$this->init['fields']);
		

		$config_form_filter = $this->init;
		$add_config_form = $this->init;
		$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/custom_text/configuration/';
		$add_config_form['action'] = base_url().'admin/adminblocks/setting_widget/custom_text/add/';
		$view = "";
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
			$view = $this->CI->load->view('layouts/custom_text/widgets/widget_custom_text',
															array(	'response' => $response,
																	'page_title' => 'Data Text',
																	'config_form_filter' => $config_form_filter,
																	'listing_config' => $this->init,
																	'add_config' => $add_config_form,
																	'current_data_widget' => $this->current_data
																	),
															true
																);
		else
			$view = $this->CI->load->view('layouts/login',array(),true);
			
	  return $view;
		
	}
	
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}
	function _hook_create_form_title_add($title){
		return "Tambah Custom Text";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Custom Text";
	}
	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
  
}
