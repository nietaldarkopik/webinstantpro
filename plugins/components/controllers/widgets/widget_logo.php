<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_logo extends Basic_widget{

  var $name = 'logo';
  var $title = 'Logo';
  var $description = 'Widget Logo';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $current_data = array();
  var $init;
  
  public function __construct()
  {
	$this->CI =& get_instance();
	$this->CI->load->library('image_lib');
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
	  return true;
  }
  
  function uninstall()
  {
	  return true;
  }
  
  function query_element_template($block_html = "")
  {
	  $query = ".logo";
	  
	  return $query;
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col)
  {
	  $this->install();
	  $block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	  $block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	  $query = $this->query_element_template();
	  $data_widget = $this->current_data;
	  
	  $get_elements = pq($query,$col);
	  $output = 0;
		
	  if($get_elements->count() > 0)
	  {
		  foreach($get_elements as $i => $get_element)
		  {
			$data_block_item_where = array(
									 "theme_id" => $data_theme['theme_id'],
									 "template_id" => $data_template['template_id'],
									 "block_container_id" => $block_group_id,
									 "block_row_id" => $block_id,
									 "widget_id" => $data_widget['widget_id'],
									 "sort_order" => $no_col
									 );

			$this->CI->db->where($data_block_item_where);
			$q_check = $this->CI->db->get("system_block_cols")->num_rows();

			if($q_check == 0)
			{
				$widget_content = pq($get_element)->find("li");
				if($widget_content->count() > 0)
				{
					$insert_data_widget = array("title" => "","name" => "");
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->insert("widget_logo_categories");
					$logo_category_id = $this->CI->db->insert_id();
					//$widget_content_id = $this->CI->db->insert_id();
					
					$insert_data_widget = array("container" => pq($get_element)->clone()->html('<bswidget widget_name="'.$data_widget['widget_name'].'" id="'.$logo_category_id.'"></bswidget>')->htmlOuter());
					$this->CI->db->where(array("logo_category_id"=>$logo_category_id));
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->update("widget_logo_categories");
					
					foreach($widget_content as $i => $w_content)
					{
						$a = pq("a",$w_content)->eq(0);
						$data_block_item = array(
												"page_name" => url_title(pq($a)->html(), '-', TRUE)."_".$logo_category_id,
												"page_title" => pq($a)->html(),
												"page_description" => "",
												"thumbnail" => "",
												"meta_title" => "",
												"meta_keywords" => "",
												"meta_description" => "",
												"author" => "",
												"date_inserted" => "",
												"date_updated" => "",
												 "status" => "active",
												 "logo_category_id" => $logo_category_id,
												 );
						$this->CI->db->insert("widget_logo",$data_block_item);
						$block_col_id = $this->CI->db->insert_id();
					}
					
					$data_block_item = array(
											 "theme_id" => $data_theme['theme_id'],
											 "template_id" => $data_template['template_id'],
											 "block_container_id" => $block_group_id,
											 "block_row_id" => $block_id,
											 "widget_id" => $data_widget['widget_id'],
											 "widget_content_id" => $logo_category_id,
											 "sort_order" => $no_col
											 );
					$this->CI->db->insert("system_block_cols",$data_block_item);
					$block_col_id = $this->CI->db->insert_id();
					
					$this->CI->db->where(array("block_col_id" => $block_col_id));
					$this->CI->db->update("system_block_cols",array("content" => pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter()));
					
				}
			}
		}
		
		$output = 1;
    }
	
	return $output;
  }
  
  function create_logo($data_widget = array())
  {
	  $configs = (isset($data_widget['configs']) and !empty($data_widget['configs']))?json_decode($data_widget['configs'],true):array();
	
	  $get_logo = $this->CI->db->where(array('key' => 'widget_logo'))->get("system_settings")->row_array();
	  $get_meta_title = $this->CI->db->where(array('key' => 'meta_title'))->get("system_settings")->row_array();
	  $output = '
				<div class="logo no-padding">
					<a title="'.((isset($get_meta_title['value']))?$get_meta_title['value']:'').'" href="'.base_url().'">
						<img src="'.((isset($get_logo['value']))?base_url('uploads/logo/'.$get_logo['value']):current_them_url.'assets/theme/img/logo.png').'" alt="'.((isset($get_meta_title['value']))?$get_meta_title['value']:'').'" />
					</a>
				</div>
				<div class="clearfix"></div>
				';
	  return $output;
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";

	$output = "";
	$logo_items = $this->create_logo($data_widget);
	$output = $logo_items;
	return $output;
  }
  
  function init($data_widget="")
  {
    
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {
	  
	  $do_action_update_logo = (isset($_FILES['logo_file_name']))?true:false;
	  $do_update_widget_logo = $this->CI->input->post("do_update_widget_logo");
	  $response = '';
	  
	  if($do_action_update_logo)
	  {
		$config['upload_path'] = './uploads/logo/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['overwrite'] = true;
		//$config['file_name'] = 'logo.jpg';
		#$config['max_size']	= '100';
		#$config['max_width']  = '1024';
		#$config['max_height']  = '768';

		$this->CI->load->library('upload', $config);
		
		if (!$this->CI->upload->do_upload('logo_file_name'))
		{
			$response = $this->CI->upload->display_errors();
			$response = '<div class="alert alert-warning">'.$response.'</div>';
		}
		else
		{
			$data = $this->CI->upload->data();
			$response = '<div class="alert alert-success">Data Logo berhasil disimpan</div>';
			$get_logo = $this->CI->db->where(array('key' => 'widget_logo'))->get("system_settings")->row_array();
			if(isset($get_logo['value']))
			{
				if(!empty($get_logo['value']) and file_exists('./uploads/logo/'.$get_logo['value']) and !is_dir('./uploads/logo/'.$get_logo['value']))
				{
					unlink('./uploads/logo/'.$get_logo['value']);
				}
				$update_logo = $this->CI->db->where(array('key' => 'widget_logo'))->update("system_settings",array('value' => $data['file_name']));
			}else{
				$insert_logo = $this->CI->db->insert("system_settings",array('name' => 'Widget Logo','key' => 'widget_logo','value' => $data['file_name']));
			}
		}
	}

		$is_login = $this->CI->user_access->is_login();
		if($is_login)
			$view = $this->CI->load->view('layouts/widget_logo/widgets/widget_logo',
															array(	'response' => $response,
																	'page_title' => 'Data Logo',
																	'current_data_widget' => $this->current_data
																	),
															true
																);
		else
			$view = $this->CI->load->view('layouts/login',array(),true);
			
	  return $view;
	}
	
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}

	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}
	function _hook_create_form_title_add($title){
		return "Tambah Logo";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Logo";
	}
	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
	
	function show_callback_widget($data_widget = array())
	{
		$block_col_id = $this->CI->input->post("block_col_id");
		$block_col_id = (empty($block_col_id) and isset($data_widget['block_col_id']))?$data_widget['block_col_id']:$block_col_id;
		
		$js = '
		function callback_widget_logo()
		{
			var form = $("#form_logo");
			var data_form = $(form).serialize();
			var url = "'.base_url('admin/adminblocks/setting_widget/logo/do_save_configuration').'";
			$.ajax({
					url: url,
					data: data_form,
					type: "post",
					success: function(msg)
					{
					}
			});
		}
		callback_widget_logo();
		';
		echo $js;
	}
	
	function do_save_configuration()
	{
		$config = $this->CI->input->post("config");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$json_config = json_encode($config);
		$this->CI->db->where(array("block_col_id" => $block_col_id));
		$this->CI->db->update("system_block_cols",array("configs" => $json_config));
		
		echo $this->show_widget(array("block_col_id" => $block_col_id,"configs" => $json_config));
	}
}
