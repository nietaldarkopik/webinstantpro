<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_image_list extends Basic_widget{

  var $name = 'image_list';
  var $title = 'Image List';
  var $description = 'Widget Image';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $current_data = array();
  var $init;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
    //Install DB
    $q_installimage_list_types = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_image_list_types` (
  `image_list_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_list_type_title` char(255) NOT NULL,
  `image_list_type_name` char(255) NOT NULL,
  `object_table` char(100) NOT NULL,
  PRIMARY KEY (`image_list_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');

    $q_installimage_categories = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_image_categories` (
  `image_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `name` char(255) NOT NULL,
  `container` text,
  PRIMARY KEY (`image_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
');

    //Install DB
    $q_installimage_lists = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_category_id` int(11) NOT NULL,
  `parent_image` int(11) DEFAULT NULL,
  `image_title` char(255) NOT NULL,
  `image_name` char(255) NOT NULL,
  `object_id` int(11) NOT NULL,
  `image_type_name` char(100) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attributes` text,
  `type` char(255) DEFAULT NULL,
  `status` char(255) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');




	if($this->CI->db->where(array("image_list_type_name" => "page"))->get("widget_image_list_types")->num_rows() == 0)
		$this->CI->db->insert("widget_image_list_types",array("image_list_type_title" =>"Page","image_list_type_name" => "page","object_table" => "user_pages"));
	if($this->CI->db->where(array("image_list_type_name" => "category"))->get("widget_image_list_types")->num_rows() == 0)
		$this->CI->db->insert("widget_image_list_types",array("image_list_type_title" =>"Category","image_list_type_name" => "category","object_table" => "user_categories"));
	if($this->CI->db->where(array("image_list_type_name" => "link"))->get("widget_image_list_types")->num_rows() == 0)
		$this->CI->db->insert("widget_image_list_types",array("image_list_type_title" =>"Link","image_list_type_name" => "link","object_table" => "plugin_links"));
	
	return ($q_installimage_list_types and $q_installimage_categories and $q_installimage_lists)?true:false;

  }
  
  function uninstall()
  {
	  //uninstall db
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_image_categories");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_image");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_image_list_types");
	return $q_uninstall;
  }
  
  function query_element_template($block_html = "")
  {
	  $query = ".image_list";
	  
	  return $query;
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col)
  {
	  $this->install();
	  $block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	  $block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	  $query = $this->query_element_template();
	  $data_widget = $this->current_data;
	  
	  $get_elements = pq($query,$col);
	  $output = 0;
		
	  if($get_elements->count() > 0)
	  {
		  foreach($get_elements as $i => $get_element)
		  {
			$data_block_item_where = array(
									 "theme_id" => $data_theme['theme_id'],
									 "template_id" => $data_template['template_id'],
									 "block_container_id" => $block_group_id,
									 "block_row_id" => $block_id,
									 "widget_id" => $data_widget['widget_id'],
									 "sort_order" => $no_col
									 );

			$this->CI->db->where($data_block_item_where);
			$q_check = $this->CI->db->get("system_block_cols")->num_rows();

			if($q_check == 0)
			{
				$widget_content = pq($get_element)->find("li");
				if($widget_content->count() > 0)
				{
					$insert_data_widget = array("title" => "","name" => "");
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->insert("widget_image_categories");
					$image_category_id = $this->CI->db->insert_id();
					//$widget_content_id = $this->CI->db->insert_id();
					
					$insert_data_widget = array("container" => pq($get_element)->clone()->html('<bswidget widget_name="'.$data_widget['widget_name'].'" id="'.$image_category_id.'"></bswidget>')->htmlOuter());
					$this->CI->db->where(array("image_category_id"=>$image_category_id));
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->update("widget_image_categories");
					
					foreach($widget_content as $i => $w_content)
					{
						$a = pq("a",$w_content)->eq(0);
						$data_block_item = array(
												"page_name" => url_title(pq($a)->html(), '-', TRUE)."_".$image_category_id,
												"page_title" => pq($a)->html(),
												"page_description" => "",
												"thumbnail" => "",
												"meta_title" => "",
												"meta_keywords" => "",
												"meta_description" => "",
												"author" => "",
												"date_inserted" => "",
												"date_updated" => "",
												 "status" => "active",
												 "image_category_id" => $image_category_id,
												 );
						$this->CI->db->insert("widget_image",$data_block_item);
						$block_col_id = $this->CI->db->insert_id();
					}
					
					$data_block_item = array(
											 "theme_id" => $data_theme['theme_id'],
											 "template_id" => $data_template['template_id'],
											 "block_container_id" => $block_group_id,
											 "block_row_id" => $block_id,
											 "widget_id" => $data_widget['widget_id'],
											 "widget_content_id" => $image_category_id,
											 "sort_order" => $no_col
											 );
					$this->CI->db->insert("system_block_cols",$data_block_item);
					$block_col_id = $this->CI->db->insert_id();
					
					$this->CI->db->where(array("block_col_id" => $block_col_id));
					$this->CI->db->update("system_block_cols",array("content" => pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter()));
					
				}
			}
		}
		
		$output = 1;
    }
	
	return $output;
  }
  
  function create_image_list($data_widget = array())
  {
	$configs = (isset($data_widget['configs']) and !empty($data_widget['configs']))?json_decode($data_widget['configs'],true):array();
	
	$default = array(		
					"widget_title"=>"Image",
					"amount_data"=>"5",
					"sort_by"=>"title",
					"sorting"=>"asc",
					"amount_char_title"=>"20",
					"amount_char_description"=>"150",
					"text_readmore"=>"Detail"
					);
	$configs = array_merge($default,$configs);

	$limit = " LIMIT 0, ".$configs['amount_data']." ";
	$sort_order = " ORDER BY ". (($configs['sorting'] == "rand")?" rand() ":$configs['sort_by']." ".$configs['sorting']);

	$data_image_list_items = $this->CI->db->query("select * from widget_image WHERE status = 'active' $sort_order $limit")->result_array();
	
	$output = "";
	foreach($data_image_list_items as $i => $image_list_item)
	{
		$image_thumb = image_thumb('crop','uploads/image/'.$image_list_item['thumbnail'],300,450);
		$output .= '<div class="col-lg-4 col-md-4 image-item margin-top-20 margin-top-20">
						<div class="thumbnail">
							<img class="media-object col-lg-12 col-md-12" src="'. base_url() . $image_thumb.'" alt="'.$image_list_item['title'].'">
							<div class="caption">
								<h3 class="title">
									<a href="#">'.strip_tags(substr($image_list_item['title'],0,$configs['amount_char_title'])).((strlen($image_list_item['title']) <= $configs['amount_char_title'])?'':'...').'</a>
								</h3>
								<p>
									'. strip_tags(substr($image_list_item['description'],0,$configs['amount_char_description'])) .((strlen($image_list_item['description']) <= $configs['amount_char_description'])?'':'...').'
								</p>
							</div>
						</div>
					</div>';
	}
	$title = "";
	if(isset($configs['widget_title']) and !empty($configs['widget_title']))
	{
		$title = '<div class="col-lg-12 col-md-12"><h2 class="title">'.$configs['widget_title'].'</h2></div>';
	}
	return '<div class="row margin-top-20 margin-top-20">' . $title . $output . '</div>';
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";

	$output = "";
	if($this->CI->db->table_exists('widget_image'))
	{
		$image_list_items = $this->create_image_list($data_widget);
		$output = $image_list_items;
		return $output;
	}
	return $output;
  }
  
  function init($data_widget="")
  {
    
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {
		$init = array(
						'table' => 'widget_image',
						'fields' => array(
											array(
												'name' => 'name',
												'label' => 'Image Name',
												'id' => 'name',
												'value' => '',
												'type' => 'input_hidden',
												'use_search' => true,
												'use_listing' => true,
												'rules' => ''
											),
											array(
												'name' => 'title',
												'label' => 'Title',
												'id' => 'title',
												'value' => '',
												'type' => 'input_text',
												'use_search' => false,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'image_script',
												'label' => 'Embed Image Script',
												'id' => 'image_script',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => true,
												'use_listing' => false,
												'rules' => 'required'
											),
											array(
												'name' => 'description',
												'label' => 'Description',
												'id' => 'description',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => true,
												'use_listing' => false,
												'rules' => 'required'
											),
											array(
												'name' => 'status',
												'label' => 'Status',
												'id' => 'status',
												'value' => '',
												'type' => 'input_selectbox',
												'options' => array('' => '---- Choose Status ----','active' => 'Publish','not active' => 'Draft'),
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											)
										),
						'primary_key' => 'image_id',
						'path' => "/",
						'controller' => 'adminblocks',
						'function' => 'widgets',
						'panel_function' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'bulk_options' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'sess_keyname' => 'components_image_list_widget_controller'
			);
			$this->init = $init;
			$this->CI->data->sess_keyname = $init['sess_keyname'];
			$this->CI->data->init($this->init);
			$this->CI->data->set_filter();
			$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
			$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
			$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
			$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
			

			$config_form_filter = $this->init;
			$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/image_list/configuration/';

			$add_config = $this->init;
			$add_config['action'] = base_url().'admin/adminblocks/setting_widget/image_list/configuration/';
			$view = "";
			
			$is_login = $this->CI->user_access->is_login();
			if($is_login)
				$view = $this->CI->load->view('layouts/widget_image/widgets/widget_image_list',
																array(	'response' => '',
																		'page_title' => 'Data Image',
																		'config_form_filter' => $config_form_filter,
																		'listing_config' => $this->init,
																		'add_config' => $add_config,
																		'current_data_widget' => $this->current_data
																		),
																true
																	);
			else
				$view = $this->CI->load->view('layouts/login',array(),true);
				
		  return $view;
	}
	
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}

	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}
	function _hook_create_form_title_add($title){
		return "Tambah Image";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Image";
	}
	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
	
	function show_callback_widget($data_widget = array())
	{
		$block_col_id = $this->CI->input->post("block_col_id");
		$block_col_id = (empty($block_col_id) and isset($data_widget['block_col_id']))?$data_widget['block_col_id']:$block_col_id;
		
		$js = '
		function callback_widget_image_list()
		{
			var form = $("#form_image_list");
			var data_form = $(form).serialize();
			var url = "'.base_url('admin/adminblocks/setting_widget/image_list/do_save_configuration').'";
			$.ajax({
					url: url,
					data: data_form,
					type: "post",
					success: function(msg)
					{
					}
			});
		}
		callback_widget_image_list();
		';
		echo $js;
	}
	
	function do_save_configuration()
	{
		$config = $this->CI->input->post("config");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$json_config = json_encode($config);
		$this->CI->db->where(array("block_col_id" => $block_col_id));
		$this->CI->db->update("system_block_cols",array("configs" => $json_config));
		
		echo $this->show_widget(array("block_col_id" => $block_col_id,"configs" => $json_config));
	}
}
