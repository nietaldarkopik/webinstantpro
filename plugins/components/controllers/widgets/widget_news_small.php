<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_news_small extends Basic_widget{

  var $name = 'news_small';
  var $title = 'News Small List';
  var $description = 'Widget News';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $current_data = array();
  var $init;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
    //Install DB
    $q_installnews_small_types = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_news_small_types` (
  `news_small_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_small_type_title` char(255) NOT NULL,
  `news_small_type_name` char(255) NOT NULL,
  `object_table` char(100) NOT NULL,
  PRIMARY KEY (`news_small_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');

    $q_installnews_categories = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_news_categories` (
  `news_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `name` char(255) NOT NULL,
  `container` text,
  PRIMARY KEY (`news_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
');

    //Install DB
    $q_installnews_smalls = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_category_id` int(11) NOT NULL,
  `parent_news` int(11) DEFAULT NULL,
  `news_title` char(255) NOT NULL,
  `news_name` char(255) NOT NULL,
  `object_id` int(11) NOT NULL,
  `news_type_name` char(100) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attributes` text,
  `type` char(255) DEFAULT NULL,
  `status` char(255) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');




	if($this->CI->db->where(array("news_small_type_name" => "page"))->get("widget_news_small_types")->num_rows() == 0)
		$this->CI->db->insert("widget_news_small_types",array("news_small_type_title" =>"Page","news_small_type_name" => "page","object_table" => "user_pages"));
	if($this->CI->db->where(array("news_small_type_name" => "category"))->get("widget_news_small_types")->num_rows() == 0)
		$this->CI->db->insert("widget_news_small_types",array("news_small_type_title" =>"Category","news_small_type_name" => "category","object_table" => "user_categories"));
	if($this->CI->db->where(array("news_small_type_name" => "link"))->get("widget_news_small_types")->num_rows() == 0)
		$this->CI->db->insert("widget_news_small_types",array("news_small_type_title" =>"Link","news_small_type_name" => "link","object_table" => "plugin_links"));
	
	return ($q_installnews_small_types and $q_installnews_categories and $q_installnews_smalls)?true:false;

  }
  
  function uninstall()
  {
	  //uninstall db
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_news_categories");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_news");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_news_small_types");
	return $q_uninstall;
  }
  
  function query_element_template($block_html = "")
  {
	  $query = ".news";
	  
	  return $query;
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col)
  {
	  $this->install();
	  $block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	  $block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	  $query = $this->query_element_template();
	  $data_widget = $this->current_data;
	  
	  $get_elements = pq($query,$col);
	  $output = 0;
		
	  if($get_elements->count() > 0)
	  {
		  foreach($get_elements as $i => $get_element)
		  {
			$data_block_item_where = array(
									 "theme_id" => $data_theme['theme_id'],
									 "template_id" => $data_template['template_id'],
									 "block_container_id" => $block_group_id,
									 "block_row_id" => $block_id,
									 "widget_id" => $data_widget['widget_id'],
									 "sort_order" => $no_col
									 );

			$this->CI->db->where($data_block_item_where);
			$q_check = $this->CI->db->get("system_block_cols")->num_rows();

			if($q_check == 0)
			{
				$widget_content = pq($get_element)->find("li");
				if($widget_content->count() > 0)
				{
					$insert_data_widget = array("title" => "","name" => "");
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->insert("widget_news_categories");
					$news_category_id = $this->CI->db->insert_id();
					//$widget_content_id = $this->CI->db->insert_id();
					
					$insert_data_widget = array("container" => pq($get_element)->clone()->html('<bswidget widget_name="'.$data_widget['widget_name'].'" id="'.$news_category_id.'"></bswidget>')->htmlOuter());
					$this->CI->db->where(array("news_category_id"=>$news_category_id));
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->update("widget_news_categories");
					
					foreach($widget_content as $i => $w_content)
					{
						$a = pq("a",$w_content)->eq(0);
						$data_block_item = array(
												"page_name" => url_title(pq($a)->html(), '-', TRUE)."_".$news_category_id,
												"page_title" => pq($a)->html(),
												"page_description" => "",
												"thumbnail" => "",
												"meta_title" => "",
												"meta_keywords" => "",
												"meta_description" => "",
												"author" => "",
												"date_inserted" => "",
												"date_updated" => "",
												 "status" => "active",
												 "news_category_id" => $news_category_id,
												 );
						$this->CI->db->insert("widget_news",$data_block_item);
						$block_col_id = $this->CI->db->insert_id();
					}
					
					$data_block_item = array(
											 "theme_id" => $data_theme['theme_id'],
											 "template_id" => $data_template['template_id'],
											 "block_container_id" => $block_group_id,
											 "block_row_id" => $block_id,
											 "widget_id" => $data_widget['widget_id'],
											 "widget_content_id" => $news_category_id,
											 "sort_order" => $no_col
											 );
					$this->CI->db->insert("system_block_cols",$data_block_item);
					$block_col_id = $this->CI->db->insert_id();
					
					$this->CI->db->where(array("block_col_id" => $block_col_id));
					$this->CI->db->update("system_block_cols",array("content" => pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter()));
					
				}
			}
		}
		
		$output = 1;
    }
	
	return $output;
  }
  
  function create_news_small_from_news_small_group($news_category_id = "")
  {
	$data_news_small_items = array();
	if($news_category_id > 0)
	{
		$data_news_small_items = $this->CI->db->query("select * from widget_news,widget_news_cat_rel WHERE widget_news.news_id = widget_news_cat_rel.news_id  AND widget_news_cat_rel.news_cat_id = '".$news_category_id."' ")->result_array();
	}else{
		$data_news_small_items = $this->CI->db->query("select * from widget_news")->result_array();
	}
	
	$output = "";
	foreach($data_news_small_items as $i => $news_small_item)
	{
		#$output .= "<li><a href='".base_url()."page/show/".$news_small_item['news_small_name']."'>".$news_small_item['news_small_title']."</a></li>";
		/*$output .= '
                                    <!-- Blog Post -->
                                    <div class="blog-post padding-bottom-20">
                                        <!-- Blog Item Header -->
                                        <div class="blog-item-header">
                                            <!-- Title -->
                                            <h2>
                                                <a href="#">
                                                    Yet Another Sample Blog Title</a>
                                            </h2>
                                            <div class="clearfix"></div>
                                            <!-- End Title -->
                                        </div>
                                        <!-- End Blog Item Header -->
                                        <!-- Blog Item Details -->
                                        <div class="blog-post-details">
                                            <!-- Author Name -->
                                            <div class="blog-post-details-item blog-post-details-item-left">
                                                <i class="fa fa-user color-gray-light"></i>
                                                <a href="#">Admin</a>
                                            </div>
                                            <!-- End Author Name -->
                                            <!-- Date -->
                                            <div class="blog-post-details-item blog-post-details-item-left">
                                                <i class="fa fa-calendar color-gray-light"></i>
                                                <a href="#">22nd Apr, 2014</a>
                                            </div>
                                            <!-- End Date -->
                                            <!-- Tags -->
                                            <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                                <i class="fa fa-tag color-gray-light"></i>
                                                <a href="#">Bootstrap</a>,
                                                <a href="#">CoffeeScript</a>,
                                                <a href="#">Ruby</a>
                                            </div>
                                            <!-- End Tags -->
                                            <!-- # of Comments -->
                                            <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                                <a href="">
                                                    <i class="fa fa-comments color-gray-light"></i>
                                                    9 Comments
                                                </a>
                                            </div>
                                            <!-- End # of Comments -->
                                        </div>
                                        <!-- End Blog Item Details -->
                                        <!-- Blog Item Body -->
                                        <div class="blog">
                                            <div class="clearfix"></div>
                                            <div class="blog-post-body row margin-top-15">
                                                <div class="col-md-5">
                                                    <img class="margin-bottom-20" src="assets/img/blog/image3.jpg" alt="thumb3">
                                                </div>
                                                <div class="col-md-7">
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                                                        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
                                                    <!-- Read More -->
                                                    <a href="#" class="btn btn-primary">
                                                        Read More
                                                        <i class="icon-chevron-right readmore-icon"></i>
                                                    </a>
                                                    <!-- End Read More -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Blog Item Body -->
                                    </div>
                                    <!-- End Blog Item -->';
			*/
			$output .= '
                                            <li class="media">
                                                <div class="media-left pull-left">
                                                    <a href="">
                                                        <img style="width: 50px;" class="media-object" src="'. base_url() .'uploads/news/'.$news_small_item['thumbnail'].'" alt="'.$news_small_item['page_title'].'">
                                                    </a>
                                                </div>
												<div class="media-body">
													<h4 class="media-heading">'.$news_small_item['page_title'].'</h4>
													<span class="recent-post-date">
														'. date("d M Y",strtotime($news_small_item['date_inserted'])) . '
													</span>
													<p>
														'. strip_tags(substr($news_small_item['page_description'],0,100)) .'
													</p>
												</div>
                                                <div class="clearfix"></div>
                                            </li>';
	}
	
	$output = '<ul class="media-list margin-top-10">'.$output.'</ul>';
	return $output;
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";

	$output = "";
	if($this->CI->db->table_exists('widget_news_categories'))
	{
		$data_news_categories = $this->CI->db->where(array("news_category_id" => $widget_content_id))->get('widget_news_categories')->row_array();
		$news_small_items = $this->create_news_small_from_news_small_group((isset($data_news_categories['news_category_id']))?$data_news_categories['news_category_id']:"");
		//$output = (isset($data_news_categories['container']))?$data_news_categories['container']:"";
		$output = $news_small_items;//str_replace('<bswidget widget_name="news_small" id="'.$widget_content_id.'"></bswidget>',$news_small_items,$output);
		return $output;
	}
	return $output;
  }
  
  function init($data_widget="")
  {
    
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {
		$init = array(
						'table' => 'widget_news_categories',
						'fields' => array(
											array(
												'name' => 'parent_category_id',
												'label' => 'Parent Category',
												'id' => 'parent_category_id',
												'value' => '',
												'type' => 'input_text',
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'category_name',
												'label' => 'Nama kategori',
												'id' => 'category_name',
												'value' => '',
												'type' => 'input_text',
												'use_search' => false,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'category_url',
												'label' => 'category_url',
												'id' => 'category_url',
												'value' => '',
												'type' => 'input_text',
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											),
											array(
												'name' => 'description',
												'label' => 'Description',
												'id' => 'description',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => true,
												'use_listing' => false,
												'rules' => 'required'
											),
											array(
												'name' => 'thumbnail',
												'label' => 'Thumbnail',
												'id' => 'thumbnail',
												'value' => '',
												'type' => 'input_file',
												'config_upload' => array(
																			'upload_path' => dirname($_SERVER['SCRIPT_FILENAME']).'/uploads/news_categories/',
																			'encrypt_name' => false,
																			'allowed_types' => 'jpg|gif|png'
																		),
												'use_search' => false,
												'use_listing' => true,
												'rules' => '',
												'list_style' => ' width="250" '
											),
											array(
												'name' => 'meta_title',
												'label' => 'Meta Title',
												'id' => 'meta_title',
												'value' => '',
												'type' => 'input_text',
												'use_search' => true,
												'use_listing' => true,
												'rules' => ''
											),
											array(
												'name' => 'meta_keywords',
												'label' => 'Meta Keywords',
												'id' => 'meta_keywords',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => true,
												'use_listing' => true,
												'rules' => ''
											),
											array(
												'name' => 'meta_description',
												'label' => 'Meta Description',
												'id' => 'meta_description',
												'value' => '',
												'type' => 'input_textarea',
												'use_search' => true,
												'use_listing' => false,
												'rules' => ''
											),
											array(
												'name' => 'status',
												'label' => 'Status',
												'id' => 'status',
												'value' => '',
												'type' => 'input_selectbox',
												'options' => array('' => '---- Choose Status ----','active' => 'Publish','not active' => 'Draft'),
												'use_search' => true,
												'use_listing' => true,
												'rules' => 'required'
											)
										),
						'primary_key' => 'news_category_id',
						'path' => "/",
						'controller' => 'adminblocks',
						'function' => 'widgets',
						'panel_function' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'bulk_options' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'sess_keyname' => 'components_news_small_widget_controller'
			);
			$this->init = $init;
			$this->CI->data->sess_keyname = $init['sess_keyname'];
			$this->CI->data->init($this->init);
			$this->CI->data->set_filter();
			$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
			$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
			$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
			$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
			

			$config_form_filter = $this->init;
			$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/news_small/configuration/';

			$add_config = $this->init;
			$add_config['action'] = base_url().'admin/adminblocks/setting_widget/news_small/configuration/';
			$view = "";
			
			$is_login = $this->CI->user_access->is_login();
			if($is_login)
				$view = $this->CI->load->view('layouts/widget_news/widgets/widget_news_small_list',
																array(	'response' => '',
																		'page_title' => 'Data News',
																		'config_form_filter' => $config_form_filter,
																		'listing_config' => $this->init,
																		'add_config' => $add_config,
																		'current_data_widget' => $this->current_data
																		),
																true
																	);
			else
				$view = $this->CI->load->view('layouts/login',array(),true);
				
		  return $view;
	}
	
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}

	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}
	function _hook_create_form_title_add($title){
		return "Tambah Custom Text";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Custom Text";
	}
	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
}
