<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_menu extends Basic_widget{

  var $name = 'menu';
  var $title = 'Menu';
  var $description = 'Widget Menu';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  var $current_data = array();
  var $init;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
  
  public function index()
  {
	  
  }
  
  function install()
  {
    //Install DB
    $q_installmenu_types = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_menu_types` (
  `menu_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_type_title` char(255) NOT NULL,
  `menu_type_name` char(255) NOT NULL,
  `object_table` char(100) NOT NULL,
  PRIMARY KEY (`menu_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');

    $q_installmenu_groups = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_menu_groups` (
  `menu_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `name` char(255) NOT NULL,
  `container` text,
  PRIMARY KEY (`menu_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
');

    //Install DB
    $q_installmenus = $this->CI->db->query('
CREATE TABLE IF NOT EXISTS `widget_menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_group_id` int(11) NOT NULL,
  `parent_menu` int(11) DEFAULT NULL,
  `menu_title` char(255) NOT NULL,
  `menu_name` char(255) NOT NULL,
  `object_id` int(11) NOT NULL,
  `menu_type_name` char(100) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attributes` text,
  `type` char(255) DEFAULT NULL,
  `status` char(255) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');




	if($this->CI->db->where(array("menu_type_name" => "page"))->get("widget_menu_types")->num_rows() == 0)
		$this->CI->db->insert("widget_menu_types",array("menu_type_title" =>"Page","menu_type_name" => "page","object_table" => "user_pages"));
	if($this->CI->db->where(array("menu_type_name" => "category"))->get("widget_menu_types")->num_rows() == 0)
		$this->CI->db->insert("widget_menu_types",array("menu_type_title" =>"Category","menu_type_name" => "category","object_table" => "user_categories"));
	if($this->CI->db->where(array("menu_type_name" => "link"))->get("widget_menu_types")->num_rows() == 0)
		$this->CI->db->insert("widget_menu_types",array("menu_type_title" =>"Link","menu_type_name" => "link","object_table" => "plugin_links"));
	
	return ($q_installmenu_types and $q_installmenu_groups and $q_installmenus)?true:false;

  }
  
  function uninstall()
  {
	  //uninstall db
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_menu_groups");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_menus");
    $q_uninstall = $this->CI->db->query("DROP TABLE widget_menu_types");
	return $q_uninstall;
  }
  
  function query_element_template($block_html = "")
  {
	  $query = "nav,.nav";
	  
	  return $query;
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col)
  {
	  $this->install();
	  $block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	  $block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	  $query = $this->query_element_template();
	  $data_widget = $this->current_data;
	  
	  $get_elements = pq($query,$col);
	  $output = 0;
		
	  if($get_elements->count() > 0)
	  {
		  foreach($get_elements as $i => $get_element)
		  {
			$data_block_item_where = array(
									 "theme_id" => $data_theme['theme_id'],
									 "template_id" => $data_template['template_id'],
									 "block_container_id" => $block_group_id,
									 "block_row_id" => $block_id,
									 "widget_id" => $data_widget['widget_id'],
									 "sort_order" => $no_col
									 );

			$this->CI->db->where($data_block_item_where);
			$q_check = $this->CI->db->get("system_block_cols")->num_rows();

			if($q_check == 0)
			{
				$widget_content = pq($get_element)->find("li");
				if($widget_content->count() > 0)
				{
					$insert_data_widget = array("title" => "","name" => "");
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->insert("widget_menu_groups");
					$menu_group_id = $this->CI->db->insert_id();
					//$widget_content_id = $this->CI->db->insert_id();
					
					$insert_data_widget = array("container" => pq($get_element)->clone()->html('<bswidget widget_name="'.$data_widget['widget_name'].'" id="'.$menu_group_id.'"></bswidget>')->htmlOuter());
					$this->CI->db->where(array("menu_group_id"=>$menu_group_id));
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->update("widget_menu_groups");
					
					foreach($widget_content as $i => $w_content)
					{
						$a = pq("a",$w_content)->eq(0);
						$data_block_item = array(
												 "menu_group_id" => $menu_group_id,
												 "parent_menu" => 0,
												 "menu_name" => url_title(pq($a)->html(), '-', TRUE)."_".$menu_group_id,
												 "menu_title" =>pq($a)->html(),
												 "object_id" => 0,
												 "menu_type_name" => 'custom',
												 "sort_order" => $i,
												 "attributes" => "",
												 "type" => "",
												 "status" => "active",
												 );
						$this->CI->db->insert("widget_menus",$data_block_item);
						$block_col_id = $this->CI->db->insert_id();
					}
					
					$data_block_item = array(
											 "theme_id" => $data_theme['theme_id'],
											 "template_id" => $data_template['template_id'],
											 "block_container_id" => $block_group_id,
											 "block_row_id" => $block_id,
											 "widget_id" => $data_widget['widget_id'],
											 "widget_content_id" => $menu_group_id,
											 "sort_order" => $no_col
											 );
					$this->CI->db->insert("system_block_cols",$data_block_item);
					$block_col_id = $this->CI->db->insert_id();
					
					$this->CI->db->where(array("block_col_id" => $block_col_id));
					$this->CI->db->update("system_block_cols",array("content" => pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter()));
					
				}
			}
		}
		
		$output = 1;
    }
	
	return $output;
  }
  
  function create_menu_from_menu_group($menu_group_id = "")
  {
	$data_menu_items = $this->CI->db->where(array("menu_group_id" => $menu_group_id))->order_by("sort_order ASC")->get('widget_menus')->result_array();
	
	$output = "";
	foreach($data_menu_items as $i => $menu_item)
	{
		$type_menu = $this->CI->db->where(array("menu_type_name" => $menu_item['type']))->get("widget_menu_types")->row_array();
		$detail_menu = $this->CI->db->where(array($type_menu['primary_key'] => $menu_item['menu_type_name']))->get($type_menu['object_table'])->row_array();
		
		if(isset($detail_menu[$type_menu['index_key']]))
		$output .= "<li><a href='".base_url()."post/show/".$menu_item['type']."/".$detail_menu[$type_menu['index_key']]."'>".$menu_item['menu_title']."</a></li>";
	}
	
	return $output;
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";
	$block_col_id = $this->CI->input->post("block_col_id");
	$output = "";
	if($this->CI->db->table_exists('widget_menu_groups'))
	{
		$data_menu_groups = $this->CI->db->where(array("menu_group_id" => $widget_content_id))->get('widget_menu_groups')->row_array();
		$menu_items = $this->create_menu_from_menu_group((isset($data_menu_groups['menu_group_id']))?$data_menu_groups['menu_group_id']:"");
		$output = (isset($data_menu_groups['container']))?$data_menu_groups['container']:"";
		$output = str_replace('<bswidget widget_name="menu" id="'.$widget_content_id.'"></bswidget>',$menu_items,$output);
		$output = str_replace('<bscol id="'.$block_col_id.'"></bscol>',$menu_items,$output);
		
		if(empty($output))
		{
			$output = '<ul class="nav navbar-nav" id="hornavmenu">'.$menu_items.'</ul>';
		}
		
		return $output;
	}
	return $output;
  }
  
  function init($data_widget="")
  {
    
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {
		$init = array(
							'table' => 'widget_menu_groups',
							'fields' => array(
												array(
													'name' => 'title',
													'label' => 'Title',
													'id' => 'title',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'name',
													'label' => 'Name',
													'id' => 'name',
													'value' => '',
													'type' => 'input_text',
													'use_search' => false,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'container',
													'label' => 'Container',
													'id' => 'container',
													'value' => '',
													'type' => 'input_textarea',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												)
											),
						'primary_key' => 'menu_group_id',
						'path' => "/",
						'controller' => 'adminblocks',
						'function' => 'widgets',
						'panel_function' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'bulk_options' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'sess_keyname' => 'components_menu_widget_controller'
			);
			$this->init = $init;
			$this->CI->data->sess_keyname = $init['sess_keyname'];
			$this->CI->data->init($this->init);
			$this->CI->data->set_filter();
			$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
			$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
			$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
			$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
			

			$config_form_filter = $this->init;
			$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/menu/configuration/';
			$view = "";
			
			$is_login = $this->CI->user_access->is_login();
			if($is_login)
				$view = $this->CI->load->view('layouts/widget_menu/widgets/widget_menu',
																array(	'response' => '',
																		'page_title' => 'Data Menu',
																		'config_form_filter' => $config_form_filter,
																		'listing_config' => $this->init,
																		'current_data_widget' => $this->current_data
																		),
																true
																	);
			else
				$view = $this->CI->load->view('layouts/login',array(),true);
				
		  return $view;
	}
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
}
