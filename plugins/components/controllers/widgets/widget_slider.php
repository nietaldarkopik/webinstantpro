<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget_slider extends Basic_widget{

  var $name = 'slider';
  var $title = 'Slider';
  var $description = 'WIdget Slider';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
	$this->CI =& get_instance();  
	$this->current_data = $this->CI->db->where(array("widget_name" => $this->name))->get("system_widgets")->row_array();
  }
  
  public function config()
  {
    $data = array();
    $data['plugin_name'] = basename(basename(basename(basename(__DIR__))));
    $data['widget_name']  = $this->name;
    $data['widget_title'] = $this->title;
    $data['description']  = $this->description;
    $data['status']  = 'publish';
    $data['widget_data']  = (isset($this->widget_data) and is_array($this->widget_data))?json_encode($this->widget_data):json_encode(array());
    $data['author']  = $this->author;
    $data['version']  = $this->version;
	
	return $data;
  }
	public function index()
	{
    $is_ajax = $this->input->post('is_ajax');
    $this->load->view('layouts/dashboard');
	}
  
  function install()
  {
    //Install DB
    $q_installslider_types = $this->CI->db->query('CREATE TABLE IF NOT EXISTS `widget_sliders` (
  `widget_slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`widget_slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');

    $q_installslider_types = $this->CI->db->query('CREATE TABLE IF NOT EXISTS `widget_slider_images` (
  `widget_slider_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_slider_id` int(11) NOT NULL,
  `title` char(255) NOT NULL,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `image_url` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`widget_slider_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
');

	return true;
  }
  
  function uninstall()
  {
    
  }
  
  function query_element_template($block_html = "")
  {
	  $query = ".slider";
	  
	  return $query;
  }
  
  function parse_template($col = "",$data_theme = "",$data_template = "",$data_block_group = "",$data_block = "",$no_col)
  {
	  $this->install();
	  $block_group_id = (isset($data_block_group['group_id']))?$data_block_group['group_id']:0;
	  $block_id = (isset($data_block['block_id']))?$data_block['block_id']:0;
	  $query = $this->query_element_template();
	  $data_widget = $this->current_data;
	  
	  $get_elements = pq($query,$col);
	  $output = 0;
		
	  if($get_elements->count() > 0)
	  {
		  foreach($get_elements as $i => $get_element)
		  {
			$data_block_item_where = array(
									 "theme_id" => $data_theme['theme_id'],
									 "template_id" => $data_template['template_id'],
									 "block_container_id" => $block_group_id,
									 "block_row_id" => $block_id,
									 "widget_id" => $data_widget['widget_id'],
									 "sort_order" => $no_col
									 );

			$this->CI->db->where($data_block_item_where);
			$q_check = $this->CI->db->get("system_block_cols")->num_rows();

			if($q_check == 0)
			{
				$widget_content = pq($get_element)->find("li");
				if($widget_content->count() > 0)
				{
					$insert_data_widget = array("title" => "","description" => "");
					$this->CI->db->set($insert_data_widget,true);
					$this->CI->db->insert("widget_sliders");
					$widget_slider_id = $this->CI->db->insert_id();
					//$widget_content_id = $this->CI->db->insert_id();
					
					#$insert_data_widget = array("container" => pq($get_element)->clone()->html('<bswidget widget_name="'.$data_widget['widget_name'].'" id="'.$widget_slider_id.'"></bswidget>')->htmlOuter());
					#$this->CI->db->where(array("widget_slider_id"=>$widget_slider_id));
					#$this->CI->db->set($insert_data_widget,true);
					#$this->CI->db->update("widget_sliders");
					
					foreach($widget_content as $i => $w_content)
					{
						$a = pq("a",$w_content)->eq(0);
						$data_block_item = array(
												 "widget_slider_id" => $widget_slider_id,
												 //"slider_name" => url_title(pq($a)->html(), '-', TRUE)."_".$widget_slider_id,
												 "title" =>pq($a)->text(),
												 "image_url" =>pq($a)->find("img")->attr("src"),
												 "link" => '#'
												 );
						$this->CI->db->insert("widget_slider_images",$data_block_item);
						$block_col_id = $this->CI->db->insert_id();
					}
					
					$data_block_item = array(
											 "theme_id" => $data_theme['theme_id'],
											 "template_id" => $data_template['template_id'],
											 "block_container_id" => $block_group_id,
											 "block_row_id" => $block_id,
											 "widget_id" => $data_widget['widget_id'],
											 "widget_content_id" => $widget_slider_id,
											 "sort_order" => $no_col
											 );
					$this->CI->db->insert("system_block_cols",$data_block_item);
					$block_col_id = $this->CI->db->insert_id();
					
					$this->CI->db->where(array("block_col_id" => $block_col_id));
					$this->CI->db->update("system_block_cols",array("content" => pq($col)->clone()->attr("data-block-item-id",$block_col_id)->html("<bscol id='".$block_col_id."'/>")->htmlOuter()));
					
				}
			}
		}
		
		$output = 1;
    }
	
	return $output;
  }
  
  function create_slider_from_slider_id($widget_slider_id = "")
  {
	$data_slider_items = $this->CI->db->where(array("widget_slider_id" => $widget_slider_id))->get('widget_slider_images')->result_array();
	$carousel_name = 'carousel-'.$widget_slider_id-'-'.rand(11,999999);
	$output = "";
	$image_item = '';
	$list_item = '';
	foreach($data_slider_items as $i => $slider_item)
	{
		$list_item .= '
					<li data-target="#'.$carousel_name.'" data-slide-to="'.$i.'" '.(($i == 0)?'class="active"':'').'></li>
				   ';
		$image_item .= '
					<div class="item'.(($i == 0)?' active':'').'">
						<img src="'.base_url().'uploads/sliders/'.$slider_item['image_url'].'">
						<div class="carousel-caption">
							'.$slider_item['text1'].'
						</div>
					</div>
					';
	}
	
	$output = '
				<div id="'.$carousel_name.'" class="carousel slide" data-ride="carousel">
				
					<ol class="carousel-indicators">
						'.$list_item.'
					</ol>
					<div class="clearfix"></div>
					
					<div class="carousel-inner" role="listbox">
						'.$image_item.'
					</div>
					<a class="left carousel-control" href="#'.$carousel_name.'" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#'.$carousel_name.'" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
				';
	return $output;
  }
  
  public function show_widget($data_widget="")
  {
	$widget_name = $this->name;
	$widget_content_id = (isset($data_widget['widget_content_id']))?$data_widget['widget_content_id']:"";
	
	if($this->CI->db->table_exists('widget_sliders'))
	{
		$data_slider_groups = $this->CI->db->where(array("widget_slider_id" => $widget_content_id))->get('widget_sliders')->row_array();
		$slider_items = $this->create_slider_from_slider_id((isset($data_slider_groups['widget_slider_id']))?$data_slider_groups['widget_slider_id']:"");
		return $slider_items;
	}
	return "";
  }
  
  function init()
  {
    
  }
  
  function configuration($listing = "listing",$number_page = 0)
  {
		$init = array(
							'table' => 'widget_sliders',
							'fields' => array(
												array(
													'name' => 'title',
													'label' => 'Title',
													'id' => 'title',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												array(
													'name' => 'description',
													'label' => 'Description',
													'id' => 'description',
													'value' => '',
													'type' => 'input_textarea',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												)
											),
						'primary_key' => 'widget_slider_id',
						'path' => "/",
						'controller' => 'adminblocks',
						'function' => 'widgets',
						'panel_function' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'bulk_options' => array(
													array('title' => 'Install','name' => 'add', 'class' => 'glyphicon-edit'),
													array('title' => 'Uninstall','name' => 'delete', 'class' => 'glyphicon-share'),
													array('title' => 'Configuration','name' => 'edit', 'class' => 'glyphicon-cog')
												  ),
						'sess_keyname' => 'components_slider_widget_controller'
		);
			$this->init = $init;
			$this->CI->data->sess_keyname = $init['sess_keyname'];
			$this->CI->data->init($this->init);
			$this->CI->data->set_filter();
			$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
			$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
			$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
			$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_true'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_edit',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_view',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_delete',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_index',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_show_panel_allowed_panel_/_adminblocks_listing',array($this,'_hook_show_panel_allowed'));
			$this->CI->hook->add_action('hook_create_form_filter_button_action',array($this,'_hook_create_form_filter_button_action'));
			

			$config_form_filter = $this->init;
			$config_form_filter['action'] = base_url().'admin/adminblocks/setting_widget/slider/configuration/';
			$view = "";
			
			$is_login = $this->CI->user_access->is_login();
			if($is_login)
				$view = $this->CI->load->view('layouts/widget_slider/widgets/widget_slider',
																array(	'response' => '',
																		'page_title' => 'Data Sliders',
																		'config_form_filter' => $config_form_filter,
																		'listing_config' => $this->init,
																		'current_data_widget' => $this->current_data
																		),
																true
																	);
			else
				$view = $this->CI->load->view('layouts/login',array(),true);
				
		  return $view;
	}
	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}
	function _hook_create_form_filter_button_action($param = ""){
		$do_action = $this->CI->input->post("do_action");
		$theme_id = $this->CI->input->post("theme_id");
		$template_id = $this->CI->input->post("template_id");
		$con_id = $this->CI->input->post("con_id");
		$row_id = $this->CI->input->post("row_id");
		$block_col_id = $this->CI->input->post("block_col_id");
		
		$append_form = '';
		$append_form .= '<input type="hidden" name="theme_id" value="'.$theme_id.'"/>';
		$append_form .= '<input type="hidden" name="con_id" value="'.$con_id.'"/>';
		$append_form .= '<input type="hidden" name="row_id" value="'.$row_id.'"/>';
		$append_form .= '<input type="hidden" name="block_col_id" value="'.$block_col_id.'"/>';
		$param = $append_form . $param;
		return $param;
	}
	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
