<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_textcontroller{

  var $name = 'custom_text';
  var $title = 'Custom Texts';
  var $description = 'Plugin Custom Text';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_custom_text_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_custom_text_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_custom_text_view'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_custom_text_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_custom_text_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_custom_text_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_create_listing_value_image_url',array($this,'_hook_create_listing_value_image_url'));
		$this->CI->hook->add_action('hook_create_listing_value_widget_slider_id',array($this,'_hook_create_listing_value_widget_slider_id'));
		
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
				
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/custom_text/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/default/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Custom Text"));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function widget_types()
	{	
		$output = array();			
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
			$data_plugins = $this->CI->plugins->get_data_plugins(array("plugin_name" => "components","status" => "active"));
			$data_plugin = (isset($data_plugins[0]))?$data_plugins[0]:array();
			if(is_array($data_plugin) and count($data_plugin) > 0)
			{
				$data_widget = $this->CI->widgets->get_data_widget(array('widget_name' => $this->name),true);
				
				$output['custom_text']['tiny_widgets'] = array(	
																  'name' => $this->name,
																  'title' => $this->title,
																  'description' => $this->description,
																  'version' => $this->version,
																  'widget' => $data_widget,
																  'plugin' => $data_plugin,
																);
			}
		}
		else
		{
			$this->CI->load->view('layouts/login');
			exit;
		}
		
		return $output;
	}
	
	function configuration()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_custom_text_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_custom_text_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_custom_text_view'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_custom_text_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_custom_text_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_custom_text_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_create_listing_value_text_content',array($this,'_hook_create_listing_value_text_content'));
		
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
				
		$is_login = $this->CI->user_access->is_login();
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/custom_text/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/custom_text/widgets/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Custom Text"));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->CI->init['fields']);
		$paging_config = array('base_url' => base_url().'menu/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->CI->init['fields']))?$this->CI->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->CI->init['fields'] = $init;
		
		$response = $this->CI->data->edit("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/edit',array('response' => $response,'page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function do_edit($object_id = "")
	{
		$do_action = $this->CI->input->post("do_action");
		if($do_action == "do_save_custom_text")
		{
			$data = array("text_content" => $this->CI->input->post("content"));
			$item_id = $this->CI->input->post("item_id");
			$this->CI->db->where(array("custom_text_id" => $item_id));
			$q_edit = $this->CI->db->update("widget_custom_text",$data);
			$output = array();
			if($q_edit)
			{
				$output['message'] = "Data berhasil diubah";
				$output['status'] = "success";
				$output['content'] = $this->CI->input->post("content");
			}else{
				$output['message'] = "Data gagal diubah";
				$output['status'] = "danger";
				$output['content'] = $this->CI->input->post("content");
			}
			echo json_encode($output);
		}
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_custom_text_listing'));
		$response = $this->CI->data->add("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Tambah Custom Text'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function do_add()
	{
		
	}
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default/view',array('response' => '','page_title' => $this->CI->page_title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_custom_text",
							'fields' => array(
												array(
													'name' => 'text_title',
													'label' => 'Text Title',
													'id' => 'text_title',
													'value' => '',
													'type' => 'input_text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												),
												/*
												array(
													'name' => 'text_name',
													'label' => 'Text Name',
													'id' => 'text_name',
													'value' => '',
													'type' => 'input_hidden',
													'use_search' => false,
													'use_listing' => false,
													'rules' => ''
												),
												*/
												array(
													'name' => 'text_content',
													'label' => 'Text Content',
													'id' => 'text_content',
													'value' => '',
													'type' => 'input_mce',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
											),
						'primary_key' => 'custom_text_id',
						'path' => "/admin/",
						'controller' => 'appearence_plugins',
						'function' => 'plugin_controller',
						'panel_function' => array(
												  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
												  array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'bulk_options' => array(
												  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
												),
						'sess_keyname' => 'components_custom_textcontroller_slider',
						'action' => base_url("admin/appearence_plugins/plugin_controller/components/custom_text/add"),
						'uri_segment' => 7
					);
		$this->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _hook_do_add($param = "")
	{
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Custom Text";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Custom Text";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_custom_text_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/custom_text/delete",$param);
		return $param;
	}
	function _components_custom_text_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/custom_text/edit",$param);
		return $param;
	}
	function _components_custom_text_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/custom_text/view",$param);
		return $param;
	}
	function _components_custom_text_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/custom_text/listing",$param);
		return $param;
	}
	function _hook_create_listing_value_text_content($param = "")
	{
		#$this->CI->load->helper("text");
		#$param = strip_tags($param);//,"<img><a><ul><li><ol><div><p><b><strong><i><font><u><br><hr>");
		#$param = character_limiter($param,200);
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
