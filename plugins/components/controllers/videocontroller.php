<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videocontroller{

  var $name = 'video';
  var $title = 'video';
  var $description = 'Plugin Video';
  var $author = 'OPQ';
  var $version = '1.0';
  var $CI;
  
  public function __construct()
  {
    $this->CI =& get_instance();
  }

	function index()
	{
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_create_listing_value_video_script',array($this,'_hook_create_listing_value_video_script'));
	
		$filter = $this->CI->input->post("data_filter");
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		
		$do_save_video = $this->CI->input->post("do_save_video");
		if(!empty($do_save_video))
		{
			$save_video = $this->_save_parent_video();
			echo $save_video;
			exit;
		}
		
		$get_functions = $this->CI->input->post("get_functions");
		$current_function = $this->CI->input->post("current_function");
		if(!empty($get_functions))
		{
			$this->_get_functions($get_functions,$current_function);
			exit;
		}
		
    $the_videos = "";
    $is_login = $this->CI->user_access->is_login();
    if(isset($filter['video_group_id']))
    {		
      $this->CI->db->where($filter);
      $this->CI->db->order_by("sort_order","ASC");
      $appearence_videos = $this->CI->db->get("widget_videos");
      $appearence_videos = $appearence_videos->result_array();
      $appearence_videos = $this->CI->user_access->mapTree($appearence_videos,0);
      $the_videos = $this->_display_structure($appearence_videos);
    }
		if($is_login)
		{
            $config_form_filter = $this->CI->init;
            #$config_form_filter['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2));
            $config_form_filter['action'] = base_url("admin/appearence_plugins/plugin_controller/components/video/");
            $config_form_add = $this->CI->init;
            #$config_form_add['action'] = site_url($this->CI->uri->segment(1).'/'.$this->CI->uri->segment(2).'/add');
                        
            $this->CI->assets->add_js_inline('
				jQuery(document).ready(function(){
					jQuery("#sortable ul").trigger("do_sortable");
				});','body');
			$this->CI->plugins->set_path("components");
			$this->CI->load->view('layouts/default_widgets/listing',array('config_form_add' => $config_form_add,'response' => '','page_title' => "Data Video",'the_videos' => $the_videos));
		}
		else
			$this->CI->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));

		$this->CI->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->CI->data->delete("",$this->CI->init['fields']);
		$paging_config = array('base_url' => base_url().'video/listing','uri_segment' => 7);
		$this->CI->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->CI->init['action'] = base_url("admin/appearence_plugins/plugin_controller/components/video/edit/".$object_id);
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_edit'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));

		
		$init = (isset($this->CI->init['fields']))?$this->CI->init['fields']:array();
		$this->CI->init['fields'] = $init;
		
		$response = $this->CI->data->edit("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default_widgets/edit',array('response' => $response,'page_title' => $this->title));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->CI->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->CI->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->CI->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$this->CI->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$response = $this->CI->data->add("",$this->CI->init['fields']);
		
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default_widgets/add',array('response' => $response,'page_title' => 'Tambah video'));
		else
			$this->CI->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->CI->data->init($this->CI->init);
		$this->CI->data->set_filter();
		$this->CI->data->primary_key_value = $object_id;	
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_delete',array($this,'_components_video_delete'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_edit',array($this,'_components_video_edit'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_view',array($this,'_components_video_view'));
		$this->CI->hook->add_action('hook_show_bulk_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
		$this->CI->hook->add_action('hook_show_panel_allowed_panel_/admin/_appearence_plugins_listing',array($this,'_components_video_listing'));
	
		
		$is_login = $this->CI->user_access->is_login();
		if($is_login)			
			$this->CI->load->view('layouts/default_widgets/view',array('response' => '','page_title' => $this->title));
		else
			$this->CI->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->index();
	}
	
	function _config($id_object = "")
	{
        
		$init = array(	'table' => "widget_video",
						'fields' => array(
										  array(
											'name' => 'title',
											'label' => 'Title',
											'id' => 'title',
											'value' => '',
											'type' => 'input_text',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										  ),
										  array(
											'name' => 'name',
											'label' => 'Page Name',
											'id' => 'name',
											'value' => '',
											'type' => 'input_hidden',
											'use_search' => false,
											'use_listing' => false,
											'rules' => ''
										  ),
										  array(
											'name' => 'video_script',
											'label' => 'Embed Video Script',
											'id' => 'video_script',
											'value' => '',
											'type' => 'input_textarea',
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required',
											'list_style' => ' width="560" '
										  ),
										  array(
											'name' => 'description',
											'label' => 'Description',
											'id' => 'description',
											'value' => '',
											'type' => 'input_mce',
											'use_search' => true,
											'use_listing' => false,
											'rules' => 'required'
										  ),
										  array(
											'name' => 'status',
											'label' => 'Status',
											'id' => 'status',
											'value' => '',
											'type' => 'input_selectbox',
											'options' => array('' => '---- Choose Status ----','active' => 'Publish','not active' => 'Draft'),
											'use_search' => true,
											'use_listing' => true,
											'rules' => 'required'
										  )
									),
									'primary_key' => 'video_id',
									'path' => "/admin/",
									'controller' => 'appearence_plugins',
									'function' => 'plugin_controller',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  //array('title' => 'List Video','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'sess_keyname' => 'components_videocontroller_video',
									'action' => base_url("admin/appearence_plugins/plugin_controller/components/video/add"),
									'uri_segment' => 7
					);
		$this->CI->init = $init;
		$this->CI->data->sess_keyname = $init['sess_keyname'];
	}
	
	function _hook_do_add($param = "")
	{
        
		$param['name'] = (isset($param['title']))?url_title($param['title'],"-",true).'-'.rand(0,999):"video-".date("ymdhis");
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Video";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Video";
	}

	function _hook_create_form_ajax_target_add(){
		return ".ajax_container";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".ajax_container";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "1";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
  
  function install()
  {
    
  }
  
  function uninstall()
  {
    
  }
  
  function init()
  {
    
  }
	function _components_video_delete($param = "")
	{
		$param = str_replace("admin/appearence_plugins/delete","admin/appearence_plugins/plugin_controller/components/video/delete",$param);
		return $param;
	}
	function _components_video_edit($param = "")
	{
		$param = str_replace("admin/appearence_plugins/edit","admin/appearence_plugins/plugin_controller/components/video/edit",$param);
		return $param;
	}
	function _components_video_view($param = "")
	{
		$param = str_replace("admin/appearence_plugins/view","admin/appearence_plugins/plugin_controller/components/video_items/listing",$param);
		return $param;
	}
	function _components_video_listing($param = "")
	{
		$param = str_replace("admin/appearence_plugins/listing","admin/appearence_plugins/plugin_controller/components/video/listing",$param);
		return $param;
	}
	function _hook_create_listing_value_video_script($param = "")
	{
		$param = str_replace('height="50"','width="250"',$param);
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
